import { LanguageHelper } from './../../language/language.helper';
// import { ServicesModule } from './services/services.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxWebstorageModule } from 'ngx-webstorage';
@NgModule({
    imports: [ RouterModule, CommonModule, NgbModule ,NgxWebstorageModule],
    exports:[],
    declarations: [ 
       ],
       providers:[LanguageHelper]

})

export class SharedModule {}
