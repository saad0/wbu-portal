export enum RegionsEnum{
    EU ,
    EFTA ,
    CARICOM ,
    PA ,
    AU ,
    USAN ,
    EEU ,
    AL ,
    ASEAN ,
    CAIS ,
    CEFTA ,
    NAFTA ,
    SAARC 
}