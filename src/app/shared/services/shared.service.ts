import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn:"root"
})
export class SharedService{

    constructor(private http:HttpClient){

    }
    getAllCountries(){
        return this.http.get('https://restcountries.eu/rest/v2/all');
    }
    getCountriesWithRegion(region:string){
        return this.http.get('https://restcountries.eu/rest/v2/regionalbloc/'+region);
    }
}