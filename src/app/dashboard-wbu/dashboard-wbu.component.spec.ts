import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardWBUComponent } from './dashboard-wbu.component';

describe('DashboardWBUComponent', () => {
  let component: DashboardWBUComponent;
  let fixture: ComponentFixture<DashboardWBUComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardWBUComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardWBUComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
