import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { regionsDto } from '../new-partner/new-partner.component';
import { RegionsEnum } from '../shared/models/regionsEnum';
import { SharedService } from '../shared/services/shared.service';
import { LanguageHelper } from '../language/language.helper';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { registerServices } from '../services/regiser.services';
import { registerDto } from '../dataModels/register.model';
// import { registerDto } from './../../../dataModels/register.model';
@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {
  isCountryOpen: boolean = false;
  registerForm: FormGroup;
  isRegister: boolean = false;
  isRTL: any;
  registerDto =  new registerDto();
  listCountries: any[] = [];
  selectedCountry = { name: "Saudi Arabia", flag: null };
  isRegionOpen: boolean = false;
  listRegion: regionsDto[] = [
    { display: "EU (European Union)", value: RegionsEnum.EU },
    { display: "EFTA (European Free Trade Association)", value: RegionsEnum.EFTA },
    { display: "CARICOM (Caribbean Community)", value: RegionsEnum.CARICOM },
    { display: "USAN (Union of South American Nations)", value: RegionsEnum.USAN },
    { display: "EEU (Eurasian Economic Union)", value: RegionsEnum.EEU },
    { display: "AL (Arab League)", value: RegionsEnum.AL },
    { display: "ASEAN (Association of Southeast Asian Nations)", value: RegionsEnum.ASEAN },
    { display: "CAIS (Central American Integration System)", value: RegionsEnum.CAIS },
    { display: "CEFTA (Central European Free Trade Agreement)", value: RegionsEnum.CEFTA },
    { display: "NAFTA (North American Free Trade Agreement)", value: RegionsEnum.NAFTA },
    { display: "SAARC (South Asian Association for Regional Cooperation)", value: RegionsEnum.SAARC },
  ];
  constructor(public shared: SharedService, private languageHelper: LanguageHelper,
  private translateService: TranslateService, private router: Router, private formBuilder: FormBuilder, private registerServices:registerServices) {
 this.isRTL = this.languageHelper.currentLang().isRTL;
 
}

  ngOnInit(): void {
    this.shared.getAllCountries().subscribe((res: any[]) => this.listCountries = [...res]);
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.compose([Validators.pattern('^[^\\s@]+@[^\\s@]+\\.[^\\s@]{2,}$'), Validators.minLength(4), Validators.maxLength(25)])]],
      fName: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(10)]],
      lName: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(10)]],
      company: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]],
      code: ['+966'],
      phone: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]],
     

    
     });
  }

  selectCountry(country) {
    this.isCountryOpen = false;
    this.selectedCountry = { ...country }
    this.registerForm.get("code").setValue("+" + country.callingCodes[0])
  }

  trueChart(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)
      && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
      return false;
    }
    return true;
  }
  
  trueChartEmail(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57)
      && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
      return false;
    }
    return true;
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  selectRegion() {
    let region = this.registerForm.get('region').value;
    console.log(RegionsEnum[region]);

    this.shared.getCountriesWithRegion(RegionsEnum[region]).subscribe((res: any[]) => this.listCountries = [...res]);
  }
  newPartner(){
    this.isRegister = true;
    if (this.registerForm.valid) {
      
      this.registerDto.company = this.registerForm.value.company
      this.registerDto.email=this.registerForm.value.email
      this.registerDto.country=this.registerForm.value.phone
      this.registerDto.phone=this.registerForm.value.phone
      this.registerDto.firstName=this.registerForm.value.fName
      this.registerDto.lastName=this.registerForm.value.lName
      // this.registerDto.country=this.registerForm.value.country

      this.registerServices.postRegister(this.registerDto).subscribe(res => {
      console.log(res.message)
      this.router.navigate(['email-confirmation'])

      })
    }
    if (this.registerForm.get('email').invalid) {
   
    }
    if (this.registerForm.get('phone').invalid) {

    }
    if (this.registerForm.get('fName').invalid) {

    }
    if (this.registerForm.get('lName').invalid) {

    }
    
    if (this.registerForm.get('company').invalid) {
  
  }
  }
}
