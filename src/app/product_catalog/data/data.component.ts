import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { voiceDto } from '../../dataModels/voiceDto.model';
import { dataServices } from '../../services/dataServices';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {
  voice: voiceDto[] = new Array<voiceDto>();

  constructor(private router: Router, private dataService: dataServices) { }

  ngOnInit(): void {
    this.dataService.getProducts().subscribe(res => {
      this.voice = [...res.products]
    });
  }
  product(name:string){
    this.router.navigate(['/product-catalog', name])
  }
}
