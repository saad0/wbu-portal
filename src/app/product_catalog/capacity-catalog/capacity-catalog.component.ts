import { productCataServices } from './../../services/productCata.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { voiceDto } from '../../dataModels/voiceDto.model';

@Component({
  selector: 'app-capacity-catalog',
  templateUrl: './capacity-catalog.component.html',
  styleUrls: ['./capacity-catalog.component.css']
})
export class CapacityCatalogComponent implements OnInit {
  regDetails: voiceDto = new voiceDto();

  constructor(private _route: ActivatedRoute, private productCataServices: productCataServices) { 
    this.regDetails.name = this._route.snapshot.paramMap.get("name");
  }

  ngOnInit(): void {
    this.productCataServices.getReqDetails(this.regDetails.name).subscribe(res => {
      this.regDetails.category = res.category;
      this.regDetails.child = res.child;
      this.regDetails.description = res.description;
      this.regDetails.name = res.name;
    });
    this.productCataServices.getImg(this.regDetails.name).subscribe(res => {
      this.regDetails.name = res.name;
      // console.log("SAA", this.regDetails.name)
    });
  }
}

