import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapacityCatalogComponent } from './capacity-catalog.component';

describe('CapacityCatalogComponent', () => {
  let component: CapacityCatalogComponent;
  let fixture: ComponentFixture<CapacityCatalogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapacityCatalogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapacityCatalogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
