import { colocationServices } from './../../services/colocationServices';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { voiceDto } from '../../dataModels/voiceDto.model';

@Component({
  selector: 'app-colocation',
  templateUrl: './colocation.component.html',
  styleUrls: ['./colocation.component.css']
})
export class ColocationComponent implements OnInit {
  voice: voiceDto[] = new Array<voiceDto>();

  constructor(private router: Router, private colocationServices: colocationServices) { }

  ngOnInit(): void {
    this.colocationServices.getProducts().subscribe(res => {
      this.voice = [...res.products]
    });
  }
  product(name:string){
    this.router.navigate(['/product-catalog', name])
  }

}
