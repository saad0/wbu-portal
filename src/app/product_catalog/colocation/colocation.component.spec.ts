import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColocationComponent } from './colocation.component';

describe('ColocationComponent', () => {
  let component: ColocationComponent;
  let fixture: ComponentFixture<ColocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
