import { voiceServices } from './../../services/voiceServices';
import { voiceDto } from './../../dataModels/voiceDto.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var ss: any;
declare var require: any;
@Component({
  selector: 'app-voice',
  templateUrl: './voice.component.html',
  styleUrls: ['./voice.component.css']
})
export class VoiceComponent implements OnInit  {
  voice: voiceDto[] = new Array<voiceDto>();

  constructor(private router: Router, private voiceServices: voiceServices) { }

  ngAfterViewInit() {
    
  }

  ngOnInit(): void {
    this.voiceServices.getProducts().subscribe(res => {
      this.voice = [...res.products]
    });
  }
  product(name:string){
    this.router.navigate(['/product-catalog', name])
  }
}
