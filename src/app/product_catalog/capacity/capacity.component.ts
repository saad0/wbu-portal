import { capacityServices } from './../../services/capacityServices';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { voiceDto } from '../../dataModels/voiceDto.model';

@Component({
  selector: 'app-capacity',
  templateUrl: './capacity.component.html',
  styleUrls: ['./capacity.component.css']
})
export class CapacityComponent implements OnInit {
  voice: voiceDto[] = new Array<voiceDto>();

  constructor(private router: Router, private capacityServices:capacityServices) { }

  ngOnInit(): void {
    this.capacityServices.getProducts().subscribe(res => {
      this.voice = [...res.products];
    });
  }
  product(name:string){
    this.router.navigate(['/product-catalog', name])
  }
}
