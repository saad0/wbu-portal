import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { MustMatch } from './_helpers/must-match.validator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private options: string[] = [];

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
    // this.dialog.open(CpChangeComponent, {
    //   width: "700px",
    //   data: { options: this.options },
    //   disableClose: true
    // });
  }

}
@Component({
  selector: 'app-cp-change',
  templateUrl: './cp-change.component.html',
})
export class CpChangeComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  password: string;
  confirm: string;
  current: string;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      currpassword: ['', Validators.required, this.password],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['', Validators.required]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.valid) {
      if (this.current == "11") {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })

        Toast.fire({
          icon: 'success',
          title: 'Password updated successfully'
        })
      }
      else {
        alert("Current password is wrong")
      }
    }
    else {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      Toast.fire({
        icon: 'error',
        title: 'Password is wrong'
      })
    }
  }

  trueChart(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 35 || charCode > 39)&& (charCode < 42 || charCode > 43)
      && (charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57)
      && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
      return false;
    }
    return true;
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
}