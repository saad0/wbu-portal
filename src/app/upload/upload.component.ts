import { Component, OnInit, Inject, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { NgbDate, NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { uploadServices } from '../services/upload.services';
import { uploadDto } from '../dataModels/uploads.model';
import { Text } from '@angular/compiler';
import { BehaviorSubject } from 'rxjs';
import { TargetServices } from '../services/target.services';
import { targetDtto } from '../dataModels/target.model';


 
// const URL = '/api/';
const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})

export class UploadComponent implements OnInit {
  // @ViewChild('file1') file1: ElementRef;
  @ViewChild('fileInput') fileInput;
  mainURl="https://172.21.51.217:19090/wbuTest3/files/upload";
  URL = "https://172.21.51.217:19090/wbu/cdrs/billing"


  private options: string[] = [];
  uploader: FileUploader;
  hasBaseDropZoneOver: boolean;
  hasAnotherDropZoneOver: boolean;
  response: string;
  size1: string = "";
  technicalForm: FormGroup;
  uploader1: FileUploader;
  public textTech = new FormControl('');
  public textControl = new FormControl('');

  public descriptionTech = new BehaviorSubject(0);
  public descriptionLength = new BehaviorSubject(0);

  showSpinner = false;
  hoveredDate: NgbDate ;
  fromDate: NgbDate
  toDate: NgbDate 
  period:NgbDate ;
  targetU:targetDtto[]= new Array<targetDtto>();


  isUpload:boolean=false;
  uploadDto = new uploadDto();
  
  billingForm: FormGroup;
  // targetU =new targetDtto();

 
  constructor(private calendar: NgbCalendar, public formatter: NgbDateParserFormatter,
    public dialog: MatDialog, private router: Router , private uploadServices:uploadServices, private TargetServices:TargetServices) {
   
      this.billingForm = new FormGroup({
        period: new FormControl("", [Validators.required]),
        product: new FormControl("", [Validators.required]),
        // fromDate: new FormControl("", [Validators.required]),
        // toDate: new FormControl("", [Validators.required]),
        // desc: new FormControl("", [Validators.required]),
        // doc: new FormControl(""),
        invoice: new FormControl("", [Validators.required])
      })
      this.technicalForm = new FormGroup({
        invoiceTech: new FormControl("", [Validators.required]),
        periodTech: new FormControl("", [Validators.required]),
        fromDateTech: new FormControl("", [Validators.required]),
        toDateTech: new FormControl("", [Validators.required]),
        productTech: new FormControl("", [Validators.required]),
        targetTech: new FormControl("", [Validators.required]),
        descTech: new FormControl("", [Validators.required]),
        docTech: new FormControl("", [Validators.required])
      })
  

    this.textControl.valueChanges.subscribe((v)=>  this.descriptionLength.next(v.length));
      this.textTech.valueChanges.subscribe((v)=>  this.descriptionTech.next(v.length));

    this.uploader = new FileUploader({
      url: URL,
      disableMultipart: true, // 'DisableMultipart' must be 'true' for formatDataFunction to be called.
      formatDataFunctionIsAsync: true,
      formatDataFunction: async (item) => {
        return new Promise((resolve, reject) => {
          resolve({
            name: item._file.name,
            length: item._file.size,
            contentType: item._file.type,
            date: new Date()
          });
        });
      }
    });
    this.hasBaseDropZoneOver = false;
    this.hasAnotherDropZoneOver = false;
    this.response = '';
    this.uploader.response.subscribe(res => this.response = res);
    this.fromDate = calendar.getToday();
    this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
 
  }
 
  openDialog() {
    // this.dialog.open(TargerUserComponent, {
    //   width: "300px",
    //   data: { options: this.options }
    // });
  }
 getTarget(){
  this.TargetServices.getTarget().subscribe(res => { 
    this.targetU =[...res.targetUsers]
    // this.targetDto.targetUsers=[...res.targetUsers]

    
    console.log(res)

   })
 }
  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }
 
  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) &&
      date.before(this.hoveredDate);
  }
 
  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }
 
  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }
 
  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }
  public onFileSelected(event) {

    let files = event.target.files;
    if (this.validateFile(files[0].name)) {
      const file: File = event[0];
      this.uploadFile(files[0]);
    }
  }
  validateFile(name: String) {
    var ext = name.substring(name.lastIndexOf('.') + 1);
    if((ext.toLowerCase() == 'txt') || (ext.toLowerCase() == 'csv')) {
      return true;
    } else {
      const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      Toast.fire({
        icon: 'error',
        title: 'Invalid File Format'
      })
  return window.location.reload();
    }
  }
  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }
 
  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }
 
  ngOnInit(): void {
   this.getTarget()
    
    
  }

  uploadFile(file:File ){
  let fileData=new FormData();
  
   fileData.append("document",file);
   fileData.append("fileDetails",new Blob([JSON.stringify({"fileType": file.type})],{type:"Application/json"}))
   console.log(fileData)
   console.log(file)
  this.uploadServices.postUpload(fileData).subscribe(res=>{
    // console.log(fileData)

      // this.billingForm.get("doc").setValue(res)
    })
    
  }
  // "fileDetails", JSON.stringify({"fileType": file.type})
  save() {
    // this.billingForm.get("fromDate").setValue(this._convertToIsoString(this.fromDate))
    // this.billingForm.get("toDate").setValue(this._convertToIsoString(this.toDate))
    // this.billingForm.get("period").setValue(this._convertToIsoString(this.period))




  // if(this.billingForm.get('doc').invalid){
  //   const Toast = Swal.mixin({
  //     toast: true,
  //     position: 'bottom-end',
  //     showConfirmButton: false,
  //     timer: 3000,
  //     timerProgressBar: true,
  //     onOpen: (toast) => {
  //       toast.addEventListener('mouseenter', Swal.stopTimer)
  //       toast.addEventListener('mouseleave', Swal.resumeTimer)
  //     }
  //   });
  //   Toast.fire({
  //     icon: 'error',
  //     title: 'Please Upload File'
  //   })
  // }

    if (this.billingForm.valid){
      this.uploadDto.billingPeriod = this.billingForm.value.period
      this.uploadDto.description = this.billingForm.value.desc
      this.uploadDto.fileId = this.billingForm.value.doc
      this.uploadDto.fromDate =this.billingForm.value.fromDate
       this.uploadDto.toDate=this.billingForm.value.toDate
       this.uploadDto.invoiceNumber=this.billingForm.value.invoice
       
      // this.uploadDto.targetTeam=this.billingForm.value.
       this.uploadDto.product=this.billingForm.value.product
       Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Save'
      }).then((result) => {
        this.showSpinner = true;
        setTimeout(() => {
          this.showSpinner = false;
        }, 5000);
        if (result.value) {
          this.uploadServices.postCdrs(this.uploadDto).subscribe(res => { 
          
            console.log(res)
            this.router.navigate(['home']);
       
         })

        }
      });
    
  }  
  if (this.billingForm.invalid){
    
    const Toast = Swal.mixin({
      toast: true,
      position: 'bottom-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      onOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    });
    Toast.fire({
      icon: 'error',
      title: 'Form Is Not Valid'
    })
  }
  
}
private _convertToIsoString(date, beginTime: boolean = null) {
  if (!date) {
    return null;
  }
  if (!(date.year && date.month && date.day)) {
    return null;
  }
  if (beginTime == true) {
    var dateFormated = new Date(
      date.year,
      date.month - 1,
      date.day,
      0,
      0,
      0
    ).toISOString();
    return dateFormated;
  } else if (beginTime == false) {
    var dateFormated = new Date(
      date.year,
      date.month - 1,
      date.day,
      23,
      59,
      59
    ).toISOString();
    return dateFormated;
  } else {
    const Now = new Date();
    var dateFormated = new Date(
      date.year,
      date.month - 1,
      date.day,
      Now.getHours(),
      Now.getMinutes(),
      Now.getSeconds()
    ).toISOString();
    return dateFormated;
  }
}
}
