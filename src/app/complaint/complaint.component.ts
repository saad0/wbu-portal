import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-complaint',
  templateUrl: './complaint.component.html',
  styleUrls: ['./complaint.component.css']
})
export class ComplaintComponent implements OnInit {
  public complaintForm: FormGroup;
  technical: boolean = false;
  general: boolean = true;
  complain: string = '';

  constructor(private router: Router) {
    this.complaintForm = new FormGroup({
      desc: new FormControl("", [Validators.required])
    })
  }

  ngOnInit(): void {
  }
  complainType(event: any) {
    this.complain = event.target.value;
    if (this.complain == "billing") {
      this.technical = false;
      this.general = true;
    }
    if (this.complain == "general") {
      this.technical = true;
      this.general = true;
    }
    if (this.complain == "technical") {
      this.technical = true;
      this.general = false;
    }
  }
  onSubmit() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      allowOutsideClick: false,
      timer: 3000,
      timerProgressBar: true,
      onOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    Swal.fire(
      'Sent',
      'Your Complaint has been sent and we will solve your complaint shortly.',
      'success'
    )
    this.router.navigate(['home'])
  }
}
