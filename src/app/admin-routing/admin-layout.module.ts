import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AdminLayoutRoutes } from './admin-layout.routing';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { ToastrModule } from 'ngx-toastr';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { BrowserModule } from '@angular/platform-browser';
import { NgxWebstorageModule } from 'ngx-webstorage';
import 'hammerjs';

import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import {CdkTableModule} from '@angular/cdk/table';
import { VoiceComponent } from '../product_catalog/voice/voice.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    MatFormFieldModule,
    MatSelectModule,
    MatSliderModule,
    TranslateModule,
    CdkTableModule,

  ],
  declarations: [
    VoiceComponent
  ],
  providers: [],
  bootstrap: []
})
@NgModule({
  exports: [
    
  ],
})
export class AdminLayoutModule { }
