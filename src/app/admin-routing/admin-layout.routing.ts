import { Routes } from '@angular/router';
import { VoiceComponent } from '../product_catalog/voice/voice.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'voice/:ids',     component: VoiceComponent},
];
