import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatAutocomplete } from '@angular/material/autocomplete';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

export interface Role {
  name: string;
}
@Component({
  selector: 'app-user-setting',
  templateUrl: './user-setting.component.html',
  styleUrls: ['./user-setting.component.css']
})
export class UserSettingComponent implements OnInit {
  avengers: { fname: string; lname: string; company: string; email: string; phone: string; country: string; date: string; status: string; }[];
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  roles: Role[] = [
    { name: 'Complaints' },
    { name: 'Contact Us' },
    { name: 'PC' },
    { name: 'Services, Issue New RFP' },
  ];
  capacities: Role[] = [
    { name: 'C042' },
    { name: 'MENA Gateway Service ' },
    { name: 'C046' },
    { name: 'C041' },
    { name: 'C043' },
  ];
  @ViewChild('roleInput') roleInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  constructor( private router: Router) { }

  ngOnInit(): void {
    this.avengers =
      [{ fname: 'Sam', lname: 'Alanazi', company: 'Bret', email: 'Sam@april.biz', phone: '0555622277', country: 'Saudi Arabia', date: '2020-08-15', status: 'Pending' },
      { fname: 'Abdo', lname: 'alhosan', company: 'TATA', email: 'Abdo@melissa.tv', phone: '0556559277', country: 'UAE', date: '2020-08-17', status: 'Closed' },
      { fname: 'Aziz', lname: 'almu', company: 'DATA', email: 'Aziz@yesenia.net', phone: '0556553223', country: 'kuwait', date: '2020-08-19', status: 'Approved' },
      { fname: 'Ree', lname: 'A', company: 'Mobily', email: 'Ree.OConner@kory.org', phone: '0559663224', country: 'Saudi Arabia', date: '2020-08-25', status: 'Pending' },
      { fname: 'Zack', lname: 'Bee', company: 'Zain', email: 'Zack@annie.ca', phone: '0556961145', country: 'USA', date: '2020-08-27', status: 'Closed' }];
  }
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our role
    if ((value || '').trim()) {
      this.roles.push({ name: value.trim() });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(role: Role): void {
    const index = this.roles.indexOf(role);

    if (index >= 0) {
      this.roles.splice(index, 1);
    }
  }
  save() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      onOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Save'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Saved!',
          'Your Changes has been made.',
          'success'
        )
        this.router.navigate(['registration-details'])
      }
    })
  }
}
