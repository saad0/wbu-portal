
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { loginDto } from '../dataModels/login.model';
import { responseLogin } from '../dataModels/responseLogin.model';

@Injectable({
    providedIn: "root"
})

export class loginServices {
    mainURl = "https://172.21.51.217:19090/wbu/login";
    constructor(private https: HttpClient) {
    }

    postlogin(obj: loginDto) {
        return this.https.post<responseLogin>(this.mainURl, obj);
    }
}

export class logoutServices {
    mainURl = "https://172.21.51.217:19090/wbu/logout";
    constructor(private https: HttpClient) {
    }

}