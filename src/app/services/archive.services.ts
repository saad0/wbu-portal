import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { archiveListDto } from '../dataModels/archiveListDto.model';
// import {archiveDto}from '../../src/app/dataModels/archive.model'

// import { archiveListDto } from './dataModels/archiveListDto.model';


@Injectable({
    providedIn:"root"
})

export class archiveServices{
    mainURL ="https://172.21.51.217:19090"
     headers = new HttpHeaders();
   
constructor(private _http:HttpClient){
    this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
}

getArchive(){
    
    return this._http.get<archiveListDto>(this.mainURL+'/wbu/admin/history?size=10&page=1'  ,{headers:this.headers})
}}