import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { partnerDto } from '../dataModels/newpartner.model';
import { ResponseDto } from '../dataModels/respone.model';

@Injectable({
    providedIn:"root"
})

export class partnerServices{
    mainURl="https://172.21.51.217:19090/wbu/registration/new-partner";
    constructor(private https:HttpClient){

    }

    
    postpartner(obj:partnerDto){
       
        return this.https.post<ResponseDto>(this.mainURl,obj);
    }
}
