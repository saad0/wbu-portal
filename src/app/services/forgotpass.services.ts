
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { ResponseDto } from '../dataModels/respone.model';

@Injectable({
    providedIn:"root"
})

export class forgotPassServices{
    mainURl="https://172.21.51.217:19090/wbu/password/forgot";
    constructor(private https:HttpClient){
    }
  
    postpass(email:string){
        return this.https.post<ResponseDto>(this.mainURl,{email:email});
    }
}

