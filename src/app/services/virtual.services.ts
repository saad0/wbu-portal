import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RegisterationListDto } from '../dataModels/registerationListDto.model';

import { ResponseDto } from '../dataModels/respone.model';

@Injectable({
    providedIn:"root"
})

export class VirtualServices{
    mainURL ="https://172.21.51.217:19090"   
     headers = new HttpHeaders();
     email:any;
constructor(private _http:HttpClient){
    this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
    this.headers = this.headers.append('Access-Control-Allow-Origin', '*');

}

getVirtual(){
    return this._http.get<RegisterationListDto>(this.mainURL+"/wbu/registration/virtual-account?size=10&page=1"   ,{headers:this.headers})
}
deleteVirtual(){
    return this._http.post(this.mainURL+"/wbu/registration/virtual-account/delete" ,{headers:this.headers})
}
}
