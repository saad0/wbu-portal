import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { archiveListDto } from '../dataModels/archiveListDto.model';

@Injectable({
    providedIn: "root"
})

export class assetsServices {
    mainURL = "https://172.21.51.217:19090";
    headers = new HttpHeaders();

    constructor(private _http: HttpClient) {
        this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
    }

    getAssets() {
        return this._http.get<archiveListDto>(this.mainURL + '/wbu/services/assets?size=4&page=1', { headers: this.headers });
    }
}
