import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RegisterationListDto } from '../dataModels/registerationListDto.model';
import { RegisteredMembersDto } from '../dataModels/registeredMembersDto.model';

@Injectable({
    providedIn:"root"
})

export class ApproveregServices{
    mainURL ="https://172.21.51.217:19090"
     headers = new HttpHeaders();
   
constructor(private _http:HttpClient){
    this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
    this.headers = this.headers.append('Access-Control-Allow-Origin', '*');

}

getReqList(){
    return this._http.get<RegisterationListDto>(this.mainURL+"/wbu/registration/submitted?size=10&page=1"   ,{headers:this.headers})
}
getReqDetails(id:number){
    return this._http.post<RegisteredMembersDto>(this.mainURL+"/wbu/registration/submitted/details",{id:id},{headers:this.headers});
}
}
