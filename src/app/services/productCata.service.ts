import { voiceDto } from './../dataModels/voiceDto.model';
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { archiveListDto } from '../dataModels/archiveListDto.model';

@Injectable({
    providedIn: "root"
})

export class productCataServices {
    mainURL = "https://172.21.51.217:19090"
    headers = new HttpHeaders();

    constructor(private _http: HttpClient) {
        this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
        this.headers = this.headers.append('Access-Control-Allow-Origin', '*');

    }
    getReqDetails(name: string) {
        return this._http.post<voiceDto>(this.mainURL + "/wbu/products/details", { name: name }, { headers: this.headers });
    }
    getImg(name: string){
        return this._http.get<voiceDto>(this.mainURL + "/wbu/products/img?imgName="+name , { headers: this.headers });
    }
}
