import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { ResponseDto } from '../dataModels/respone.model';
import { uploadDto } from '../dataModels/uploads.model';

@Injectable({
    providedIn: "root"
})

export class uploadServices {
    mainURl = "https://172.21.51.217:19090/wbu/files/upload";
    URL = "https://172.21.51.217:19090/wbu/cdrs/billing"

    constructor(private https: HttpClient) {


    }


    public postUpload(obj: FormData) {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
        console.log(headers)
        return this.https.post<ResponseDto>(this.mainURl, obj, { headers: headers });

    }

  postCdrs(obj:uploadDto) {


        let headers = new HttpHeaders();
        headers = headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
        console.log(headers)
        return this.https.post<ResponseDto>(this.URL,obj,{ headers: headers });

    }
    submitCDR(){
        
    }
}