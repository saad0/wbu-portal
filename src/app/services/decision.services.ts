import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { decisionDto } from '../dataModels/decisionDto.mode';

@Injectable({
    providedIn:"root"
})

export class DecisionServices{  
    mainURL ="https://172.21.51.217:19090"
     headers = new HttpHeaders();
   
constructor(private _http:HttpClient){
    this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
    this.headers = this.headers.append('Access-Control-Allow-Origin', '*');

}
getDecision(Body:decisionDto){
    return this._http.post(this.mainURL+"/wbu/registration/decision",Body,{headers:this.headers});

}
}