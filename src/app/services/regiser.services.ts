import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { registerDto } from '../dataModels/register.model';
import { ResponseDto } from '../dataModels/respone.model';

@Injectable({
    providedIn:"root"
})

export class registerServices{
    mainURl="https://172.21.51.217:19090/wbu/registration/partner";
    constructor(private https:HttpClient){

    }

    
    postRegister(obj:registerDto){
       
        return this.https.post<ResponseDto>(this.mainURl,obj);
    }
}
