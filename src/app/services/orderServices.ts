import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { archiveListDto } from '../dataModels/archiveListDto.model';
// import {archiveDto}from '../../src/app/dataModels/archive.model'
// import { archiveListDto } from './dataModels/archiveListDto.model';
@Injectable({
    providedIn: "root"
})


export class orderServices {
    mainURL = "https://172.21.51.217:19090";
    headers = new HttpHeaders();

    constructor(private _http: HttpClient) {
        this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
    }

    getOrders() {

        return this._http.get<archiveListDto>(this.mainURL + '/wbu/services/orders?size=10&page=1', { headers: this.headers });
    }
}
