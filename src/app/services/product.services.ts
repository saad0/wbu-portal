import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { productDto } from '../dataModels/productDto.model';

@Injectable({
    providedIn: "root"
})


export class productServices {
    mainURL = "https://172.21.51.217:19090";
    headers = new HttpHeaders();

    constructor(private _http: HttpClient) {
        this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
        // this.headers=this.headers.append('category','Capacity')
    }

    getPro() {

        return this._http.get<productDto>(this.mainURL + '/wbu/products?category=Capacity', { headers: this.headers });
    }
}

