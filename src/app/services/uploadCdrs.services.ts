import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { uploadDto } from '../dataModels/uploads.model';

@Injectable({
    providedIn: "root"
})

export class uploadCdrServices {
    mainURl = "https://172.21.51.217:19090";
    constructor(private https: HttpClient) {


    }


    public postCdrs() {

        let headers = new HttpHeaders();
        headers = headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
        console.log(headers)
        return this.https.post<uploadDto>(this.mainURl+"/wbu/cdrs/billing",{ headers: headers });

    }
}