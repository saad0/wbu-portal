import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { targetDtto } from '../dataModels/target.model';


@Injectable({
    providedIn:"root"
})

export class TargetServices{
    mainURl="https://172.21.51.217:19090";
    // headers: HttpHeaders | { [header: string]: string | string[]; };
    constructor(private https:HttpClient){
        

    }

    
    getTarget(){
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
        console.log(headers)
        return this.https.get<targetDtto>(this.mainURl+"/wbu/cdrs/users/billing");
    
    }
}
