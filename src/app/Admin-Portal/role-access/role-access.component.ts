import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

export interface Role {
  name: string;
}
@Component({
  selector: 'app-role-access',
  templateUrl: './role-access.component.html',
  styleUrls: ['./role-access.component.css']
})
export class RoleAccessComponent implements OnInit {
  roles: Role[] = [
    {name: 'Portal Admin'},
    {name: 'Upload CDR'},
    {name: 'Dwonalod CDR'},
  ];

  constructor(private router: Router) { }

  ngOnInit(): void {
  }


  create() {
    this.router.navigate(['new-role'])
  }
}
