import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ApproveregServices} from '../../services/approvereg.service'
import { RegisteredMembersDto } from '../../dataModels/registeredMembersDto.model';

@Component({
  selector: 'app-approve-registration',
  templateUrl: './approve-registration.component.html',
  styleUrls: ['./approve-registration.component.css']
})
export class ApproveRegistrationComponent implements OnInit {


  rows:RegisteredMembersDto[]= new Array<RegisteredMembersDto>();

  constructor(private router: Router,private ApproveregServices:ApproveregServices) { }

  ngOnInit(): void {
    // this.rows.push({id:55,firstName:"reema",lastName:"mohsen",company:"IST",country:"SA",email:"email@email.com",phone:"015645",status:"Active"});
    this.ApproveregServices.getReqList().subscribe(res=>{
      this.rows=  [...res.registrationReq];
    })
  }
  detail(id:number){
    this.router.navigateByUrl("/registration-action/"+id)
  }
}
