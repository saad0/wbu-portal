import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { VirtualServices } from '../../services/Virtual.services';
import { RegisteredMembersDto } from '../../dataModels/registeredMembersDto.model';


@Component({
  selector: 'app-virtual-account',
  templateUrl: './virtual-account.component.html',
  styleUrls: ['./virtual-account.component.css']
})
export class VirtualAccountComponent implements OnInit {
  updateRecevier: FormGroup;
  virtual :RegisteredMembersDto[]= new Array<RegisteredMembersDto>();
  virtualDto: any;
  // virtualDto = new virtualDto();


  constructor(private router: Router , private VirtualServices:VirtualServices) { }

  ngOnInit(): void {


    this.VirtualServices.getVirtual().subscribe(res => {
      this.virtual=[...res.registrationReq]
 
    });
  }
  onDelete(obj:RegisteredMembersDto){
    this.VirtualServices.deleteVirtual().subscribe(res=>{
     
      console.log(res)
    })

  }
  save() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      onOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    Swal.fire({
      title: 'Are you sure you want delet virtual account?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Save'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Saved!',
          'Your Changes has been made.',
          'success'
        )
        this.router.navigate(['virtual-account'])
      }
    })
  }
  }

