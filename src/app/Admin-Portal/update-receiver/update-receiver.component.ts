import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-receiver',
  templateUrl: './update-receiver.component.html',
  styleUrls: ['./update-receiver.component.css']
})
export class UpdateReceiverComponent implements OnInit {
  toggleProperty = false;

  constructor(private toast: ToastrService, private router: Router) {
 
  }

  ngOnInit(): void {
  }
  toggle() {
    this.toggleProperty = !this.toggleProperty;
  }
  save() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Save'
      }).then((result) => {
        if (result.value) {
          Swal.fire(
            'Saved!',
            'Your Changes has been made.',
            'success'
          )
          this.router.navigate(['portal-admin'])
        }
      })
    }
}