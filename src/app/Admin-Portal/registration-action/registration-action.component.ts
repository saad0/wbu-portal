import { Component, OnInit } from '@angular/core';

export interface Capacity {
  name: string;
}
import { FormControl } from '@angular/forms';
import { ElementRef, ViewChild } from '@angular/core';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { RegisteredMembersDto } from '../../dataModels/registeredMembersDto.model';
import { ApproveregServices } from '../../services/approvereg.service';
import { DecisionServices } from '../../services/decision.services'
import { decisionDto } from '../../dataModels/decisionDto.mode';


@Component({
  selector: 'app-registration-action',
  templateUrl: './registration-action.component.html',
  styleUrls: ['./registration-action.component.css']
})
export class RegistrationActionComponent implements OnInit {
  roleForm = new FormControl();
  selectable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  removable = true;
  capacities: Capacity[] = [
    { name: 'C042' },
    { name: 'MENA Gateway Service ' },
    { name: 'C046' },
    { name: 'C041' },
    { name: 'C043' },
  ];
  filteredRoles: Observable<string[]>;
  roles: string[] = [];
  allRoles: string[] = ['General billing ', 'Billing read only ', 'General commercial', 'Commercial read only', 'Technical read only', 'General technical', 'Guest', 'admin'];  // decisionDto = new decisionDto();
  role: decisionDto[] = new Array<decisionDto>();


  @ViewChild('roleInput') roleInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  public approve_text = new FormControl('');
  public descriptionLength = new BehaviorSubject(0);
  regDetails: RegisteredMembersDto = new RegisteredMembersDto();
  constructor(private router: Router, private _route: ActivatedRoute, private _approverReg: ApproveregServices, private DecisionServices: DecisionServices) {
    this.regDetails.id = +this._route.snapshot.paramMap.get("id");
    this.approve_text.valueChanges.subscribe((v) => this.descriptionLength.next(v.length));

    this.filteredRoles = this.roleForm.valueChanges.pipe(
      startWith(null),
      map((role: string | null) => role ? this._filter(role) : this.allRoles.slice()));
  }
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our role
    if ((value || '').trim()) {
      this.roles.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.roleForm.setValue(null);
  }

  remove(role: string): void {
    const index = this.roles.indexOf(role);

    if (index >= 0) {
      this.roles.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.roles.push(event.option.viewValue);
    this.roleInput.nativeElement.value = '';
    this.roleForm.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allRoles.filter(role => role.toLowerCase().indexOf(filterValue) === 0);
  }
  noText(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 1) {
      return false;
    }
    return true;
  }
  ngOnInit(): void {
    // this.regDetails={id:55,firstName:"reema",lastName:"Afnan",company:"IST",country:"SA",email:"email@email.com",phone:"015645",status:"Active"};

    this._approverReg.getReqDetails(this.regDetails.id).subscribe(res => {
      this.regDetails.company = res.company;
      this.regDetails.country = res.country;
      this.regDetails.email = res.email;
      this.regDetails.firstName = res.firstName;
      this.regDetails.lastName = res.lastName;
      this.regDetails.phone = res.phone;
      this.regDetails.status = res.status;
    })
  }
  reject() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      onOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    Swal.fire({
      title: 'Reject',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Saved!',
          'Your Changes has been made.',
          'success'
        )
        this.router.navigateByUrl("/approve-registration")
      }
    })
  }

  approve() {
    let decObj: decisionDto = new decisionDto();
    decObj.decision= true;
    decObj.remark=this.regDetails.remark;
    decObj.reqId= this.regDetails.id;
    decObj.roles=this.roles;

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      onOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Approve'
    }).then((result) => {
      if (result.value) {
        this.DecisionServices.getDecision(decObj).subscribe(res => {
          Swal.fire(
            'Saved!',
            'Your Changes has been made.',
            'success'
          )
        })
      }
    })
  }
}
