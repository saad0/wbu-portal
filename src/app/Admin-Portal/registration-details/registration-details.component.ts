import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-registration-details',
  templateUrl: './registration-details.component.html',
  styleUrls: ['./registration-details.component.css']
})
export class RegistrationDetailsComponent {
  filterValues = {};
  dataSource = new MatTableDataSource();
  displayedColumns: string[] = ['fname', 'lname', 'company', 'email', 'phone', 'country', 'date', 'status'];
  avengers = []; 
  filterSelectObj = [];
  constructor() {
   
    // Object to create Filter for
    this.filterSelectObj = [
      {
        name: 'STATUS',
        columnProp: 'status',
        options: []
      }
    ]
  }

  ngOnInit() {
    this.avengers = 
    [{  fname: 'Sam', lname: 'Alanazi', company: 'Bret', email: 'Sam@april.biz', phone: '0555622277', country: 'Saudi Arabia',date: '2020-08-15', status: 'Rejeted'},
    {  fname: 'Abdo', lname: 'alhosan', company: 'TATA', email: 'Abdo@melissa.tv', phone: '0556559277', country: 'UAE',date: '2020-08-17', status: 'Submmited'}, 
    {  fname: 'Aziz', lname: 'almu', company: 'DATA', email: 'Aziz@yesenia.net', phone: '0556553223', country: 'kuwait',date: '2020-08-19', status: 'Approved'}, 
    {  fname: 'Ree', lname: 'A', company: 'Mobily', email: 'Ree.OConner@kory.org', phone: '0559663224', country: 'Saudi Arabia',date: '2020-08-25', status: 'Rejeted'}, 
    {  fname: 'Zack', lname: 'Bee', company: 'Zain', email: 'Zack@annie.ca', phone: '0556961145', country: 'USA',date: '2020-08-27', status: 'Submmited'}]; 
    this.dataSource.data = this.avengers;
    this.filterSelectObj.filter((o) => {
      o.options = this.getFilterObject(this.avengers, o.columnProp);
    });
    this.dataSource.filterPredicate = this.createFilter();
  }

  getFilterObject(fullObj, key) {
    const uniqChk = [];
    fullObj.filter((obj) => {
      if (!uniqChk.includes(obj[key])) {
        uniqChk.push(obj[key]);
      }
      return obj;
    });
    return uniqChk;
  }

  // Called on Filter change
  filterChange(filter, event) {
    //let filterValues = {}
    this.filterValues[filter.columnProp] = event.target.value.trim().toLowerCase()
    this.dataSource.filter = JSON.stringify(this.filterValues)
  }

  // Custom filter method fot Angular Material Datatable
  createFilter() {
    let filterFunction = function (data: any, filter: string): boolean {
      let searchTerms = JSON.parse(filter);
      let isFilterSet = false;
      for (const col in searchTerms) {
        if (searchTerms[col].toString() !== '') {
          isFilterSet = true;
        } else {
          delete searchTerms[col];
        }
      }
      let nameSearch = () => {
        let found = false;
        if (isFilterSet) {
          for (const col in searchTerms) {
            searchTerms[col].trim().toLowerCase().split(' ').forEach(word => {
              if (data[col].toString().toLowerCase().indexOf(word) != -1 && isFilterSet) {
                found = true
              }
            });
          }
          return found
        } else {
          return true;
        }
      }
      return nameSearch()
    }
    return filterFunction
  }


  // Reset table filters
  resetFilters() {
    this.filterValues = {}
    this.filterSelectObj.forEach((value, key) => {
      value.modelValue = undefined;
    })
    this.dataSource.filter = "";
  }
}