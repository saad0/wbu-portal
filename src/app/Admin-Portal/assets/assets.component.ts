import { assetsServices } from './../../services/assetsServices';
import { assetsDto } from './../../dataModels/assetsDto.model';
import { Component } from '@angular/core';

@Component({
  selector: 'app-assets',
  templateUrl: './assets.component.html',
  styleUrls: ['./assets.component.css']
})
export class AssetsComponent {
  searchText;
  asset: assetsDto[] = new Array<assetsDto>();
  constructor(private assetsServices: assetsServices) { }

  ngOnInit() {
    this.assetsServices.getAssets().subscribe(res => {
      this.asset = [...res.asstes]
    });
  }
}
