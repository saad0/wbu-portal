import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { archiveServices } from '../../services/archive.services';
import { archiveDto } from '../../dataModels/archive.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageEvent: PageEvent;
  archive: archiveDto[] = new Array<archiveDto>();
  dataSource: MatTableDataSource<archiveDto>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private archiveServices: archiveServices) { }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    if (setPageSizeOptionsInput) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }
  }

  ngOnInit() {
    this.onTableUpdate();
    this.archiveServices.getArchive().subscribe(res => {
      this.archive = [...res.log]
    });
  }
  onTableUpdate() {
    this.dataSource = new MatTableDataSource<any>(this.archive);
    this.dataSource.paginator = this.paginator;
  }
}
