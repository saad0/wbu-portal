import { Component} from '@angular/core';
import { orderDto } from '../../dataModels/orderDto.model';
import { orderServices } from '../../services/orderServices';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})

export class OrdersComponent {
  ord: orderDto[] = new Array<orderDto>();
  searchText;
  constructor(private orderServices:orderServices){}

  ngOnInit() {
    this.orderServices.getOrders().subscribe(res => {
      this.ord = [...res.orders]
    });
  }
}