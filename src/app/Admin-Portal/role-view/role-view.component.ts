import { Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-role-view',
  templateUrl: './role-view.component.front.html',
  styleUrls: ['./role-view.component.css']
})
export class RoleViewComponent {
  toggleProperty = false;

  constructor() {
   
  }
  ngOnInit(): void {
  }
  toggle() {
    this.toggleProperty = !this.toggleProperty;
  }
}
