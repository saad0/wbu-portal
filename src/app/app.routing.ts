import { DetailsComponent } from './details/details.component';
import { DownloadsOperationsComponent } from './downloads-operations/downloads-operations.component';
import { ComplaintComponent } from './complaint/complaint.component';
import { UploadComponent } from './upload/upload.component';
import { NewPartnerComponent } from './new-partner/new-partner.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { ContactUsComponent } from './contact-us/contact-us.component';

import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component'
import { RegisterComponent } from './views/register/register.component';
import { from } from 'rxjs';
import { NavComponent } from './nav/nav.component';
import { MainLayoutComponent } from './containers/main-layout/main-layout.component';
import { SidebarComponent } from './containers/sidebar/sidebar.component';
import { HomeComponent, CpChangeComponent } from './home/home.component';
import { HelpComponent } from './help/help.component';
import { DashboardWBUComponent } from './dashboard-wbu/dashboard-wbu.component';
import { DisplayInvoiceComponent } from './display-invoice/display-invoice.component';
import { ForgotPasswordComponent } from './views/forgot-password/forgot-password.component';
import { ApproveRegistrationComponent } from './Admin-Portal/approve-registration/approve-registration.component';
import { ContactAdminComponent } from './Admin-Portal/contact-admin/contact-admin.component';
import { PortalAdminComponent } from './Admin-Portal/portal-admin/portal-admin.component';
import { VirtualAccountComponent } from './Admin-Portal/virtual-account/virtual-account.component';
import { UpdateReceiverComponent } from './Admin-Portal/update-receiver/update-receiver.component';
import { HistoryComponent } from './Admin-Portal/history/history.component';
import { RoleAccessComponent } from './Admin-Portal/role-access/role-access.component';
import { RoleViewComponent } from './Admin-Portal/role-view/role-view.component';
import { NewRoleComponent } from './Admin-Portal/new-role/new-role.component';
import { RegisterUserComponent } from './register-user/register-user.component';
import { ProductCatalogComponent } from './product-catalog/product-catalog.component';
import { VoiceComponent } from './product_catalog/voice/voice.component';
import { CapacityComponent } from './product_catalog/capacity/capacity.component';
import { RegistrationDetailsComponent } from './Admin-Portal/registration-details/registration-details.component';
import { CapacityCatalogComponent } from './product_catalog/capacity-catalog/capacity-catalog.component';
import { RegistrationActionComponent } from './Admin-Portal/registration-action/registration-action.component';
import { EmailConfirmationComponent } from './email-confirmation/email-confirmation.component';
import { EmailResetComponent } from './email-reset/email-reset.component';
import { PasswordExpiryComponent } from './views/password-expiry/password-expiry.component';
import { EmailApprovedComponent } from './views/email-approved/email-approved.component';
import { DataComponent } from './product_catalog/data/data.component';
import { ColocationComponent } from './product_catalog/colocation/colocation.component';
import { OrdersComponent } from './Admin-Portal/orders/orders.component';
import { AssetsComponent } from './Admin-Portal/assets/assets.component';
import { UserSettingComponent } from './user-setting/user-setting.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterUserComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path:'forgot-password',
    component:ForgotPasswordComponent
   },

   {
    path:'partner',
    component:NewPartnerComponent
   },
   {
    path: 'email-confirmation',
    component: EmailConfirmationComponent
  },
  {
    path: 'email-reset',
    component: EmailResetComponent
  },
  {
    path: 'password-expiry',
    component: PasswordExpiryComponent
  },
  {
    path: 'email-approved',
    component: EmailApprovedComponent

  },


  // {
  //   path:'nav',
  //   component:NavComponent,

  // },

  // {
  //   path:'contact',
  //   component:ContactUsComponent,

  // },
  // {
  //   path:'Dashboard',
  //   component:DashboardComponent,

  // },




  // {
  //   path: '',
  //   component: SidebarComponent,
  //   data: {
  //     title: 'Home'
  //   }



   
  //   children: [
  //     {
  //       path: 'base',
  //       loadChildren: () => import('./views/base/base.module').then(m => m.BaseModule)
  //     },
  //     {
  //       path: 'buttons',
  //       loadChildren: () => import('./views/buttons/buttons.module').then(m => m.ButtonsModule)
  //     },
  //     {
  //       path: 'charts',
  //       loadChildren: () => import('./views/chartjs/chartjs.module').then(m => m.ChartJSModule)
  //     },
  //     {
  //       path: 'dashboard',
  //       loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
  //     },
  //     {
  //       path: 'icons',
  //       loadChildren: () => import('./views/icons/icons.module').then(m => m.IconsModule)
  //     },
  //     {
  //       path: 'notifications',
  //       loadChildren: () => import('./views/notifications/notifications.module').then(m => m.NotificationsModule)
  //     },
  //     {
  //       path: 'theme',
  //       loadChildren: () => import('./views/theme/theme.module').then(m => m.ThemeModule)
  //     },
  //     {
  //       path: 'widgets',
  //       loadChildren: () => import('./views/widgets/widgets.module').then(m => m.WidgetsModule)
  //     }
  //   ]
  // },


  {
    path:'',
    component: MainLayoutComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'contact',
        component: ContactUsComponent,

      },

      {
        path: 'upload',
        component: UploadComponent,

      },
      {
        path: 'home',
        component: CpChangeComponent,

      },
      {
        path: 'complaint',
        component: ComplaintComponent,

      },

      {
        path: 'download',
        component: DownloadsOperationsComponent,
      },
      {
        path: 'display-invoice',
        component: DisplayInvoiceComponent,
      },
      {
        path: 'help',
        component: HelpComponent,

      },
      {
        path: 'dashboard',
        component: DashboardWBUComponent,

      },
      {
        path: 'details',
        component: DetailsComponent
      },
      {
        path: 'portal-admin',
        component: PortalAdminComponent
      },
      {
        path: 'approve-registration',
        component: ApproveRegistrationComponent
      },
      {
        path: 'registration-action/:id',
        component: RegistrationActionComponent
      },
      {
        path: 'registration-details',
        component: RegistrationDetailsComponent
      },
      {
        path: 'contact-admin',
        component: ContactAdminComponent
      },
      {
        path: 'virtual-account',
        component: VirtualAccountComponent
      },
      {
        path: 'role-access',
        component: RoleAccessComponent
      },
      {
        path: 'update-receiver',
        component: UpdateReceiverComponent
      },
      {
        path: 'history',
        component: HistoryComponent
      },
      {
        path: 'role-view',
        component: RoleViewComponent
      },
      {
        path: 'new-role',
        component: NewRoleComponent
      },
      {
        path: 'product-catalog',
        component: ProductCatalogComponent
      },
      {
        path: 'voice',
        component: VoiceComponent
      },
      {
        path: 'capacity',
        component: CapacityComponent
      },
      {
        path: 'data',
        component: DataComponent
      },
      {
        path: 'colocation',
        component: ColocationComponent
      },
      {
        path: 'product-catalog/:name',
        component: CapacityCatalogComponent
      },
      {
        path: 'orders',
        component: OrdersComponent
      },
      {
        path: 'assets',
        component: AssetsComponent
      },
      {
        path: 'user-setting',
        component: UserSettingComponent
      },
    ]
  }
  ,
  { path: '**', component: P404Component },
 
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
