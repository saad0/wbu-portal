import { MatPaginatorModule } from '@angular/material/paginator';
import {MatInputModule} from '@angular/material/input';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { BrowserModule } from '@angular/platform-browser';
import { MatDialogModule } from '@angular/material/dialog';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { ToastrModule } from 'ngx-toastr';
import { NgxPaginationModule } from 'ngx-pagination';
import { MatStepperModule} from '@angular/material/stepper'
import {MatExpansionModule} from '@angular/material/expansion';
import { NgModule } from '@angular/core';
import { MatMenuModule} from '@angular/material/menu';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserIdleModule } from 'angular-user-idle';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import {MatChipsModule} from '@angular/material/chips'
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';

// Import containers

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';



const APP_CONTAINERS = [
  MainLayoutComponent,
  SidebarComponent,
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppSidebarModule,


} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { from } from 'rxjs';
import { ReactiveFormsModule, FormControl, FormsModule } from '@angular/forms';

import { HomeComponent, CpChangeComponent } from './home/home.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavComponent } from './nav/nav.component';
import { MainLayoutComponent } from './containers/main-layout/main-layout.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { NewPartnerComponent } from './new-partner/new-partner.component';
import { UploadComponent } from './upload/upload.component';
import { FileUploadModule } from 'ng2-file-upload';
import { ComplaintComponent } from './complaint/complaint.component';
import { DownloadsOperationsComponent } from './downloads-operations/downloads-operations.component';
import { HelpComponent } from './help/help.component';
import { MatSelectModule } from "@angular/material/select";
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { DashboardWBUComponent } from './dashboard-wbu/dashboard-wbu.component';
import { SharedModule } from './shared/models/share.module';
import { DetailsComponent } from './details/details.component';
import { DisplayInvoiceComponent } from './display-invoice/display-invoice.component';
import { ForgotPasswordComponent } from './views/forgot-password/forgot-password.component';
import { PortalAdminComponent } from './Admin-Portal/portal-admin/portal-admin.component';
import { ApproveRegistrationComponent } from './Admin-Portal/approve-registration/approve-registration.component';
import { ContactAdminComponent } from './Admin-Portal/contact-admin/contact-admin.component';
import { VirtualAccountComponent } from './Admin-Portal/virtual-account/virtual-account.component';
import { HistoryComponent } from './Admin-Portal/history/history.component';
import { UpdateReceiverComponent } from './Admin-Portal/update-receiver/update-receiver.component';
import { RoleAccessComponent } from './Admin-Portal/role-access/role-access.component';
import { RoleViewComponent } from './Admin-Portal/role-view/role-view.component';
import { NewRoleComponent } from './Admin-Portal/new-role/new-role.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import {MatIconModule} from '@angular/material/icon';
import { RegisterUserComponent } from './register-user/register-user.component';
import { ProductCatalogComponent } from './product-catalog/product-catalog.component';
import { VoiceComponent } from './product_catalog/voice/voice.component';
import { DataComponent } from './product_catalog/data/data.component';
import { CapacityComponent } from './product_catalog/capacity/capacity.component';
import { ColocationComponent } from './product_catalog/colocation/colocation.component';
import { MatCardModule } from '@angular/material/card';
import { AdminRoutingComponent } from './admin-routing/admin-routing.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RegistrationDetailsComponent } from './Admin-Portal/registration-details/registration-details.component';
import { CapacityCatalogComponent } from './product_catalog/capacity-catalog/capacity-catalog.component';
import { RegistrationActionComponent } from './Admin-Portal/registration-action/registration-action.component';
import { FlipCardFrontComponent } from './Admin-Portal/update-receiver/flip-card-front';
import { FlipCardBackComponent } from './Admin-Portal/update-receiver/flip-card-back';
import { NguCarouselModule } from '@ngu/carousel';
import { OrdersComponent } from './Admin-Portal/orders/orders.component';
import { CountdownModule } from 'ngx-countdown';
import { EmailResetComponent } from './email-reset/email-reset.component';
import { MatTableModule } from '@angular/material/table';
// import { AssetsComponent } from './Admin-Portal/assets/assets.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { PasswordExpiryComponent } from './views/password-expiry/password-expiry.component';
import { EmailApprovedComponent } from './views/email-approved/email-approved.component';
import { EmailConfirmationComponent } from './email-confirmation/email-confirmation.component';
import { AssetsComponent } from './Admin-Portal/assets/assets.component';
import { P403Component } from './views/error/403.component';
import { UserSettingComponent } from './user-setting/user-setting.component';


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  imports: [
    BrowserModule,
    UserIdleModule.forRoot({idle: 1800, timeout: 1, ping: 120}),
    NgbModule,
    MatDialogModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    MatChipsModule,
    MatMenuModule,
    MatTableModule,
    CountdownModule,
    MatInputModule,
    MatExpansionModule,
    MatIconModule,
    MatPaginatorModule,
    MatCardModule,
    MatAutocompleteModule,
    HttpClientModule,
    NguCarouselModule,
    MatProgressSpinnerModule,
    ToastrModule.forRoot(), // ToastrModule added
    MatSelectModule,
    SharedModule,
    MatStepperModule, 
    NgxWebstorageModule.forRoot({ prefix: 'app', separator: ':'}),
    BrowserAnimationsModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ReactiveFormsModule,
    FormsModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    FileUploadModule,
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot()

  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P403Component,
    P500Component,
    LoginComponent,
    FlipCardFrontComponent,
    FlipCardBackComponent,
    RegisterComponent,
    HomeComponent,
    NavComponent,
    ContactUsComponent,
    NewPartnerComponent,
    UploadComponent,
    CpChangeComponent,
    ComplaintComponent,
    DownloadsOperationsComponent,
    HelpComponent,
    DashboardWBUComponent,
    DetailsComponent,
    DisplayInvoiceComponent,
    PortalAdminComponent,
    ApproveRegistrationComponent,
    ContactAdminComponent,
    VirtualAccountComponent,
    RoleAccessComponent,
    ForgotPasswordComponent,
    HistoryComponent,
    UpdateReceiverComponent,
    RoleViewComponent,
    NewRoleComponent,
    RegisterUserComponent,
    ProductCatalogComponent,
    VoiceComponent,
    DataComponent,
    CapacityComponent,
    ColocationComponent,
    AdminRoutingComponent,
    SidebarComponent,
    RegistrationDetailsComponent,
    CapacityCatalogComponent,
    RegistrationActionComponent,
    OrdersComponent,
    EmailConfirmationComponent,
    EmailResetComponent,
    AssetsComponent,
    PasswordExpiryComponent,
    EmailApprovedComponent,
    UserSettingComponent,
  ],
  providers: [{
    provide: LocationStrategy, 
    useClass: HashLocationStrategy
  }],
  bootstrap: [AppComponent]  
})
export class AppModule { }
