import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})
export class RegisterComponent {
  registerForm: FormGroup;
  isRegister: boolean = false;

  constructor(private router: Router, private toast: ToastrService ) {
    this.registerForm = new FormGroup({
      Email: new FormControl('', [Validators.required, Validators.email, Validators.compose([Validators.required, Validators.pattern('^[^\\s@]+@[^\\s@]+\\.[^\\s@]{2,}$'), Validators.minLength(4), Validators.maxLength(25)])]),
      phone: new FormControl('', [Validators.required, Validators.minLength(100), Validators.maxLength(15)]),
      company: new FormControl('', [Validators.required, Validators.minLength(50), Validators.maxLength(3)]),
      country: new FormControl('', [Validators.required, Validators.minLength(50), Validators.maxLength(3)]),
      firstname: new FormControl('', [Validators.required, Validators.minLength(50), Validators.maxLength(3)]),
      lastname: new FormControl('', [Validators.required, Validators.minLength(50), Validators.maxLength(3)]),



    })
  }
  ngOnInit() {

  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  trueChart(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)
      && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
      return false;
    }
    return true;
  }

  trueChartEmail(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57)
      && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
      return false;
    }
    return true;
  }
  onRegister() {
    this.isRegister = true;
    
    if (this.registerForm.valid) {
      
      
      this.router.navigate(['home'])
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      Toast.fire({
        icon: 'success',
        title: 'successfully registered'
      })

     
  }
}}

    
  //   } if (this.registerForm.get('Email').invalid) {
  //     const Toast = Swal.mixin({
  //       toast: true,
  //       position: 'bottom-end',
  //       showConfirmButton: false,
  //       timer: 3000,
  //       timerProgressBar: true,
  //       onOpen: (toast) => {
  //         toast.addEventListener('mouseenter', Swal.stopTimer)
  //         toast.addEventListener('mouseleave', Swal.resumeTimer)
  //       }
  //     })

  //     Toast.fire({
  //       icon: 'error',
  //       title: 'Email Address/Password are Worng'
  //     })
  //     // window.location.reload();
  //   }
    
  //   if (this.registerForm.get('phone').invalid) {
  //     const Toast = Swal.mixin({
  //       toast: true,
  //       position: 'bottom-end',
  //       showConfirmButton: false,
  //       timer: 3000,
  //       timerProgressBar: true,
  //       onOpen: (toast) => {
  //         toast.addEventListener('mouseenter', Swal.stopTimer)
  //         toast.addEventListener('mouseleave', Swal.resumeTimer)
  //       }
  //     })

  //     Toast.fire({
  //       icon: 'error',
  //       title: 'Phone entered is Worng'
  //     })
  //     // window.location.reload();

  //   }
  //   if (this.registerForm.get('code').invalid) {
  //     const Toast = Swal.mixin({
  //       toast: true,
  //       position: 'bottom-end',
  //       showConfirmButton: false,
  //       timer: 3000,
  //       timerProgressBar: true,
  //       onOpen: (toast) => {
  //         toast.addEventListener('mouseenter', Swal.stopTimer)
  //         toast.addEventListener('mouseleave', Swal.resumeTimer)
  //       }
  //     })

  //     Toast.fire({
  //       icon: 'error',
  //       title: 'Code Entered is Wrong'
  //     })
  //     // window.location.reload();

  //   }
  // }
//   registerNav() {
//   }
// }
