import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailApprovedComponent } from './email-approved.component';

describe('EmailApprovedComponent', () => {
  let component: EmailApprovedComponent;
  let fixture: ComponentFixture<EmailApprovedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailApprovedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
