import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LanguageHelper } from './../../language/language.helper';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { loginServices } from '../../services/login.service';
import { loginDto } from '../../dataModels/login.model';


@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
  login: FormGroup;
  isSubmited: boolean = false;
  isRTL: any;
  english: any;
  arabic: any;
  username: string;
  password: string;
  today = new Date();
  lastLogin = '';
  loginDto = new loginDto();

  constructor(private router: Router, private languageHelper: LanguageHelper,
    private toast: ToastrService, private translateService: TranslateService, private loginServices: loginServices) {
    this.getAllLangs();
    this.isRTL = this.languageHelper.currentLang().isRTL;

    this.login = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'), Validators.minLength(4), Validators.email, Validators.maxLength(25)]),
      password: new FormControl('', Validators.required)

    })
  }
  ngOnInit() {

    this.translateService.setDefaultLang('en');
    this.translateService.use(this.languageHelper.currentLang().name);
    this.isRTL = this.languageHelper.currentLang().isRTL;
  }
  changeLang(lang) {
    this.languageHelper.changeLang(lang);
    window.location.reload();
  }

  trueChartEmail(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57)
      && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
      return false;
    }
    return true;
  }

  getAllLangs() {
    let arr = this.languageHelper.getAllLanguages();
    this.english = arr[0];
    this.arabic = arr[1];
    return this.languageHelper.getAllLanguages();
  }
  onSubmit() {
    this.isSubmited = true;
    if (this.login.valid) {
      this.loginDto.email = this.login.value.email
      this.loginDto.password = this.login.value.password
      this.loginServices.postlogin(this.loginDto).subscribe(res => {
        localStorage.setItem("token",res.sessionId)
        localStorage.setItem("usename",res.username)
        localStorage.setItem("lastLogin",res.lastLogin)
        this.router.navigate(['/home'])

        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })

        Toast.fire({
          icon: 'success',
          title: 'Signed in successfully'
        })
        console.log(res)
      })
    } else {
      const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      Toast.fire({
        icon: 'error',
        title: 'Email Address/Password are wrong'
      })

    }
  }
  register() {
    this.router.navigate(['register']);

  }
  forget() {
    this.router.navigate(['forgot-password']);
  }
}