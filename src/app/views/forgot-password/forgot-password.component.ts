import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { forgotPassServices } from '../../services/forgotpass.services'

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {
  forgetForm: FormGroup;
  isRegister: boolean = false;


  constructor(private router: Router, private toast: ToastrService, private forgotPassServices: forgotPassServices) {
    this.forgetForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.compose([Validators.required, Validators.email, Validators.pattern('^[^\\s@]+@[^\\s@]+\\.[^\\s@]{2,}$'), Validators.minLength(4), Validators.maxLength(25)])]),
    })
  }
  ngOnInit(): void {

  }

  trueChartEmail(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57)
      && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
      return false;
    }
    return true;
  }

  forgot() {
    this.isRegister = true;
    if (this.forgetForm.valid) {
      this.forgotPassServices.postpass(this.forgetForm.value.email).subscribe(res => {

        this.router.navigate(['email-reset'])
        console.log(this.forgetForm)
        console.log(res)

      })
    } if (this.forgetForm.get('email').invalid) {
      const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      Toast.fire({
        icon: 'error',
        title: 'Email Address Entered is wrong'
      })
    }
  }
}
