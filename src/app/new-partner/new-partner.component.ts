import { RegionsEnum } from './../shared/models/regionsEnum';
import { SharedService } from './../shared/services/shared.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LanguageHelper } from '../language/language.helper';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { partnerDto } from '../dataModels/newpartner.model';
import { partnerServices } from '../services/newpartner.services';

@Component({
  selector: 'app-new-partner',
  templateUrl: './new-partner.component.html',
  styleUrls: ['./new-partner.component.css']
})
export class NewPartnerComponent implements OnInit {
  isCountryOpen: boolean = false;
  partnerForm: FormGroup;
  isRegister: boolean = false;
  listCountries: any[] = [];
  selectedCountry = { name: "Select Country", flag: null };
  isRegionOpen: boolean = false;

  listRegion: regionsDto[] = [
    { display: "EU (European Union)", value: RegionsEnum.EU },

    { display: "EFTA (European Free Trade Association)", value: RegionsEnum.EFTA },
    { display: "CARICOM (Caribbean Community)", value: RegionsEnum.CARICOM },
    { display: "USAN (Union of South American Nations)", value: RegionsEnum.USAN },
    { display: "EEU (Eurasian Economic Union)", value: RegionsEnum.EEU },
    { display: "AL (Arab League)", value: RegionsEnum.AL },
    { display: "ASEAN (Association of Southeast Asian Nations)", value: RegionsEnum.ASEAN },

    { display: "CAIS (Central American Integration System)", value: RegionsEnum.CAIS },

    { display: "CEFTA (Central European Free Trade Agreement)", value: RegionsEnum.CEFTA },

    { display: "NAFTA (North American Free Trade Agreement)", value: RegionsEnum.NAFTA },

    { display: "SAARC (South Asian Association for Regional Cooperation)", value: RegionsEnum.SAARC },
  ];
  isRTL: any;
  english: any;
  arabic: any;
  partnerDto = new partnerDto();

  constructor(public shared: SharedService, private languageHelper: LanguageHelper,
    private translateService: TranslateService, private router: Router, private partnerServices: partnerServices) {
    this.getAllLangs();
    this.isRTL = this.languageHelper.currentLang().isRTL;
    this.partnerForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email, Validators.compose([Validators.required, Validators.pattern('^[^\\s@]+@[^\\s@]+\\.[^\\s@]{2,}$'), Validators.minLength(4), Validators.maxLength(25)])]),
      fName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
      lName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
      company: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]),
      phone: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
    })
  }

  ngOnInit() {
    this.shared.getAllCountries().subscribe((res: any[]) => this.listCountries = [...res]);

  }

  selectCountry(country) {
    console.log(country);
    this.isCountryOpen = false;
    this.selectedCountry = { ...country }
    this.partnerForm.get("phone").setValue("+" + country.callingCodes[0])
  }

  trueChart(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)
      && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
      return false;
    }
    return true;
  }

  trueChartEmail(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57)
      && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
      return false;
    }
    return true;
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  selectRegion() {
    let region = this.partnerForm.get('region').value;
    console.log(RegionsEnum[region]);

    this.shared.getCountriesWithRegion(RegionsEnum[region]).subscribe((res: any[]) => this.listCountries = [...res]);
  }

  newPartner() {
    this.isRegister = true;
    if (this.partnerForm.valid) {
      this.router.navigate(['home'])
      this.partnerDto.company = this.partnerForm.value.company
      this.partnerDto.phone = this.partnerForm.value.phone
      this.partnerDto.email = this.partnerForm.value.email
      this.partnerDto.name = this.partnerForm.value.fName
      this.partnerDto.name = this.partnerForm.value.lName
      this.partnerServices.postpartner(this.partnerDto).subscribe(res => {


        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })

        Toast.fire({
          icon: 'success',
          title: 'successfully registered'
        })
        console.log(res)
      })
    }
    if (this.partnerForm.get('email').invalid) {
      const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      Toast.fire({
        icon: 'error',
        title: 'Email Address is Worng'
      })
    }
    if (this.partnerForm.get('phone').invalid) {
      const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      Toast.fire({
        icon: 'error',
        title: 'Phone entered is Worng'
      })
    }
    if (this.partnerForm.get('fName').invalid) {
      const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      Toast.fire({
        icon: 'error',
        title: 'First Name Entered is Wrong'
      })
    }
    if (this.partnerForm.get('lName').invalid) {
      const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      Toast.fire({
        icon: 'error',
        title: 'Last Name Entered is Wrong'
      })
    }

    if (this.partnerForm.get('company').invalid) {
      const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      Toast.fire({
        icon: 'error',
        title: 'Company Name Entered is Wrong'
      })
    }
  }
  changeLang(lang) {
    this.languageHelper.changeLang(lang);
    window.location.reload();
  }

  getAllLangs() {
    let arr = this.languageHelper.getAllLanguages();
    this.english = arr[0];
    this.arabic = arr[1];
    return this.languageHelper.getAllLanguages();
  }
}
export interface regionsDto {
  display: string;
  value: RegionsEnum;
}