import { Component, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import {NgbDate, NgbCalendar, NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';



@Component({
  selector: 'app-downloads-operations',
  templateUrl: './downloads-operations.component.html',
  styleUrls: ['./downloads-operations.component.css']
})
export class DownloadsOperationsComponent implements OnInit {
  uploader:FileUploader;
  hasBaseDropZoneOver:boolean;
  hasAnotherDropZoneOver:boolean;
  response:string;
  hoveredDate: NgbDate | null = null;
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  CDR: boolean = true;
  CDRTech: boolean = true;
  CDRsearch: boolean = false;

  constructor(private calendar: NgbCalendar, public formatter: NgbDateParserFormatter, private router: Router) { 

    this.fromDate = calendar.getToday();
    this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
      
  }
  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }
  ngOnInit(): void {
  }
  
  displyTech(){
    this.CDRTech = false;
    this.CDRsearch = true;
  }
  displybilling(){
    this.CDR = false;
    this.CDRsearch = true;
  }
  cancel(){
    window.location.reload();
    this.router.navigateByUrl("/download")
  }
}


 


