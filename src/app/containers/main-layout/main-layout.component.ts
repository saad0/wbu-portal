import { FormGroup, FormControl } from '@angular/forms';
import { LanguageHelper } from './../../language/language.helper';
import { TranslateService } from '@ngx-translate/core';
import { AfterViewChecked, Component, ElementRef, OnInit, Renderer2, ViewChild, OnDestroy } from '@angular/core';
import { CollapseDirective } from 'ngx-bootstrap/collapse';
import { Router } from '@angular/router';
import { UserIdleService } from 'angular-user-idle';
import { Subscription } from 'rxjs';
import {MatMenuTrigger} from '@angular/material/menu'
import Swal from 'sweetalert2';
import { LocalStorage } from 'ngx-webstorage';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.css']
})
export class MainLayoutComponent implements OnInit, AfterViewChecked, OnDestroy {
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;

  searchForm: FormGroup
  private _isCollapsed: boolean = true;
  english: any;
  arabic: any;
  fullyear: number = new Date().getFullYear();

  private _username:string;
  get username(){
    return this._username;
  }
  set username(value){
    this._username=value;
  }
  
  private _lastLogin:string;
  get lastLogin(){
    return this._lastLogin;
  }
  set lastLogin(value){
    this._lastLogin=value;
  }
  set isCollapsed(value) {
    this._isCollapsed = value;

  }
  get isCollapsed() {
    if (this.collapseRef) {
      // temp fix for "overflow: hidden"
      if (getComputedStyle(this.collapseRef.nativeElement).getPropertyValue('display') === 'flex') {
        this.renderer.removeStyle(this.collapseRef.nativeElement, 'overflow');
      }
    }
    return this._isCollapsed;
  }

  @ViewChild(CollapseDirective, { read: ElementRef, static: false }) collapse !: CollapseDirective;

  collapseRef;
  isRTL: any;

  constructor(
    private renderer: Renderer2, private languageHelper: LanguageHelper,
    private translateService: TranslateService, private router: Router, private userIdle: UserIdleService
  ) {
    this.getAllLangs();

    this.isRTL = this.languageHelper.currentLang().isRTL;
  }
  timeOut$: Subscription;
  timeStart$: Subscription;
  timer;

  openMyMenu() {
    this.trigger.toggleMenu();
  }
  ngOnInit() {
    this.username=localStorage.getItem("username");
    this.lastLogin=localStorage.getItem("lastLogin");
    this.searchForm = new FormGroup({
      searchValue: new FormControl('')

    })
    this.translateService.setDefaultLang('en');
    this.translateService.use(this.languageHelper.currentLang().name);
    this.isRTL = this.languageHelper.currentLang().isRTL;
    this.userIdle.startWatching();
    this.timeStart$ = this.userIdle.onTimerStart().subscribe();
    this.timeOut$ = this.userIdle.onTimeout().subscribe(() => {
      var timer = 10,
        isTimerStarted = false;
      (function customSwal() {
        // swal({
        //   title: "Time Out!",
        //   text: "Sesstion will expire in " + timer + " seconds",
        //   buttons: {
        //     cancel: true,
        //     confirm: true,
        //   },
        //   timer: !isTimerStarted ? timer * 1000 : 1100,
        // });]
        
        isTimerStarted = true;
        if (timer) {
          if (timer == 8) {
            window.location.reload();
            this.router.navigateByUrl('\home')
          }
          timer--;
          setTimeout(customSwal, 1000);
        }
      })();

    });
  }
  get localStorage(){
    return localStorage;
  }
  changeLang(lang) {
    debugger;
    this.languageHelper.changeLang(lang);
    window.location.reload();
  }

  getAllLangs() {
    let arr = this.languageHelper.getAllLanguages();
    this.english = arr[0];
    this.arabic = arr[1];
    return this.languageHelper.getAllLanguages();
  }
  ngAfterViewChecked(): void {
    this.collapseRef = this.collapse;
  }

  portal() {
    this.router.navigate(['portal-admin']);
  }
  ngOnDestroy() {
    this.timeOut$.unsubscribe();
    this.timeStart$.unsubscribe();
  }
}