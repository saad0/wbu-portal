import { Component, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent {
  filterValues = {};
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  dataSource = new MatTableDataSource();
  ELEMENT_DATA;
  displayedColumns: string[] = ['complaint', 'type', 'assigned', 'customer', 'status', 'creation', 'close', 'mttr'];
  filterSelectObj = [];

  constructor() {

    this.filterSelectObj = [
      {
        type: 'Complaint',
        columnProp: 'complaint',
        options: []
      }, {
        type: 'Type',
        columnProp: 'type',
        options: []
      }, {
        type: 'Assigned',
        columnProp: 'assigned',
        options: []
      }, {
        type: 'Customer',
        columnProp: 'customer',
        options: []
      }, {
        type: 'Status',
        columnProp: 'status',
        options: []
      }, {
        type: 'Creation',
        columnProp: 'creation',
        options: []
      }, {
        type: 'Close',
        columnProp: 'close',
        options: []
      }, {
        type: 'MTTR',
        columnProp: 'mttr',
        options: []
      }
    ]
  }

  ngOnInit() {
    this.getRemoteData();
    this.dataSource.filterPredicate = this.createFilter();
    // this.onTableUpdate()
  }

  getFilterObject(fullObj, key) {
    const uniqChk = [];
    fullObj.filter((obj) => {
      if (!uniqChk.includes(obj[key])) {
        uniqChk.push(obj[key]);
      }
      return obj;
    });
    return uniqChk;
  }
  onTableUpdate() {
    this.dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
    this.dataSource.paginator = this.paginator;

  }
  getRemoteData() {
    const remoteDummyData = [
      {
        "complaint": "SR-124",
        "type": "Technical",
        "assigned": "WSMC",
        "customer": "Zain",
        "status": "In progress",
        "creation": "01-07-2020",
        "close": "11-07-2020",
        "mttr": "under proceing"
      },
      {
        "complaint": "SR-125",
        "type": "Commercial",
        "assigned": "WSMC",
        "customer": "Mobily",
        "status": "Closed",
        "creation": "02-07-2020",
        "close": "05-08-2020",
        "mttr": "Closed"
      }, {
        "complaint": "SR-127",
        "type": "Technical",
        "assigned": "WSMC",
        "customer": "Jawwy",
        "status": "In progress",
        "creation": "12-07-2020",
        "close": "02-08-2020",
        "mttr": "under proceing"
      }, {
        "complaint": "SR-129",
        "type": "Commercial",
        "assigned": "WSMC",
        "customer": "Zain",
        "status": "In progress",
        "creation": "22-07-2020",
        "close": "21-08-2020",
        "mttr": "Closed"
      }, {
        "complaint": "SR-130",
        "type": "Billing",
        "assigned": "WSMC",
        "customer": "Jawwy",
        "status": "Pending",
        "creation": "04-08-2020",
        "close": "28-08-2020",
        "mttr": "Pending"
      }, {
        "complaint": "SR-131",
        "type": "Technical",
        "assigned": "WSMC",
        "customer": "Zain",
        "status": "In progress",
        "creation": "25-07-2020",
        "close": "06-08-2020",
        "mttr": "Pending"
      }, {
        "complaint": "SR-132",
        "type": "Technical",
        "assigned": "WSMC",
        "customer": "Mobily",
        "status": "In progress",
        "creation": "07-08-2020",
        "close": "23-08-2020",
        "mttr": "under proceing"
      }, {
        "complaint": "SR-133",
        "type": "Billing",
        "assigned": "WSMC",
        "customer": "Zain",
        "status": "In progress",
        "creation": "22-07-2020",
        "close": "14-08-2020",
        "mttr": "under proceing"
      },
    ];
    this.ELEMENT_DATA = remoteDummyData
    this.dataSource.data = remoteDummyData;
    this.filterSelectObj.filter((o) => {
      o.options = this.getFilterObject(remoteDummyData, o.columnProp);
    });
  }

  filterChange(filter, event) {
    this.filterValues[filter.columnProp] = event.target.value.trim().toLowerCase()
    this.dataSource.filter = JSON.stringify(this.filterValues)
  }

  createFilter() {
    let filterFunction = function (data: any, filter: string): boolean {
      let searchTerms = JSON.parse(filter);
      let isFilterSet = false;
      for (const col in searchTerms) {
        if (searchTerms[col].toString() !== '') {
          isFilterSet = true;
        } else {
          delete searchTerms[col];
        }
      }

      let nameSearch = () => {
        let found = false;
        if (isFilterSet) {
          for (const col in searchTerms) {
            searchTerms[col].trim().toLowerCase().split(' ').forEach(word => {
              if (data[col].toString().toLowerCase().indexOf(word) != -1 && isFilterSet) {
                found = true
              }
            });
          }
          return found
        } else {
          return true;
        }
      }
      return nameSearch()
    }
    return filterFunction
  }

  resetFilters() {
    this.filterValues = {}
    this.filterSelectObj.forEach((value, key) => {
      value.modelValue = undefined;
    })
    this.dataSource.filter = "";
  }
}