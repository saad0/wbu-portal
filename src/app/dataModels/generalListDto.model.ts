export class GeneralListDto{
    pageSize:number;
    count:number;
    totalPages:number;
}