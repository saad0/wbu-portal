import { RegisteredMembersDto } from './registeredMembersDto.model';
import { GeneralListDto } from './generalListDto.model';

export class RegisterationListDto extends GeneralListDto{
   
    registrationReq:RegisteredMembersDto[];
    // log:archiveDto[];

}