import { voiceDto } from './voiceDto.model';
import { archiveDto } from './archive.model';
import { orderDto } from './orderDto.model';
import { targetDtto } from './target.model';
import {productDto} from './productDto.model'
import { assetsDto } from './assetsDto.model';
export class archiveListDto{
    pageSize:number;
    count:number;
    totalPages:number;
    log:archiveDto[];
    orders:orderDto[];
    asstes:assetsDto[];
    voice:voiceDto[];
    targetUsers:targetDtto[];
    products:productDto[];
    
}