export class RegisteredMembersDto{
    id:number;
    firstName:string;
    lastName:string;
    company:string;
    email:string;
    phone:string;
    country:string;
    status:string;
    date:string;
    remark:string;
}