export class assetsDto{
  added_value_charge:number;
  civil_work_charge: number;
  flat_charge: number;
  govt_fee: number;
  id: number;
  interconnect_charge: number;
  mrc: number
  ni_number: string;
  nrc: number
  operation_maintainence_charge: number;
  operatorCode: string;
  orderNum: string;
  parameter: string;
  port_provider_charge: number;
  restoration_charge: number;
  segmentSCharge: number;
  segmentStCharge: number;
  segmentTCcharge: number;
  service: string;
  service_id: string;
  status: string;
  system_designation: string;
  tv_charge: number;
  value: string;
}