export class orderDto{
    addedValueCharge:number;
      billingDate: string;
      civilWorkCharge: number;
      flatCharge: number;
      govtFee: number;
      id: number;
      interconnectCharge: number;
      mrc: number
      niNumber: string;
      nrc: number
      operationMaintainenceCharge: number;
      operatorCode: string;
      orderNum: string;
      parameter: string;
      portProviderCharge: number;
      restorationCharge: number;
      segmentSCharge: number;
      segmentStCharge: number;
      segmentTCcharge: number;
      service: string;
      serviceId: string;
      status: string;
      systemDesignation: string;
      tvCharge: number;
      value: string;
}