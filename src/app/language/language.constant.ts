export const LANGUAGES: any[] = [
    { name:'en',title:'English',isRTL:false ,icon:'assets/img/flags/US.png'},
    { name:'ar',title:'عربي',isRTL:true ,icon:'assets/img/flags/SA.png'}
];
