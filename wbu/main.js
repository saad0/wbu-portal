(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/approve-registration/approve-registration.component.html":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/approve-registration/approve-registration.component.html ***!
  \*****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header bg-gradient-danger pb-8 pt-5 \">\n    <div class=\"container-fluid\">\n        <div class=\"header-body\">\n            <!-- Table -->\n            <div class=\"card shadow\">\n                <div class=\"card-header border-0\">\n                    <div class=\"row align-items-center\">\n                        <div class=\"col\">\n\n                            <h3 class=\"mb-0\">Approve Registration </h3>\n                        </div>\n                        <div class=\"col text-right\">\n                            <a href=\"#!\" class=\"btn btn-sm btn-primary\">See all</a>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"table-responsive\">\n                    <!-- Projects table -->\n                    <table class=\"table align-items-center table-flush\">\n                        <thead class=\"thead-light\">\n                            <tr>\n                                <th scope=\"col\">#</th>\n                                <th scope=\"col\">Email</th>\n                                <th scope=\"col\">Company</th>\n                                <th scope=\"col\">Registration Date </th>\n                                <th scope=\"col\"> Remark</th>\n\n                            </tr>\n                        </thead>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>\n                                <div class=\"custom-control custom-control-alternative custom-checkbox\">\n                                    <input class=\"custom-control-input\" id=\" customCheckLogin\" type=\"checkbox\" maxlength=\"50\">\n                                    <label class=\"custom-control-label\" for=\" customCheckLogin\" >\n                                    </label>\n                                </div>\n                            </td>\n                            <td>ABD@company.com</td>\n                            <td>Company A</td>\n                            <td>03-02-2020</td>\n                            <td> <button type=\"button\" class=\"btn btn-primary\"\n                                    (click)=\"onSubmitZ(true)\">Approve</button>\n                                <button type=\"button\" class=\"btn btn-danger btn-danger\"\n                                    (click)=\"onSubmitZ(false)\">Reject</button> </td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>\n                                <div class=\"custom-control custom-control-alternative custom-checkbox\">\n                                    <input class=\"custom-control-input\" id=\" customCheckLogin\" type=\"checkbox\" maxlength=\"50\">\n                                    <label class=\"custom-control-label\" for=\" customCheckLogin\">\n                                    </label>\n                                </div>\n                            </td>\n                            <td>ABD@company.com</td>\n                            <td>Company A</td>\n                            <td>03-02-2020</td>\n                            <td> <button type=\"button\" class=\"btn btn-primary btn-round\"\n                                    (click)=\"onSubmitZ(true)\">Approve</button>\n                                <button type=\"button\"  class=\"btn btn-danger btn-danger\"\n                                    (click)=\"onSubmitZ(false)\">Reject</button>\n                            </td>\n                        </tbody>\n                    </table>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/contact-admin/contact-admin.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/contact-admin/contact-admin.component.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header bg-gradient-danger pb-8 pt-5 \">\n    <div class=\"container-fluid\">\n        <div class=\"header-body\">\n            <!-- Table -->\n            <div class=\"card shadow\">\n                <div class=\"card-header border-0\">\n                    <div class=\"row \">\n                        <div class=\"col\">\n                            <h3 class=\"mb-0\">Contact Admin </h3>\n                            <div id=\"outer\">\n                                <div class=\"inner\">\n                                    <div class=\"input-group mb-4\">\n                                        <div class=\"input-group-prepend\">\n                                            <span class=\"input-group-text\"><i class=\"fa fa-search\"\n                                                    aria-hidden=\"true\"></i></span>\n                                        </div>\n                                        <input type=\"text\" maxlength=\"50\" style=\"width: 19rem;\" autocomplete=\"off\" class=\"form-control\"\n                                            placeholder=\"Search by Email\" required>\n                                    </div>\n                                </div>\n                                <div class=\"inner\">\n                                    <div class=\"input-group mb-4\">\n                                        <div class=\"input-group-prepend\">\n                                            <span class=\"input-group-text\"><i class=\"fa fa-search\"\n                                                    aria-hidden=\"true\"></i></span>\n                                        </div>\n                                        <input type=\"text\" style=\"width: 9rem;\" maxlength=\"50\" autocomplete=\"off\" class=\"form-control\"\n                                            placeholder=\"Search by Company\" required>\n                                    </div>\n                                </div>\n                                <h4 class=\"mb-0 label-text-center\">Area To Reflect Contact Attributes From NAWAQL</h4>\n\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"table-responsive\">\n\n                        <!-- Projects table -->\n                        <table class=\"table align-items-center table-flush\">\n                            <thead class=\"thead-light\">\n                                <tr>\n                                    <!-- <th scope=\"col\">#</th> -->\n                                    <th scope=\"col\">Email</th>\n                                    <th scope=\"col\">Company</th>\n                                    <th scope=\"col\">Portal Flag </th>\n                                    <th scope=\"col\"> Portal Role</th>\n\n                                </tr>\n                            </thead>\n                            <tbody style=\"font-weight: 600; color: #525f7f;\">\n                                <td>ABD@company.com</td>\n                                <td>Company A</td>\n                                <td>Active</td>\n                                <td>\n                                    <select class=\"form-control\">\n                                        <option disabled selected value></option>\n                                        <option>General Billing</option>\n                                        <option>General Commercial</option>\n                                        <option>Technical</option>\n                                        <option>Other</option>\n                                    </select></td>\n                            </tbody>\n                            <tbody style=\"font-weight: 600; color: #525f7f;\">\n                                <td>ABD@company.com</td>\n                                <td>Company A</td>\n                                <td>Inactive</td>\n                                <td>\n                                    <select class=\"form-control\">\n                                        <option disabled selected value></option>\n                                        <option>General Billing</option>\n                                        <option>General Commercial</option>\n                                        <option>Technical</option>\n                                        <option>Other</option>\n                                    </select></td>\n                            </tbody>\n                        </table>\n                        <div class=\"row\">\n                            <div class=\"col\">\n                              <div class=\"text-center\">\n                                <button class=\"btn submit my-4\" (click)=\"disply()\" type=\"button\">Update Portal Role</button>\n                              </div>\n                            </div>\n                          </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/history/history.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/history/history.component.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header bg-gradient-danger pb-8 pt-5 \">\n    <div class=\"container-fluid\">\n        <div class=\"header-body\">\n            <!-- Table -->\n            <div class=\"card shadow\">\n                <div class=\"card-header border-0\">\n                    <div class=\"row align-items-center\">\n                        <div class=\"col\">\n\n                            <h3 class=\"mb-0\">History </h3>\n                        </div>\n                        <div class=\"col text-right\">\n                            <a href=\"#!\" class=\"btn btn-sm btn-primary\">See all</a>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"table-responsive table-res\">\n                    <!-- Projects table -->\n                    <table class=\"table align-items-center table-flush\">\n                        <thead class=\"thead-light\">\n                            <tr>\n                                <th scope=\"col\">User Email</th>\n                                <th scope=\"col\">Action</th>\n                                <th scope=\"col\">Date</th>\n\n                            </tr>\n                        </thead>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Add string2 service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Delete string service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Add string2 service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Delete string service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Add string2 service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Delete string service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Add string2 service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Delete string service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Add string2 service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Delete string service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Add string2 service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Delete string service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Add string2 service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Delete string service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Add string2 service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Delete string service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Add string2 service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Delete string service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Add string2 service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Delete string service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Add string2 service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Delete string service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Add string2 service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Delete string service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Add string2 service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>a@stc.com.sa</td>\n                            <td>Delete string service to role string</td>\n                            <td>2020-06-18</td>\n                        </tbody>\n                    </table>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/new-role/new-role.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/new-role/new-role.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"bg-gradient-danger header pb-5 \">\n    <div class=\"container-fluid\">\n\n    </div>\n</div>\n<div class=\"bg-gradient-danger header pt-5 pt-md-8\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\">\n    <div class=\"container mt--8 pb-5\">\n        <div class=\"row justify-content-center\">\n            <div class=\"col-lg-5 col-md-7 thumbnail\">\n                <div class=\"card bg-secondary shadow border-0\">\n                    <img src=\"/assets/stcs.png\" height=\"300\">\n                    <div class=\"caption\">\n                    </div>\n                    <div class=\"card-body px-lg-5\">\n                        <form [formGroup]=\"newRole\" (ngSubmit)=\"save()\" autocomplete=\"off\">\n                            <div class=\"form-group mb-3\">\n                                <div class=\"input-group input-group-alternative\">\n                                    <mat-form-field appearance=\"legacy\">\n                                        <input matInput placeholder=\"New Role Title\" formControlName=\"title\">\n                                    </mat-form-field>\n                                </div>\n                            </div>\n                            <div class=\"form-group mb-3\">\n                                <div class=\"input-group input-group-alternative\">\n                                    <mat-form-field class=\"example-chip-list\">\n                                        <mat-chip-list #chipList aria-label=\"role selection\">\n                                            <mat-chip *ngFor=\"let role of roles\" [selectable]=\"selectable\"\n                                                [removable]=\"removable\" (removed)=\"remove(role)\">\n                                                {{role}}\n                                                <mat-icon matChipRemove *ngIf=\"removable\">cancel</mat-icon>\n                                            </mat-chip>\n                                            <input placeholder=\"New Privileges\" #roleInput [matAutocomplete]=\"auto\"\n                                                [matChipInputFor]=\"chipList\" formControlName=\"privilege\"\n                                                [matChipInputSeparatorKeyCodes]=\"separatorKeysCodes\"\n                                                (matChipInputTokenEnd)=\"add($event)\">\n                                        </mat-chip-list>\n                                        <mat-autocomplete #auto=\"matAutocomplete\" (optionSelected)=\"selected($event)\">\n                                            <mat-option *ngFor=\"let role of filteredRoles | async\" [value]=\"role\">\n                                                {{role}}\n                                            </mat-option>\n                                        </mat-autocomplete>\n                                    </mat-form-field>\n                                </div>\n                            </div>\n\n                            <div class=\"text-center\">\n                                <button type=\"submit\" id=\"myBtn\" class=\"btn btn-primary\"\n                                    style=\"background-color:#ff375d;\">Save</button>\n\n                                <button type=\"button\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"><a\n                                        style=\"color: white;\" [routerLink]=\"['/home']\">Cancel</a></button>\n                            </div>\n                        </form>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/portal-admin/portal-admin.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/portal-admin/portal-admin.component.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-8\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\">\n    <div class=\"container-fluid\">\n        <div class=\"header-body\">\n            <!-- Card stats -->\n\n            <div class=\"row\">\n                <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\n                    <div class=\"card card-stats mb-4 mb-xl-0\"> <a class=\"color-p\"\n                            [routerLink]=\"['/approve-registration']\">\n                            <div class=\"card-body\">\n                                <div class=\"row\">\n                                    <div class=\"col\">\n                                        <span class=\"h2 font-weight-bold mb-0\" style=\"font-size: 1.11rem;\">Approve\n                                            Registration</span>\n\n                                    </div>\n                                    <div class=\"col-auto\">\n                                        <img src=\"./assets/user.png\" height=\"50px\" width=\"50px\">\n                                    </div>\n                                </div>\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\n                                </p>\n                            </div>\n                        </a>\n                    </div>\n                </div>\n                <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\n                    <div class=\"card card-stats mb-4 mb-xl-0\">\n                        <a class=\"color-p\" [routerLink]=\"['/contact-admin']\">\n                            <div class=\"card-body\">\n                                <div class=\"row\">\n                                    <div class=\"col\">\n                                        <span class=\"h2 font-weight-bold mb-0\">Contact Admin</span>\n                                    </div>\n                                    <div class=\"col-auto\">\n                                        <img src=\"./assets/contact.png\" height=\"50px\" width=\"50px\">\n                                    </div>\n                                </div>\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\n                                </p>\n                            </div>\n                        </a>\n                    </div>\n                </div>\n                <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\n                    <div class=\"card card-stats mb-4 mb-xl-0\">\n                        <a class=\"color-p\" [routerLink]=\"['/virtual-account']\">\n                            <div class=\"card-body\">\n                                <div class=\"row\">\n                                    <div class=\"col\">\n                                        <span class=\"h2 font-weight-bold mb-0\">Virtual Contacts</span>\n                                    </div>\n                                    <div class=\"col-auto\">\n                                        <img src=\"./assets/virtual.png\" height=\"50px\" width=\"50px\">\n                                    </div>\n                                </div>\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\n                                </p>\n                            </div>\n                        </a>\n                    </div>\n                </div>\n                <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\n                    <div class=\"card card-stats mb-4 mb-xl-0\">\n                        <a class=\"color-p\" [routerLink]=\"['/role-access']\">\n                            <div class=\"card-body\">\n                                <div class=\"row\">\n                                    <div class=\"col\">\n                                        <span class=\"h2 font-weight-bold mb-0\">Role & Access</span>\n                                    </div>\n                                    <div class=\"col-auto\">\n                                        <img src=\"./assets/access.png\" height=\"50px\" width=\"50px\">\n                                    </div>\n                                </div>\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\n                                </p>\n                            </div>\n                        </a>\n                    </div>\n                </div>\n                <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\n                    <div class=\"card card-stats mb-4 mb-xl-0\">\n                        <a class=\"color-p\" [routerLink]=\"['/history']\">\n                            <div class=\"card-body\">\n                                <div class=\"row\">\n                                    <div class=\"col\">\n                                        <span class=\"h2 font-weight-bold mb-0\" style=\"font-size: 1.11rem;\">Archive</span>\n                                    </div>\n                                    <div class=\"col-auto\">\n                                        <img src=\"./assets/archive.png\" height=\"50px\" width=\"50px\">\n                                    </div>\n                                </div>\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\n                                </p>\n                            </div>\n                        </a>\n                    </div>\n                </div>\n                <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\n                    <div class=\"card card-stats mb-4 mb-xl-0\">\n                        <a class=\"color-p\" [routerLink]=\"['/update-receiver']\">\n                            <div class=\"card-body\">\n                                <div class=\"row\">\n                                    <div class=\"col\">\n                                        <span class=\"h2 font-weight-bold mb-0\" style=\"font-size: 1.11rem;\">Update Recevier</span>\n                                    </div>\n                                    <div class=\"col-auto\">\n                                        <img src=\"./assets/emails.png\" height=\"50px\" width=\"50px\">\n                                    </div>\n                                </div>\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\n                                </p>\n                            </div>\n                        </a>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"container-fluid mt--7\">\n    <div class=\"row mt-5\">\n        <div class=\"col-xl-8 mb-5 mb-xl-0\">\n        </div>\n        <div class=\"col-xl-4\">\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/role-access/role-access.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/role-access/role-access.component.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header bg-gradient-danger pb-8 pt-5 \">\n    <div class=\"container-fluid\">\n        <div class=\"header-body\">\n            <!-- Table -->\n            <div class=\"card shadow\">\n                <div class=\"card-header border-0\">\n                    <div class=\"row align-items-center\">\n                        <div class=\"col\">\n                            <h3 class=\"mb-0\">Role & Access Control </h3>\n                        </div>\n                        <div class=\"row\">\n                            <div class=\"col\">\n                                <div class=\"text-center\">\n                                    <button class=\"btn submit my-4\" (click)=\"create()\" type=\"button\">New Role</button>\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"col text-right\">\n                            <a href=\"#!\" class=\"btn btn-sm btn-primary\">See all</a>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"table-responsive\">\n                    <!-- Projects table -->\n                    <table class=\"table align-items-center table-flush\">\n                        <thead class=\"thead-light\">\n                            <tr>\n                                <th scope=\"col\">#</th>\n                                <th scope=\"col\">Role Title</th>\n                                <th scope=\"col\"> Remark</th>\n                            </tr>\n                        </thead>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>1</td>\n                            <td>admin</td>\n                            <td> <button type=\"button\" class=\"btn btn-danger btn-danger\" (click)=\"more()\">More\n                                    Info</button> </td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>2</td>\n                            <td>Commercial</td>\n                            <td> <button type=\"button\" class=\"btn btn-danger btn-danger\" (click)=\"more()\">More\n                                    Info</button> </td>\n                        </tbody>\n                    </table>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/role-view/role-view.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/role-view/role-view.component.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header bg-gradient-danger pb-8 pt-5 \">\n    <div class=\"container-fluid\">\n        <div class=\"header-body\">\n            <!-- Table -->\n            <div class=\"card shadow\">\n                <div class=\"card-header border-0\">\n                    <div class=\"row align-items-center\">\n                        <div class=\"col\">\n                            <h3 class=\"mb-0\">View Role & Access Control </h3>\n                        </div>\n                      \n                        <div class=\"col text-right\">\n                            <a href=\"#!\" class=\"btn btn-sm btn-primary\">See all</a>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"table-responsive\">\n                    <!-- Projects table -->\n                    <table class=\"table align-items-center table-flush\">\n                        <thead class=\"thead-light\">\n                            <tr>\n                                <th scope=\"col\">Role Title</th>\n                                <th scope=\"col\">Creator Email</th>\n                                <th scope=\"col\">Created Date </th>\n                                <th scope=\"col\">Last Modification Date </th>\n                                <th scope=\"col\">Privilege</th>\n                            </tr>\n                        </thead>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>admin</td>\n                            <td>a@stc.com.sa</td>\n                            <td>2020-01-02</td>\n                            <td>2020-01-05</td>\n                            <td>Privilege1, Privilege5</td>\n                            \n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>Commercial</td>\n                            <td>a@stc.com.sa</td>\n                            <td>2020-01-02</td>\n                            <td>2020-01-05</td>\n                            <td>Privilege2</td>\n                        </tbody>\n                    </table>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/update-receiver/update-receiver.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/update-receiver/update-receiver.component.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"bg-gradient-danger header pb-5 pt-5 \">\n    <div class=\"container-fluid\">\n       \n    </div>\n</div>\n<div class=\"bg-gradient-danger header pb-8 pt-5 pt-md-8\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\">\n    <div class=\"container mt--8 pb-5\">\n        <div class=\"row justify-content-center card-body-img\">\n            <div class=\"col-lg-5 col-md-7\">\n                <!-- <div class=\"text-center text-muted mb-4\">\n                    <img src=\"/assets/iphone.png\" style=\"margin-top: 8rem;\">\n               </div> -->\n                <div class=\"card bg-secondary shadow border-0\">\n                    <div class=\"card-body px-lg-5 py-lg-5\">\n                        <div class=\"text-center text-muted mb-4\">\n                             <img src=\"/assets/email.png\" >\n                        </div>\n                        <form [formGroup]=\"updateRecevier\">\n                            <h2 class=\"text-center h2 font-weight-bold mb-0\"> Update Receiver</h2>\n                            <div class=\"form-group mb-3\">\n                                <div class=\"input-group input-group-alternative\">\n                                    <div class=\"input-group-prepend\">\n                                        <span class=\"input-group-text\"><i class=\"ni ni-email-83\"></i> To Receiver </span> \n                                    </div>\n                                    <input type=\"text\" formControlName=\"toEmail\" (keydown.enter)=\"login()\" class=\"form-control\"\n                                        autocomplete=\"off\" placeholder=\"Email Address\" (focus)=\"focus=true\"\n                                        (blur)=\"focus=false\" maxlength=\"50\">\n                                </div>\n                            </div>\n                            <div class=\"form-group mb-3\">\n                                <div class=\"input-group input-group-alternative\">\n                                    <div class=\"input-group-prepend\">\n                                        <span class=\"input-group-text\"><i class=\"ni ni-email-83\"></i> CC Receiver</span>\n                                    </div>\n                                    <input type=\"text\" (keydown.enter)=\"login()\" class=\"form-control\"\n                                        autocomplete=\"off\" placeholder=\"Email Address\" (focus)=\"focus=true\" formControlName=\"CCEmail\" \n                                        (blur)=\"focus=false\" maxlength=\"50\">\n                                </div>\n                            </div>\n                            <div class=\"text-center\">\n                                <button type=\"button\" id=\"myBtn\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"\n                                  (click)=\"save()\">Save</button>\n            \n                                <button type=\"button\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"><a style=\"color: white;\"\n                                    [routerLink]=\"['/home']\">Cancel</a></button>\n                              </div>\n                        </form>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/virtual-account/virtual-account.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/virtual-account/virtual-account.component.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header bg-gradient-danger pb-8 pt-5 \">\n    <div class=\"container-fluid\">\n        <div class=\"header-body\">\n            <!-- Table -->\n            <div class=\"card shadow\">\n                <div class=\"card-header border-0\">\n                    <div class=\"row align-items-center\">\n                        <div class=\"col\">\n                            <h3 class=\"mb-0\">Virual Account </h3>\n                        </div>\n                        <div class=\"col text-right\">\n                            <a href=\"#!\" class=\"btn btn-sm btn-primary\">See all</a>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"table-responsive\">\n                    <!-- Projects table -->\n                    <table class=\"table align-items-center table-flush\">\n                        <thead class=\"thead-light\">\n                            <tr>\n                                <th scope=\"col\">#</th>\n                                <th scope=\"col\">Email</th>\n                                <th scope=\"col\">Company</th>\n                                <th scope=\"col\">Registration Date </th>\n                                <th scope=\"col\"> Remark</th>\n\n                            </tr>\n                        </thead>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>\n                                <div class=\"custom-control custom-control-alternative custom-checkbox\">\n                                    <input class=\"custom-control-input\" id=\" customCheckLogin\" type=\"checkbox\">\n                                    <label class=\"custom-control-label\" for=\" customCheckLogin\">\n                                    </label>\n                                </div>\n                            </td>\n                            <td>ABD@company.com</td>\n                            <td>Company A</td>\n                            <td>03-02-2020</td>\n                            <td> <button type=\"button\" class=\"btn btn-danger btn-danger\"\n                                    (click)=\"onSubmitZ()\">Enrolled</button> </td>\n                        </tbody>\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\n                            <td>\n                                <div class=\"custom-control custom-control-alternative custom-checkbox\">\n                                    <input class=\"custom-control-input\" id=\" customCheckLogin\" type=\"checkbox\">\n                                    <label class=\"custom-control-label\" for=\" customCheckLogin\">\n                                    </label>\n                                </div>\n                            </td>\n                            <td>ABD@company.com</td>\n                            <td>Company A</td>\n                            <td>03-02-2020</td>\n                            <td><button type=\"button\"  class=\"btn btn-danger btn-danger\"\n                                    (click)=\"onSubmitZ()\">Enrolled</button>\n                            </td>\n                        </tbody>\n                    </table>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/complaint/complaint.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/complaint/complaint.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>complaint works!</p>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n  <!-- Page content -->\r\n  <section class=\"contactUs\">\r\n      <div class=\"container\">\r\n          <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-8 col-md-4\">\r\n              <div class=\"card bg-secondary shadow border-0\">\r\n                <div class=\"card-body px-lg-5 py-lg-5\">\r\n                  <div class=\"btn-group\" role=\"group\" aria-label=\"Button group with nested dropdown\">\r\n                  </div>\r\n                  <form [formGroup]=\"contactForm\" (ngSubmit)=\"onSubmit()\">\r\n                    <div class=\"resp_padding\">\r\n                      <div class=\"contactus_question\">\r\n                        <img\r\n                          src=\"https://my.stc.com.sa:443/wps/contenthandler/dav/fs-type1/themes/stc.mystc.theme85/MySTC_en/images/ask-question-large-icon.png\"\r\n                          width=\"87\" height=\"87\" alt=\"Ask a Question\">\r\n                      </div>\r\n         \r\n                      <div class=\"floated-spans help-livechat-go contactus_images\">\r\n                        <div class=\"images-containers bottom-margin\"><img\r\n                            src=\"https://my.stc.com.sa:443/wps/contenthandler/dav/fs-type1/themes/stc.mystc.theme85/MySTC_en/images/ask-question-title1.png\"\r\n                            width=\"169\" height=\"32\" alt=\"Ask a question\"></div>\r\n                        <div class=\"bottom_margin purple_label\" style=\"width:325px;\">We care about every single message we\r\n                          receive.</div>\r\n                      </div>\r\n         \r\n                      <br class=\"clr\">\r\n                      <br class=\"clr\">\r\n                      <div id=\"validationErrorMessageForContactus\" class=\"bottom_margin error_msg\"\r\n                        style=\"display:none; background-color:#fff; border:solid 1px #e4188b; border-radius:6px; -webkit-border-radius:6px; -moz-border-radius:6px; padding-bottom:5px\">\r\n                      </div>\r\n                      <div id=\"dataSection\">\r\n                        <div class=\"bottom_margin purple_label floated_divs\" style=\"width:190px;\"><label for=\"request_type\">its\r\n                            about</label><span class=\"required_lable\"> *</span></div>\r\n         \r\n                        <div class=\"floated_divs mobile_input_width right_margin_20\" style=\"width: 300px;\">\r\n                          <div class=\"bottom_margin\">\r\n                            <div class=\"_select medium_dropdown\">\r\n                              <span class=\"right_margin\">\r\n                                <select name=\"request_type\" id=\"request_type\">\r\n                                  <option value=\"-1\">What?</option>\r\n                                  <option value=\"H\">Help and Support</option>\r\n                                  <option value=\"S\">Suggestion</option>\r\n                                  <option value=\"I\">Inquiry</option>\r\n                                </select>\r\n                              </span>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                      \r\n                      <!-- Shall we update section Ends -->\r\n                      <div class=\"text-muted mb-4\">\r\n                        <!-- <small>Or sign in with credentials</small> -->\r\n                        <div class=\"form-group mb-3\">\r\n                          <label> Customer Company Name </label>\r\n                          <div class=\"input-group input-group-alternative\">\r\n                            <input type=\"text\" class=\"form-control\" maxlength=\"30\" placeholder=\"Customer Company Name\"\r\n                              (focus)=\"focus=true\" (blur)=\"focus=false\">\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"form-group mb-3\">\r\n                          <label> Contact Name </label>\r\n                          <div class=\"input-group input-group-alternative\">\r\n                            <input type=\"text\" class=\"form-control\" maxlength=\"30\" placeholder=\"Contact Name\"\r\n                              (focus)=\"focus=true\" (blur)=\"focus=false\">\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"form-group mb-3\">\r\n                          <label> Contact Mobile Number </label>\r\n                          <div class=\"input-group input-group-alternative\">\r\n                            <input type=\"text\" class=\"form-control\" maxlength=\"15\"  placeholder=\"Contact Mobile Number\"\r\n                              (focus)=\"focus=true\" (blur)=\"focus=false\">\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"form-group mb-3\">\r\n                          <label> Email </label>\r\n                          <div class=\"input-group input-group-alternative\">\r\n                            <input type=\"text\" class=\"form-control\" maxlength=\"30\" placeholder=\"Email\" (focus)=\"focus=true\"\r\n                              (blur)=\"focus=false\" required>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"form-group mb-3\">\r\n                          <span> Message Subject </span>\r\n                          <div class=\"input-group input-group-alternative\">\r\n                            <input type=\"text\" class=\"form-control\" maxlength=\"45\" [formControl]=\"textontrol\" \r\n                              (focus)=\"focus=true\" (blur)=\"focus=false\">\r\n                          </div>\r\n                          <span class=\"pull-right char\"><em>{{ subjectLength | async }} / 45 characters</em></span>\r\n         \r\n                        </div>\r\n                        <label> Message Details </label>\r\n                        <textarea type=\"textarea\" [formControl]=\"textControl\" class=\"form-control mb-1\" maxlength=\"1000\"></textarea>\r\n                        <span class=\"pull-right char\"><em>{{ descriptionLength | async }} / 225 characters</em></span>\r\n                        <div class=\"clearfix\"></div>\r\n\r\n\r\n\r\n\r\n                        \r\n                        <div class=\"text-center pt-5\">\r\n                          <button type=\"button\" id=\"myBtn\" class=\"btn btn-send\"> Send</button>\r\n                          <button class=\"btn btn-cancel ml-1\" type=\"button\" [routerLink]=\"['/home']\">  Cancel</button>\r\n  \r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </form>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n    </section>\r\n  ");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/containers/default-layout/default-layout.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/containers/default-layout/default-layout.component.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header\r\n  [navbarBrandRouterLink]=\"['/dashboard']\"\r\n  [fixed]=\"true\"\r\n  [navbarBrandFull]=\"{src: 'assets/img/brand/logo.svg', width: 89, height: 25, alt: 'CoreUI Logo'}\"\r\n  [navbarBrandMinimized]=\"{src: 'assets/img/brand/sygnet.svg', width: 30, height: 30, alt: 'CoreUI Logo'}\"\r\n  [sidebarToggler]=\"'lg'\"\r\n  [asideMenuToggler]=\"'lg'\">\r\n  <ul class=\"nav navbar-nav d-md-down-none\">\r\n    <li class=\"nav-item px-3\">\r\n      <a class=\"nav-link\" href=\"#\">Dashboard</a>\r\n    </li>\r\n    <li class=\"nav-item px-3\">\r\n      <a class=\"nav-link\" href=\"#\">Users</a>\r\n    </li>\r\n    <li class=\"nav-item px-3\">\r\n      <a class=\"nav-link\" href=\"#\">Settings</a>\r\n    </li>\r\n  </ul>\r\n  <ul class=\"nav navbar-nav ml-auto\">\r\n    <li class=\"nav-item d-md-down-none\">\r\n      <a class=\"nav-link\" href=\"#\"><i class=\"icon-bell\"></i><span class=\"badge badge-pill badge-danger\">5</span></a>\r\n    </li>\r\n    <li class=\"nav-item d-md-down-none\">\r\n      <a class=\"nav-link\" href=\"#\"><i class=\"icon-list\"></i></a>\r\n    </li>\r\n    <li class=\"nav-item d-md-down-none\">\r\n      <a class=\"nav-link\" href=\"#\"><i class=\"icon-location-pin\"></i></a>\r\n    </li>\r\n    <li class=\"nav-item dropdown\" dropdown placement=\"bottom right\">\r\n      <a class=\"nav-link\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\" dropdownToggle (click)=\"false\">\r\n        <img src=\"assets/img/avatars/6.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\"/>\r\n      </a>\r\n      <div class=\"dropdown-menu dropdown-menu-right\" *dropdownMenu aria-labelledby=\"simple-dropdown\">\r\n        <div class=\"dropdown-header text-center\"><strong>Account</strong></div>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-bell-o\"></i> Updates<span class=\"badge badge-info\">42</span></a>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-envelope-o\"></i> Messages<span class=\"badge badge-success\">42</span></a>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-tasks\"></i> Tasks<span class=\"badge badge-danger\">42</span></a>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-comments\"></i> Comment<span class=\"badge badge-warning\">42</span></a>\r\n        <div class=\"dropdown-header text-center\"><strong>Settings</strong></div>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-user\"></i> Profile</a>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-wrench\"></i> Setting</a>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-usd\"></i> Payments<span class=\"badge badge-dark\">42</span></a>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-file\"></i> Projects<span class=\"badge badge-primary\">42</span></a>\r\n        <div class=\"divider\"></div>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-shield\"></i> Lock account</a>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-lock\"></i> Logout</a>\r\n      </div>\r\n    </li>\r\n  </ul>\r\n</app-header>\r\n<div class=\"app-body\">\r\n  <app-sidebar #appSidebar [fixed]=\"true\" [display]=\"'lg'\" [minimized]=\"sidebarMinimized\" (minimizedChange)=\"toggleMinimize($event)\">\r\n    <app-sidebar-nav [navItems]=\"navItems\" [perfectScrollbar] [disabled]=\"appSidebar.minimized\"></app-sidebar-nav>\r\n    <app-sidebar-minimizer></app-sidebar-minimizer>\r\n  </app-sidebar>\r\n  <!-- Main content -->\r\n  <main class=\"main\">\r\n    <!-- Breadcrumb -->\r\n    <!-- breaking change 'cui-breadcrumb' -->\r\n    <cui-breadcrumb>\r\n      <!-- Breadcrumb Menu-->\r\n      <li class=\"breadcrumb-menu d-md-down-none\">\r\n        <div class=\"btn-group\" role=\"group\" aria-label=\"Button group with nested dropdown\">\r\n          <a class=\"btn\" href=\"#\"><i class=\"icon-speech\"></i></a>\r\n          <a class=\"btn\" [routerLink]=\"['/dashboard']\"><i class=\"icon-graph\"></i> &nbsp;Dashboard</a>\r\n          <a class=\"btn\" href=\"#\"><i class=\"icon-settings\"></i> &nbsp;Settings</a>\r\n        </div>\r\n      </li>\r\n    </cui-breadcrumb>\r\n    <!-- deprecation warning for 'app-breadcrumb' -->\r\n    <!--<ol class=\"breadcrumb\">-->\r\n      <!--<app-breadcrumb></app-breadcrumb>-->\r\n      <!--&lt;!&ndash; Breadcrumb Menu&ndash;&gt;-->\r\n      <!--<li class=\"breadcrumb-menu d-md-down-none\">-->\r\n        <!--<div class=\"btn-group\" role=\"group\" aria-label=\"Button group with nested dropdown\">-->\r\n          <!--<a class=\"btn\" href=\"#\"><i class=\"icon-speech\"></i></a>-->\r\n          <!--<a class=\"btn\" [routerLink]=\"['/dashboard']\"><i class=\"icon-graph\"></i> &nbsp;Dashboard</a>-->\r\n          <!--<a class=\"btn\" href=\"#\"><i class=\"icon-settings\"></i> &nbsp;Settings</a>-->\r\n        <!--</div>-->\r\n      <!--</li>-->\r\n    <!--</ol>-->\r\n    <div class=\"container-fluid\">\r\n      <router-outlet></router-outlet>\r\n    </div><!-- /.container-fluid -->\r\n  </main>\r\n  <app-aside [fixed]=\"true\" [display]=\"false\" [ngClass]=\"'test'\">\r\n    <tabset>\r\n      <tab>\r\n        <ng-template tabHeading><i class=\"icon-list\"></i></ng-template>\r\n        <div class=\"list-group list-group-accent\">\r\n          <div class=\"list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small\">Today</div>\r\n          <div class=\"list-group-item list-group-item-accent-warning list-group-item-divider\">\r\n            <div class=\"avatar float-right\">\r\n              <img class=\"img-avatar\" src=\"assets/img/avatars/7.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n            </div>\r\n            <div>Meeting with\r\n              <strong>Lucas</strong>\r\n            </div>\r\n            <small class=\"text-muted mr-3\">\r\n              <i class=\"icon-calendar\"></i>  1 - 3pm</small>\r\n            <small class=\"text-muted\">\r\n              <i class=\"icon-location-pin\"></i>  Palo Alto, CA</small>\r\n          </div>\r\n          <div class=\"list-group-item list-group-item-accent-info\">\r\n            <div class=\"avatar float-right\">\r\n              <img class=\"img-avatar\" src=\"assets/img/avatars/4.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n            </div>\r\n            <div>Skype with\r\n              <strong>Megan</strong>\r\n            </div>\r\n            <small class=\"text-muted mr-3\">\r\n              <i class=\"icon-calendar\"></i>  4 - 5pm</small>\r\n            <small class=\"text-muted\">\r\n              <i class=\"icon-social-skype\"></i>  On-line</small>\r\n          </div>\r\n          <div class=\"list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small\">Tomorrow</div>\r\n          <div class=\"list-group-item list-group-item-accent-danger list-group-item-divider\">\r\n            <div>New UI Project -\r\n              <strong>deadline</strong>\r\n            </div>\r\n            <small class=\"text-muted mr-3\">\r\n              <i class=\"icon-calendar\"></i>  10 - 11pm</small>\r\n            <small class=\"text-muted\">\r\n              <i class=\"icon-home\"></i>  creativeLabs HQ</small>\r\n            <div class=\"avatars-stack mt-2\">\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/2.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/3.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/4.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/5.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/6.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"list-group-item list-group-item-accent-success list-group-item-divider\">\r\n            <div>\r\n              <strong>#10 Startups.Garden</strong> Meetup</div>\r\n            <small class=\"text-muted mr-3\">\r\n              <i class=\"icon-calendar\"></i>  1 - 3pm</small>\r\n            <small class=\"text-muted\">\r\n              <i class=\"icon-location-pin\"></i>  Palo Alto, CA</small>\r\n          </div>\r\n          <div class=\"list-group-item list-group-item-accent-primary list-group-item-divider\">\r\n            <div>\r\n              <strong>Team meeting</strong>\r\n            </div>\r\n            <small class=\"text-muted mr-3\">\r\n              <i class=\"icon-calendar\"></i>  4 - 6pm</small>\r\n            <small class=\"text-muted\">\r\n              <i class=\"icon-home\"></i>  creativeLabs HQ</small>\r\n            <div class=\"avatars-stack mt-2\">\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/2.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/3.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/4.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/5.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/6.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/7.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/8.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </tab>\r\n      <tab>\r\n        <ng-template tabHeading><i class=\"icon-speech\"></i></ng-template>\r\n        <div class=\"p-3\">\r\n          <div class=\"message\">\r\n            <div class=\"py-3 pb-5 mr-3 float-left\">\r\n              <div class=\"avatar\">\r\n                <img src=\"assets/img/avatars/7.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\r\n                <span class=\"avatar-status badge-success\"></span>\r\n              </div>\r\n            </div>\r\n            <div>\r\n              <small class=\"text-muted\">Lukasz Holeczek</small>\r\n              <small class=\"text-muted float-right mt-1\">1:52 PM</small>\r\n            </div>\r\n            <div class=\"text-truncate font-weight-bold\">Lorem ipsum dolor sit amet</div>\r\n            <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>\r\n          </div>\r\n          <hr>\r\n          <div class=\"message\">\r\n            <div class=\"py-3 pb-5 mr-3 float-left\">\r\n              <div class=\"avatar\">\r\n                <img src=\"assets/img/avatars/7.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\r\n                <span class=\"avatar-status badge-success\"></span>\r\n              </div>\r\n            </div>\r\n            <div>\r\n              <small class=\"text-muted\">Lukasz Holeczek</small>\r\n              <small class=\"text-muted float-right mt-1\">1:52 PM</small>\r\n            </div>\r\n            <div class=\"text-truncate font-weight-bold\">Lorem ipsum dolor sit amet</div>\r\n            <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>\r\n          </div>\r\n          <hr>\r\n          <div class=\"message\">\r\n            <div class=\"py-3 pb-5 mr-3 float-left\">\r\n              <div class=\"avatar\">\r\n                <img src=\"assets/img/avatars/7.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\r\n                <span class=\"avatar-status badge-success\"></span>\r\n              </div>\r\n            </div>\r\n            <div>\r\n              <small class=\"text-muted\">Lukasz Holeczek</small>\r\n              <small class=\"text-muted float-right mt-1\">1:52 PM</small>\r\n            </div>\r\n            <div class=\"text-truncate font-weight-bold\">Lorem ipsum dolor sit amet</div>\r\n            <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>\r\n          </div>\r\n          <hr>\r\n          <div class=\"message\">\r\n            <div class=\"py-3 pb-5 mr-3 float-left\">\r\n              <div class=\"avatar\">\r\n                <img src=\"assets/img/avatars/7.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\r\n                <span class=\"avatar-status badge-success\"></span>\r\n              </div>\r\n            </div>\r\n            <div>\r\n              <small class=\"text-muted\">Lukasz Holeczek</small>\r\n              <small class=\"text-muted float-right mt-1\">1:52 PM</small>\r\n            </div>\r\n            <div class=\"text-truncate font-weight-bold\">Lorem ipsum dolor sit amet</div>\r\n            <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>\r\n          </div>\r\n          <hr>\r\n          <div class=\"message\">\r\n            <div class=\"py-3 pb-5 mr-3 float-left\">\r\n              <div class=\"avatar\">\r\n                <img src=\"assets/img/avatars/7.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\r\n                <span class=\"avatar-status badge-success\"></span>\r\n              </div>\r\n            </div>\r\n            <div>\r\n              <small class=\"text-muted\">Lukasz Holeczek</small>\r\n              <small class=\"text-muted float-right mt-1\">1:52 PM</small>\r\n            </div>\r\n            <div class=\"text-truncate font-weight-bold\">Lorem ipsum dolor sit amet</div>\r\n            <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>\r\n          </div>\r\n        </div>\r\n      </tab>\r\n      <tab>\r\n        <ng-template tabHeading><i class=\"icon-settings\"></i></ng-template>\r\n        <div class=\"p-3\">\r\n          <h6>Settings</h6>\r\n          <div class=\"aside-options\">\r\n            <div class=\"clearfix mt-4\">\r\n              <small><b>Option 1</b></small>\r\n              <label class=\"switch switch-label switch-pill switch-success switch-sm float-right\">\r\n                <input type=\"checkbox\" class=\"switch-input\" checked>\r\n                <span class=\"switch-slider\" data-checked=\"On\" data-unchecked=\"Off\"></span>\r\n              </label>\r\n            </div>\r\n            <div>\r\n              <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small>\r\n            </div>\r\n          </div>\r\n          <div class=\"aside-options\">\r\n            <div class=\"clearfix mt-3\">\r\n              <small><b>Option 2</b></small>\r\n              <label class=\"switch switch-label switch-pill switch-success switch-sm float-right\">\r\n                <input type=\"checkbox\" class=\"switch-input\">\r\n                <span class=\"switch-slider\" data-checked=\"On\" data-unchecked=\"Off\"></span>\r\n              </label>\r\n            </div>\r\n            <div>\r\n              <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small>\r\n            </div>\r\n          </div>\r\n          <div class=\"aside-options\">\r\n            <div class=\"clearfix mt-3\">\r\n              <small><b>Option 3</b></small>\r\n              <label class=\"switch switch-label switch-pill switch-success switch-sm float-right\">\r\n                <input type=\"checkbox\" class=\"switch-input\">\r\n                <span class=\"switch-slider\" data-checked=\"On\" data-unchecked=\"Off\"></span>\r\n                <span class=\"switch-handle\"></span>\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <div class=\"aside-options\">\r\n            <div class=\"clearfix mt-3\">\r\n              <small><b>Option 4</b></small>\r\n              <label class=\"switch switch-label switch-pill switch-success switch-sm float-right\">\r\n                <input type=\"checkbox\" class=\"switch-input\" checked>\r\n                <span class=\"switch-slider\" data-checked=\"On\" data-unchecked=\"Off\"></span>\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <hr>\r\n          <h6>System Utilization</h6>\r\n          <div class=\"text-uppercase mb-1 mt-4\"><small><b>CPU Usage</b></small></div>\r\n          <div class=\"progress progress-xs\">\r\n            <div class=\"progress-bar bg-info\" role=\"progressbar\" style=\"width: 25%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n          </div>\r\n          <small class=\"text-muted\">348 Processes. 1/4 Cores.</small>\r\n          <div class=\"text-uppercase mb-1 mt-2\"><small><b>Memory Usage</b></small></div>\r\n          <div class=\"progress progress-xs\">\r\n            <div class=\"progress-bar bg-warning\" role=\"progressbar\" style=\"width: 70%\" aria-valuenow=\"70\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n          </div>\r\n          <small class=\"text-muted\">11444GB/16384MB</small>\r\n          <div class=\"text-uppercase mb-1 mt-2\"><small><b>SSD 1 Usage</b></small></div>\r\n          <div class=\"progress progress-xs\">\r\n            <div class=\"progress-bar bg-danger\" role=\"progressbar\" style=\"width: 95%\" aria-valuenow=\"95\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n          </div>\r\n          <small class=\"text-muted\">243GB/256GB</small>\r\n          <div class=\"text-uppercase mb-1 mt-2\"><small><b>SSD 2 Usage</b></small></div>\r\n          <div class=\"progress progress-xs\">\r\n            <div class=\"progress-bar bg-success\" role=\"progressbar\" style=\"width: 10%\" aria-valuenow=\"10\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n          </div>\r\n          <small class=\"text-muted\">25GB/256GB</small>\r\n        </div>\r\n      </tab>\r\n    </tabset>\r\n  </app-aside>\r\n</div>\r\n\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/containers/main-layout/main-layout.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/containers/main-layout/main-layout.component.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <div class=\"header navbar-expand-lg  custm-n\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\"></div> -->\r\n\r\n<!-- <nav class=\"navbar navbar-expand-lg  custm-n\" id=\"navbarSupportedContent\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\"></nav> -->\r\n<div class=\"animated fadeIn custom-nav\">\r\n  <nav class=\"navbar navbar-expand-lg  custm-n\" style=\"background-color: #4F008C; padding: 3vh; height: 10vh;\">\r\n    <img src=\"./assets/FA_STC_RGB_NEG.ico\" height=\"60px\" width=\"80px\" [routerLink]=\"['/home']\">\r\n    <button aria-controls=\"navbarSupportedContent\" (click)=\"isCollapsed = !isCollapsed\"\r\n      [attr.aria-expanded]=\"!isCollapsed\" class=\"navbar-toggler\" type=\"button\" aria-label=\"Toggle navigation\">\r\n      <span class=\"navbar-toggler-icon\"></span>\r\n    </button>\r\n\r\n    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\" [collapse]=\"isCollapsed\" [isAnimated]=\"true\">\r\n      <ul class=\"navbar-nav mr-auto custom-nav-items color-white\">\r\n\r\n        <li class=\"nav-item dropdown\" dropdown>\r\n          <a id=\"link-dropdown\" class=\"nav-link dropdown-toggle color-white\" href dropdownToggle (click)=\"false\"\r\n            aria-controls=\"your-dropdown\" style=\"color: white;\">\r\n            {{'HEADER.MENU.SERVICE'| translate}}\r\n          </a>\r\n\r\n          <div id=\"your-dropdown\" class=\"dropdown-menu\" aria-labelledby=\"link-dropdown\" *dropdownMenu>\r\n            <a class=\"dropdown-item\" [routerLink]=\"['/display-invoice']\">\r\n              {{'HEADER.SUB_MENU.DISPLAY_INVOICE'| translate}}</a>\r\n            <a class=\"dropdown-item\"> {{'HEADER.SUB_MENU.DISPLAY_SERVICE'| translate}}</a>\r\n            <!-- <div class=\"dropdown-divider\"></div>\r\n                      <a class=\"dropdown-item\" href=\"#\">Something else here</a> -->\r\n          </div>\r\n        </li>\r\n        <li class=\"nav-item dropdown\" dropdown>\r\n          <a id=\"link-dropdown\" class=\"nav-link dropdown-toggle color-white\" href dropdownToggle (click)=\"false\"\r\n            aria-controls=\"your-dropdown\" style=\"color: white;\">\r\n            {{'HEADER.MENU.REPORT'| translate}}\r\n          </a>\r\n          <div id=\"your-dropdown\" class=\"dropdown-menu\" aria-labelledby=\"link-dropdown\" *dropdownMenu>\r\n            <a class=\"dropdown-item\" href=\"#\"\r\n              [routerLink]=\"['/dashboard']\">{{'HEADER.SUB_MENU.DASHBOARD'| translate}}</a>\r\n            <a class=\"dropdown-item\" href=\"#\" [routerLink]=\"['/details']\"> {{'HEADER.SUB_MENU.DETAILS'| translate}}</a>\r\n            <!-- <div class=\"dropdown-divider\"></div>\r\n                      <a class=\"dropdown-item\" href=\"#\">Something else here</a> -->\r\n          </div>\r\n        </li>\r\n\r\n\r\n        <li class=\"nav-item dropdown\" dropdown>\r\n          <a id=\"link-dropdown\" class=\"nav-link dropdown-toggle color-white\" href dropdownToggle (click)=\"false\"\r\n            aria-controls=\"your-dropdown\" style=\"color: white;\">\r\n            {{'HEADER.MENU.CDR'| translate}}\r\n          </a>\r\n          <div id=\"your-dropdown\" class=\"dropdown-menu\" aria-labelledby=\"link-dropdown\" *dropdownMenu>\r\n            <a class=\"dropdown-item\" href=\"#\" [routerLink]=\"['/upload']\">{{'HEADER.SUB_MENU.UPLOAD'| translate}}</a>\r\n            <a class=\"dropdown-item\" href=\"#\" [routerLink]=\"['/download']\">\r\n              {{'HEADER.SUB_MENU.DOWNLOAD'| translate}}</a>\r\n            <!-- <div class=\"dropdown-divider\"></div>\r\n                      <a class=\"dropdown-item\" href=\"#\">Something else here</a> -->\r\n          </div>\r\n        </li>\r\n\r\n\r\n        <li class=\"nav-item dropdown\" dropdown>\r\n          <a id=\"link-dropdown\" class=\"nav-link dropdown-toggle color-white\" href dropdownToggle (click)=\"false\"\r\n            aria-controls=\"your-dropdown \" style=\"color: white;\">\r\n            {{'HEADER.MENU.CONTACT_US'| translate}}\r\n          </a>\r\n          <div id=\"your-dropdown\" class=\"dropdown-menu\" aria-labelledby=\"link-dropdown\" *dropdownMenu>\r\n            <a class=\"dropdown-item\" href=\"#\" [routerLink]=\"['/help']\"> {{'HEADER.SUB_MENU.HELP'| translate}} </a>\r\n            <a class=\"dropdown-item\" href=\"#\" [routerLink]=\"['/contact']\">{{'HEADER.MENU.CONTACT_US'| translate}} </a>\r\n\r\n          </div>\r\n        </li>\r\n        <li class=\"nav-item \">\r\n          <a class=\"nav-link color-white\" href=\"#\" [routerLink]=\"['/complaint']\" style=\"color: white;\">\r\n            {{'HEADER.MENU.COMPLAIN'| translate}}\r\n          </a><span class=\"sr-only\">(current)</span>\r\n        </li>\r\n      </ul>\r\n      <!-- <ul class=\"navbar-nav mr-auto custom-nav-items color-white\">\r\n        <li class=\"nav-item active\">\r\n        \r\n        </li>\r\n      </ul> -->\r\n      <ul class=\"navbar-nav align-items-center d-none d-md-flex color-white\">\r\n        <a *ngIf=\"!isRTL\" (click)=\"changeLang(arabic)\">\r\n          <i class=\"ni ni-support-16\"></i>\r\n          <span>ع</span>\r\n        </a>\r\n        <a *ngIf=\"isRTL\" (click)=\"changeLang(english)\">\r\n          <i class=\"ni ni-support-16\"></i>\r\n          <span>En</span>\r\n        </a>\r\n        <li class=\"nav-item color-white\" ngbDropdown placement=\"bottom-right\">\r\n          <a class=\"nav-link pr-0\" role=\"button\" ngbDropdownToggle>\r\n            <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>\r\n          </a>\r\n          <div class=\"dropdown-menu-arrow dropdown-menu-right\" ngbDropdownMenu>\r\n            <div class=\" dropdown-header noti-title\">\r\n              <h3 class=\"text-overflow m-0 h3\"> {{'HEADER.WELCOME'| translate}}!</h3>\r\n            </div>\r\n            <a routerLinkActive=\"active\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-single-02\"></i>\r\n              <span>{{'HEADER.MYPROFILE'| translate}}</span>\r\n            </a>\r\n            <a (click)=\"portal()\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-single-16\"></i>\r\n              <span>Portal Admin</span>\r\n            </a>\r\n            <a class=\"dropdown-item\" *ngIf=\"!isRTL\" (click)=\"changeLang(arabic)\">\r\n              <i class=\"ni ni-support-16\"></i>\r\n              <span>العربيه</span>\r\n            </a>\r\n            <a class=\"dropdown-item\" *ngIf=\"isRTL\" (click)=\"changeLang(english)\">\r\n              <i class=\"ni ni-support-16\"></i>\r\n              <span>English</span>\r\n            </a>\r\n            <div class=\"dropdown-divider\"></div>\r\n            <a routerLink=\"/login\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-user-run\"></i>\r\n              <span>{{'HEADER.LOGOUT'| translate}}</span>\r\n            </a>\r\n          </div>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </nav>\r\n</div>\r\n<router-outlet></router-outlet>\r\n<!-- Footer -->\r\n<footer class=\"page-footer pt-4 bg-gradient-danger\">\r\n  <img src=\"./assets/FA_STC_RGB_NEG.ico\" height=\"40px\" width=\"60px\">\r\n  <h5 class=\"h2 font-weight-bold mb-0\">Copyright © 2020 stc. All rights reserved.</h5>\r\n\r\n</footer>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard-wbu/dashboard-wbu.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard-wbu/dashboard-wbu.component.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-8\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"header-body\">\r\n            <div class=\"card shadow\">\r\n                <div class=\"card-header border-0\">\r\n                    <div class=\"row align-items-center\">\r\n                        <div class=\"col\">\r\n                            <h3 class=\"h2\">Dashboard</h3>\r\n                        </div>\r\n                        <div class=\"col text-right\">\r\n                            <!-- <a href=\"#!\" class=\"btn btn-sm btn-primary\">See all</a> -->\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <div class=\"container\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-sm-2 cards\">\r\n                                <label>OPEN</label>\r\n                                <i class=\"fa fa-key\" aria-hidden=\"true\"></i>\r\n                                <div>\r\n                                    <label class=\"card-txt\">00</label>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-sm-2 cards\">\r\n                                <label>DELAY</label>\r\n                                <i class=\"fa fa-clock-o\" aria-hidden=\"true\"></i>\r\n                                <div>\r\n                                    <label class=\"card-txt\">00</label>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-sm-2 cards\">\r\n                                <label>CLOSED</label>\r\n                                <i class=\"fa fa-cog\" aria-hidden=\"true\"></i>\r\n                                <div>\r\n                                    <label class=\"card-txt\">00</label>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-sm-2 cards\">\r\n                                <label>PENDING </label> <i class=\"fa fa-hourglass-end\" aria-hidden=\"true\"></i>\r\n                                <div>\r\n                                    <label class=\"card-txt\">00</label>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"chart-wrapper graph-align-right\" >\r\n                        <canvas baseChart [datasets]=\"barChartData\" [labels]=\"barChartLabels\"\r\n                            [options]=\"barChartOptions\" [plugins]=\"barChartPlugins\" [legend]=\"barChartLegend\"\r\n                            [chartType]=\"barChartType\">\r\n                        </canvas>\r\n                    </div>\r\n                    <div class=\"chart-wrapper graph-align-left\">\r\n                        <canvas baseChart [data]=\"pieChartData\" [labels]=\"pieChartLabels\" [chartType]=\"pieChartType\"\r\n                            [options]=\"pieChartOptions\" [plugins]=\"pieChartPlugins\" [legend]=\"pieChartLegend\">\r\n                        </canvas>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/details/details.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/details/details.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-8\">\n    <div class=\"container-fluid\">\n        <div class=\"header-body\">\n            <!-- Table -->\n            <div class=\"card shadow\">\n                <div class=\"card-header border-0\">\n                    <div class=\"row align-items-center\">\n                        <div class=\"col\">\n                            <div class=\"h3\">Report Details </div>\n                        </div>\n                        <div class=\"col text-right\">\n                            <a href=\"#!\" class=\"btn btn-sm btn-primary\">See all</a>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"table-responsive\">\n                    <!-- Projects table -->\n                    <table class=\"table align-items-center table-flush\">\n                        <thead class=\"thead-light\">\n                            <tr>\n                                <th scope=\"col\" > Complaint <input class=\"form-control ml-2\" type=\"text\" maxlength=\"10\" [formControl]=\"filter\" /></th>\n                                <th scope=\"col\">Type <input class=\"form-control ml-2\" type=\"text\" maxlength=\"10\" [formControl]=\"filter\" /></th>\n                                <th scope=\"col\">Assigned to <input class=\"form-control ml-2\" type=\"text\" maxlength=\"10\" [formControl]=\"filter\" /></th>\n                                <th scope=\"col\"> Customer <input class=\"form-control ml-2\" type=\"text\" maxlength=\"10\" [formControl]=\"filter\" /></th>\n                                <th scope=\"col\"> Status <input class=\"form-control ml-2\" type=\"text\" maxlength=\"10\" [formControl]=\"filter\" /> </th>\n                                <th scope=\"col\"> Creation date <input class=\"form-control ml-2\" type=\"text\" maxlength=\"10\" [formControl]=\"filter\" /></th>\n                                <th scope=\"col\"> Close date <input class=\"form-control ml-2\" type=\"text\" maxlength=\"10\" [formControl]=\"filter\" /></th>\n                                <th scope=\"col\"> MTTR (H) <input class=\"form-control ml-2\" type=\"text\" maxlength=\"10\" [formControl]=\"filter\" /></th>            \n                            </tr>\n                        </thead>\n                        <tr>\n                            <td>SR-124</td>\n                            <td>Technical</td>\n                            <td>WSMC</td>\n                            <td>Zain</td>\n                            <td>In progress</td>\n                            <td>01-10-2020</td>\n                            <td>01-10-20204</td>\n                            <td>under proceing</td>\n                        </tr>\n                        <tr>\n                            <td>SR-124</td>\n                            <td>Technical</td>\n                            <td>WSMC</td>\n                            <td>Zain</td>\n                            <td>In progress</td>\n                            <td>01-10-2020</td>\n                            <td>01-10-20204</td>\n                            <td>under proceing</td>\n                        </tr>\n                        <tr>\n                            <td>SR-124</td>\n                            <td>Technical</td>\n                            <td>WSMC</td>\n                            <td>Zain</td>\n                            <td>In progress</td>\n                            <td>01-10-2020</td>\n                            <td>01-10-20204</td>\n                            <td>under proceing</td>\n                        </tr>\n                        <tr>\n                            <td>SR-124</td>\n                            <td>Technical</td>\n                            <td>WSMC</td>\n                            <td>Zain</td>\n                            <td>In progress</td>\n                            <td>01-10-2020</td>\n                            <td>01-10-20204</td>\n                            <td>under proceing</td>\n                        </tr>\n                        <tr>\n                            <td>SR-124</td>\n                            <td>Technical</td>\n                            <td>WSMC</td>\n                            <td>Zain</td>\n                            <td>In progress</td>\n                            <td>01-10-2020</td>\n                            <td>01-10-20204</td>\n                            <td>under proceing</td>\n                        </tr>\n                    </table>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/display-invoice/display-invoice.component.html":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/display-invoice/display-invoice.component.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-9\" [hidden]=invoiceSearch>\n    <div class=\"container mt--8 pb-5\">\n        <div class=\"row justify-content-center\">\n            <div class=\"col-lg-8 col-md-7\">\n                <div class=\"card bg-secondary shadow border-0\">\n                    <div class=\"header-body\">\n                        <!-- Table -->\n                        <div class=\"card shadow\">\n                            <div class=\"card-header border-0\" style=\"background-color: #F7FAFC;\">\n                                <div class=\"row align-items-center\">\n                                    <div class=\"col-auto\" style=\"padding-left: 40%;\">\n                                        <div class=\"icon icon-shape text-white rounded-circle shadow\">\n                                            <div class=\"col-auto\">\n                                                <img src=\"./assets/invoice.png\" height=\"70px\" width=\"70px\"\n                                                    style=\"margin-top: -5.5rem; margin-left: 5.5rem;\">\n                                            </div>\n                                            <i class=\"fas fa-chart-pie\"></i>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                          \n                            <div class=\"col\">\n                                <h3 class=\"mb-0 mb-1\"> Display Invoice </h3>\n                            </div>\n                            <div class=\"row\">\n                                <div class=\"col-lg-4\">\n                                    <label for=\"input-city\" class=\"form-control-label\">Invoice Number</label>\n                                    <input class=\"form-control\" type=\"text\" maxlength=\"10\" />\n                                </div>\n                                <div class=\"col-lg-4\">\n                                    <label for=\"input-city\" class=\"form-control-label\">Billing Period</label>\n                                    <select class=\"form-control\">\n                                        <option disabled selected value></option>\n                                        <option>20141201</option>\n                                        <option>20141202</option>\n                                        <option>20141203</option>\n                                        <option>20141204</option>\n                                    </select>\n                                </div>\n                                <div class=\"form-group hidden\">\n                                    <div class=\"input-group\">\n                                        <input name=\"datepicker\" class=\"form-control\" ngbDatepicker\n                                            #datepicker=\"ngbDatepicker\" maxlength=\"10\" (dateSelect)=\"onDateSelection($event)\"\n                                            [dayTemplate]=\"t\">\n                                        <ng-template #t let-date let-focused=\"focused\">\n                                            <span class=\"custom-day\" [class.focused]=\"focused\"\n                                                (mouseenter)=\"hoveredDate = date\" (mouseleave)=\"hoveredDate = null\">\n                                                {{ date.day }}\n                                            </span>\n                                        </ng-template>\n                                    </div>\n                                </div>\n                                <div class=\"form-group col-lg-4\">\n                                    <label class=\"form-control-label\"\n                                        for=\"input-city\">{{\"Invoice Date\" | titlecase}}</label>\n                                    <div class=\"input-group\">\n                                        <input #dpFromDate class=\"form-control\" placeholder=\"yyyy-mm-dd\"\n                                            name=\"dpFromDate\" maxlength=\"10\" [value]=\"formatter.format(fromDate)\"\n                                            (input)=\"fromDate = validateInput(fromDate, dpFromDate.value)\">\n                                        <div class=\"input-group-append\">\n                                            <button class=\"btn btn-outline-secondary calendar\"\n                                                (click)=\"datepicker.toggle()\" type=\"button\">\n                                                <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\n                                            </button>\n                                        </div>\n                                    </div>\n                                </div>\n\n                                <div class=\"col-lg-4\">\n                                    <label for=\"ccnumber\" class=\"form-control-label\">Service Type</label>\n\n                                    <select class=\"form-control\" id=\"yyy\">\n                                        <option disabled selected value></option>\n                                        <option>Voice</option>\n                                        <option>Non-Voice</option>\n                                        <option>Voice & Non-Voice</option>\n                                    </select>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <div class=\"col\">\n                                    <div class=\"text-center\">\n                                        <button class=\"btn submit my-4 \" (click)=\"disply()\"\n                                            type=\"button\">Search</button>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-8\" [hidden]=invoice>\n    <div class=\"container mt--8 pb-5\">\n        <div class=\"row justify-content-center\">\n            <div class=\"col-lg-8 col-md-7\">\n                <div class=\"card bg-secondary shadow border-0\">\n                    <div class=\"header-body\">\n                        <!-- Table -->\n                        <div class=\"card shadow\">\n                            <div class=\"card-header border-0\">\n                                <div class=\"row align-items-center\">\n                                    <div class=\"col\">\n                                        <h3 class=\"mb-0\">Invoice Details</h3>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"table-responsive\">\n                                <table class=\"table align-items-center table-flush\">\n                                    <thead class=\"thead-light\">\n                                        <tr>\n                                            <th scope=\"col\">Invoice Number</th>\n                                            <th scope=\"col\">Billing Period</th>\n                                            <th scope=\"col\">Billing Date</th>\n                                            <th scope=\"col\">Invoice Link</th>\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                        <tr>\n                                            <td>\n                                                0000 0000 0000 0000\n                                            </td>\n                                            <td>\n                                                20141202\n                                            </td>\n                                            <td>\n                                                01-02-2020\n                                            </td>\n                                            <td>\n                                                <a href=\"/images/myw3schoolsimage.jpg\" download>Download </a>\n                                            </td>\n                                        </tr>\n                                    </tbody>\n                                </table>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/downloads-operations/downloads-operations.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/downloads-operations/downloads-operations.component.html ***!
  \****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ngb-tabset [destroyOnHide]=\"false\">\r\n  <ngb-tab>\r\n    <ng-template ngbTabTitle><b>Billing</b> </ng-template>\r\n    <ng-template ngbTabContent>\r\n      <div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-8\">\r\n        <div class=\"container mt--8 pb-5\">\r\n          <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-8 col-md-7\">\r\n              <div class=\"card bg-secondary shadow border-0\">\r\n                <div class=\"header-body\">\r\n                  <!-- Table -->\r\n                  <div class=\"card shadow\">\r\n                    <div class=\"card-header border-0\">\r\n                      <div class=\"row align-items-center\">\r\n                        <div class=\"col\">\r\n                          <h3 class=\"mb-0\">CDR Billing Contact </h3>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                      <div class=\"col-lg-4\">\r\n                        <label for=\"input-city\" class=\"form-control-label\">Invoice Number</label>\r\n                        <select class=\"form-control\" id=\"yyy\">\r\n                          <option>0000 0000 1233 2212</option>\r\n                          <option>0000 4452 1233 2212</option>\r\n                          <option>0000 0000 7512 2212</option>\r\n                          <option>0000 0000 1233 5823</option>\r\n      \r\n                        </select>\r\n                      </div> \r\n                      <div class=\"form-group hidden\">\r\n                        <div class=\"input-group\">\r\n                          <input name=\"datepicker\" maxlength=\"10\" class=\"form-control\" ngbDatepicker #datepicker=\"ngbDatepicker\"\r\n                            [autoClose]=\"'outside'\" (dateSelect)=\"onDateSelection($event)\" [displayMonths]=\"2\"\r\n                            [dayTemplate]=\"t\" outsideDays=\"hidden\" [startDate]=\"fromDate!\">\r\n                          <ng-template #t let-date let-focused=\"focused\">\r\n                            <span class=\"custom-day\" [class.focused]=\"focused\" [class.range]=\"isRange(date)\"\r\n                              [class.faded]=\"isHovered(date) || isInside(date)\" (mouseenter)=\"hoveredDate = date\"\r\n                              (mouseleave)=\"hoveredDate = null\">\r\n                              {{ date.day }}\r\n                            </span>\r\n                          </ng-template>\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"form-group col-lg-4\">\r\n                        <label class=\"form-control-label\" for=\"input-city\">{{\"date from\" | titlecase}}</label>\r\n                        <div class=\"input-group\">\r\n                          <input #dpFromDate class=\"form-control\" maxlength=\"10\" placeholder=\"yyyy-mm-dd\" name=\"dpFromDate\"\r\n                            [value]=\"formatter.format(fromDate)\"\r\n                            (input)=\"fromDate = validateInput(fromDate, dpFromDate.value)\">\r\n                          <div class=\"input-group-append\">\r\n                            <button class=\"btn btn-outline-secondary calendar\" (click)=\"datepicker.toggle()\"\r\n                              type=\"button\">\r\n                              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                            </button>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"form-group col-lg-4\">\r\n                        <label class=\"form-control-label\" for=\"input-city\">{{\"date to\" | titlecase}}\r\n                        </label>\r\n                        <div class=\"input-group\">\r\n                          <input #dpToDate class=\"form-control\" maxlength=\"10\" placeholder=\"yyyy-mm-dd\" name=\"dpToDate\"\r\n                            [value]=\"formatter.format(toDate)\" (input)=\"toDate = validateInput(toDate, dpToDate.value)\">\r\n                          <div class=\"input-group-append\">\r\n                            <button class=\"btn btn-outline-secondary calendar\" (click)=\"datepicker.toggle()\"\r\n                              type=\"button\">\r\n                              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                            </button>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n\r\n                      <div class=\"col-lg-4\">\r\n                        <label for=\"ccnumber\" class=\"form-control-label\">Product</label>\r\n\r\n                        <select class=\"form-control\" id=\"yyy\">\r\n                          <option>Voice</option>\r\n                          <option>SMS</option>\r\n                          <option>MMS</option>\r\n                          <option>Other</option>\r\n\r\n                        </select>\r\n                      </div>\r\n\r\n                      <!-- <div class=\"col-lg-4\">\r\n                        <label for=\"ccnumber\" class=\"form-control-label\">Target User</label>\r\n\r\n                        <mat-select class=\"form-control\" (selectionChange)=\"openDialog()\">\r\n                          <mat-option disabled selected value></mat-option>\r\n                          <mat-option>Ali</mat-option>\r\n                          <mat-option>Mohammed</mat-option>\r\n                          <mat-option>Saad</mat-option>\r\n                          <mat-option>Saud</mat-option>\r\n\r\n                        </mat-select>\r\n                      </div> -->\r\n                      <div class=\"col-lg-4\">\r\n                        <div class=\"form-group\">\r\n                          <label for=\"exampleFormControlTextarea1\" class=\"form-control-label\">Description</label>\r\n                          <textarea class=\"form-control\" id=\"exampleFormControlTextarea1\" rows=\"3\"></textarea>\r\n                        </div>\r\n                      </div>\r\n\r\n                      <div class=\"col-lg-4\">\r\n                        <div class=\"downloads\">\r\n\r\n                          <a href=\"/images/myw3schoolsimage.jpg\" download>\r\n                            <button class=\"downloadFile\">Downloads file </button>\r\n                          </a>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                      <div class=\"col\">\r\n                        <div class=\"text-center\">\r\n                          <button class=\"btn submit my-4 \" (click)=\"disply()\" type=\"button\">Submit</button>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n    </ng-template>\r\n  </ngb-tab>\r\n  <ngb-tab>\r\n    <ng-template ngbTabTitle><b>Technical </b> </ng-template>\r\n    <ng-template ngbTabContent>\r\n      <div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-8\">\r\n        <div class=\"container mt--8 pb-5\">\r\n          <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-8 col-md-7\">\r\n              <div class=\"card bg-secondary shadow border-0\">\r\n                <div class=\"header-body\">\r\n                  <!-- Table -->\r\n                  <div class=\"card shadow\">\r\n                    <div class=\"card-header border-0\">\r\n                      <div class=\"row align-items-center\">\r\n                        <div class=\"col\">\r\n\r\n                          <h3 class=\"mb-0\">CDR Technical Contact </h3>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                      <div class=\"col-lg-4\">\r\n                        <label for=\"input-city\" class=\"form-control-label\">Invoice Number</label>\r\n                        <select class=\"form-control\" id=\"yyy\">\r\n                          <option>0000 0000 1233 2212</option>\r\n                          <option>0000 4452 1233 2212</option>\r\n                          <option>0000 0000 7512 2212</option>\r\n                          <option>0000 0000 1233 5823</option>\r\n      \r\n                        </select>\r\n                      </div> \r\n\r\n                      <div class=\"form-group hidden\">\r\n                        <div class=\"input-group\">\r\n                          <input name=\"datepicker\" class=\"form-control\" maxlength=\"10\" ngbDatepicker #datepicker=\"ngbDatepicker\"\r\n                            [autoClose]=\"'outside'\" (dateSelect)=\"onDateSelection($event)\" [displayMonths]=\"2\"\r\n                            [dayTemplate]=\"t\" outsideDays=\"hidden\" [startDate]=\"fromDate!\">\r\n                          <ng-template #t let-date let-focused=\"focused\">\r\n                            <span class=\"custom-day\" [class.focused]=\"focused\" [class.range]=\"isRange(date)\"\r\n                              [class.faded]=\"isHovered(date) || isInside(date)\" (mouseenter)=\"hoveredDate = date\"\r\n                              (mouseleave)=\"hoveredDate = null\">\r\n                              {{ date.day }}\r\n                            </span>\r\n                          </ng-template>\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"form-group col-lg-4\">\r\n                        <label class=\"form-control-label\" for=\"input-city\">{{\"date from\" | titlecase}}</label>\r\n                        <div class=\"input-group\">\r\n                          <input #dpFromDate class=\"form-control\" maxlength=\"10\" placeholder=\"yyyy-mm-dd\" name=\"dpFromDate\"\r\n                            [value]=\"formatter.format(fromDate)\"\r\n                            (input)=\"fromDate = validateInput(fromDate, dpFromDate.value)\">\r\n                          <div class=\"input-group-append\">\r\n                            <button class=\"btn btn-outline-secondary calendar\" (click)=\"datepicker.toggle()\"\r\n                              type=\"button\">\r\n                              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                            </button>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"form-group col-lg-4\">\r\n                        <label class=\"form-control-label\" for=\"input-city\">{{\"date to\" | titlecase}}\r\n                        </label>\r\n                        <div class=\"input-group\">\r\n                          <input #dpToDate class=\"form-control\" maxlength=\"10\" placeholder=\"yyyy-mm-dd\" name=\"dpToDate\"\r\n                            [value]=\"formatter.format(toDate)\" (input)=\"toDate = validateInput(toDate, dpToDate.value)\">\r\n                          <div class=\"input-group-append\">\r\n                            <button class=\"btn btn-outline-secondary calendar\" (click)=\"datepicker.toggle()\"\r\n                              type=\"button\">\r\n                              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                            </button>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n\r\n                      <div class=\"col-lg-4\">\r\n                        <label for=\"ccnumber\" class=\"form-control-label\">Product</label>\r\n\r\n                        <select class=\"form-control\" id=\"yyy\">\r\n                          <option>Voice</option>\r\n                          <option>SMS</option>\r\n                          <option>MMS</option>\r\n                          <option>Other</option>\r\n\r\n                        </select>\r\n                      </div>\r\n\r\n                      <!-- <div class=\"col-lg-4\">\r\n                        <label for=\"ccnumber\" class=\"form-control-label\">Target User</label>\r\n\r\n                        <mat-select class=\"form-control\" (selectionChange)=\"openDialog()\">\r\n                          <mat-option disabled selected value></mat-option>\r\n                          <mat-option>Ali</mat-option>\r\n                          <mat-option>Mohammed</mat-option>\r\n                          <mat-option>Saad</mat-option>\r\n                          <mat-option>Saud</mat-option>\r\n\r\n                        </mat-select>\r\n                      </div> -->\r\n                      <div class=\"col-lg-4\">\r\n                        <div class=\"form-group\">\r\n                          <label for=\"exampleFormControlTextarea1\" class=\"form-control-label\">Description</label>\r\n                          <textarea class=\"form-control\" id=\"exampleFormControlTextarea1\" rows=\"3\"></textarea>\r\n                        </div>\r\n                      </div>\r\n\r\n\r\n                      <div class=\"col-lg-4\">\r\n                        <div class=\"downloads\">\r\n\r\n                          <a href=\"/images/myw3schoolsimage.jpg\" download>\r\n                            <button class=\"downloadFile\">Downloads file </button>\r\n                          </a>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                      <div class=\"col\">\r\n                        <div class=\"text-center\">\r\n                          <button class=\"btn submit my-4 \" (click)=\"disply()\" type=\"button\">Submit</button>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n    </ng-template>\r\n  </ngb-tab>\r\n</ngb-tabset>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/help/help.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/help/help.component.html ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/cp-change.component.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/cp-change.component.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<form [formGroup]=\"registerForm\" (ngSubmit)=\"onSubmit()\" (keydown.enter)=\"onSubmit()\" (keypress)=\"trueChart($event)\" autocomplete=\"off\">\r\n  <h2 class=\"text-center h2 font-weight-bold mb-0\" style=\"padding-bottom: 2rem;\"> Change Password</h2>\r\n  <div class=\"form-row\">\r\n    <div class=\"form-group col\">\r\n      <label>Current Password</label>\r\n      <input type=\"password\" formControlName=\"currpassword\" [(ngModel)]=\"current\" onpaste=\"return false;\" class=\"form-control\"\r\n        [ngClass]=\"{'is-invalid': submitted && f.currpassword.errors }\" />\r\n      <div *ngIf=\"submitted && f.currpassword.errors\" class=\"invalid-feedback\">\r\n        <div *ngIf=\"f.currpassword.errors.required\">Current Password is required</div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"form-group col\">\r\n      <label>New Password</label>\r\n      <input type=\"password\" formControlName=\"password\" [(ngModel)]=\"password\" onpaste=\"return false;\" class=\"form-control\"\r\n        [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\" />\r\n      <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\r\n        <div *ngIf=\"f.password.errors.required\">Password is required</div>\r\n        <div *ngIf=\"f.password.errors.minlength\">Password must be at least 8 characters</div>\r\n      </div>\r\n    </div>\r\n    <div class=\"form-group col\">\r\n      <label>Confirm Password</label>\r\n      <input type=\"password\" formControlName=\"confirmPassword\" [(ngModel)]=\"confirm\" onpaste=\"return false;\" class=\"form-control\"\r\n        [ngClass]=\"{ 'is-invalid': submitted && f.confirmPassword.errors }\" />\r\n      <div *ngIf=\"submitted && f.confirmPassword.errors\" class=\"invalid-feedback\">\r\n        <div *ngIf=\"f.confirmPassword.errors.required\">Confirm Password is required</div>\r\n        <div *ngIf=\"f.confirmPassword.errors.mustMatch\">Passwords must match</div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"text-center\">\r\n    <button class=\"btn btn-secondary\">Confirm</button>\r\n    <button class=\"btn btn-secondary\" type=\"reset\" (click)=\"onReset()\">Reset</button>\r\n  </div>\r\n</form>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container-fluid\" style=\"background-color: #4F008C;\">\r\n  <div class=\"header-body border-line\">\r\n    <div class=\"col \">\r\n      <label class=\"color-white font-home\">Special Offers</label>\r\n    </div>\r\n    <section class=\"main-slider\">\r\n      <carousel [interval]=\"false\" style=\"bottom: -20px;\">\r\n        <div class=\"row\">\r\n\r\n          <slide>\r\n            <div class=\"col-xl-3 col-lg-6 display-line\" data-aos=\"fade-up\">\r\n              <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                <div class=\"input-group mb-3\">\r\n                  <label class=\"form-control color-white\" style=\"background-color: #E6315B;\">Number of in Assets\r\n                  </label>\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text-card\">100</span>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                <div class=\"input-group mb-3\">\r\n                  <label class=\"form-control color-white\" style=\"background-color: #E96734;\">\r\n                    Number of in Progress Order </label>\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text-card \">20</span>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                <div class=\"input-group mb-3\">\r\n                  <label class=\"form-control color-white\" style=\"background-color: #5FC58D;\">\r\n                    Number of Ticket</label>\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text-card \">20</span>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </slide>\r\n          <slide>\r\n            <div class=\"col-xl-3 col-lg-6 display-line\" data-aos=\"fade-up\">\r\n              <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                <div class=\"input-group mb-3\">\r\n                  <label class=\"form-control color-white\" style=\"background-color: #E6315B;\">\r\n                    Number of Ticket</label>\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text-card\">20</span>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                <div class=\"input-group mb-3\">\r\n                  <label class=\"form-control color-white\" style=\"background-color: #E96734;\">\r\n                    Number of Ticket</label>\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text-card\"> 20 </span>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                <div class=\"input-group mb-3\">\r\n                  <label class=\"form-control color-white\" style=\"background-color: #66CED9;\">Number of Ticket</label>\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text-card\"> 20 </span>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </slide>\r\n        </div>\r\n      </carousel>\r\n    </section>\r\n  </div>\r\n</div>\r\n\r\n<section class=\"main-slider\">\r\n  <carousel [interval]=\"false\">\r\n    <slide>\r\n      <img src=\"https://picsum.photos/id/1/900/500\" alt=\"First slide\" style=\"display: block; width: 100%; \">\r\n    </slide>\r\n    <slide>\r\n      <img src=\"https://picsum.photos/id/1026/900/500\" alt=\"Second slide\" style=\"display: block; width: 100%;\">\r\n    </slide>\r\n    <slide>\r\n      <img src=\"https://picsum.photos/id/1031/900/500\" alt=\"Third slide\" style=\"display: block; width: 100%;\">\r\n    </slide>\r\n  </carousel>\r\n</section>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/nav/nav.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nav/nav.component.html ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<style>\r\n\r\n    * {\r\n  \r\n      box-sizing: border-box\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    body {\r\n  \r\n      font-family: Verdana, sans-serif;\r\n  \r\n      margin: 0\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    .mySlides {\r\n  \r\n      display: none\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    img {\r\n  \r\n      width: 500px;\r\n  \r\n      display: block;\r\n  \r\n      margin-left: auto;\r\n  \r\n      margin-right: auto;\r\n  \r\n      background-color: var(--air) !important;\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    /* Position the \"next button\" to the right */\r\n  \r\n    .next {\r\n  \r\n      right: 0;\r\n  \r\n      border-radius: 3px 0 0 3px;\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    /* On hover, add a black background color with a little bit see-through */\r\n  \r\n    .prev:hover,\r\n  \r\n    .next:hover {\r\n  \r\n      background-color: rgba(0, 0, 0, 0.8);\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    /* Caption text */\r\n  \r\n    .text {\r\n  \r\n      color: #f2f2f2;\r\n  \r\n      font-size: 15px;\r\n  \r\n      padding: 8px 12px;\r\n  \r\n      position: absolute;\r\n  \r\n      bottom: 8px;\r\n  \r\n      width: 10%;\r\n  \r\n      text-align: center;\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    /* On smaller screens, decrease text size */\r\n  \r\n    @media only screen and (max-width: 300px) {\r\n  \r\n   \r\n  \r\n      .prev,\r\n  \r\n      .next,\r\n  \r\n      .text {\r\n  \r\n        font-size: 11px\r\n  \r\n      }\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    .dropbtn {\r\n  \r\n      background-color:#FF375D;\r\n  \r\n      color: white;\r\n  \r\n      padding: 16px;\r\n  \r\n      font-size: 16px;\r\n  \r\n      border-radius: 30px;\r\n  \r\n      cursor: pointer;\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    .dropdown {\r\n  \r\n      position: relative;\r\n  \r\n      display: inline-block;\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    .dropdown-content {\r\n  \r\n      display: none;\r\n  \r\n      position: absolute;\r\n  \r\n      background-color: #f9f9f9;\r\n  \r\n      min-width: 160px;\r\n  \r\n      box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\r\n  \r\n      z-index: 1;\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    .dropdown-content a:hover {\r\n  \r\n      background-color: #f1f1f1\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    .dropdown:hover .dropdown-content {\r\n  \r\n      display: block;\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    .dropdown:hover .dropbtn {\r\n  \r\n      background-color: #FF375D;\r\n  \r\n    }\r\n  \r\n  </style>\r\n  \r\n  \r\n  \r\n  \r\n  <div class=\"navbar\" style=\"font-weight: 600; background-color: #4F008C;\">\r\n  \r\n    <div class=\"text-center text-muted mb-4\">\r\n  \r\n      <img src=\"./assets/FA_STC_RGB_NEG.ico\" style=\"width: 114px;\">\r\n      \r\n  \r\n    </div>\r\n\r\n\r\n\r\n    \r\n  \r\n    <!-- <a href=\"#home\">Home</a>\r\n  \r\n    <a href=\"#news\">News</a> -->\r\n  \r\n    <div class=\"dropdown\">\r\n  \r\n      <button class=\"dropbtn\">Report</button>\r\n  \r\n      <div class=\"dropdown-content\">\r\n  \r\n        <li>\r\n  \r\n          \r\n  \r\n        </li>\r\n  \r\n      </div>\r\n  \r\n    </div>\r\n  \r\n   \r\n\r\n\r\n\r\n    \r\n  \r\n    <div class=\"dropdown\">\r\n  \r\n      <button class=\"dropbtn\">CDR</button>\r\n  \r\n      <div class=\"dropdown-content\">\r\n  \r\n   \r\n  \r\n      </div>\r\n  \r\n    </div>\r\n  \r\n   \r\n  \r\n    <div class=\"dropdown\">\r\n  \r\n      <button class=\"dropbtn\">Log out</button>\r\n  \r\n      <div class=\"dropdown-content\">\r\n  \r\n      </div>\r\n  \r\n   \r\n  \r\n    </div>\r\n  \r\n  </div>\r\n  \r\n  <ngb-carousel>\r\n  \r\n    <ng-template ngbSlide>\r\n  \r\n      <div class=\"picsum-img-wrapper\">\r\n  \r\n        <img\r\n  \r\n          src=\"https://www.stc.com.sa/wps/wcm/connect/arabic/individual/resources/3/5/35d5b96b-99ac-4e1a-ba70-841acda4bed2/640x466_Ar.png\"\r\n  \r\n          alt=\"Angular Carousel 1\">\r\n  \r\n      </div>\r\n  \r\n  \r\n     \r\n  \r\n  \r\n        <h1 class=\"purpletext text  animate coverimage fadeInUp\" data-animation=\"fadeInUp\" data-delay=\"200\" style=\"background-color:#FF375D\">عروضنا تجيك وأنت في بيتك مع تطبيق mystc</h1>\r\n      \r\n     \r\n      <div class=\"carousel-caption\">\r\n  \r\n       \r\n      </div>\r\n  \r\n    </ng-template>\r\n  \r\n    <ng-template ngbSlide>\r\n  \r\n      <div class=\"picsum-img-wrapper\">\r\n  \r\n        <img\r\n  \r\n          src=\"https://www.stc.com.sa/wps/wcm/connect/arabic/individual/resources/4/5/455d19ba-5a8b-426c-b88f-a228239eafef/Corporate-Banners-640x466.png\"\r\n  \r\n          border=\"0\" alt=\"خصم %50 إنترنت لامحدود مع بيتي فايبر 100\" width=\"2000\" height=\"490\"\r\n  \r\n          title=\"خصم %50 إنترنت لامحدود مع بيتي فايبر 100\">\r\n  \r\n   \r\n  \r\n      </div>\r\n  \r\n      <div class=\"carousel-caption\">\r\n  \r\n      <h6 style=\"color: #FF375D;\">WBU PORTAL</h6>\r\n  \r\n     \r\n  \r\n      </div>\r\n  \r\n    </ng-template>\r\n  \r\n  </ngb-carousel>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/new-partner/new-partner.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/new-partner/new-partner.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"app-body\">\r\n  <img src=\"/assets/stcLOGO.png\" width=\"150px\" height=\"150px\">\r\n\r\n  <main class=\"main d-flex align-items-center\" style=\"min-height: 100vh;background-color: #4F008C;\">\r\n\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6 mx-auto\">\r\n          <div class=\"card mx-4\">\r\n            <div class=\"card-body p-4\">\r\n              <form [formGroup]=\"partnerForm\" (ngSubmit)=\"newPartner()\"  (keydown.enter)=\"newPartner()\" autocomplete=\"off\">\r\n                <div class=\"h2\"> {{'NEW_PARTNER.TITLE'| translate}} </div>\r\n                <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                  <span class=\"mb-0 text-sm  font-weight-bold\">{{'NEW_PARTNER.FIRST_NAME'| translate}}</span>\r\n                  <div class=\"input-group mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\">\r\n                        <i class=\"fa fa-user\" aria-hidden=\"true\"></i>\r\n                      </span>\r\n                    </div>\r\n                    <input type=\"text\" maxlength=\"10\" onpaste=\"return false;\" (keypress)=\"trueChart($event)\"  class=\"form-control\" placeholder=\"{{'NEW_PARTNER.FIRST_NAME'| translate}}\"\r\n                      formControlName=\"fName\" autocomplete=\"off\" required>\r\n                      <div *ngIf=\"partnerForm.invalid  && isRegister\" style=\"width: 100%;\">\r\n                        <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('fName').errors.required\">First Name Is Required\r\n                        </div>\r\n                        <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('fName').errors.fName\">First Name Is Not Valid</div>\r\n                      </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                  <span class=\"mb-0 text-sm  font-weight-bold\">{{'NEW_PARTNER.LAST_NAME'| translate}}</span>\r\n                  <div class=\"input-group mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\">\r\n                        <i class=\"fa fa-user\" aria-hidden=\"true\"></i>\r\n                      </span>\r\n                    </div>\r\n                    <input type=\"text\" maxlength=\"10\" onpaste=\"return false;\" (keypress)=\"trueChart($event)\"  class=\"form-control\" placeholder=\"{{'NEW_PARTNER.LAST_NAME'| translate}}\"\r\n                      formControlName=\"lName\" autocomplete=\"off\" required>\r\n                      <div *ngIf=\"partnerForm.invalid  && isRegister\" style=\"width: 100%;\">\r\n                        <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('lName').errors.required\">Last Name Is Required\r\n                        </div>\r\n                        <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('lName').errors.lName\">Last Name Is Not Valid</div>\r\n                      </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                  <span class=\"mb-0 text-sm  font-weight-bold\">{{'NEW_PARTNER.EMAIL'| translate}}</span>\r\n                  <div class=\"input-group mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\">\r\n                        <i class=\"fa fa-envelope\" aria-hidden=\"true\"></i>\r\n                      </span>\r\n                    </div>\r\n                    <input type=\"email\" maxlength=\"25\" onpaste=\"return false;\" class=\"form-control\" (keypress)=\"trueChartEmail($event)\" formControlName=\"email\"\r\n                      placeholder=\"{{'NEW_PARTNER.EMAIL'| translate}}\" autocomplete=\"off\" required>\r\n                      <div *ngIf=\"partnerForm.invalid  && isRegister\" style=\"width: 100%;\">\r\n                        <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('email').errors.required\">Email Is Required\r\n                        </div>\r\n                        <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('email').errors.email\">Email Is Not Valid</div>\r\n                      </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                  <span class=\"mb-0 text-sm  font-weight-bold\">{{'NEW_PARTNER.COMPANY'| translate}}</span>\r\n                  <div class=\"input-group mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\">\r\n                        <i class=\"fa fa-building\" aria-hidden=\"true\"></i>\r\n                      </span>\r\n                    </div>\r\n                    <input type=\"text\" maxlength=\"30\" onpaste=\"return false;\" class=\"form-control\" (keypress)=\"trueChart($event)\" formControlName=\"company\"\r\n                      placeholder=\"{{'NEW_PARTNER.COMPANY'| translate}}\" autocomplete=\"off\" autopostback=\"ture\" required>\r\n                      <div *ngIf=\"partnerForm.invalid  && isRegister\" style=\"width: 100%;\">\r\n                        <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('company').errors.required\">Company Name Is Required\r\n                        </div>\r\n                        <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('company').errors.company\">Company Name Is Not Valid</div>\r\n                      </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                  <span class=\"mb-0 text-sm  font-weight-bold\">{{'NEW_PARTNER.REGION'| translate}}</span>\r\n                  <div class=\"form-group\">\r\n                    <select class=\"form-control\" id=\"exampleFormControlSelect1\"\r\n                      (change)=\"selectRegion()\">\r\n                      <option *ngFor=\"let reg of listRegion \" [value]=\"reg.value\">{{reg.display}}</option>\r\n                    </select>\r\n                  </div>\r\n                </div>\r\n                <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                <span class=\"mb-0 text-sm  font-weight-bold\">{{'NEW_PARTNER.COUNTRY'| translate}}</span>\r\n                <div class=\" country-picker\">\r\n                  <div class=\"picker-button\" (click)=\"isCountryOpen = !isCountryOpen\">\r\n                    <i class=\"fa fa-globe\" aria-hidden=\"true\"></i>\r\n                  </div>\r\n                  <div class=\"current\">\r\n                    <img [src]=\"selectedCountry.flag\" alt=\"\">\r\n                    <span>{{selectedCountry.name}}</span>\r\n                  </div>\r\n                  <ul *ngIf=\"isCountryOpen\">\r\n                    <li *ngFor=\"let country of listCountries\" (click)=\"selectCountry(country)\">\r\n                      <img [src]=\"country.flag\" alt=\"\">\r\n                      <span>{{country.name}}</span>\r\n                    </li>\r\n                  </ul>\r\n                </div>\r\n              </div>\r\n                <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                  <span class=\"mb-0 text-sm  font-weight-bold\">{{'NEW_PARTNER.PHONE'| translate}}</span>\r\n                  <div class=\"input-group mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\">\r\n                        <i class=\"fa fa-phone\" aria-hidden=\"true\"></i>\r\n                        <!-- <i class=\"icon-phone\"></i> -->\r\n                      </span>\r\n                    </div>\r\n                    <input type=\"text\" class=\"form-control\" (keypress)=\"numberOnly($event)\" maxlength=\"15\" placeholder=\"{{'NEW_PARTNER.PHONE'| translate}}\"\r\n                      formControlName=\"phone\" autocomplete=\"off\" onpaste=\"return false;\" required>\r\n                      <div *ngIf=\"partnerForm.invalid  && isRegister\" style=\"width: 100%;\">\r\n                        <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('phone').errors.required\">Phone Is Required\r\n                        </div>\r\n                        <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('phone').errors.fName\">Phone Is Not Valid</div>\r\n                      </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\" text-center\">\r\n                  <button class=\"btn btn-primary\" type=\"submit\" style=\"background-color: #ff375d;\">Register</button>\r\n                  <button class=\"btn btn-primary\" type=\"button\" style=\"background-color:#ff375d;\"> <a style=\"color: white;\" [routerLink]=\"['/login']\"> Cancel</a></button>\r\n                </div>\r\n              </form>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/ttt/ttt.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/ttt/ttt.component.html ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>ttt works!</p>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/upload/targer-user.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/upload/targer-user.component.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1 mat-dialog-title>User Information</h1>\r\n<div mat-dialog-content>\r\n  Last login:\r\n  <ul>\r\n    <li>\r\n      <span >&#10003;</span> 11:00 PM\r\n    </li>\r\n  </ul>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/upload/upload.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/upload/upload.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ngb-tabset [destroyOnHide]=\"false\">\r\n  <ngb-tab>\r\n    <ng-template ngbTabTitle><b>Billing</b> </ng-template>\r\n    <ng-template ngbTabContent>\r\n      <div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-8\">\r\n        <div class=\"container mt--8 pb-5\">\r\n          <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-8 col-md-7\">\r\n              <div class=\"card bg-secondary shadow border-0\">\r\n                <div class=\"header-body\">\r\n                  <!-- Table -->\r\n                  <form [formGroup]=\"billingForm\" (ngSubmit)=\"save()\" autocomplete=\"off\">\r\n\r\n                    <div class=\"card shadow\" style=\"background-color: #f7fafc;\">\r\n                      <div class=\"card-header border-0\">\r\n                        <div class=\"row align-items-center\">\r\n                          <div class=\"col\">\r\n\r\n                            <h3 class=\"mb-0\">CDR Billing Contact </h3>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"row\">\r\n\r\n                        <div class=\"col-lg-4\">\r\n                          <label for=\"input-city\" class=\"form-control-label\">Invoice Number</label>\r\n                          <select class=\"form-control\" id=\"yyy\">\r\n                            <option>0000 0000 1233 2212</option>\r\n                            <option>0000 4452 1233 2212</option>\r\n                            <option>0000 0000 7512 2212</option>\r\n                            <option>0000 0000 1233 5823</option>\r\n\r\n                          </select>\r\n                        </div>\r\n\r\n                        <div class=\"form-group hidden\">\r\n                          <div class=\"input-group\">\r\n                            <input name=\"datepicker\" maxlength=\"10\" class=\"form-control\" ngbDatepicker\r\n                              #datepicker=\"ngbDatepicker\" [autoClose]=\"'outside'\" (dateSelect)=\"onDateSelection($event)\"\r\n                              [displayMonths]=\"2\" [dayTemplate]=\"t\" outsideDays=\"hidden\" [startDate]=\"fromDate!\">\r\n                            <ng-template #t let-date let-focused=\"focused\">\r\n                              <span class=\"custom-day\" [class.focused]=\"focused\" [class.range]=\"isRange(date)\"\r\n                                [class.faded]=\"isHovered(date) || isInside(date)\" (mouseenter)=\"hoveredDate = date\"\r\n                                (mouseleave)=\"hoveredDate = null\">\r\n                                {{ date.day }}\r\n                              </span>\r\n                            </ng-template>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"form-group col-lg-4\">\r\n                          <label class=\"form-control-label\" for=\"input-city\">{{\"date from\" | titlecase}}</label>\r\n                          <div class=\"input-group\">\r\n                            <input #dpFromDate class=\"form-control\" maxlength=\"10\" placeholder=\"yyyy-mm-dd\"\r\n                              name=\"dpFromDate\" [value]=\"formatter.format(fromDate)\"\r\n                              (input)=\"fromDate = validateInput(fromDate, dpFromDate.value)\">\r\n                            <div class=\"input-group-append\">\r\n                              <button class=\"btn btn-outline-secondary calendar\" (click)=\"datepicker.toggle()\"\r\n                                type=\"button\">\r\n                                <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                              </button>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"form-group col-lg-4\">\r\n                          <label class=\"form-control-label\" for=\"input-city\">{{\"date to\" | titlecase}}\r\n                          </label>\r\n                          <div class=\"input-group\">\r\n                            <input #dpToDate class=\"form-control\" maxlength=\"10\" placeholder=\"yyyy-mm-dd\"\r\n                              name=\"dpToDate\" [value]=\"formatter.format(toDate)\"\r\n                              (input)=\"toDate = validateInput(toDate, dpToDate.value)\">\r\n                            <div class=\"input-group-append\">\r\n                              <button class=\"btn btn-outline-secondary calendar\" (click)=\"datepicker.toggle()\"\r\n                                type=\"button\">\r\n                                <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                              </button>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-lg-4\">\r\n                          <label for=\"ccnumber\" class=\"form-control-label\">Product</label>\r\n\r\n                          <select class=\"form-control\" id=\"yyy\">\r\n                            <option>Voice</option>\r\n                            <option>SMS</option>\r\n                            <option>MMS</option>\r\n                            <option>Other</option>\r\n\r\n                          </select>\r\n                        </div>\r\n\r\n                        <div class=\"col-lg-4\">\r\n                          <label for=\"ccnumber\" class=\"form-control-label\">Target User</label>\r\n                          <div class=\"form-group\"><input class=\"form-control form-control-alternative\" maxlength=\"15\"\r\n                              id=\"input-city\" placeholder=\"SAAD\" type=\"text\" readonly>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col-lg-4\">\r\n                          <div class=\"form-group\">\r\n                            <label for=\"exampleFormControlTextarea1\" class=\"form-control-label\">Description</label>\r\n                            <textarea class=\"form-control\" id=\"exampleFormControlTextarea1\" rows=\"3\"></textarea>\r\n                          </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-lg-4\">\r\n                          <label class=\"form-control-label\"> Upload Files</label>\r\n                          <div class=\"form-group\">\r\n                            <input #file1 type=\"file\" ng2FileSelect [uploader]=\"uploader\"\r\n                              (change)=\"getFileDetailF($event)\" />\r\n                            <div class=\"col-md-9\">\r\n                              <table class=\"table-headed\">\r\n                                <tr>\r\n                                  <th>Size: </th>\r\n                                  <th>{{ size1}}</th>\r\n                                </tr>\r\n                                <tr>\r\n                                  <!-- <th >Modified Date: </th> -->\r\n                                  <!-- <th >{{ modifiedDate1 | date: 'dd MMM yyyy'}}</th> -->\r\n                                </tr>\r\n                              </table>\r\n                            </div>\r\n                          </div>\r\n                          <div class=\"form-group\">\r\n                            <input #file2 type=\"file\" ng2FileSelect [uploader]=\"uploader\"\r\n                              (change)=\"getFileDetailS($event)\" />\r\n                            <div class=\"col-md-9\">\r\n                              <table class=\"table-headed\">\r\n                                <thead>\r\n                                  <tr>\r\n                                    <th class=\"text-left\">Size: {{ size2}}</th>\r\n                                  </tr>\r\n                                </thead>\r\n                              </table>\r\n                            </div>\r\n                          </div>\r\n                          <div class=\"form-group\">\r\n                            <input #file3 type=\"file\" ng2FileSelect [uploader]=\"uploader\"\r\n                              (change)=\"getFileDetailTH($event)\" />\r\n                            <div class=\"col-md-9\">\r\n                              <table class=\"table-headed\">\r\n                                <thead>\r\n                                  <tr>\r\n                                    <th class=\"text-left\">Size: {{ size3}}</th>\r\n                                  </tr>\r\n                                </thead>\r\n                              </table>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"row\">\r\n                        <div class=\"col\">\r\n                          <div class=\"text-center\">\r\n                            <button class=\"btn submit my-4 \">Submit</button>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </form>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n    </ng-template>\r\n  </ngb-tab>\r\n  <ngb-tab>\r\n    <ng-template ngbTabTitle><b>Technical </b> </ng-template>\r\n    <ng-template ngbTabContent>\r\n      <div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-8\">\r\n        <div class=\"container mt--8 pb-5\">\r\n          <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-8 col-md-7\">\r\n              <div class=\"card bg-secondary shadow border-0\">\r\n                <div class=\"header-body\">\r\n                  <!-- Table -->\r\n                  <div class=\"card shadow\">\r\n                    <div class=\"card-header border-0\">\r\n                      <div class=\"row align-items-center\">\r\n                        <div class=\"col\">\r\n\r\n                          <h3 class=\"mb-0\">CDR Technical Contact </h3>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                      <div class=\"col-lg-4\">\r\n                        <label for=\"input-city\" class=\"form-control-label\">Invoice Number</label>\r\n                        <select class=\"form-control\" id=\"yyy\">\r\n                          <option>0000 0000 1233 2212</option>\r\n                          <option>0000 4452 1233 2212</option>\r\n                          <option>0000 0000 7512 2212</option>\r\n                          <option>0000 0000 1233 5823</option>\r\n\r\n                        </select>\r\n                      </div>\r\n\r\n                      <div class=\"form-group hidden\">\r\n                        <div class=\"input-group\">\r\n                          <input name=\"datepicker\" maxlength=\"10\" class=\"form-control\" ngbDatepicker\r\n                            #datepicker=\"ngbDatepicker\" [autoClose]=\"'outside'\" (dateSelect)=\"onDateSelection($event)\"\r\n                            [displayMonths]=\"2\" [dayTemplate]=\"t\" outsideDays=\"hidden\" [startDate]=\"fromDate!\">\r\n                          <ng-template #t let-date let-focused=\"focused\">\r\n                            <span class=\"custom-day\" [class.focused]=\"focused\" [class.range]=\"isRange(date)\"\r\n                              [class.faded]=\"isHovered(date) || isInside(date)\" (mouseenter)=\"hoveredDate = date\"\r\n                              (mouseleave)=\"hoveredDate = null\">\r\n                              {{ date.day }}\r\n                            </span>\r\n                          </ng-template>\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"form-group col-lg-4\">\r\n                        <label class=\"form-control-label\" for=\"input-city\">{{\"date from\" | titlecase}}</label>\r\n                        <div class=\"input-group\">\r\n                          <input #dpFromDate class=\"form-control\" maxlength=\"10\" placeholder=\"yyyy-mm-dd\"\r\n                            name=\"dpFromDate\" [value]=\"formatter.format(fromDate)\"\r\n                            (input)=\"fromDate = validateInput(fromDate, dpFromDate.value)\">\r\n                          <div class=\"input-group-append\">\r\n                            <button class=\"btn btn-outline-secondary calendar\" (click)=\"datepicker.toggle()\"\r\n                              type=\"button\">\r\n                              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                            </button>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"form-group col-lg-4\">\r\n                        <label class=\"form-control-label\" for=\"input-city\">{{\"date to\" | titlecase}}\r\n                        </label>\r\n                        <div class=\"input-group\">\r\n                          <input #dpToDate class=\"form-control\" maxlength=\"10\" placeholder=\"yyyy-mm-dd\" name=\"dpToDate\"\r\n                            [value]=\"formatter.format(toDate)\" (input)=\"toDate = validateInput(toDate, dpToDate.value)\">\r\n                          <div class=\"input-group-append\">\r\n                            <button class=\"btn btn-outline-secondary calendar\" (click)=\"datepicker.toggle()\"\r\n                              type=\"button\">\r\n                              <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                            </button>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n\r\n                      <div class=\"col-lg-4\">\r\n                        <label for=\"ccnumber\" class=\"form-control-label\">Product</label>\r\n\r\n                        <select class=\"form-control\" id=\"yyy\">\r\n                          <option>Voice</option>\r\n                          <option>SMS</option>\r\n                          <option>MMS</option>\r\n                          <option>Other</option>\r\n                        </select>\r\n                      </div>\r\n\r\n                      <!-- <div class=\"col-lg-4\">\r\n                  <label for=\"ccnumber\" class=\"form-control-label\">Target User</label>\r\n\r\n                  <mat-select class=\"form-control\" (selectionChange)=\"openDialog()\">\r\n                    <mat-option disabled selected value></mat-option>\r\n                    <mat-option>Ali</mat-option>\r\n                    <mat-option>Mohammed</mat-option>\r\n                    <mat-option>Saad</mat-option>\r\n                    <mat-option>Saud</mat-option>\r\n\r\n                  </mat-select>\r\n                </div> -->\r\n                      <div class=\"col-lg-4\">\r\n                        <div class=\"form-group\">\r\n                          <label for=\"exampleFormControlTextarea1\" class=\"form-control-label\">Description</label>\r\n                          <textarea class=\"form-control\" id=\"exampleFormControlTextarea1\" rows=\"3\"></textarea>\r\n                        </div>\r\n                      </div>\r\n\r\n                      <div class=\"col-lg-4\">\r\n                        <label class=\"form-control-label\"> Upload File</label>\r\n                        <div class=\"form-group\">\r\n\r\n                          <input type=\"file\" ng2FileSelect [uploader]=\"uploader\" />\r\n                          <!-- <input type=\"file\" class=\"form-control form-control-alternative\" ng2FileSelect [uploader]=\"uploader\" multiple  /> -->\r\n\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                      <div class=\"col\">\r\n                        <div class=\"text-center\">\r\n                          <button class=\"btn submit my-4 \">Submit</button>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n    </ng-template>\r\n  </ngb-tab>\r\n</ngb-tabset>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/error/404.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/error/404.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"app flex-row align-items-center\">\r\n  <div class=\"container\">\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"col-md-6\">\r\n        <div class=\"clearfix\">\r\n          <h1 class=\"float-left display-3 mr-4\">404</h1>\r\n          <h4 class=\"pt-3\">Oops! You're lost.</h4>\r\n          <p class=\"text-muted\">The page you are looking for was not found.</p>\r\n        </div>\r\n        <div class=\"input-prepend input-group\">\r\n          <div class=\"input-group-prepend\">\r\n            <span class=\"input-group-text\"><i class=\"fa fa-search\"></i></span>\r\n          </div>\r\n          <input id=\"prependedInput\" class=\"form-control\" size=\"16\" type=\"text\" placeholder=\"What are you looking for?\">\r\n          <span class=\"input-group-append\">\r\n            <button class=\"btn btn-info\" type=\"button\">Search</button>\r\n          </span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/error/500.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/error/500.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"app flex-row align-items-center\">\r\n  <div class=\"container\">\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"col-md-6\">\r\n        <div class=\"clearfix\">\r\n          <h1 class=\"float-left display-3 mr-4\">500</h1>\r\n          <h4 class=\"pt-3\">Houston, we have a problem!</h4>\r\n          <p class=\"text-muted\">The page you are looking for is temporarily unavailable.</p>\r\n        </div>\r\n        <div class=\"input-prepend input-group\">\r\n          <div class=\"input-group-prepend\">\r\n            <span class=\"input-group-text\"><i class=\"fa fa-search\"></i></span>\r\n          </div>\r\n          <input id=\"prependedInput\" class=\"form-control\" size=\"16\" type=\"text\" placeholder=\"What are you looking for?\">\r\n          <span class=\"input-group-append\">\r\n            <button class=\"btn btn-info\" type=\"button\">Search</button>\r\n          </span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/forgot-password/forgot-password.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/forgot-password/forgot-password.component.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header bg-gradient-danger pb-5 pt-5 \">\n    <div class=\"container-fluid\">\n        <div class=\"header-body\" style=\"background-color: #4F008C;\">\n            <img src=\"./assets/FA_STC_RGB_NEG.ico\" style=\"width: 114px;\">\n        </div>\n    </div>\n</div>\n<div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-8\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\">\n    <div class=\"container mt--8 pb-5\">\n        <div class=\"row justify-content-center\">\n            <div class=\"col-lg-5 col-md-7\">\n                <div class=\"card bg-secondary shadow border-0\">\n                    <div class=\"card-body px-lg-5 py-lg-5\">\n                        <div class=\"text-center text-muted mb-4\">\n                            <h3 style=\"color: #4F008C;\"><i class=\"fa fa-lock fa-4x\"></i></h3>\n                        </div>\n                        <form [formGroup]=\"forgetForm\" (ngSubmit)=\"forgot()\" (keydown.enter)=\"forgot()\" autocomplete=\"off\">\n                            <h2 class=\"text-center h2 font-weight-bold mb-0\"> Forgot Password?</h2>\n                            <h3 class=\"text-center h2 font-weight-bold mb-0\"> <small>You can reset your password here.\n                                </small></h3>\n                            <div class=\"form-group mb-3\">\n                                <div class=\"input-group input-group-alternative\">\n                                    <div class=\"input-group-prepend\">\n                                        <span class=\"input-group-text\"><i class=\"ni ni-email-83\"></i></span>\n                                    </div>\n                                    <input type=\"email\" (keypress)=\"trueChartEmail($event)\" formControlName=\"email\"\n                                        maxlength=\"25\" class=\"form-control\" autocomplete=\"off\"\n                                        placeholder=\"Email Address\" (focus)=\"focus=true\" onpaste=\"return false;\"\n                                        (blur)=\"focus=false\">\n                                    <div *ngIf=\"forgetForm.invalid && isRegister\" style=\"width: 100%;\">\n                                        <div class=\"invalid-custom\" *ngIf=\"forgetForm.get('email').errors.required\">\n                                            Email Is Required\n                                        </div>\n                                        <div class=\"invalid-custom\" *ngIf=\"forgetForm.get('email').errors.email\">\n                                            Email Is Not Valid</div>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"text-center\">\n                                <button type=\"submit\" id=\"myBtn\" class=\"btn btn-primary\"\n                                    style=\"background-color:#ff375d;\">Send</button>\n                                <button type=\"button\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"><a\n                                        style=\"color: white;\" [routerLink]=\"['/login']\">Cancel</a></button>\n                            </div>\n                        </form>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/login/login.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/login/login.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"app-body\">\r\n  <img src=\"/assets/stcLOGO.png\" width=\"150px\" height=\"150px\">\r\n\r\n  <main class=\"main d-flex align-items-center\" style=\"background-color: #4F008C;\">\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-8 mx-auto\">\r\n          <div class=\"card-group\">\r\n            <div class=\"card p-4\">\r\n              <div class=\"card-body\">\r\n                <form [formGroup]=\"login\" (ngSubmit)=\"onSubmit()\" (keydown.enter)=\"onSubmit()\"autocomplete=\"off\">\r\n                  <h3 style=\"font-family: inherit; font-weight: 600; line-height: 1.5; font-size: 1.25rem;\">\r\n                    {{'LOGIN.TITLE'| translate}}</h3>\r\n                  <!-- Sign In to your account</p> -->\r\n                  <div class=\"input-group mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\"><i class=\"fa fa-envelope\"></i></span>\r\n                    </div>\r\n                    <input type=\"text\" id=\"myInput\"  class=\"form-control\" (keypress)=\"trueChartEmail($event)\" formControlName=\"email\" [(ngModel)]=\"username\"\r\n                      maxlength=\"30\" placeholder=\"Email \" required>\r\n                    <div *ngIf=\"login.invalid  && isSubmited\" style=\"width: 100%;\">\r\n                      <div class=\"invalid-custom\" *ngIf=\"login.get('email').errors.required\">Email Is Required</div>\r\n                      <div class=\"invalid-custom\" *ngIf=\"login.get('email').errors.email\">Email Is Not Valid</div>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"input-group mb-4\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n                    </div>\r\n                    <input type=\"password\" class=\"form-control\" formControlName=\"email\" [(ngModel)]=\"password\"\r\n                      placeholder=\"Password\" maxlength=\"25\" required>\r\n                  </div>\r\n                  <div class=\"text-center\">\r\n                    <button type=\"button\" id=\"myBtn\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"\r\n                      (click)=\"onSubmit()\">{{'LOGIN.SIGN_IN'| translate}}</button>\r\n\r\n                    <button type=\"button\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"><a\r\n                        style=\"color: white;\" [routerLink]=\"['/register']\">{{'LOGIN.NEW_USER'| translate}}</a></button>\r\n\r\n                    <button type=\"button\" class=\"btn btn-link px-0\" (click)=\"login()\"><a style=\"color: #FF375D;\"\r\n                        [routerLink]=\"['/forgot-password']\">{{'LOGIN.RESET_PASSWORD'| translate}}\r\n                      </a></button>\r\n                  </div>\r\n\r\n                </form>\r\n              </div>\r\n            </div>\r\n            <div class=\"card text-white  py-5 d-md-down-none\" style=\"background-color:#4F008C ;\">\r\n              <!-- <div class=\"card-body text-center\" > -->\r\n              <!-- <div>\r\n                  <h2 class=\"color-white\">{{'LOGIN.SIGN_UP'| translate}}</h2>\r\n                  <br>\r\n                  <button type=\"button\" class=\"btn btn-primary active mt-3\"\r\n                    style=\"background-color:#4F008C; border-color:#FF375D; \"> <a\r\n                      style=\"color: #FF375D; background-color:#4F008C; \"\r\n                      [routerLink]=\"['/register']\">{{'LOGIN.NEW_USER'| translate}}</a></button>\r\n                  <div style=\"color: white; margin-top: 4vh;\">\r\n                    <a [routerLink]=\"['/partner']\" style=\"color:white;\">{{'LOGIN.NEW_PARTNER'| translate}}</a>\r\n                  </div>\r\n                  <div style=\" margin-top: 10vh;\">\r\n                    <a *ngIf=\"!isRTL\" (click)=\"changeLang(arabic)\">\r\n                      <h3>العربية</h3>\r\n                    </a>\r\n                    <a *ngIf=\"isRTL\" (click)=\"changeLang(english)\">\r\n                      <h3>English</h3>\r\n                    </a>\r\n                  </div> -->\r\n              <!-- </div> -->\r\n              <!-- </div> -->\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/register/register.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/register/register.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"app-body\">\r\n  <img src=\"/assets/stcLOGO.png\" width=\"150px\" height=\"150px\">\r\n\r\n  <main class=\"main d-flex align-items-center\" style=\"background-color: #4F008C;\">\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6 mx-auto\">\r\n          <div class=\"card mx-4\">\r\n            <div class=\"card-body p-4\">\r\n              <form [formGroup]=\"registerForm\" (ngSubmit)=\"onRegister()\" (keydown.enter)=\"onRegister()\" autocomplete=\"off\">\r\n                <h2 style=\"font-family: inherit; font-weight: 600; line-height: 1.5; font-size: 1.25rem;\">{{'NEW_PARTNER.TITLE'| translate}}</h2>\r\n                <div class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"> <i class=\"icon-phone\"></i></span>\r\n                  </div>\r\n                  <input type=\"text\" maxlength=\"3\" autocomplete=\"off\" class=\"form-control\" onpaste=\"return false;\" (keypress)=\"trueChart($event)\" placeholder=\"Customer Code\" formControlName=\"code\"\r\n                    required>\r\n                  <div *ngIf=\"registerForm.invalid  && isRegister\" style=\"width: 100%;\">\r\n                    <div class=\"invalid-custom\" *ngIf=\"registerForm.get('code').errors.required\">Code Is Required</div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"> <i class=\"icon-phone\"></i>\r\n                    </span>\r\n                  </div>\r\n                  <input type=\"text\" autocomplete=\"off\" maxlength=\"15\" class=\"form-control\" onpaste=\"return false;\" (keypress)=\"numberOnly($event)\" placeholder=\"Phone number\" formControlName=\"phone\"\r\n                    required>\r\n                  <div *ngIf=\"registerForm.invalid  && isRegister\" style=\"width: 100%;\">\r\n                  <div class=\"invalid-custom\" *ngIf=\"registerForm.get('phone').errors.required\">Phone Is Required</div>\r\n                  <div class=\"invalid-custom\" *ngIf=\"registerForm.get('phone').errors.phone\">Email Is Not Valid</div>\r\n\r\n                </div>\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\">@</span>\r\n                  </div>\r\n                  <input type=\"email\" class=\"form-control\" autocomplete=\"off\" maxlength=\"25\" onpaste=\"return false;\" (keypress)=\"trueChartEmail($event)\" formControlName=\"Email\" placeholder=\"Email \"\r\n                    required>\r\n                  <div *ngIf=\"registerForm.invalid && isRegister\" style=\"width: 100%;\">\r\n                    <div class=\"invalid-custom\" *ngIf=\"registerForm.get('Email').errors.required\">Email Is Required\r\n                    </div>\r\n                    <div class=\"invalid-custom\" *ngIf=\"registerForm.get('Email').errors.email\">Email Is Not Valid</div>\r\n                  </div>\r\n                </div>\r\n                <div class=\" text-center\">\r\n                  <button class=\"btn btn-primary\" type=\"submit\" style=\"background-color:#ff375d;\">Register</button>\r\n                  <button class=\"btn btn-primary\" type=\"button\" style=\"background-color:#ff375d;\"> <a style=\"color: white;\" [routerLink]=\"['/login']\"> Cancel</a></button>\r\n                  <button class=\"btn btn-link px-0\" type=\"button\"> <a style=\"color: #FF375D;\" [routerLink]=\"['/partner']\"> {{'LOGIN.NEW_PARTNER'| translate}}</a></button>\r\n                </div>  \r\n              </form>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n</div>");

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/Admin-Portal/approve-registration/approve-registration.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/Admin-Portal/approve-registration/approve-registration.component.css ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("p {\r\n    font-family: Lato;\r\n  }\r\n  \r\n  table.sjs-table > tbody > tr:first-child > td {\r\n       background-color: lightgreen;\r\n  }\r\n  \r\n  .mb-0{\r\n   margin-bottom: 0.5rem;\r\n   font-weight: 600;\r\n   line-height: 1.5;\r\n   color: #32325d;\r\n  }\r\n  \r\n  .row{\r\n   margin: 0;\r\n   font-family: myFirstFont, sans-serif;\r\n   font-size: 1rem;\r\n   font-weight: 400;\r\n   line-height: 1.5;\r\n   color: #525f7f;\r\n   text-align: left;\r\n  }\r\n  \r\n  .custom-control-label::after {\r\n   border-style: groove;\r\n\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL2FwcHJvdmUtcmVnaXN0cmF0aW9uL2FwcHJvdmUtcmVnaXN0cmF0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxpQkFBaUI7RUFDbkI7O0VBRUE7T0FDSyw0QkFBNEI7RUFDakM7O0VBRUE7R0FDQyxxQkFBcUI7R0FDckIsZ0JBQWdCO0dBQ2hCLGdCQUFnQjtHQUNoQixjQUFjO0VBQ2Y7O0VBQ0E7R0FDQyxTQUFTO0dBQ1Qsb0NBQW9DO0dBQ3BDLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZ0JBQWdCO0dBQ2hCLGNBQWM7R0FDZCxnQkFBZ0I7RUFDakI7O0VBRUE7R0FDQyxvQkFBb0I7O0VBRXJCIiwiZmlsZSI6InNyYy9hcHAvQWRtaW4tUG9ydGFsL2FwcHJvdmUtcmVnaXN0cmF0aW9uL2FwcHJvdmUtcmVnaXN0cmF0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJwIHtcclxuICAgIGZvbnQtZmFtaWx5OiBMYXRvO1xyXG4gIH1cclxuICBcclxuICB0YWJsZS5zanMtdGFibGUgPiB0Ym9keSA+IHRyOmZpcnN0LWNoaWxkID4gdGQge1xyXG4gICAgICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmVlbjtcclxuICB9XHJcblxyXG4gIC5tYi0we1xyXG4gICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XHJcbiAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgIGNvbG9yOiAjMzIzMjVkO1xyXG4gIH1cclxuICAucm93e1xyXG4gICBtYXJnaW46IDA7XHJcbiAgIGZvbnQtZmFtaWx5OiBteUZpcnN0Rm9udCwgc2Fucy1zZXJpZjtcclxuICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICBmb250LXdlaWdodDogNDAwO1xyXG4gICBsaW5lLWhlaWdodDogMS41O1xyXG4gICBjb2xvcjogIzUyNWY3ZjtcclxuICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICB9XHJcblxyXG4gIC5jdXN0b20tY29udHJvbC1sYWJlbDo6YWZ0ZXIge1xyXG4gICBib3JkZXItc3R5bGU6IGdyb292ZTtcclxuXHJcbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/Admin-Portal/approve-registration/approve-registration.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/Admin-Portal/approve-registration/approve-registration.component.ts ***!
  \*************************************************************************************/
/*! exports provided: ApproveRegistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApproveRegistrationComponent", function() { return ApproveRegistrationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ApproveRegistrationComponent = /** @class */ (function () {
    function ApproveRegistrationComponent() {
    }
    ApproveRegistrationComponent.prototype.ngOnInit = function () {
    };
    ApproveRegistrationComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-approve-registration',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./approve-registration.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/approve-registration/approve-registration.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./approve-registration.component.css */ "./src/app/Admin-Portal/approve-registration/approve-registration.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], ApproveRegistrationComponent);
    return ApproveRegistrationComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/contact-admin/contact-admin.component.css":
/*!************************************************************************!*\
  !*** ./src/app/Admin-Portal/contact-admin/contact-admin.component.css ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("p {\r\n    font-family: Lato;\r\n  }\r\n  \r\n  table.sjs-table > tbody > tr:first-child > td {\r\n       background-color: lightgreen;\r\n  }\r\n  \r\n  .mb-0{\r\n   margin-bottom: 0.5rem;\r\n   font-weight: 600;\r\n   line-height: 1.5;\r\n   color: #32325d;\r\n  }\r\n  \r\n  .row{\r\n   margin: 0;\r\n   font-family: myFirstFont, sans-serif;\r\n   font-size: 1rem;\r\n   font-weight: 400;\r\n   line-height: 1.5;\r\n   color: #525f7f;\r\n   text-align: left;\r\n  }\r\n  \r\n  .custom-control-label::after {\r\n   border-style: groove;\r\n\r\n  }\r\n  \r\n  #outer\r\n{\r\n    width:100%;\r\n    text-align: center;\r\n}\r\n  \r\n  .inner\r\n{\r\n    display: inline-block;\r\n}\r\n  \r\n  .label-text-center{\r\n    display: block;\r\n    padding-top: 20px;\r\n}\r\n  \r\n  .btn{\r\n    background-color: #FF375E;\r\n    color: #fff;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL2NvbnRhY3QtYWRtaW4vY29udGFjdC1hZG1pbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0VBQ25COztFQUVBO09BQ0ssNEJBQTRCO0VBQ2pDOztFQUVBO0dBQ0MscUJBQXFCO0dBQ3JCLGdCQUFnQjtHQUNoQixnQkFBZ0I7R0FDaEIsY0FBYztFQUNmOztFQUNBO0dBQ0MsU0FBUztHQUNULG9DQUFvQztHQUNwQyxlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGdCQUFnQjtHQUNoQixjQUFjO0dBQ2QsZ0JBQWdCO0VBQ2pCOztFQUVBO0dBQ0Msb0JBQW9COztFQUVyQjs7RUFFQTs7SUFFRSxVQUFVO0lBQ1Ysa0JBQWtCO0FBQ3RCOztFQUNBOztJQUVJLHFCQUFxQjtBQUN6Qjs7RUFFQTtJQUNJLGNBQWM7SUFDZCxpQkFBaUI7QUFDckI7O0VBRUE7SUFDSSx5QkFBeUI7SUFDekIsV0FBVztBQUNmIiwiZmlsZSI6InNyYy9hcHAvQWRtaW4tUG9ydGFsL2NvbnRhY3QtYWRtaW4vY29udGFjdC1hZG1pbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsicCB7XHJcbiAgICBmb250LWZhbWlseTogTGF0bztcclxuICB9XHJcbiAgXHJcbiAgdGFibGUuc2pzLXRhYmxlID4gdGJvZHkgPiB0cjpmaXJzdC1jaGlsZCA+IHRkIHtcclxuICAgICAgIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0Z3JlZW47XHJcbiAgfVxyXG5cclxuICAubWItMHtcclxuICAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xyXG4gICBmb250LXdlaWdodDogNjAwO1xyXG4gICBsaW5lLWhlaWdodDogMS41O1xyXG4gICBjb2xvcjogIzMyMzI1ZDtcclxuICB9XHJcbiAgLnJvd3tcclxuICAgbWFyZ2luOiAwO1xyXG4gICBmb250LWZhbWlseTogbXlGaXJzdEZvbnQsIHNhbnMtc2VyaWY7XHJcbiAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgY29sb3I6ICM1MjVmN2Y7XHJcbiAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgfVxyXG5cclxuICAuY3VzdG9tLWNvbnRyb2wtbGFiZWw6OmFmdGVyIHtcclxuICAgYm9yZGVyLXN0eWxlOiBncm9vdmU7XHJcblxyXG4gIH1cclxuXHJcbiAgI291dGVyXHJcbntcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLmlubmVyXHJcbntcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG5cclxuLmxhYmVsLXRleHQtY2VudGVye1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBwYWRkaW5nLXRvcDogMjBweDtcclxufVxyXG5cclxuLmJ0bntcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNGRjM3NUU7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/Admin-Portal/contact-admin/contact-admin.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/Admin-Portal/contact-admin/contact-admin.component.ts ***!
  \***********************************************************************/
/*! exports provided: ContactAdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactAdminComponent", function() { return ContactAdminComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ContactAdminComponent = /** @class */ (function () {
    function ContactAdminComponent() {
    }
    ContactAdminComponent.prototype.ngOnInit = function () {
    };
    ContactAdminComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-contact-admin',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./contact-admin.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/contact-admin/contact-admin.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./contact-admin.component.css */ "./src/app/Admin-Portal/contact-admin/contact-admin.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], ContactAdminComponent);
    return ContactAdminComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/history/history.component.css":
/*!************************************************************!*\
  !*** ./src/app/Admin-Portal/history/history.component.css ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("p {\r\n    font-family: Lato;\r\n  }\r\n  \r\n  table.sjs-table > tbody > tr:first-child > td {\r\n       background-color: lightgreen;\r\n  }\r\n  \r\n  .mb-0{\r\n   margin-bottom: 0.5rem;\r\n   font-weight: 600;\r\n   line-height: 1.5;\r\n   color: #32325d;\r\n  }\r\n  \r\n  .row{\r\n   margin: 0;\r\n   font-family: myFirstFont, sans-serif;\r\n   font-size: 1rem;\r\n   font-weight: 400;\r\n   line-height: 1.5;\r\n   color: #525f7f;\r\n   text-align: left;\r\n  }\r\n  \r\n  .custom-control-label::after {\r\n   border-style: groove;\r\n\r\n  }\r\n  \r\n  .table-res{\r\n     height: 400px;\r\n     overflow-y: auto;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL2hpc3RvcnkvaGlzdG9yeS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0VBQ25COztFQUVBO09BQ0ssNEJBQTRCO0VBQ2pDOztFQUVBO0dBQ0MscUJBQXFCO0dBQ3JCLGdCQUFnQjtHQUNoQixnQkFBZ0I7R0FDaEIsY0FBYztFQUNmOztFQUNBO0dBQ0MsU0FBUztHQUNULG9DQUFvQztHQUNwQyxlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGdCQUFnQjtHQUNoQixjQUFjO0dBQ2QsZ0JBQWdCO0VBQ2pCOztFQUVBO0dBQ0Msb0JBQW9COztFQUVyQjs7RUFFQTtLQUNHLGFBQWE7S0FDYixnQkFBZ0I7RUFDbkIiLCJmaWxlIjoic3JjL2FwcC9BZG1pbi1Qb3J0YWwvaGlzdG9yeS9oaXN0b3J5LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJwIHtcclxuICAgIGZvbnQtZmFtaWx5OiBMYXRvO1xyXG4gIH1cclxuICBcclxuICB0YWJsZS5zanMtdGFibGUgPiB0Ym9keSA+IHRyOmZpcnN0LWNoaWxkID4gdGQge1xyXG4gICAgICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmVlbjtcclxuICB9XHJcblxyXG4gIC5tYi0we1xyXG4gICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XHJcbiAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgIGNvbG9yOiAjMzIzMjVkO1xyXG4gIH1cclxuICAucm93e1xyXG4gICBtYXJnaW46IDA7XHJcbiAgIGZvbnQtZmFtaWx5OiBteUZpcnN0Rm9udCwgc2Fucy1zZXJpZjtcclxuICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICBmb250LXdlaWdodDogNDAwO1xyXG4gICBsaW5lLWhlaWdodDogMS41O1xyXG4gICBjb2xvcjogIzUyNWY3ZjtcclxuICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICB9XHJcblxyXG4gIC5jdXN0b20tY29udHJvbC1sYWJlbDo6YWZ0ZXIge1xyXG4gICBib3JkZXItc3R5bGU6IGdyb292ZTtcclxuXHJcbiAgfVxyXG5cclxuICAudGFibGUtcmVze1xyXG4gICAgIGhlaWdodDogNDAwcHg7XHJcbiAgICAgb3ZlcmZsb3cteTogYXV0bztcclxuICB9Il19 */");

/***/ }),

/***/ "./src/app/Admin-Portal/history/history.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/Admin-Portal/history/history.component.ts ***!
  \***********************************************************/
/*! exports provided: HistoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryComponent", function() { return HistoryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HistoryComponent = /** @class */ (function () {
    function HistoryComponent() {
    }
    HistoryComponent.prototype.ngOnInit = function () {
    };
    HistoryComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-history',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./history.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/history/history.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./history.component.css */ "./src/app/Admin-Portal/history/history.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], HistoryComponent);
    return HistoryComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/new-role/new-role.component.css":
/*!**************************************************************!*\
  !*** ./src/app/Admin-Portal/new-role/new-role.component.css ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".costom-btn {\r\n    margin: auto;\r\n    margin: auto;\r\n    line-height: initial;\r\n    background-color: blueviolet;\r\n    margin-left: 22vh;\r\n    margin-top: 22vh;\r\n}\r\n\r\n     li {\r\n      margin: 20 20px;\r\n    }\r\n\r\n     .btn-c\r\n{\r\n    margin-top: 10vh;\r\n}\r\n\r\n     .h2{\r\n    font-family: inherit;\r\n    font-weight: 600;\r\n    line-height: 1.5;\r\n    font-size: 1.25rem;\r\n    margin-top: -15px;\r\n    padding-bottom: 15px;\r\n    color: white;\r\n}\r\n\r\n     img {\r\n    display: block;\r\n}\r\n\r\n     .thumbnail {\r\n    position: relative;\r\n    display: inline-block;\r\n}\r\n\r\n     .example-chip-list {\r\n    width: 100%;\r\n  }\r\n\r\n     .caption {\r\n    position: -webkit-sticky;\r\n    position: sticky;\r\n    top: 7%;\r\n    left: 67%;\r\n    transform: translate( -50%, -50% );\r\n    text-align: center;\r\n    color: white;\r\n    font-weight: bold;\r\n}\r\n\r\n     .card-body-img{\r\n   background-image: url('stcs.png'); \r\n   background-repeat: no-repeat;\r\n   background-size: 27rem;\r\n   background-position-y: -15pt;\r\n   background-position-x: -51pt;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL25ldy1yb2xlL25ldy1yb2xlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxZQUFZO0lBQ1osWUFBWTtJQUNaLG9CQUFvQjtJQUNwQiw0QkFBNEI7SUFDNUIsaUJBQWlCO0lBQ2pCLGdCQUFnQjtBQUNwQjs7S0FFSztNQUNDLGVBQWU7SUFDakI7O0tBRUo7O0lBRUksZ0JBQWdCO0FBQ3BCOztLQUVBO0lBQ0ksb0JBQW9CO0lBQ3BCLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixvQkFBb0I7SUFDcEIsWUFBWTtBQUNoQjs7S0FFQTtJQUNJLGNBQWM7QUFDbEI7O0tBRUE7SUFDSSxrQkFBa0I7SUFDbEIscUJBQXFCO0FBQ3pCOztLQUNBO0lBQ0ksV0FBVztFQUNiOztLQUNGO0lBQ0ksd0JBQWdCO0lBQWhCLGdCQUFnQjtJQUNoQixPQUFPO0lBQ1AsU0FBUztJQUNULGtDQUFrQztJQUNsQyxrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLGlCQUFpQjtBQUNyQjs7S0FFQTtHQUNHLGlDQUF3QztHQUN4Qyw0QkFBNEI7R0FDNUIsc0JBQXNCO0dBQ3RCLDRCQUE0QjtHQUM1Qiw0QkFBNEI7QUFDL0IiLCJmaWxlIjoic3JjL2FwcC9BZG1pbi1Qb3J0YWwvbmV3LXJvbGUvbmV3LXJvbGUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb3N0b20tYnRuIHtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIGxpbmUtaGVpZ2h0OiBpbml0aWFsO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZXZpb2xldDtcclxuICAgIG1hcmdpbi1sZWZ0OiAyMnZoO1xyXG4gICAgbWFyZ2luLXRvcDogMjJ2aDtcclxufVxyXG5cclxuICAgICBsaSB7XHJcbiAgICAgIG1hcmdpbjogMjAgMjBweDtcclxuICAgIH1cclxuXHJcbi5idG4tY1xyXG57XHJcbiAgICBtYXJnaW4tdG9wOiAxMHZoO1xyXG59XHJcblxyXG4uaDJ7XHJcbiAgICBmb250LWZhbWlseTogaW5oZXJpdDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gICAgZm9udC1zaXplOiAxLjI1cmVtO1xyXG4gICAgbWFyZ2luLXRvcDogLTE1cHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuaW1nIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4udGh1bWJuYWlsIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG4uZXhhbXBsZS1jaGlwLWxpc3Qge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4uY2FwdGlvbiB7XHJcbiAgICBwb3NpdGlvbjogc3RpY2t5O1xyXG4gICAgdG9wOiA3JTtcclxuICAgIGxlZnQ6IDY3JTtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKCAtNTAlLCAtNTAlICk7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG5cclxuLmNhcmQtYm9keS1pbWd7XHJcbiAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImFzc2V0cy9zdGNzLnBuZ1wiKTsgXHJcbiAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgIGJhY2tncm91bmQtc2l6ZTogMjdyZW07XHJcbiAgIGJhY2tncm91bmQtcG9zaXRpb24teTogLTE1cHQ7XHJcbiAgIGJhY2tncm91bmQtcG9zaXRpb24teDogLTUxcHQ7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/Admin-Portal/new-role/new-role.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/Admin-Portal/new-role/new-role.component.ts ***!
  \*************************************************************/
/*! exports provided: NewRoleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewRoleComponent", function() { return NewRoleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/keycodes */ "./node_modules/@angular/cdk/fesm5/keycodes.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/fesm5/autocomplete.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_8__);










var NewRoleComponent = /** @class */ (function () {
    function NewRoleComponent(toast, router) {
        var _this = this;
        this.toast = toast;
        this.router = router;
        this.visible = true;
        this.selectable = true;
        this.removable = true;
        this.separatorKeysCodes = [_angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_5__["ENTER"], _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_5__["COMMA"]];
        this.fruitCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]();
        this.roles = [];
        this.allRoles = ['Admin', 'Commercial', 'Technical', 'Super Admin'];
        this.newRole = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            title: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[a-zA-Z]+$'), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(1)]),
            privilege: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(1)])
        });
        this.filteredRoles = this.fruitCtrl.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (role) { return role ? _this._filter(role) : _this.allRoles.slice(); }));
    }
    NewRoleComponent.prototype.add = function (event) {
        var input = event.input;
        var value = event.value;
        // Add our role
        if ((value || '').trim()) {
            this.roles.push(value.trim());
        }
        // Reset the input value
        if (input) {
            input.value = '';
        }
        this.fruitCtrl.setValue(null);
    };
    NewRoleComponent.prototype.remove = function (role) {
        var index = this.roles.indexOf(role);
        if (index >= 0) {
            this.roles.splice(index, 1);
        }
    };
    NewRoleComponent.prototype.selected = function (event) {
        this.roles.push(event.option.viewValue);
        this.roleInput.nativeElement.value = '';
        this.fruitCtrl.setValue(null);
    };
    NewRoleComponent.prototype._filter = function (value) {
        var filterValue = value.toLowerCase();
        return this.allRoles.filter(function (role) { return role.toLowerCase().indexOf(filterValue) === 0; });
    };
    NewRoleComponent.prototype.ngOnInit = function () {
    };
    NewRoleComponent.prototype.save = function () {
        var _this = this;
        if (this.newRole.valid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                allowOutsideClick: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.resumeTimer);
                }
            });
            sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Save'
            }).then(function (result) {
                if (result.value) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Saved!', 'Your Changes has been made.', 'success');
                    _this.router.navigate(['home']);
                }
            });
        }
        if (this.newRole.invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Invalid Role Title'
            });
        }
    };
    NewRoleComponent.ctorParameters = function () { return [
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('roleInput'),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewRoleComponent.prototype, "roleInput", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('auto'),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_6__["MatAutocomplete"])
    ], NewRoleComponent.prototype, "matAutocomplete", void 0);
    NewRoleComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-new-role',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./new-role.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/new-role/new-role.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./new-role.component.css */ "./src/app/Admin-Portal/new-role/new-role.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], NewRoleComponent);
    return NewRoleComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/portal-admin/portal-admin.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/Admin-Portal/portal-admin/portal-admin.component.css ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".col {\r\n    max-width: 100%;\r\n    flex-basis: 0;\r\n    flex-grow: 1;\r\n    padding-top: 19px;\r\n}\r\n\r\n.color-p{\r\n    color: #525F7F\r\n}\r\n\r\n.col-xl-3 {\r\npadding-bottom: 17px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL3BvcnRhbC1hZG1pbi9wb3J0YWwtYWRtaW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGVBQWU7SUFDZixhQUFhO0lBQ2IsWUFBWTtJQUNaLGlCQUFpQjtBQUNyQjs7QUFFQTtJQUNJO0FBQ0o7O0FBQ0E7QUFDQSxvQkFBb0I7QUFDcEIiLCJmaWxlIjoic3JjL2FwcC9BZG1pbi1Qb3J0YWwvcG9ydGFsLWFkbWluL3BvcnRhbC1hZG1pbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbCB7XHJcbiAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICBmbGV4LWJhc2lzOiAwO1xyXG4gICAgZmxleC1ncm93OiAxO1xyXG4gICAgcGFkZGluZy10b3A6IDE5cHg7XHJcbn1cclxuXHJcbi5jb2xvci1we1xyXG4gICAgY29sb3I6ICM1MjVGN0ZcclxufVxyXG4uY29sLXhsLTMge1xyXG5wYWRkaW5nLWJvdHRvbTogMTdweDtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/Admin-Portal/portal-admin/portal-admin.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/Admin-Portal/portal-admin/portal-admin.component.ts ***!
  \*********************************************************************/
/*! exports provided: PortalAdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PortalAdminComponent", function() { return PortalAdminComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PortalAdminComponent = /** @class */ (function () {
    function PortalAdminComponent() {
    }
    PortalAdminComponent.prototype.ngOnInit = function () {
    };
    PortalAdminComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-portal-admin',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./portal-admin.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/portal-admin/portal-admin.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./portal-admin.component.css */ "./src/app/Admin-Portal/portal-admin/portal-admin.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], PortalAdminComponent);
    return PortalAdminComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/role-access/role-access.component.css":
/*!********************************************************************!*\
  !*** ./src/app/Admin-Portal/role-access/role-access.component.css ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("p {\r\n    font-family: Lato;\r\n  }\r\n  \r\n  table.sjs-table > tbody > tr:first-child > td {\r\n       background-color: lightgreen;\r\n  }\r\n  \r\n  .mb-0{\r\n   margin-bottom: 0.5rem;\r\n   font-weight: 600;\r\n   line-height: 1.5;\r\n   color: #32325d;\r\n  }\r\n  \r\n  .row{\r\n   margin: 0;\r\n   font-family: myFirstFont, sans-serif;\r\n   font-size: 1rem;\r\n   font-weight: 400;\r\n   line-height: 1.5;\r\n   color: #525f7f;\r\n   text-align: left;\r\n  }\r\n  \r\n  .custom-control-label::after {\r\n   border-style: groove;\r\n\r\n  }\r\n  \r\n  .btn{\r\n     background-color: #FF375E;\r\n     color: #fff;\r\n }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL3JvbGUtYWNjZXNzL3JvbGUtYWNjZXNzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxpQkFBaUI7RUFDbkI7O0VBRUE7T0FDSyw0QkFBNEI7RUFDakM7O0VBRUE7R0FDQyxxQkFBcUI7R0FDckIsZ0JBQWdCO0dBQ2hCLGdCQUFnQjtHQUNoQixjQUFjO0VBQ2Y7O0VBQ0E7R0FDQyxTQUFTO0dBQ1Qsb0NBQW9DO0dBQ3BDLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZ0JBQWdCO0dBQ2hCLGNBQWM7R0FDZCxnQkFBZ0I7RUFDakI7O0VBRUE7R0FDQyxvQkFBb0I7O0VBRXJCOztFQUVBO0tBQ0cseUJBQXlCO0tBQ3pCLFdBQVc7Q0FDZiIsImZpbGUiOiJzcmMvYXBwL0FkbWluLVBvcnRhbC9yb2xlLWFjY2Vzcy9yb2xlLWFjY2Vzcy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsicCB7XHJcbiAgICBmb250LWZhbWlseTogTGF0bztcclxuICB9XHJcbiAgXHJcbiAgdGFibGUuc2pzLXRhYmxlID4gdGJvZHkgPiB0cjpmaXJzdC1jaGlsZCA+IHRkIHtcclxuICAgICAgIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0Z3JlZW47XHJcbiAgfVxyXG5cclxuICAubWItMHtcclxuICAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xyXG4gICBmb250LXdlaWdodDogNjAwO1xyXG4gICBsaW5lLWhlaWdodDogMS41O1xyXG4gICBjb2xvcjogIzMyMzI1ZDtcclxuICB9XHJcbiAgLnJvd3tcclxuICAgbWFyZ2luOiAwO1xyXG4gICBmb250LWZhbWlseTogbXlGaXJzdEZvbnQsIHNhbnMtc2VyaWY7XHJcbiAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgY29sb3I6ICM1MjVmN2Y7XHJcbiAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgfVxyXG5cclxuICAuY3VzdG9tLWNvbnRyb2wtbGFiZWw6OmFmdGVyIHtcclxuICAgYm9yZGVyLXN0eWxlOiBncm9vdmU7XHJcblxyXG4gIH1cclxuXHJcbiAgLmJ0bntcclxuICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkYzNzVFO1xyXG4gICAgIGNvbG9yOiAjZmZmO1xyXG4gfSJdfQ== */");

/***/ }),

/***/ "./src/app/Admin-Portal/role-access/role-access.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/Admin-Portal/role-access/role-access.component.ts ***!
  \*******************************************************************/
/*! exports provided: RoleAccessComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleAccessComponent", function() { return RoleAccessComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var RoleAccessComponent = /** @class */ (function () {
    function RoleAccessComponent(router) {
        this.router = router;
    }
    RoleAccessComponent.prototype.ngOnInit = function () {
    };
    RoleAccessComponent.prototype.more = function () {
        this.router.navigate(['role-view']);
    };
    RoleAccessComponent.prototype.create = function () {
        this.router.navigate(['new-role']);
    };
    RoleAccessComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    RoleAccessComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-role-access',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./role-access.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/role-access/role-access.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./role-access.component.css */ "./src/app/Admin-Portal/role-access/role-access.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], RoleAccessComponent);
    return RoleAccessComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/role-view/role-view.component.css":
/*!****************************************************************!*\
  !*** ./src/app/Admin-Portal/role-view/role-view.component.css ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("p {\r\n    font-family: Lato;\r\n  }\r\n  \r\n  table.sjs-table > tbody > tr:first-child > td {\r\n       background-color: lightgreen;\r\n  }\r\n  \r\n  .mb-0{\r\n   margin-bottom: 0.5rem;\r\n   font-weight: 600;\r\n   line-height: 1.5;\r\n   color: #32325d;\r\n  }\r\n  \r\n  .row{\r\n   margin: 0;\r\n   font-family: myFirstFont, sans-serif;\r\n   font-size: 1rem;\r\n   font-weight: 400;\r\n   line-height: 1.5;\r\n   color: #525f7f;\r\n   text-align: left;\r\n  }\r\n  \r\n  .custom-control-label::after {\r\n   border-style: groove;\r\n\r\n  }\r\n  \r\n  .btn{\r\n     background-color: #FF375E;\r\n     color: #fff;\r\n }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL3JvbGUtdmlldy9yb2xlLXZpZXcuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGlCQUFpQjtFQUNuQjs7RUFFQTtPQUNLLDRCQUE0QjtFQUNqQzs7RUFFQTtHQUNDLHFCQUFxQjtHQUNyQixnQkFBZ0I7R0FDaEIsZ0JBQWdCO0dBQ2hCLGNBQWM7RUFDZjs7RUFDQTtHQUNDLFNBQVM7R0FDVCxvQ0FBb0M7R0FDcEMsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixnQkFBZ0I7R0FDaEIsY0FBYztHQUNkLGdCQUFnQjtFQUNqQjs7RUFFQTtHQUNDLG9CQUFvQjs7RUFFckI7O0VBRUE7S0FDRyx5QkFBeUI7S0FDekIsV0FBVztDQUNmIiwiZmlsZSI6InNyYy9hcHAvQWRtaW4tUG9ydGFsL3JvbGUtdmlldy9yb2xlLXZpZXcuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInAge1xyXG4gICAgZm9udC1mYW1pbHk6IExhdG87XHJcbiAgfVxyXG4gIFxyXG4gIHRhYmxlLnNqcy10YWJsZSA+IHRib2R5ID4gdHI6Zmlyc3QtY2hpbGQgPiB0ZCB7XHJcbiAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyZWVuO1xyXG4gIH1cclxuXHJcbiAgLm1iLTB7XHJcbiAgIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcclxuICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgY29sb3I6ICMzMjMyNWQ7XHJcbiAgfVxyXG4gIC5yb3d7XHJcbiAgIG1hcmdpbjogMDtcclxuICAgZm9udC1mYW1pbHk6IG15Rmlyc3RGb250LCBzYW5zLXNlcmlmO1xyXG4gICBmb250LXNpemU6IDFyZW07XHJcbiAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgIGNvbG9yOiAjNTI1ZjdmO1xyXG4gICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIH1cclxuXHJcbiAgLmN1c3RvbS1jb250cm9sLWxhYmVsOjphZnRlciB7XHJcbiAgIGJvcmRlci1zdHlsZTogZ3Jvb3ZlO1xyXG5cclxuICB9XHJcblxyXG4gIC5idG57XHJcbiAgICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGMzc1RTtcclxuICAgICBjb2xvcjogI2ZmZjtcclxuIH0iXX0= */");

/***/ }),

/***/ "./src/app/Admin-Portal/role-view/role-view.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/Admin-Portal/role-view/role-view.component.ts ***!
  \***************************************************************/
/*! exports provided: RoleViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleViewComponent", function() { return RoleViewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var RoleViewComponent = /** @class */ (function () {
    function RoleViewComponent() {
    }
    RoleViewComponent.prototype.ngOnInit = function () {
    };
    RoleViewComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-role-view',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./role-view.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/role-view/role-view.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./role-view.component.css */ "./src/app/Admin-Portal/role-view/role-view.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], RoleViewComponent);
    return RoleViewComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/update-receiver/update-receiver.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/Admin-Portal/update-receiver/update-receiver.component.css ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".costom-btn {\r\n    margin: auto;\r\n    margin: auto;\r\n    line-height: initial;\r\n    background-color: blueviolet;\r\n    margin-left: 22vh;\r\n    margin-top: 22vh;\r\n}\r\n\r\n     li {\r\n      margin: 20 20px;\r\n    }\r\n\r\n     .btn-c\r\n{\r\n    margin-top: 10vh;\r\n}\r\n\r\n     .h2{\r\n    font-family: inherit;\r\n    font-weight: 600;\r\n    line-height: 1.5;\r\n    font-size: 1.25rem;\r\n    margin-top: -32px;\r\n    padding-bottom: 15px\r\n}\r\n\r\n     .card-body-img{\r\n   background-image: url('iphone.png'); \r\n   background-repeat: no-repeat;\r\n   background-position-y: -85pt;\r\n   background-position-x: -150pt;\r\n\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL3VwZGF0ZS1yZWNlaXZlci91cGRhdGUtcmVjZWl2ZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFlBQVk7SUFDWixZQUFZO0lBQ1osb0JBQW9CO0lBQ3BCLDRCQUE0QjtJQUM1QixpQkFBaUI7SUFDakIsZ0JBQWdCO0FBQ3BCOztLQUVLO01BQ0MsZUFBZTtJQUNqQjs7S0FFSjs7SUFFSSxnQkFBZ0I7QUFDcEI7O0tBRUE7SUFDSSxvQkFBb0I7SUFDcEIsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsaUJBQWlCO0lBQ2pCO0FBQ0o7O0tBRUE7R0FDRyxtQ0FBMEM7R0FDMUMsNEJBQTRCO0dBQzVCLDRCQUE0QjtHQUM1Qiw2QkFBNkI7O0FBRWhDIiwiZmlsZSI6InNyYy9hcHAvQWRtaW4tUG9ydGFsL3VwZGF0ZS1yZWNlaXZlci91cGRhdGUtcmVjZWl2ZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb3N0b20tYnRuIHtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIGxpbmUtaGVpZ2h0OiBpbml0aWFsO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZXZpb2xldDtcclxuICAgIG1hcmdpbi1sZWZ0OiAyMnZoO1xyXG4gICAgbWFyZ2luLXRvcDogMjJ2aDtcclxufVxyXG5cclxuICAgICBsaSB7XHJcbiAgICAgIG1hcmdpbjogMjAgMjBweDtcclxuICAgIH1cclxuXHJcbi5idG4tY1xyXG57XHJcbiAgICBtYXJnaW4tdG9wOiAxMHZoO1xyXG59XHJcblxyXG4uaDJ7XHJcbiAgICBmb250LWZhbWlseTogaW5oZXJpdDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gICAgZm9udC1zaXplOiAxLjI1cmVtO1xyXG4gICAgbWFyZ2luLXRvcDogLTMycHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTVweFxyXG59XHJcblxyXG4uY2FyZC1ib2R5LWltZ3tcclxuICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiYXNzZXRzL2lwaG9uZS5wbmdcIik7IFxyXG4gICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IC04NXB0O1xyXG4gICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXg6IC0xNTBwdDtcclxuXHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/Admin-Portal/update-receiver/update-receiver.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/Admin-Portal/update-receiver/update-receiver.component.ts ***!
  \***************************************************************************/
/*! exports provided: UpdateReceiverComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateReceiverComponent", function() { return UpdateReceiverComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");






var UpdateReceiverComponent = /** @class */ (function () {
    function UpdateReceiverComponent(toast, router) {
        this.toast = toast;
        this.router = router;
        this.updateRecevier = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormGroup"]({
            toEmail: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'), _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].minLength(1)]),
            CCEmail: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'), _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].minLength(1)])
        });
    }
    UpdateReceiverComponent.prototype.ngOnInit = function () {
    };
    UpdateReceiverComponent.prototype.save = function () {
        var _this = this;
        if (this.updateRecevier.valid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.resumeTimer);
                }
            });
            sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Save'
            }).then(function (result) {
                if (result.value) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire('Saved!', 'Your Changes has been made.', 'success');
                    _this.router.navigate(['home']);
                }
            });
        }
        if (this.updateRecevier.invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Invalid Email Address'
            });
        }
    };
    UpdateReceiverComponent.ctorParameters = function () { return [
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
    ]; };
    UpdateReceiverComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-update-receiver',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./update-receiver.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/update-receiver/update-receiver.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./update-receiver.component.css */ "./src/app/Admin-Portal/update-receiver/update-receiver.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], UpdateReceiverComponent);
    return UpdateReceiverComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/virtual-account/virtual-account.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/Admin-Portal/virtual-account/virtual-account.component.css ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("p {\r\n    font-family: Lato;\r\n  }\r\n  \r\n  table.sjs-table > tbody > tr:first-child > td {\r\n       background-color: lightgreen;\r\n  }\r\n  \r\n  .mb-0{\r\n   margin-bottom: 0.5rem;\r\n   font-weight: 600;\r\n   line-height: 1.5;\r\n   color: #32325d;\r\n  }\r\n  \r\n  .row{\r\n   margin: 0;\r\n   font-family: myFirstFont, sans-serif;\r\n   font-size: 1rem;\r\n   font-weight: 400;\r\n   line-height: 1.5;\r\n   color: #525f7f;\r\n   text-align: left;\r\n  }\r\n  \r\n  .custom-control-label::after {\r\n   border-style: groove;\r\n\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL3ZpcnR1YWwtYWNjb3VudC92aXJ0dWFsLWFjY291bnQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGlCQUFpQjtFQUNuQjs7RUFFQTtPQUNLLDRCQUE0QjtFQUNqQzs7RUFFQTtHQUNDLHFCQUFxQjtHQUNyQixnQkFBZ0I7R0FDaEIsZ0JBQWdCO0dBQ2hCLGNBQWM7RUFDZjs7RUFDQTtHQUNDLFNBQVM7R0FDVCxvQ0FBb0M7R0FDcEMsZUFBZTtHQUNmLGdCQUFnQjtHQUNoQixnQkFBZ0I7R0FDaEIsY0FBYztHQUNkLGdCQUFnQjtFQUNqQjs7RUFFQTtHQUNDLG9CQUFvQjs7RUFFckIiLCJmaWxlIjoic3JjL2FwcC9BZG1pbi1Qb3J0YWwvdmlydHVhbC1hY2NvdW50L3ZpcnR1YWwtYWNjb3VudC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsicCB7XHJcbiAgICBmb250LWZhbWlseTogTGF0bztcclxuICB9XHJcbiAgXHJcbiAgdGFibGUuc2pzLXRhYmxlID4gdGJvZHkgPiB0cjpmaXJzdC1jaGlsZCA+IHRkIHtcclxuICAgICAgIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0Z3JlZW47XHJcbiAgfVxyXG5cclxuICAubWItMHtcclxuICAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xyXG4gICBmb250LXdlaWdodDogNjAwO1xyXG4gICBsaW5lLWhlaWdodDogMS41O1xyXG4gICBjb2xvcjogIzMyMzI1ZDtcclxuICB9XHJcbiAgLnJvd3tcclxuICAgbWFyZ2luOiAwO1xyXG4gICBmb250LWZhbWlseTogbXlGaXJzdEZvbnQsIHNhbnMtc2VyaWY7XHJcbiAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgY29sb3I6ICM1MjVmN2Y7XHJcbiAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgfVxyXG5cclxuICAuY3VzdG9tLWNvbnRyb2wtbGFiZWw6OmFmdGVyIHtcclxuICAgYm9yZGVyLXN0eWxlOiBncm9vdmU7XHJcblxyXG4gIH0iXX0= */");

/***/ }),

/***/ "./src/app/Admin-Portal/virtual-account/virtual-account.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/Admin-Portal/virtual-account/virtual-account.component.ts ***!
  \***************************************************************************/
/*! exports provided: VirtualAccountComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VirtualAccountComponent", function() { return VirtualAccountComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var VirtualAccountComponent = /** @class */ (function () {
    function VirtualAccountComponent() {
    }
    VirtualAccountComponent.prototype.ngOnInit = function () {
    };
    VirtualAccountComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-virtual-account',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./virtual-account.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/Admin-Portal/virtual-account/virtual-account.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./virtual-account.component.css */ "./src/app/Admin-Portal/virtual-account/virtual-account.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], VirtualAccountComponent);
    return VirtualAccountComponent;
}());



/***/ }),

/***/ "./src/app/_nav.ts":
/*!*************************!*\
  !*** ./src/app/_nav.ts ***!
  \*************************/
/*! exports provided: navItems */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "navItems", function() { return navItems; });
var navItems = [
    {
        name: 'Dashboard',
        url: '/dashboard',
        icon: 'icon-speedometer',
        badge: {
            variant: 'info',
            text: 'NEW'
        }
    },
    {
        title: true,
        name: 'Theme'
    },
    {
        name: 'Colors',
        url: '/theme/colors',
        icon: 'icon-drop'
    },
    {
        name: 'Typography',
        url: '/theme/typography',
        icon: 'icon-pencil'
    },
    {
        title: true,
        name: 'Components'
    },
    {
        name: 'Base',
        url: '/base',
        icon: 'icon-puzzle',
        children: [
            {
                name: 'Cards',
                url: '/base/cards',
                icon: 'icon-puzzle'
            },
            {
                name: 'Carousels',
                url: '/base/carousels',
                icon: 'icon-puzzle'
            },
            {
                name: 'Collapses',
                url: '/base/collapses',
                icon: 'icon-puzzle'
            },
            {
                name: 'Forms',
                url: '/base/forms',
                icon: 'icon-puzzle'
            },
            {
                name: 'Navbars',
                url: '/base/navbars',
                icon: 'icon-puzzle'
            },
            {
                name: 'Pagination',
                url: '/base/paginations',
                icon: 'icon-puzzle'
            },
            {
                name: 'Popovers',
                url: '/base/popovers',
                icon: 'icon-puzzle'
            },
            {
                name: 'Progress',
                url: '/base/progress',
                icon: 'icon-puzzle'
            },
            {
                name: 'Switches',
                url: '/base/switches',
                icon: 'icon-puzzle'
            },
            {
                name: 'Tables',
                url: '/base/tables',
                icon: 'icon-puzzle'
            },
            {
                name: 'Tabs',
                url: '/base/tabs',
                icon: 'icon-puzzle'
            },
            {
                name: 'Tooltips',
                url: '/base/tooltips',
                icon: 'icon-puzzle'
            }
        ]
    },
    {
        name: 'Buttons',
        url: '/buttons',
        icon: 'icon-cursor',
        children: [
            {
                name: 'Buttons',
                url: '/buttons/buttons',
                icon: 'icon-cursor'
            },
            {
                name: 'Dropdowns',
                url: '/buttons/dropdowns',
                icon: 'icon-cursor'
            },
            {
                name: 'Brand Buttons',
                url: '/buttons/brand-buttons',
                icon: 'icon-cursor'
            }
        ]
    },
    {
        name: 'Charts',
        url: '/charts',
        icon: 'icon-pie-chart'
    },
    {
        name: 'Icons',
        url: '/icons',
        icon: 'icon-star',
        children: [
            {
                name: 'CoreUI Icons',
                url: '/icons/coreui-icons',
                icon: 'icon-star',
                badge: {
                    variant: 'success',
                    text: 'NEW'
                }
            },
            {
                name: 'Flags',
                url: '/icons/flags',
                icon: 'icon-star'
            },
            {
                name: 'Font Awesome',
                url: '/icons/font-awesome',
                icon: 'icon-star',
                badge: {
                    variant: 'secondary',
                    text: '4.7'
                }
            },
            {
                name: 'Simple Line Icons',
                url: '/icons/simple-line-icons',
                icon: 'icon-star'
            }
        ]
    },
    {
        name: 'Notifications',
        url: '/notifications',
        icon: 'icon-bell',
        children: [
            {
                name: 'Alerts',
                url: '/notifications/alerts',
                icon: 'icon-bell'
            },
            {
                name: 'Badges',
                url: '/notifications/badges',
                icon: 'icon-bell'
            },
            {
                name: 'Modals',
                url: '/notifications/modals',
                icon: 'icon-bell'
            }
        ]
    },
    {
        name: 'Widgets',
        url: '/widgets',
        icon: 'icon-calculator',
        badge: {
            variant: 'info',
            text: 'NEW'
        }
    },
    {
        divider: true
    },
    {
        title: true,
        name: 'Extras',
    },
    {
        name: 'Pages',
        url: '/pages',
        icon: 'icon-star',
        children: [
            {
                name: 'Login',
                url: '/login',
                icon: 'icon-star'
            },
            {
                name: 'Register',
                url: '/register',
                icon: 'icon-star'
            },
            {
                name: 'Error 404',
                url: '/404',
                icon: 'icon-star'
            },
            {
                name: 'Error 500',
                url: '/500',
                icon: 'icon-star'
            }
        ]
    },
    {
        name: 'Disabled',
        url: '/dashboard',
        icon: 'icon-ban',
        badge: {
            variant: 'secondary',
            text: 'NEW'
        },
        attributes: { disabled: true },
    },
    {
        name: 'Download CoreUI',
        url: 'http://coreui.io/angular/',
        icon: 'icon-cloud-download',
        class: 'mt-auto',
        variant: 'success',
        attributes: { target: '_blank', rel: 'noopener' }
    },
    {
        name: 'Try CoreUI PRO',
        url: 'http://coreui.io/pro/angular/',
        icon: 'icon-layers',
        variant: 'danger',
        attributes: { target: '_blank', rel: 'noopener' }
    }
];


/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var AppComponent = /** @class */ (function () {
    function AppComponent(router) {
        this.router = router;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.router.events.subscribe(function (evt) {
            if (!(evt instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"])) {
                return;
            }
            window.scrollTo(0, 0);
        });
    };
    AppComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            // tslint:disable-next-line
            selector: 'body',
            template: '<router-outlet></router-outlet>'
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: HttpLoaderFactory, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function() { return HttpLoaderFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/fesm5/input.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var ngx_bootstrap_progressbar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/progressbar */ "./node_modules/ngx-bootstrap/progressbar/fesm5/ngx-bootstrap-progressbar.js");
/* harmony import */ var ngx_bootstrap_popover__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/popover */ "./node_modules/ngx-bootstrap/popover/fesm5/ngx-bootstrap-popover.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_collapse__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/collapse */ "./node_modules/ngx-bootstrap/collapse/fesm5/ngx-bootstrap-collapse.js");
/* harmony import */ var ngx_bootstrap_carousel__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-bootstrap/carousel */ "./node_modules/ngx-bootstrap/carousel/fesm5/ngx-bootstrap-carousel.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/fesm5/dialog.js");
/* harmony import */ var ngx_webstorage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-webstorage */ "./node_modules/ngx-webstorage/fesm5/ngx-webstorage.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/fesm5/stepper.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var angular_user_idle__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! angular-user-idle */ "./node_modules/angular-user-idle/fesm5/angular-user-idle.js");
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/fesm5/ngx-perfect-scrollbar.js");
/* harmony import */ var _angular_material_chips__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/chips */ "./node_modules/@angular/material/fesm5/chips.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _containers__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./containers */ "./src/app/containers/index.ts");
/* harmony import */ var _views_error_404_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./views/error/404.component */ "./src/app/views/error/404.component.ts");
/* harmony import */ var _views_error_500_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./views/error/500.component */ "./src/app/views/error/500.component.ts");
/* harmony import */ var _views_login_login_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./views/login/login.component */ "./src/app/views/login/login.component.ts");
/* harmony import */ var _views_register_register_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./views/register/register.component */ "./src/app/views/register/register.component.ts");
/* harmony import */ var _coreui_angular__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @coreui/angular */ "./node_modules/@coreui/angular/fesm5/coreui-angular.js");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ngx-bootstrap/tabs */ "./node_modules/ngx-bootstrap/tabs/fesm5/ngx-bootstrap-tabs.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/fesm5/ng2-charts.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/fesm5/ngx-translate-http-loader.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _nav_nav_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./nav/nav.component */ "./src/app/nav/nav.component.ts");
/* harmony import */ var _containers_main_layout_main_layout_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./containers/main-layout/main-layout.component */ "./src/app/containers/main-layout/main-layout.component.ts");
/* harmony import */ var _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./contact-us/contact-us.component */ "./src/app/contact-us/contact-us.component.ts");
/* harmony import */ var _new_partner_new_partner_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./new-partner/new-partner.component */ "./src/app/new-partner/new-partner.component.ts");
/* harmony import */ var _upload_upload_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./upload/upload.component */ "./src/app/upload/upload.component.ts");
/* harmony import */ var _ttt_ttt_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./ttt/ttt.component */ "./src/app/ttt/ttt.component.ts");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/fesm5/ng2-file-upload.js");
/* harmony import */ var _complaint_complaint_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./complaint/complaint.component */ "./src/app/complaint/complaint.component.ts");
/* harmony import */ var _downloads_operations_downloads_operations_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./downloads-operations/downloads-operations.component */ "./src/app/downloads-operations/downloads-operations.component.ts");
/* harmony import */ var _help_help_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./help/help.component */ "./src/app/help/help.component.ts");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/fesm5/select.js");
/* harmony import */ var _dashboard_wbu_dashboard_wbu_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./dashboard-wbu/dashboard-wbu.component */ "./src/app/dashboard-wbu/dashboard-wbu.component.ts");
/* harmony import */ var _shared_models_share_module__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./shared/models/share.module */ "./src/app/shared/models/share.module.ts");
/* harmony import */ var _details_details_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./details/details.component */ "./src/app/details/details.component.ts");
/* harmony import */ var _display_invoice_display_invoice_component__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./display-invoice/display-invoice.component */ "./src/app/display-invoice/display-invoice.component.ts");
/* harmony import */ var _views_forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./views/forgot-password/forgot-password.component */ "./src/app/views/forgot-password/forgot-password.component.ts");
/* harmony import */ var _Admin_Portal_portal_admin_portal_admin_component__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./Admin-Portal/portal-admin/portal-admin.component */ "./src/app/Admin-Portal/portal-admin/portal-admin.component.ts");
/* harmony import */ var _Admin_Portal_approve_registration_approve_registration_component__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./Admin-Portal/approve-registration/approve-registration.component */ "./src/app/Admin-Portal/approve-registration/approve-registration.component.ts");
/* harmony import */ var _Admin_Portal_contact_admin_contact_admin_component__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ./Admin-Portal/contact-admin/contact-admin.component */ "./src/app/Admin-Portal/contact-admin/contact-admin.component.ts");
/* harmony import */ var _Admin_Portal_virtual_account_virtual_account_component__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ./Admin-Portal/virtual-account/virtual-account.component */ "./src/app/Admin-Portal/virtual-account/virtual-account.component.ts");
/* harmony import */ var _Admin_Portal_history_history_component__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./Admin-Portal/history/history.component */ "./src/app/Admin-Portal/history/history.component.ts");
/* harmony import */ var _Admin_Portal_update_receiver_update_receiver_component__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! ./Admin-Portal/update-receiver/update-receiver.component */ "./src/app/Admin-Portal/update-receiver/update-receiver.component.ts");
/* harmony import */ var _Admin_Portal_role_access_role_access_component__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! ./Admin-Portal/role-access/role-access.component */ "./src/app/Admin-Portal/role-access/role-access.component.ts");
/* harmony import */ var _Admin_Portal_role_view_role_view_component__WEBPACK_IMPORTED_MODULE_59__ = __webpack_require__(/*! ./Admin-Portal/role-view/role-view.component */ "./src/app/Admin-Portal/role-view/role-view.component.ts");
/* harmony import */ var _Admin_Portal_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_60__ = __webpack_require__(/*! ./Admin-Portal/new-role/new-role.component */ "./src/app/Admin-Portal/new-role/new-role.component.ts");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_61__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/fesm5/autocomplete.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_62__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/fesm5/icon.js");




















var DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
    suppressScrollX: true
};

// Import containers





var APP_CONTAINERS = [
    _containers_main_layout_main_layout_component__WEBPACK_IMPORTED_MODULE_37__["MainLayoutComponent"],
    _containers__WEBPACK_IMPORTED_MODULE_21__["DefaultLayoutComponent"]
];

// Import routing module

// Import 3rd party components



































function HttpLoaderFactory(http) {
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_32__["TranslateHttpLoader"](http, './assets/i18n/', '.json');
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_13__["NgModule"])({
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_8__["BrowserModule"],
                angular_user_idle__WEBPACK_IMPORTED_MODULE_17__["UserIdleModule"].forRoot({ idle: 1800, timeout: 1, ping: 120 }),
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_35__["NgbModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_9__["MatDialogModule"],
                _angular_material_chips__WEBPACK_IMPORTED_MODULE_19__["MatChipsModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_1__["MatInputModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_62__["MatIconModule"],
                _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_61__["MatAutocompleteModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_15__["HttpClientModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_11__["ToastrModule"].forRoot(),
                _angular_material_select__WEBPACK_IMPORTED_MODULE_46__["MatSelectModule"],
                _shared_models_share_module__WEBPACK_IMPORTED_MODULE_48__["SharedModule"],
                _angular_material_stepper__WEBPACK_IMPORTED_MODULE_12__["MatStepperModule"],
                ngx_webstorage__WEBPACK_IMPORTED_MODULE_10__["NgxWebstorageModule"].forRoot({ prefix: 'app', separator: ':' }),
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_16__["BrowserAnimationsModule"],
                _app_routing__WEBPACK_IMPORTED_MODULE_27__["AppRoutingModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_31__["TranslateModule"].forRoot({
                    loader: {
                        provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_31__["TranslateLoader"],
                        useFactory: HttpLoaderFactory,
                        deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_15__["HttpClient"]]
                    }
                }),
                _angular_forms__WEBPACK_IMPORTED_MODULE_33__["ReactiveFormsModule"],
                _coreui_angular__WEBPACK_IMPORTED_MODULE_26__["AppAsideModule"],
                _coreui_angular__WEBPACK_IMPORTED_MODULE_26__["AppBreadcrumbModule"].forRoot(),
                _coreui_angular__WEBPACK_IMPORTED_MODULE_26__["AppHeaderModule"],
                _coreui_angular__WEBPACK_IMPORTED_MODULE_26__["AppSidebarModule"],
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_18__["PerfectScrollbarModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_28__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_29__["TabsModule"].forRoot(),
                ng2_charts__WEBPACK_IMPORTED_MODULE_30__["ChartsModule"],
                ng2_file_upload__WEBPACK_IMPORTED_MODULE_42__["FileUploadModule"],
                ngx_bootstrap_carousel__WEBPACK_IMPORTED_MODULE_7__["CarouselModule"].forRoot(),
                ngx_bootstrap_collapse__WEBPACK_IMPORTED_MODULE_6__["CollapseModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_5__["PaginationModule"].forRoot(),
                ngx_bootstrap_popover__WEBPACK_IMPORTED_MODULE_4__["PopoverModule"].forRoot(),
                ngx_bootstrap_progressbar__WEBPACK_IMPORTED_MODULE_3__["ProgressbarModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_2__["TooltipModule"].forRoot()
            ],
            declarations: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])([
                _app_component__WEBPACK_IMPORTED_MODULE_20__["AppComponent"]
            ], APP_CONTAINERS, [
                _views_error_404_component__WEBPACK_IMPORTED_MODULE_22__["P404Component"],
                _views_error_500_component__WEBPACK_IMPORTED_MODULE_23__["P500Component"],
                _views_login_login_component__WEBPACK_IMPORTED_MODULE_24__["LoginComponent"],
                _views_register_register_component__WEBPACK_IMPORTED_MODULE_25__["RegisterComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_34__["HomeComponent"],
                _nav_nav_component__WEBPACK_IMPORTED_MODULE_36__["NavComponent"],
                _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_38__["ContactUsComponent"],
                _new_partner_new_partner_component__WEBPACK_IMPORTED_MODULE_39__["NewPartnerComponent"],
                _upload_upload_component__WEBPACK_IMPORTED_MODULE_40__["UploadComponent"],
                _ttt_ttt_component__WEBPACK_IMPORTED_MODULE_41__["TttComponent"],
                _upload_upload_component__WEBPACK_IMPORTED_MODULE_40__["TargerUserComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_34__["CpChangeComponent"],
                _complaint_complaint_component__WEBPACK_IMPORTED_MODULE_43__["ComplaintComponent"],
                _downloads_operations_downloads_operations_component__WEBPACK_IMPORTED_MODULE_44__["DownloadsOperationsComponent"],
                _help_help_component__WEBPACK_IMPORTED_MODULE_45__["HelpComponent"],
                _dashboard_wbu_dashboard_wbu_component__WEBPACK_IMPORTED_MODULE_47__["DashboardWBUComponent"],
                _details_details_component__WEBPACK_IMPORTED_MODULE_49__["DetailsComponent"],
                _display_invoice_display_invoice_component__WEBPACK_IMPORTED_MODULE_50__["DisplayInvoiceComponent"],
                _Admin_Portal_portal_admin_portal_admin_component__WEBPACK_IMPORTED_MODULE_52__["PortalAdminComponent"],
                _Admin_Portal_approve_registration_approve_registration_component__WEBPACK_IMPORTED_MODULE_53__["ApproveRegistrationComponent"],
                _Admin_Portal_contact_admin_contact_admin_component__WEBPACK_IMPORTED_MODULE_54__["ContactAdminComponent"],
                _Admin_Portal_virtual_account_virtual_account_component__WEBPACK_IMPORTED_MODULE_55__["VirtualAccountComponent"],
                _Admin_Portal_role_access_role_access_component__WEBPACK_IMPORTED_MODULE_58__["RoleAccessComponent"],
                _views_forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_51__["ForgotPasswordComponent"],
                _Admin_Portal_history_history_component__WEBPACK_IMPORTED_MODULE_56__["HistoryComponent"],
                _Admin_Portal_update_receiver_update_receiver_component__WEBPACK_IMPORTED_MODULE_57__["UpdateReceiverComponent"],
                _Admin_Portal_role_view_role_view_component__WEBPACK_IMPORTED_MODULE_59__["RoleViewComponent"],
                _Admin_Portal_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_60__["NewRoleComponent"],
            ]),
            providers: [{
                    provide: _angular_common__WEBPACK_IMPORTED_MODULE_14__["LocationStrategy"],
                    useClass: _angular_common__WEBPACK_IMPORTED_MODULE_14__["HashLocationStrategy"]
                }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_20__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: routes, AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _details_details_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./details/details.component */ "./src/app/details/details.component.ts");
/* harmony import */ var _downloads_operations_downloads_operations_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./downloads-operations/downloads-operations.component */ "./src/app/downloads-operations/downloads-operations.component.ts");
/* harmony import */ var _complaint_complaint_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./complaint/complaint.component */ "./src/app/complaint/complaint.component.ts");
/* harmony import */ var _upload_upload_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./upload/upload.component */ "./src/app/upload/upload.component.ts");
/* harmony import */ var _new_partner_new_partner_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./new-partner/new-partner.component */ "./src/app/new-partner/new-partner.component.ts");
/* harmony import */ var _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contact-us/contact-us.component */ "./src/app/contact-us/contact-us.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _views_error_404_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./views/error/404.component */ "./src/app/views/error/404.component.ts");
/* harmony import */ var _views_error_500_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./views/error/500.component */ "./src/app/views/error/500.component.ts");
/* harmony import */ var _views_login_login_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./views/login/login.component */ "./src/app/views/login/login.component.ts");
/* harmony import */ var _views_register_register_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./views/register/register.component */ "./src/app/views/register/register.component.ts");
/* harmony import */ var _containers_main_layout_main_layout_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./containers/main-layout/main-layout.component */ "./src/app/containers/main-layout/main-layout.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _help_help_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./help/help.component */ "./src/app/help/help.component.ts");
/* harmony import */ var _dashboard_wbu_dashboard_wbu_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./dashboard-wbu/dashboard-wbu.component */ "./src/app/dashboard-wbu/dashboard-wbu.component.ts");
/* harmony import */ var _display_invoice_display_invoice_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./display-invoice/display-invoice.component */ "./src/app/display-invoice/display-invoice.component.ts");
/* harmony import */ var _views_forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./views/forgot-password/forgot-password.component */ "./src/app/views/forgot-password/forgot-password.component.ts");
/* harmony import */ var _Admin_Portal_approve_registration_approve_registration_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./Admin-Portal/approve-registration/approve-registration.component */ "./src/app/Admin-Portal/approve-registration/approve-registration.component.ts");
/* harmony import */ var _Admin_Portal_contact_admin_contact_admin_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./Admin-Portal/contact-admin/contact-admin.component */ "./src/app/Admin-Portal/contact-admin/contact-admin.component.ts");
/* harmony import */ var _Admin_Portal_portal_admin_portal_admin_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./Admin-Portal/portal-admin/portal-admin.component */ "./src/app/Admin-Portal/portal-admin/portal-admin.component.ts");
/* harmony import */ var _Admin_Portal_virtual_account_virtual_account_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./Admin-Portal/virtual-account/virtual-account.component */ "./src/app/Admin-Portal/virtual-account/virtual-account.component.ts");
/* harmony import */ var _Admin_Portal_update_receiver_update_receiver_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./Admin-Portal/update-receiver/update-receiver.component */ "./src/app/Admin-Portal/update-receiver/update-receiver.component.ts");
/* harmony import */ var _Admin_Portal_history_history_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./Admin-Portal/history/history.component */ "./src/app/Admin-Portal/history/history.component.ts");
/* harmony import */ var _Admin_Portal_role_access_role_access_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./Admin-Portal/role-access/role-access.component */ "./src/app/Admin-Portal/role-access/role-access.component.ts");
/* harmony import */ var _Admin_Portal_role_view_role_view_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./Admin-Portal/role-view/role-view.component */ "./src/app/Admin-Portal/role-view/role-view.component.ts");
/* harmony import */ var _Admin_Portal_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./Admin-Portal/new-role/new-role.component */ "./src/app/Admin-Portal/new-role/new-role.component.ts");




























var routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
    },
    {
        path: '404',
        component: _views_error_404_component__WEBPACK_IMPORTED_MODULE_9__["P404Component"],
        data: {
            title: 'Page 404'
        }
    },
    {
        path: '500',
        component: _views_error_500_component__WEBPACK_IMPORTED_MODULE_10__["P500Component"],
        data: {
            title: 'Page 500'
        }
    },
    {
        path: 'login',
        component: _views_login_login_component__WEBPACK_IMPORTED_MODULE_11__["LoginComponent"],
        data: {
            title: 'Login Page'
        }
    },
    {
        path: 'register',
        component: _views_register_register_component__WEBPACK_IMPORTED_MODULE_12__["RegisterComponent"],
        data: {
            title: 'Register Page'
        }
    },
    {
        path: 'forgot-password',
        component: _views_forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_18__["ForgotPasswordComponent"]
    },
    {
        path: 'partner',
        component: _new_partner_new_partner_component__WEBPACK_IMPORTED_MODULE_5__["NewPartnerComponent"]
    },
    // {
    //   path:'nav',
    //   component:NavComponent,
    // },
    // {
    //   path:'contact',
    //   component:ContactUsComponent,
    // },
    // {
    //   path:'Dashboard',
    //   component:DashboardComponent,
    // },
    // {
    //   path: '',
    //   component: DefaultLayoutComponent,
    //   data: {
    //     title: 'Home'
    //   },
    //   children: [
    //     {
    //       path: 'base',
    //       loadChildren: () => import('./views/base/base.module').then(m => m.BaseModule)
    //     },
    //     {
    //       path: 'buttons',
    //       loadChildren: () => import('./views/buttons/buttons.module').then(m => m.ButtonsModule)
    //     },
    //     {
    //       path: 'charts',
    //       loadChildren: () => import('./views/chartjs/chartjs.module').then(m => m.ChartJSModule)
    //     },
    //     {
    //       path: 'dashboard',
    //       loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
    //     },
    //     {
    //       path: 'icons',
    //       loadChildren: () => import('./views/icons/icons.module').then(m => m.IconsModule)
    //     },
    //     {
    //       path: 'notifications',
    //       loadChildren: () => import('./views/notifications/notifications.module').then(m => m.NotificationsModule)
    //     },
    //     {
    //       path: 'theme',
    //       loadChildren: () => import('./views/theme/theme.module').then(m => m.ThemeModule)
    //     },
    //     {
    //       path: 'widgets',
    //       loadChildren: () => import('./views/widgets/widgets.module').then(m => m.WidgetsModule)
    //     }
    //   ]
    // },
    {
        path: '',
        component: _containers_main_layout_main_layout_component__WEBPACK_IMPORTED_MODULE_13__["MainLayoutComponent"],
        children: [
            {
                path: 'home',
                component: _home_home_component__WEBPACK_IMPORTED_MODULE_14__["HomeComponent"]
            },
            {
                path: 'contact',
                component: _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_6__["ContactUsComponent"],
            },
            {
                path: 'upload',
                component: _upload_upload_component__WEBPACK_IMPORTED_MODULE_4__["UploadComponent"],
            },
            {
                path: 'upload',
                component: _upload_upload_component__WEBPACK_IMPORTED_MODULE_4__["TargerUserComponent"],
            },
            {
                path: 'home',
                component: _home_home_component__WEBPACK_IMPORTED_MODULE_14__["CpChangeComponent"],
            },
            {
                path: 'complaint',
                component: _complaint_complaint_component__WEBPACK_IMPORTED_MODULE_3__["ComplaintComponent"],
            },
            {
                path: 'download',
                component: _downloads_operations_downloads_operations_component__WEBPACK_IMPORTED_MODULE_2__["DownloadsOperationsComponent"],
            },
            {
                path: 'display-invoice',
                component: _display_invoice_display_invoice_component__WEBPACK_IMPORTED_MODULE_17__["DisplayInvoiceComponent"],
            },
            {
                path: 'help',
                component: _help_help_component__WEBPACK_IMPORTED_MODULE_15__["HelpComponent"],
            },
            {
                path: 'dashboard',
                component: _dashboard_wbu_dashboard_wbu_component__WEBPACK_IMPORTED_MODULE_16__["DashboardWBUComponent"],
            },
            {
                path: 'details',
                component: _details_details_component__WEBPACK_IMPORTED_MODULE_1__["DetailsComponent"]
            },
            {
                path: 'portal-admin',
                component: _Admin_Portal_portal_admin_portal_admin_component__WEBPACK_IMPORTED_MODULE_21__["PortalAdminComponent"]
            },
            {
                path: 'approve-registration',
                component: _Admin_Portal_approve_registration_approve_registration_component__WEBPACK_IMPORTED_MODULE_19__["ApproveRegistrationComponent"]
            },
            {
                path: 'contact-admin',
                component: _Admin_Portal_contact_admin_contact_admin_component__WEBPACK_IMPORTED_MODULE_20__["ContactAdminComponent"]
            },
            {
                path: 'virtual-account',
                component: _Admin_Portal_virtual_account_virtual_account_component__WEBPACK_IMPORTED_MODULE_22__["VirtualAccountComponent"]
            },
            {
                path: 'role-access',
                component: _Admin_Portal_role_access_role_access_component__WEBPACK_IMPORTED_MODULE_25__["RoleAccessComponent"]
            },
            {
                path: 'update-receiver',
                component: _Admin_Portal_update_receiver_update_receiver_component__WEBPACK_IMPORTED_MODULE_23__["UpdateReceiverComponent"]
            },
            {
                path: 'history',
                component: _Admin_Portal_history_history_component__WEBPACK_IMPORTED_MODULE_24__["HistoryComponent"]
            },
            {
                path: 'role-view',
                component: _Admin_Portal_role_view_role_view_component__WEBPACK_IMPORTED_MODULE_26__["RoleViewComponent"]
            },
            {
                path: 'new-role',
                component: _Admin_Portal_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_27__["NewRoleComponent"]
            },
        ]
    },
    { path: '**', component: _views_error_404_component__WEBPACK_IMPORTED_MODULE_9__["P404Component"] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_7__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/complaint/complaint.component.css":
/*!***************************************************!*\
  !*** ./src/app/complaint/complaint.component.css ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBsYWludC9jb21wbGFpbnQuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/complaint/complaint.component.ts":
/*!**************************************************!*\
  !*** ./src/app/complaint/complaint.component.ts ***!
  \**************************************************/
/*! exports provided: ComplaintComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComplaintComponent", function() { return ComplaintComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ComplaintComponent = /** @class */ (function () {
    function ComplaintComponent() {
    }
    ComplaintComponent.prototype.ngOnInit = function () {
    };
    ComplaintComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-complaint',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./complaint.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/complaint/complaint.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./complaint.component.css */ "./src/app/complaint/complaint.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], ComplaintComponent);
    return ComplaintComponent;
}());



/***/ }),

/***/ "./src/app/contact-us/contact-us.component.css":
/*!*****************************************************!*\
  !*** ./src/app/contact-us/contact-us.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header {\r\n    color:white;\r\n    font-size: 27px;\r\n    font: bold;\r\n    padding: 10px;\r\n    margin-top: 15vh;\r\n    max-width: 100%;\r\n}\r\n\r\n.bigicon {\r\n    font-size: 35px;\r\n    color:  #FF375D;}\r\n\r\n.costom-contact{\r\n        margin: auto;\r\n    }\r\n\r\n.btn-lg{\r\n        background-color:#FF375D ;\r\n      margin-right: 26vh;\r\n    }\r\n\r\n.bb{\r\n        background-color: #4F008C;\r\n\r\n    }\r\n\r\n/* .nav-item{\r\n        font: bold;\r\n        color: #FF375D;\r\n    } */\r\n\r\n.costom-con {\r\n    color: white;     \r\n    font-size: larger;\r\n    margin-left: 40vh;\r\n\r\n}\r\n\r\n.sub{\r\n\r\n    margin-right: 30vh;\r\n}\r\n\r\n.btn-cancel{\r\n    background-color: #FF375D;\r\n    color: aliceblue;\r\n}\r\n\r\n.btn-send{\r\n    background-color:#4F008C;\r\n    color: aliceblue;\r\n\r\n}\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29udGFjdC11cy9jb250YWN0LXVzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxXQUFXO0lBQ1gsZUFBZTtJQUNmLFVBQVU7SUFDVixhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsZUFBZSxDQUFDOztBQUNoQjtRQUNJLFlBQVk7SUFDaEI7O0FBRUE7UUFDSSx5QkFBeUI7TUFDM0Isa0JBQWtCO0lBQ3BCOztBQUNBO1FBQ0kseUJBQXlCOztJQUU3Qjs7QUFDQTs7O09BR0c7O0FBRVA7SUFDSSxZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLGlCQUFpQjs7QUFFckI7O0FBQ0E7O0lBRUksa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0kseUJBQXlCO0lBQ3pCLGdCQUFnQjtBQUNwQjs7QUFDQTtJQUNJLHdCQUF3QjtJQUN4QixnQkFBZ0I7O0FBRXBCIiwiZmlsZSI6InNyYy9hcHAvY29udGFjdC11cy9jb250YWN0LXVzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyIHtcclxuICAgIGNvbG9yOndoaXRlO1xyXG4gICAgZm9udC1zaXplOiAyN3B4O1xyXG4gICAgZm9udDogYm9sZDtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxNXZoO1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uYmlnaWNvbiB7XHJcbiAgICBmb250LXNpemU6IDM1cHg7XHJcbiAgICBjb2xvcjogICNGRjM3NUQ7fVxyXG4gICAgLmNvc3RvbS1jb250YWN0e1xyXG4gICAgICAgIG1hcmdpbjogYXV0bztcclxuICAgIH1cclxuXHJcbiAgICAuYnRuLWxne1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6I0ZGMzc1RCA7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogMjZ2aDtcclxuICAgIH1cclxuICAgIC5iYntcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNEYwMDhDO1xyXG5cclxuICAgIH1cclxuICAgIC8qIC5uYXYtaXRlbXtcclxuICAgICAgICBmb250OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjRkYzNzVEO1xyXG4gICAgfSAqL1xyXG5cclxuLmNvc3RvbS1jb24ge1xyXG4gICAgY29sb3I6IHdoaXRlOyAgICAgXHJcbiAgICBmb250LXNpemU6IGxhcmdlcjtcclxuICAgIG1hcmdpbi1sZWZ0OiA0MHZoO1xyXG5cclxufVxyXG4uc3Vie1xyXG5cclxuICAgIG1hcmdpbi1yaWdodDogMzB2aDtcclxufVxyXG5cclxuLmJ0bi1jYW5jZWx7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkYzNzVEO1xyXG4gICAgY29sb3I6IGFsaWNlYmx1ZTtcclxufVxyXG4uYnRuLXNlbmR7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiM0RjAwOEM7XHJcbiAgICBjb2xvcjogYWxpY2VibHVlO1xyXG5cclxufVxyXG4gICJdfQ== */");

/***/ }),

/***/ "./src/app/contact-us/contact-us.component.ts":
/*!****************************************************!*\
  !*** ./src/app/contact-us/contact-us.component.ts ***!
  \****************************************************/
/*! exports provided: ContactUsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsComponent", function() { return ContactUsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var ContactUsComponent = /** @class */ (function () {
    function ContactUsComponent() {
        var _this = this;
        this.descriptionLength = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](0);
        this.subjectLength = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](0);
        this.textControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('');
        this.textontrol = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('');
        this.textControl.valueChanges.subscribe(function (v) { return _this.descriptionLength.next(v.length); });
        this.textontrol.valueChanges.subscribe(function (s) { return _this.subjectLength.next(s.length); });
    }
    ContactUsComponent.prototype.ngOnInit = function () {
        this.optionsSelect = [
            { value: 'Feedback', label: 'Feedback' },
            { value: 'Report a bug', label: 'Report a bug' },
            { value: 'Feature request', label: 'Feature request' },
            { value: 'Other stuff', label: 'Other stuff' },
        ];
    };
    ContactUsComponent.prototype.onSubmit = function () {
    };
    ContactUsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-contact-us',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./contact-us.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./contact-us.component.css */ "./src/app/contact-us/contact-us.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], ContactUsComponent);
    return ContactUsComponent;
}());



/***/ }),

/***/ "./src/app/containers/default-layout/default-layout.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/containers/default-layout/default-layout.component.ts ***!
  \***********************************************************************/
/*! exports provided: DefaultLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DefaultLayoutComponent", function() { return DefaultLayoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _nav__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../_nav */ "./src/app/_nav.ts");



var DefaultLayoutComponent = /** @class */ (function () {
    function DefaultLayoutComponent() {
        this.sidebarMinimized = false;
        this.navItems = _nav__WEBPACK_IMPORTED_MODULE_2__["navItems"];
    }
    DefaultLayoutComponent.prototype.toggleMinimize = function (e) {
        this.sidebarMinimized = e;
    };
    DefaultLayoutComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./default-layout.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/containers/default-layout/default-layout.component.html")).default
        })
    ], DefaultLayoutComponent);
    return DefaultLayoutComponent;
}());



/***/ }),

/***/ "./src/app/containers/default-layout/index.ts":
/*!****************************************************!*\
  !*** ./src/app/containers/default-layout/index.ts ***!
  \****************************************************/
/*! exports provided: DefaultLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _default_layout_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./default-layout.component */ "./src/app/containers/default-layout/default-layout.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DefaultLayoutComponent", function() { return _default_layout_component__WEBPACK_IMPORTED_MODULE_0__["DefaultLayoutComponent"]; });




/***/ }),

/***/ "./src/app/containers/index.ts":
/*!*************************************!*\
  !*** ./src/app/containers/index.ts ***!
  \*************************************/
/*! exports provided: DefaultLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _default_layout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./default-layout */ "./src/app/containers/default-layout/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DefaultLayoutComponent", function() { return _default_layout__WEBPACK_IMPORTED_MODULE_0__["DefaultLayoutComponent"]; });




/***/ }),

/***/ "./src/app/containers/main-layout/main-layout.component.css":
/*!******************************************************************!*\
  !*** ./src/app/containers/main-layout/main-layout.component.css ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".custm-n{\r\n    /* height: 10vh; */\r\n    font-size: larger;\r\n    font-weight: bold;\r\n   height: -webkit-max-content;\r\n   height: -moz-max-content;\r\n   height: max-content;\r\n}\r\n.color-white{\r\n  color:white;\r\n}\r\na:hover {\r\n  border-radius: 25px;\r\n    background-color: #ff375d;\r\n    color:white;\r\n  }\r\n.h3{\r\n  color: #525f7f;\r\n}\r\n.h2{\r\n  color: white;\r\n  display: inline-flex;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29udGFpbmVycy9tYWluLWxheW91dC9tYWluLWxheW91dC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixpQkFBaUI7R0FDbEIsMkJBQW1CO0dBQW5CLHdCQUFtQjtHQUFuQixtQkFBbUI7QUFDdEI7QUFDQTtFQUNFLFdBQVc7QUFDYjtBQUNBO0VBQ0UsbUJBQW1CO0lBQ2pCLHlCQUF5QjtJQUN6QixXQUFXO0VBQ2I7QUFDRjtFQUNFLGNBQWM7QUFDaEI7QUFDQTtFQUNFLFlBQVk7RUFDWixvQkFBb0I7QUFDdEIiLCJmaWxlIjoic3JjL2FwcC9jb250YWluZXJzL21haW4tbGF5b3V0L21haW4tbGF5b3V0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY3VzdG0tbntcclxuICAgIC8qIGhlaWdodDogMTB2aDsgKi9cclxuICAgIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgIGhlaWdodDogbWF4LWNvbnRlbnQ7XHJcbn1cclxuLmNvbG9yLXdoaXRle1xyXG4gIGNvbG9yOndoaXRlO1xyXG59XHJcbmE6aG92ZXIge1xyXG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmYzNzVkO1xyXG4gICAgY29sb3I6d2hpdGU7XHJcbiAgfVxyXG4uaDN7XHJcbiAgY29sb3I6ICM1MjVmN2Y7XHJcbn1cclxuLmgye1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/containers/main-layout/main-layout.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/containers/main-layout/main-layout.component.ts ***!
  \*****************************************************************/
/*! exports provided: MainLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainLayoutComponent", function() { return MainLayoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _language_language_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../language/language.helper */ "./src/app/language/language.helper.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_collapse__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/collapse */ "./node_modules/ngx-bootstrap/collapse/fesm5/ngx-bootstrap-collapse.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular_user_idle__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular-user-idle */ "./node_modules/angular-user-idle/fesm5/angular-user-idle.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! sweetalert */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(sweetalert__WEBPACK_IMPORTED_MODULE_8__);









var MainLayoutComponent = /** @class */ (function () {
    function MainLayoutComponent(renderer, languageHelper, translateService, router, userIdle) {
        this.renderer = renderer;
        this.languageHelper = languageHelper;
        this.translateService = translateService;
        this.router = router;
        this.userIdle = userIdle;
        this._isCollapsed = true;
        this.getAllLangs();
        this.isRTL = this.languageHelper.currentLang().isRTL;
    }
    Object.defineProperty(MainLayoutComponent.prototype, "isCollapsed", {
        get: function () {
            if (this.collapseRef) {
                // temp fix for "overflow: hidden"
                if (getComputedStyle(this.collapseRef.nativeElement).getPropertyValue('display') === 'flex') {
                    this.renderer.removeStyle(this.collapseRef.nativeElement, 'overflow');
                }
            }
            return this._isCollapsed;
        },
        set: function (value) {
            this._isCollapsed = value;
        },
        enumerable: true,
        configurable: true
    });
    MainLayoutComponent.prototype.ngOnInit = function () {
        this.searchForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            searchValue: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.translateService.setDefaultLang('en');
        this.translateService.use(this.languageHelper.currentLang().name);
        this.isRTL = this.languageHelper.currentLang().isRTL;
        this.userIdle.startWatching();
        this.timeStart$ = this.userIdle.onTimerStart().subscribe();
        this.timeOut$ = this.userIdle.onTimeout().subscribe(function () {
            var timer = 10, isTimerStarted = false;
            (function customSwal() {
                sweetalert__WEBPACK_IMPORTED_MODULE_8___default()({
                    title: "Time Out!",
                    text: "Sesstion will expire in " + timer + " seconds",
                    buttons: {
                        cancel: true,
                        confirm: true,
                    },
                    timer: !isTimerStarted ? timer * 1000 : 1100,
                });
                isTimerStarted = true;
                if (timer) {
                    if (timer == 8) {
                        window.location.reload();
                        this.router.navigateByUrl('\home');
                    }
                    timer--;
                    setTimeout(customSwal, 1000);
                }
            })();
        });
    };
    MainLayoutComponent.prototype.changeLang = function (lang) {
        debugger;
        this.languageHelper.changeLang(lang);
        window.location.reload();
    };
    MainLayoutComponent.prototype.getAllLangs = function () {
        var arr = this.languageHelper.getAllLanguages();
        this.english = arr[0];
        this.arabic = arr[1];
        return this.languageHelper.getAllLanguages();
    };
    MainLayoutComponent.prototype.ngAfterViewChecked = function () {
        this.collapseRef = this.collapse;
    };
    MainLayoutComponent.prototype.portal = function () {
        this.router.navigate(['portal-admin']);
    };
    MainLayoutComponent.prototype.ngOnDestroy = function () {
        this.timeOut$.unsubscribe();
        this.timeStart$.unsubscribe();
    };
    MainLayoutComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Renderer2"] },
        { type: _language_language_helper__WEBPACK_IMPORTED_MODULE_2__["LanguageHelper"] },
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
        { type: angular_user_idle__WEBPACK_IMPORTED_MODULE_7__["UserIdleService"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ViewChild"])(ngx_bootstrap_collapse__WEBPACK_IMPORTED_MODULE_5__["CollapseDirective"], { read: _angular_core__WEBPACK_IMPORTED_MODULE_4__["ElementRef"], static: false }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", ngx_bootstrap_collapse__WEBPACK_IMPORTED_MODULE_5__["CollapseDirective"])
    ], MainLayoutComponent.prototype, "collapse", void 0);
    MainLayoutComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
            selector: 'app-main-layout',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./main-layout.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/containers/main-layout/main-layout.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./main-layout.component.css */ "./src/app/containers/main-layout/main-layout.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_4__["Renderer2"], _language_language_helper__WEBPACK_IMPORTED_MODULE_2__["LanguageHelper"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], angular_user_idle__WEBPACK_IMPORTED_MODULE_7__["UserIdleService"]])
    ], MainLayoutComponent);
    return MainLayoutComponent;
}());



/***/ }),

/***/ "./src/app/dashboard-wbu/dashboard-wbu.component.css":
/*!***********************************************************!*\
  !*** ./src/app/dashboard-wbu/dashboard-wbu.component.css ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".font-dashboard{\r\n    background-color:  #4F008C;\r\n    padding-top: 4px;\r\n    padding-bottom: 4px;\r\n\r\n}\r\n.cards{\r\n margin-top: 20px;\r\npadding-top: 10x;\r\npadding-bottom: 33px;\r\nbackground-color :#D5D8DC;\r\nwidth: 10px;\r\nmargin: 10px;\r\npadding-top: 15px;\r\ntext-align: center;\r\nfont-weight: 500;\r\nheight: 80px;\r\ncolor: #ff375d;\r\nborder-radius: 25px;\r\n\r\n}\r\n.chart-wrapper{\r\n    width: 30%;\r\n    height: 100vh;\r\n    margin-top: 50px;\r\n}\r\n.pt-5{\r\n    padding-top: 3rem !important;\r\n}\r\n.h2{\r\n    font-family: inherit;\r\n    font-weight: 600;\r\n    line-height: 1.5;\r\n    font-size: 1.25rem;\r\n}\r\n.graph-align-right{\r\n    margin-right: 300px;\r\n    float: right;\r\n}\r\n.graph-align-left{\r\n    /* float: center; */\r\n    margin-left: 100px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkLXdidS9kYXNoYm9hcmQtd2J1LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSwwQkFBMEI7SUFDMUIsZ0JBQWdCO0lBQ2hCLG1CQUFtQjs7QUFFdkI7QUFDQTtDQUNDLGdCQUFnQjtBQUNqQixnQkFBZ0I7QUFDaEIsb0JBQW9CO0FBQ3BCLHlCQUF5QjtBQUN6QixXQUFXO0FBQ1gsWUFBWTtBQUNaLGlCQUFpQjtBQUNqQixrQkFBa0I7QUFDbEIsZ0JBQWdCO0FBQ2hCLFlBQVk7QUFDWixjQUFjO0FBQ2QsbUJBQW1COztBQUVuQjtBQUNBO0lBQ0ksVUFBVTtJQUNWLGFBQWE7SUFDYixnQkFBZ0I7QUFDcEI7QUFFQTtJQUNJLDRCQUE0QjtBQUNoQztBQUVBO0lBQ0ksb0JBQW9CO0lBQ3BCLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0FBQ3RCO0FBQ0E7SUFDSSxtQkFBbUI7SUFDbkIsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksbUJBQW1CO0lBQ25CLGtCQUFrQjtBQUN0QiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC13YnUvZGFzaGJvYXJkLXdidS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvbnQtZGFzaGJvYXJke1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogICM0RjAwOEM7XHJcbiAgICBwYWRkaW5nLXRvcDogNHB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDRweDtcclxuXHJcbn1cclxuLmNhcmRze1xyXG4gbWFyZ2luLXRvcDogMjBweDtcclxucGFkZGluZy10b3A6IDEweDtcclxucGFkZGluZy1ib3R0b206IDMzcHg7XHJcbmJhY2tncm91bmQtY29sb3IgOiNENUQ4REM7XHJcbndpZHRoOiAxMHB4O1xyXG5tYXJnaW46IDEwcHg7XHJcbnBhZGRpbmctdG9wOiAxNXB4O1xyXG50ZXh0LWFsaWduOiBjZW50ZXI7XHJcbmZvbnQtd2VpZ2h0OiA1MDA7XHJcbmhlaWdodDogODBweDtcclxuY29sb3I6ICNmZjM3NWQ7XHJcbmJvcmRlci1yYWRpdXM6IDI1cHg7XHJcblxyXG59XHJcbi5jaGFydC13cmFwcGVye1xyXG4gICAgd2lkdGg6IDMwJTtcclxuICAgIGhlaWdodDogMTAwdmg7XHJcbiAgICBtYXJnaW4tdG9wOiA1MHB4O1xyXG59XHJcblxyXG4ucHQtNXtcclxuICAgIHBhZGRpbmctdG9wOiAzcmVtICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5oMntcclxuICAgIGZvbnQtZmFtaWx5OiBpbmhlcml0O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgICBmb250LXNpemU6IDEuMjVyZW07XHJcbn1cclxuLmdyYXBoLWFsaWduLXJpZ2h0e1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAzMDBweDtcclxuICAgIGZsb2F0OiByaWdodDtcclxufVxyXG4uZ3JhcGgtYWxpZ24tbGVmdHtcclxuICAgIC8qIGZsb2F0OiBjZW50ZXI7ICovXHJcbiAgICBtYXJnaW4tbGVmdDogMTAwcHg7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/dashboard-wbu/dashboard-wbu.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/dashboard-wbu/dashboard-wbu.component.ts ***!
  \**********************************************************/
/*! exports provided: DashboardWBUComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardWBUComponent", function() { return DashboardWBUComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/fesm5/ng2-charts.js");



var DashboardWBUComponent = /** @class */ (function () {
    function DashboardWBUComponent() {
        this.barChartOptions = {
            responsive: true,
        };
        this.barChartLabels = ['General', 'Technical', 'FD'];
        this.barChartType = 'bar';
        this.barChartLegend = true;
        this.barChartPlugins = [];
        this.barChartData = [
            { data: [45, 37, 66, 70, 46, 33], label: 'Total per Complain type' }
        ];
        this.pieChartOptions = {
            responsive: true,
        };
        this.pieChartLabels = [['FD'], ['Technical'], 'General'];
        this.pieChartData = [30, 50, 20];
        this.pieChartType = 'pie';
        this.pieChartLegend = true;
        this.pieChartPlugins = [];
        Object(ng2_charts__WEBPACK_IMPORTED_MODULE_2__["monkeyPatchChartJsTooltip"])();
        Object(ng2_charts__WEBPACK_IMPORTED_MODULE_2__["monkeyPatchChartJsLegend"])();
    }
    DashboardWBUComponent.prototype.ngOnInit = function () {
    };
    DashboardWBUComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard-wbu',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./dashboard-wbu.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard-wbu/dashboard-wbu.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./dashboard-wbu.component.css */ "./src/app/dashboard-wbu/dashboard-wbu.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], DashboardWBUComponent);
    return DashboardWBUComponent;
}());



/***/ }),

/***/ "./src/app/details/details.component.css":
/*!***********************************************!*\
  !*** ./src/app/details/details.component.css ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".bg-gradient-danger{\r\n    background: #4F008c !important;\r\n}\r\n.pt-md-8{\r\n    padding-top: 2rem !important;\r\n}\r\n.h3 .mat-typography{\r\n    font-family: inherit;\r\n    font-weight: 600 !important;\r\n    line-height: 1.5;\r\n    margin-bottom: 0 !important;\r\n    font-size: 1.0625rem;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGV0YWlscy9kZXRhaWxzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSw4QkFBOEI7QUFDbEM7QUFDQTtJQUNJLDRCQUE0QjtBQUNoQztBQUNBO0lBQ0ksb0JBQW9CO0lBQ3BCLDJCQUEyQjtJQUMzQixnQkFBZ0I7SUFDaEIsMkJBQTJCO0lBQzNCLG9CQUFvQjtBQUN4QiIsImZpbGUiOiJzcmMvYXBwL2RldGFpbHMvZGV0YWlscy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJnLWdyYWRpZW50LWRhbmdlcntcclxuICAgIGJhY2tncm91bmQ6ICM0RjAwOGMgIWltcG9ydGFudDtcclxufVxyXG4ucHQtbWQtOHtcclxuICAgIHBhZGRpbmctdG9wOiAycmVtICFpbXBvcnRhbnQ7XHJcbn1cclxuLmgzIC5tYXQtdHlwb2dyYXBoeXtcclxuICAgIGZvbnQtZmFtaWx5OiBpbmhlcml0O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMCAhaW1wb3J0YW50O1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgIG1hcmdpbi1ib3R0b206IDAgIWltcG9ydGFudDtcclxuICAgIGZvbnQtc2l6ZTogMS4wNjI1cmVtO1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/details/details.component.ts":
/*!**********************************************!*\
  !*** ./src/app/details/details.component.ts ***!
  \**********************************************/
/*! exports provided: DetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailsComponent", function() { return DetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DetailsComponent = /** @class */ (function () {
    function DetailsComponent() {
    }
    DetailsComponent.prototype.ngOnInit = function () {
    };
    DetailsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-details',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./details.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/details/details.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./details.component.css */ "./src/app/details/details.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], DetailsComponent);
    return DetailsComponent;
}());



/***/ }),

/***/ "./src/app/display-invoice/display-invoice.component.css":
/*!***************************************************************!*\
  !*** ./src/app/display-invoice/display-invoice.component.css ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("::ng-deep .nav-tabs .nav-link.active{\r\n    background :#ffff;\r\n  }\r\n  /* ::ng-deep .nav-tabs .nav-link.active{\r\n  background: #4F008c !important;\r\n  } */\r\n  .form-group.hidden {\r\n  width: 0;\r\n  margin: 0;\r\n  border: none;\r\n  padding: 0;\r\n  }\r\n  .form-group.hidden .form-control{\r\n  display: none;\r\n  }\r\n  ::ng-deep .custom-day {\r\n  text-align: center;\r\n  padding: 0.185rem 0.25rem;\r\n  display: inline-block;\r\n  height: 2rem;\r\n  width: 2rem;\r\n  }\r\n  .mb-0{\r\n  font-size: 1.0625rem;\r\n  font-weight: 600;\r\n  }\r\n  .mb-1{\r\n    margin-left: 4rem;\r\n    font-size: x-large;\r\n  }\r\n  .header{\r\n  background: #f7fafc !important;\r\n  }\r\n  .form-control-label{\r\n  font-weight: 600;\r\n  }\r\n  .col-lg-4{\r\n  margin-left: 5rem;\r\n  margin-top: 3rem;\r\n  }\r\n  ::ng-deep .custom-day.focused {\r\n  background-color: #e6e6e6;\r\n  }\r\n  ::ng-deep .custom-day.range, ::ng-deep .custom-day:hover {\r\n  background-color: rgb(2, 117, 216);\r\n  color: white;\r\n  }\r\n  ::ng-deep .custom-day.faded {\r\n  background-color: rgba(2, 117, 216, 0.5);\r\n  }\r\n  .btn-outline-secondary {\r\n    color: #23282c;\r\n    border: 1px solid #cad1d7;\r\n    background-color: #fff;\r\n  }\r\n  ::ng-deep .tab-content .tab-pane {\r\n  padding: 0rem !important;\r\n  }\r\n  ::ng-deep .justify-content-start {\r\n  padding-left: 15px !important;\r\n  }\r\n  .submit{\r\n  color: #fff;\r\n  background-color: #FF375E;\r\n  border-color: #FF375E;\r\n  font-weight: 600;\r\n  text-align: center;\r\n  overflow: visible;\r\n    white-space: nowrap;\r\n    vertical-align: middle;\r\n    -webkit-user-select: none;\r\n    -moz-user-select: none;\r\n    -ms-user-select: none;\r\n    user-select: none;\r\n    border: 1px solid transparent;\r\n    padding: 0.625rem 1.25rem;\r\n    font-size: 1rem;\r\n    line-height: 1.5;\r\n    border-radius: 0.375rem;\r\n    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;\r\n  box-shadow: 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08);\r\n  }\r\n  .card{\r\n    background: #f7fafc;\r\n  }\r\n  .col-lg-8 {\r\n    padding-top: 5rem;\r\n  }\r\n  .icon-shape{\r\n    padding-right: 100px;\r\n    padding-top: 100px;\r\n    margin-top: -5rem;\r\n    background-color: currentcolor;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGlzcGxheS1pbnZvaWNlL2Rpc3BsYXktaW52b2ljZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0VBQ25CO0VBQ0E7O0tBRUc7RUFDSDtFQUNBLFFBQVE7RUFDUixTQUFTO0VBQ1QsWUFBWTtFQUNaLFVBQVU7RUFDVjtFQUNBO0VBQ0EsYUFBYTtFQUNiO0VBQ0E7RUFDQSxrQkFBa0I7RUFDbEIseUJBQXlCO0VBQ3pCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osV0FBVztFQUNYO0VBRUE7RUFDQSxvQkFBb0I7RUFDcEIsZ0JBQWdCO0VBQ2hCO0VBRUE7SUFDRSxpQkFBaUI7SUFDakIsa0JBQWtCO0VBQ3BCO0VBQ0E7RUFDQSw4QkFBOEI7RUFDOUI7RUFDQTtFQUNBLGdCQUFnQjtFQUNoQjtFQUVBO0VBQ0EsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQjtFQUNBO0VBQ0EseUJBQXlCO0VBQ3pCO0VBQ0E7RUFDQSxrQ0FBa0M7RUFDbEMsWUFBWTtFQUNaO0VBQ0E7RUFDQSx3Q0FBd0M7RUFDeEM7RUFDQTtJQUNFLGNBQWM7SUFDZCx5QkFBeUI7SUFDekIsc0JBQXNCO0VBQ3hCO0VBRUE7RUFDQSx3QkFBd0I7RUFDeEI7RUFDQTtFQUNBLDZCQUE2QjtFQUM3QjtFQUVBO0VBQ0EsV0FBVztFQUNYLHlCQUF5QjtFQUN6QixxQkFBcUI7RUFDckIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixpQkFBaUI7SUFDZixtQkFBbUI7SUFDbkIsc0JBQXNCO0lBQ3RCLHlCQUF5QjtJQUN6QixzQkFBc0I7SUFDdEIscUJBQXFCO0lBQ3JCLGlCQUFpQjtJQUNqQiw2QkFBNkI7SUFDN0IseUJBQXlCO0lBQ3pCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsdUJBQXVCO0lBQ3ZCLHFJQUFxSTtFQUN2SSwyRUFBMkU7RUFDM0U7RUFFQTtJQUNFLG1CQUFtQjtFQUNyQjtFQUVBO0lBQ0UsaUJBQWlCO0VBQ25CO0VBRUE7SUFDRSxvQkFBb0I7SUFDcEIsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQiw4QkFBOEI7RUFDaEMiLCJmaWxlIjoic3JjL2FwcC9kaXNwbGF5LWludm9pY2UvZGlzcGxheS1pbnZvaWNlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6Om5nLWRlZXAgLm5hdi10YWJzIC5uYXYtbGluay5hY3RpdmV7XHJcbiAgICBiYWNrZ3JvdW5kIDojZmZmZjtcclxuICB9XHJcbiAgLyogOjpuZy1kZWVwIC5uYXYtdGFicyAubmF2LWxpbmsuYWN0aXZle1xyXG4gIGJhY2tncm91bmQ6ICM0RjAwOGMgIWltcG9ydGFudDtcclxuICB9ICovXHJcbiAgLmZvcm0tZ3JvdXAuaGlkZGVuIHtcclxuICB3aWR0aDogMDtcclxuICBtYXJnaW46IDA7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIHBhZGRpbmc6IDA7XHJcbiAgfVxyXG4gIC5mb3JtLWdyb3VwLmhpZGRlbiAuZm9ybS1jb250cm9se1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG4gIDo6bmctZGVlcCAuY3VzdG9tLWRheSB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHBhZGRpbmc6IDAuMTg1cmVtIDAuMjVyZW07XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIGhlaWdodDogMnJlbTtcclxuICB3aWR0aDogMnJlbTtcclxuICB9XHJcbiAgXHJcbiAgLm1iLTB7XHJcbiAgZm9udC1zaXplOiAxLjA2MjVyZW07XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB9XHJcblxyXG4gIC5tYi0xe1xyXG4gICAgbWFyZ2luLWxlZnQ6IDRyZW07XHJcbiAgICBmb250LXNpemU6IHgtbGFyZ2U7XHJcbiAgfVxyXG4gIC5oZWFkZXJ7XHJcbiAgYmFja2dyb3VuZDogI2Y3ZmFmYyAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAuZm9ybS1jb250cm9sLWxhYmVse1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5jb2wtbGctNHtcclxuICBtYXJnaW4tbGVmdDogNXJlbTtcclxuICBtYXJnaW4tdG9wOiAzcmVtO1xyXG4gIH1cclxuICA6Om5nLWRlZXAgLmN1c3RvbS1kYXkuZm9jdXNlZCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U2ZTZlNjtcclxuICB9XHJcbiAgOjpuZy1kZWVwIC5jdXN0b20tZGF5LnJhbmdlLCA6Om5nLWRlZXAgLmN1c3RvbS1kYXk6aG92ZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyLCAxMTcsIDIxNik7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIH1cclxuICA6Om5nLWRlZXAgLmN1c3RvbS1kYXkuZmFkZWQge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMiwgMTE3LCAyMTYsIDAuNSk7XHJcbiAgfVxyXG4gIC5idG4tb3V0bGluZS1zZWNvbmRhcnkge1xyXG4gICAgY29sb3I6ICMyMzI4MmM7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2FkMWQ3O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICB9XHJcbiAgXHJcbiAgOjpuZy1kZWVwIC50YWItY29udGVudCAudGFiLXBhbmUge1xyXG4gIHBhZGRpbmc6IDByZW0gIWltcG9ydGFudDtcclxuICB9XHJcbiAgOjpuZy1kZWVwIC5qdXN0aWZ5LWNvbnRlbnQtc3RhcnQge1xyXG4gIHBhZGRpbmctbGVmdDogMTVweCAhaW1wb3J0YW50O1xyXG4gIH1cclxuICBcclxuICAuc3VibWl0e1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNGRjM3NUU7XHJcbiAgYm9yZGVyLWNvbG9yOiAjRkYzNzVFO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG92ZXJmbG93OiB2aXNpYmxlO1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgICAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgLW1vei11c2VyLXNlbGVjdDogbm9uZTtcclxuICAgIC1tcy11c2VyLXNlbGVjdDogbm9uZTtcclxuICAgIHVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgdHJhbnNwYXJlbnQ7XHJcbiAgICBwYWRkaW5nOiAwLjYyNXJlbSAxLjI1cmVtO1xyXG4gICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xyXG4gICAgdHJhbnNpdGlvbjogY29sb3IgMC4xNXMgZWFzZS1pbi1vdXQsIGJhY2tncm91bmQtY29sb3IgMC4xNXMgZWFzZS1pbi1vdXQsIGJvcmRlci1jb2xvciAwLjE1cyBlYXNlLWluLW91dCwgYm94LXNoYWRvdyAwLjE1cyBlYXNlLWluLW91dDtcclxuICBib3gtc2hhZG93OiAwIDRweCA2cHggcmdiYSg1MCwgNTAsIDkzLCAwLjExKSwgMCAxcHggM3B4IHJnYmEoMCwgMCwgMCwgMC4wOCk7XHJcbiAgfVxyXG4gIFxyXG4gIC5jYXJke1xyXG4gICAgYmFja2dyb3VuZDogI2Y3ZmFmYztcclxuICB9XHJcblxyXG4gIC5jb2wtbGctOCB7XHJcbiAgICBwYWRkaW5nLXRvcDogNXJlbTtcclxuICB9XHJcblxyXG4gIC5pY29uLXNoYXBle1xyXG4gICAgcGFkZGluZy1yaWdodDogMTAwcHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTAwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAtNXJlbTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGN1cnJlbnRjb2xvcjtcclxuICB9Il19 */");

/***/ }),

/***/ "./src/app/display-invoice/display-invoice.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/display-invoice/display-invoice.component.ts ***!
  \**************************************************************/
/*! exports provided: DisplayInvoiceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DisplayInvoiceComponent", function() { return DisplayInvoiceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");



var DisplayInvoiceComponent = /** @class */ (function () {
    function DisplayInvoiceComponent(calendar, formatter) {
        this.calendar = calendar;
        this.formatter = formatter;
        this.hoveredDate = null;
        this.invoice = true;
        this.invoiceSearch = false;
        this.fromDate = calendar.getToday();
        this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
    }
    DisplayInvoiceComponent.prototype.onDateSelection = function (date) {
        if (!this.fromDate && !this.toDate) {
            this.fromDate = date;
        }
        else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
            this.toDate = date;
        }
        else {
            this.toDate = null;
            this.fromDate = date;
        }
    };
    DisplayInvoiceComponent.prototype.isHovered = function (date) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
    };
    DisplayInvoiceComponent.prototype.isInside = function (date) {
        return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
    };
    DisplayInvoiceComponent.prototype.isRange = function (date) {
        return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
    };
    DisplayInvoiceComponent.prototype.validateInput = function (currentValue, input) {
        var parsed = this.formatter.parse(input);
        return parsed && this.calendar.isValid(_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbDate"].from(parsed)) ? _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbDate"].from(parsed) : currentValue;
    };
    DisplayInvoiceComponent.prototype.ngOnInit = function () {
    };
    DisplayInvoiceComponent.prototype.disply = function () {
        this.invoice = false;
        this.invoiceSearch = true;
    };
    DisplayInvoiceComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbCalendar"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbDateParserFormatter"] }
    ]; };
    DisplayInvoiceComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-display-invoice',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./display-invoice.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/display-invoice/display-invoice.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./display-invoice.component.css */ "./src/app/display-invoice/display-invoice.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbCalendar"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbDateParserFormatter"]])
    ], DisplayInvoiceComponent);
    return DisplayInvoiceComponent;
}());



/***/ }),

/***/ "./src/app/downloads-operations/downloads-operations.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/downloads-operations/downloads-operations.component.css ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("::ng-deep .nav-tabs .nav-link.active{\r\n  background :#ffff;\r\n}\r\n/* ::ng-deep .nav-tabs .nav-link.active{\r\nbackground: #4F008c !important;\r\n} */\r\n.form-group.hidden {\r\nwidth: 0;\r\nmargin: 0;\r\nborder: none;\r\npadding: 0;\r\n}\r\n.form-group.hidden .form-control{\r\ndisplay: none;\r\n}\r\n::ng-deep .custom-day {\r\ntext-align: center;\r\npadding: 0.185rem 0.25rem;\r\ndisplay: inline-block;\r\nheight: 2rem;\r\nwidth: 2rem;\r\n}\r\n.mb-0{\r\nfont-size: 1.0625rem;\r\nfont-weight: 600;\r\n}\r\n.header{\r\nbackground: #f7fafc !important;\r\n}\r\n.form-control-label{\r\nfont-weight: 600;\r\n}\r\n.col-lg-4{\r\nmargin-left: 1rem;\r\nmargin-top: 1rem;\r\n}\r\n::ng-deep .custom-day.focused {\r\nbackground-color: #e6e6e6;\r\n}\r\n::ng-deep .custom-day.range, ::ng-deep .custom-day:hover {\r\nbackground-color: rgb(2, 117, 216);\r\ncolor: white;\r\n}\r\n::ng-deep .custom-day.faded {\r\nbackground-color: rgba(2, 117, 216, 0.5);\r\n}\r\n.btn-outline-secondary {\r\n  color: #23282c;\r\n  border: 1px solid #cad1d7;\r\n  background-color: #fff;\r\n}\r\n::ng-deep .tab-content .tab-pane {\r\npadding: 0rem !important;\r\n}\r\n::ng-deep .justify-content-start {\r\npadding-left: 15px !important;\r\n}\r\n.submit{\r\ncolor: #fff;\r\nbackground-color: #FF375E;\r\nborder-color: #FF375E;\r\nfont-weight: 600;\r\ntext-align: center;\r\noverflow: visible;\r\n  white-space: nowrap;\r\n  vertical-align: middle;\r\n  -webkit-user-select: none;\r\n  -moz-user-select: none;\r\n  -ms-user-select: none;\r\n  user-select: none;\r\n  border: 1px solid transparent;\r\n  padding: 0.625rem 1.25rem;\r\n  font-size: 1rem;\r\n  line-height: 1.5;\r\n  border-radius: 0.375rem;\r\n  transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;\r\nbox-shadow: 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08);\r\n}\r\n.card{\r\n  background: #f7fafc;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZG93bmxvYWRzLW9wZXJhdGlvbnMvZG93bmxvYWRzLW9wZXJhdGlvbnMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGlCQUFpQjtBQUNuQjtBQUNBOztHQUVHO0FBQ0g7QUFDQSxRQUFRO0FBQ1IsU0FBUztBQUNULFlBQVk7QUFDWixVQUFVO0FBQ1Y7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCLHlCQUF5QjtBQUN6QixxQkFBcUI7QUFDckIsWUFBWTtBQUNaLFdBQVc7QUFDWDtBQUVBO0FBQ0Esb0JBQW9CO0FBQ3BCLGdCQUFnQjtBQUNoQjtBQUNBO0FBQ0EsOEJBQThCO0FBQzlCO0FBQ0E7QUFDQSxnQkFBZ0I7QUFDaEI7QUFFQTtBQUNBLGlCQUFpQjtBQUNqQixnQkFBZ0I7QUFDaEI7QUFDQTtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0Esa0NBQWtDO0FBQ2xDLFlBQVk7QUFDWjtBQUNBO0FBQ0Esd0NBQXdDO0FBQ3hDO0FBQ0E7RUFDRSxjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLHNCQUFzQjtBQUN4QjtBQUVBO0FBQ0Esd0JBQXdCO0FBQ3hCO0FBQ0E7QUFDQSw2QkFBNkI7QUFDN0I7QUFFQTtBQUNBLFdBQVc7QUFDWCx5QkFBeUI7QUFDekIscUJBQXFCO0FBQ3JCLGdCQUFnQjtBQUNoQixrQkFBa0I7QUFDbEIsaUJBQWlCO0VBQ2YsbUJBQW1CO0VBQ25CLHNCQUFzQjtFQUN0Qix5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLHFCQUFxQjtFQUNyQixpQkFBaUI7RUFDakIsNkJBQTZCO0VBQzdCLHlCQUF5QjtFQUN6QixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLHVCQUF1QjtFQUN2QixxSUFBcUk7QUFDdkksMkVBQTJFO0FBQzNFO0FBRUE7RUFDRSxtQkFBbUI7QUFDckIiLCJmaWxlIjoic3JjL2FwcC9kb3dubG9hZHMtb3BlcmF0aW9ucy9kb3dubG9hZHMtb3BlcmF0aW9ucy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOjpuZy1kZWVwIC5uYXYtdGFicyAubmF2LWxpbmsuYWN0aXZle1xyXG4gIGJhY2tncm91bmQgOiNmZmZmO1xyXG59XHJcbi8qIDo6bmctZGVlcCAubmF2LXRhYnMgLm5hdi1saW5rLmFjdGl2ZXtcclxuYmFja2dyb3VuZDogIzRGMDA4YyAhaW1wb3J0YW50O1xyXG59ICovXHJcbi5mb3JtLWdyb3VwLmhpZGRlbiB7XHJcbndpZHRoOiAwO1xyXG5tYXJnaW46IDA7XHJcbmJvcmRlcjogbm9uZTtcclxucGFkZGluZzogMDtcclxufVxyXG4uZm9ybS1ncm91cC5oaWRkZW4gLmZvcm0tY29udHJvbHtcclxuZGlzcGxheTogbm9uZTtcclxufVxyXG46Om5nLWRlZXAgLmN1c3RvbS1kYXkge1xyXG50ZXh0LWFsaWduOiBjZW50ZXI7XHJcbnBhZGRpbmc6IDAuMTg1cmVtIDAuMjVyZW07XHJcbmRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuaGVpZ2h0OiAycmVtO1xyXG53aWR0aDogMnJlbTtcclxufVxyXG5cclxuLm1iLTB7XHJcbmZvbnQtc2l6ZTogMS4wNjI1cmVtO1xyXG5mb250LXdlaWdodDogNjAwO1xyXG59XHJcbi5oZWFkZXJ7XHJcbmJhY2tncm91bmQ6ICNmN2ZhZmMgIWltcG9ydGFudDtcclxufVxyXG4uZm9ybS1jb250cm9sLWxhYmVse1xyXG5mb250LXdlaWdodDogNjAwO1xyXG59XHJcblxyXG4uY29sLWxnLTR7XHJcbm1hcmdpbi1sZWZ0OiAxcmVtO1xyXG5tYXJnaW4tdG9wOiAxcmVtO1xyXG59XHJcbjo6bmctZGVlcCAuY3VzdG9tLWRheS5mb2N1c2VkIHtcclxuYmFja2dyb3VuZC1jb2xvcjogI2U2ZTZlNjtcclxufVxyXG46Om5nLWRlZXAgLmN1c3RvbS1kYXkucmFuZ2UsIDo6bmctZGVlcCAuY3VzdG9tLWRheTpob3ZlciB7XHJcbmJhY2tncm91bmQtY29sb3I6IHJnYigyLCAxMTcsIDIxNik7XHJcbmNvbG9yOiB3aGl0ZTtcclxufVxyXG46Om5nLWRlZXAgLmN1c3RvbS1kYXkuZmFkZWQge1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDIsIDExNywgMjE2LCAwLjUpO1xyXG59XHJcbi5idG4tb3V0bGluZS1zZWNvbmRhcnkge1xyXG4gIGNvbG9yOiAjMjMyODJjO1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNjYWQxZDc7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxufVxyXG5cclxuOjpuZy1kZWVwIC50YWItY29udGVudCAudGFiLXBhbmUge1xyXG5wYWRkaW5nOiAwcmVtICFpbXBvcnRhbnQ7XHJcbn1cclxuOjpuZy1kZWVwIC5qdXN0aWZ5LWNvbnRlbnQtc3RhcnQge1xyXG5wYWRkaW5nLWxlZnQ6IDE1cHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLnN1Ym1pdHtcclxuY29sb3I6ICNmZmY7XHJcbmJhY2tncm91bmQtY29sb3I6ICNGRjM3NUU7XHJcbmJvcmRlci1jb2xvcjogI0ZGMzc1RTtcclxuZm9udC13ZWlnaHQ6IDYwMDtcclxudGV4dC1hbGlnbjogY2VudGVyO1xyXG5vdmVyZmxvdzogdmlzaWJsZTtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcclxuICAtbW96LXVzZXItc2VsZWN0OiBub25lO1xyXG4gIC1tcy11c2VyLXNlbGVjdDogbm9uZTtcclxuICB1c2VyLXNlbGVjdDogbm9uZTtcclxuICBib3JkZXI6IDFweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICBwYWRkaW5nOiAwLjYyNXJlbSAxLjI1cmVtO1xyXG4gIGZvbnQtc2l6ZTogMXJlbTtcclxuICBsaW5lLWhlaWdodDogMS41O1xyXG4gIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xyXG4gIHRyYW5zaXRpb246IGNvbG9yIDAuMTVzIGVhc2UtaW4tb3V0LCBiYWNrZ3JvdW5kLWNvbG9yIDAuMTVzIGVhc2UtaW4tb3V0LCBib3JkZXItY29sb3IgMC4xNXMgZWFzZS1pbi1vdXQsIGJveC1zaGFkb3cgMC4xNXMgZWFzZS1pbi1vdXQ7XHJcbmJveC1zaGFkb3c6IDAgNHB4IDZweCByZ2JhKDUwLCA1MCwgOTMsIDAuMTEpLCAwIDFweCAzcHggcmdiYSgwLCAwLCAwLCAwLjA4KTtcclxufVxyXG5cclxuLmNhcmR7XHJcbiAgYmFja2dyb3VuZDogI2Y3ZmFmYztcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/downloads-operations/downloads-operations.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/downloads-operations/downloads-operations.component.ts ***!
  \************************************************************************/
/*! exports provided: DownloadsOperationsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DownloadsOperationsComponent", function() { return DownloadsOperationsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");



var DownloadsOperationsComponent = /** @class */ (function () {
    function DownloadsOperationsComponent(calendar, formatter) {
        this.calendar = calendar;
        this.formatter = formatter;
        this.hoveredDate = null;
        this.fromDate = calendar.getToday();
        this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
    }
    DownloadsOperationsComponent.prototype.onDateSelection = function (date) {
        if (!this.fromDate && !this.toDate) {
            this.fromDate = date;
        }
        else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
            this.toDate = date;
        }
        else {
            this.toDate = null;
            this.fromDate = date;
        }
    };
    DownloadsOperationsComponent.prototype.isHovered = function (date) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
    };
    DownloadsOperationsComponent.prototype.isInside = function (date) {
        return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
    };
    DownloadsOperationsComponent.prototype.isRange = function (date) {
        return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
    };
    DownloadsOperationsComponent.prototype.validateInput = function (currentValue, input) {
        var parsed = this.formatter.parse(input);
        return parsed && this.calendar.isValid(_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbDate"].from(parsed)) ? _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbDate"].from(parsed) : currentValue;
    };
    DownloadsOperationsComponent.prototype.ngOnInit = function () {
    };
    DownloadsOperationsComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbCalendar"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbDateParserFormatter"] }
    ]; };
    DownloadsOperationsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-downloads-operations',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./downloads-operations.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/downloads-operations/downloads-operations.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./downloads-operations.component.css */ "./src/app/downloads-operations/downloads-operations.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbCalendar"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbDateParserFormatter"]])
    ], DownloadsOperationsComponent);
    return DownloadsOperationsComponent;
}());



/***/ }),

/***/ "./src/app/help/help.component.css":
/*!*****************************************!*\
  !*** ./src/app/help/help.component.css ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".carousel-inner .active,\r\n.carousel-inner .active + .carousel-item,\r\n.carousel-inner .active + .carousel-item + .carousel-item {\r\n  display: block;\r\n}\r\n\r\n.carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),\r\n.carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,\r\n.carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item {\r\n  transition: none;\r\n}\r\n\r\n.carousel-inner .active.carousel-item + .carousel-item + .carousel-item + .carousel-item {\r\n  position: absolute;\r\n  top: 0;\r\n  right: -33.3333%;\r\n  z-index: -1;\r\n  display: block;\r\n  visibility: visible;\r\n}\r\n\r\n/* left or forward direction */\r\n\r\n.active.carousel-item-left + .carousel-item-next.carousel-item-left,\r\n.carousel-item-next.carousel-item-left + .carousel-item,\r\n.carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,\r\n.carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item {\r\n  position: relative;\r\n  transform: translate3d(-100%, 0, 0);\r\n  visibility: visible;\r\n}\r\n\r\n/* farthest right hidden item must be abso position for animations */\r\n\r\n.carousel-inner .carousel-item-prev.carousel-item-right {\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0;\r\n  z-index: -1;\r\n  display: block;\r\n  visibility: visible;\r\n}\r\n\r\n/* right or prev direction */\r\n\r\n.active.carousel-item-right + .carousel-item-prev.carousel-item-right,\r\n.carousel-item-prev.carousel-item-right + .carousel-item,\r\n.carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,\r\n.carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item {\r\n  position: relative;\r\n  transform: translate3d(100%, 0, 0);\r\n  visibility: visible;\r\n  display: block;\r\n}\r\n\r\na.carousel-control-next, a.carousel-control-prev  {\r\n  position: fixed;\r\n  top: 10px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaGVscC9oZWxwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7OztFQUdFLGNBQWM7QUFDaEI7O0FBRUE7OztFQUdFLGdCQUFnQjtBQUNsQjs7QUFHQTtFQUNFLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sZ0JBQWdCO0VBQ2hCLFdBQVc7RUFDWCxjQUFjO0VBQ2QsbUJBQW1CO0FBQ3JCOztBQUVBLDhCQUE4Qjs7QUFDOUI7Ozs7RUFJRSxrQkFBa0I7RUFDbEIsbUNBQW1DO0VBQ25DLG1CQUFtQjtBQUNyQjs7QUFFQSxvRUFBb0U7O0FBQ3BFO0VBQ0Usa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixPQUFPO0VBQ1AsV0FBVztFQUNYLGNBQWM7RUFDZCxtQkFBbUI7QUFDckI7O0FBRUEsNEJBQTRCOztBQUM1Qjs7OztFQUlFLGtCQUFrQjtFQUNsQixrQ0FBa0M7RUFDbEMsbUJBQW1CO0VBQ25CLGNBQWM7QUFDaEI7O0FBQ0E7RUFDRSxlQUFlO0VBQ2YsU0FBUztBQUNYIiwiZmlsZSI6InNyYy9hcHAvaGVscC9oZWxwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2Fyb3VzZWwtaW5uZXIgLmFjdGl2ZSxcclxuLmNhcm91c2VsLWlubmVyIC5hY3RpdmUgKyAuY2Fyb3VzZWwtaXRlbSxcclxuLmNhcm91c2VsLWlubmVyIC5hY3RpdmUgKyAuY2Fyb3VzZWwtaXRlbSArIC5jYXJvdXNlbC1pdGVtIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxufVxyXG5cclxuLmNhcm91c2VsLWlubmVyIC5jYXJvdXNlbC1pdGVtLmFjdGl2ZTpub3QoLmNhcm91c2VsLWl0ZW0tcmlnaHQpOm5vdCguY2Fyb3VzZWwtaXRlbS1sZWZ0KSxcclxuLmNhcm91c2VsLWlubmVyIC5jYXJvdXNlbC1pdGVtLmFjdGl2ZTpub3QoLmNhcm91c2VsLWl0ZW0tcmlnaHQpOm5vdCguY2Fyb3VzZWwtaXRlbS1sZWZ0KSArIC5jYXJvdXNlbC1pdGVtLFxyXG4uY2Fyb3VzZWwtaW5uZXIgLmNhcm91c2VsLWl0ZW0uYWN0aXZlOm5vdCguY2Fyb3VzZWwtaXRlbS1yaWdodCk6bm90KC5jYXJvdXNlbC1pdGVtLWxlZnQpICsgLmNhcm91c2VsLWl0ZW0gKyAuY2Fyb3VzZWwtaXRlbSB7XHJcbiAgdHJhbnNpdGlvbjogbm9uZTtcclxufVxyXG5cclxuXHJcbi5jYXJvdXNlbC1pbm5lciAuYWN0aXZlLmNhcm91c2VsLWl0ZW0gKyAuY2Fyb3VzZWwtaXRlbSArIC5jYXJvdXNlbC1pdGVtICsgLmNhcm91c2VsLWl0ZW0ge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDA7XHJcbiAgcmlnaHQ6IC0zMy4zMzMzJTtcclxuICB6LWluZGV4OiAtMTtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xyXG59XHJcblxyXG4vKiBsZWZ0IG9yIGZvcndhcmQgZGlyZWN0aW9uICovXHJcbi5hY3RpdmUuY2Fyb3VzZWwtaXRlbS1sZWZ0ICsgLmNhcm91c2VsLWl0ZW0tbmV4dC5jYXJvdXNlbC1pdGVtLWxlZnQsXHJcbi5jYXJvdXNlbC1pdGVtLW5leHQuY2Fyb3VzZWwtaXRlbS1sZWZ0ICsgLmNhcm91c2VsLWl0ZW0sXHJcbi5jYXJvdXNlbC1pdGVtLW5leHQuY2Fyb3VzZWwtaXRlbS1sZWZ0ICsgLmNhcm91c2VsLWl0ZW0gKyAuY2Fyb3VzZWwtaXRlbSxcclxuLmNhcm91c2VsLWl0ZW0tbmV4dC5jYXJvdXNlbC1pdGVtLWxlZnQgKyAuY2Fyb3VzZWwtaXRlbSArIC5jYXJvdXNlbC1pdGVtICsgLmNhcm91c2VsLWl0ZW0ge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKC0xMDAlLCAwLCAwKTtcclxuICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xyXG59XHJcblxyXG4vKiBmYXJ0aGVzdCByaWdodCBoaWRkZW4gaXRlbSBtdXN0IGJlIGFic28gcG9zaXRpb24gZm9yIGFuaW1hdGlvbnMgKi9cclxuLmNhcm91c2VsLWlubmVyIC5jYXJvdXNlbC1pdGVtLXByZXYuY2Fyb3VzZWwtaXRlbS1yaWdodCB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMDtcclxuICBsZWZ0OiAwO1xyXG4gIHotaW5kZXg6IC0xO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHZpc2liaWxpdHk6IHZpc2libGU7XHJcbn1cclxuXHJcbi8qIHJpZ2h0IG9yIHByZXYgZGlyZWN0aW9uICovXHJcbi5hY3RpdmUuY2Fyb3VzZWwtaXRlbS1yaWdodCArIC5jYXJvdXNlbC1pdGVtLXByZXYuY2Fyb3VzZWwtaXRlbS1yaWdodCxcclxuLmNhcm91c2VsLWl0ZW0tcHJldi5jYXJvdXNlbC1pdGVtLXJpZ2h0ICsgLmNhcm91c2VsLWl0ZW0sXHJcbi5jYXJvdXNlbC1pdGVtLXByZXYuY2Fyb3VzZWwtaXRlbS1yaWdodCArIC5jYXJvdXNlbC1pdGVtICsgLmNhcm91c2VsLWl0ZW0sXHJcbi5jYXJvdXNlbC1pdGVtLXByZXYuY2Fyb3VzZWwtaXRlbS1yaWdodCArIC5jYXJvdXNlbC1pdGVtICsgLmNhcm91c2VsLWl0ZW0gKyAuY2Fyb3VzZWwtaXRlbSB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMTAwJSwgMCwgMCk7XHJcbiAgdmlzaWJpbGl0eTogdmlzaWJsZTtcclxuICBkaXNwbGF5OiBibG9jaztcclxufVxyXG5hLmNhcm91c2VsLWNvbnRyb2wtbmV4dCwgYS5jYXJvdXNlbC1jb250cm9sLXByZXYgIHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgdG9wOiAxMHB4O1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/help/help.component.ts":
/*!****************************************!*\
  !*** ./src/app/help/help.component.ts ***!
  \****************************************/
/*! exports provided: HelpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelpComponent", function() { return HelpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HelpComponent = /** @class */ (function () {
    function HelpComponent() {
        this.name = 'Angular';
    }
    HelpComponent.prototype.ngAfterViewInit = function () {
        $("#myCarousel").on("slide.bs.carousel", function (e) {
            var $e = $(e.relatedTarget);
            var idx = $e.index();
            var itemsPerSlide = 3;
            var totalItems = $(".carousel-item").length;
            if (idx >= totalItems - (itemsPerSlide - 1)) {
                var it = (totalItems - idx * itemsPerSlide);
                for (var i = 0; i < it; i++) {
                    // append slides to end
                    if (e.direction == "left") {
                        $(".carousel-item")
                            .eq(i)
                            .appendTo(".carousel-inner");
                    }
                    else {
                        $(".carousel-item")
                            .eq(0)
                            .appendTo($(this).find(".carousel-inner"));
                    }
                }
            }
        });
    };
    HelpComponent.prototype.ngOnInit = function () {
    };
    HelpComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-help',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./help.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/help/help.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./help.component.css */ "./src/app/help/help.component.css")).default]
        })
    ], HelpComponent);
    return HelpComponent;
}());



/***/ }),

/***/ "./src/app/home/_helpers/must-match.validator.ts":
/*!*******************************************************!*\
  !*** ./src/app/home/_helpers/must-match.validator.ts ***!
  \*******************************************************/
/*! exports provided: MustMatch */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MustMatch", function() { return MustMatch; });
// custom validator to check that two fields match
function MustMatch(controlName, matchingControlName) {
    return function (formGroup) {
        var control = formGroup.controls[controlName];
        var matchingControl = formGroup.controls[matchingControlName];
        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
        }
        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        }
        else {
            matchingControl.setErrors(null);
        }
    };
}


/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("[dir=rtl]  \r\n.btn-neutral{\r\nfloat: left;}\r\n[dir=rtl]  \r\n.text-muted{\r\ntext-align: right;\r\n}\r\n[dir=rtl]  \r\n.h2,.mr-2, .mx-2{\r\n    float: right;\r\n}\r\n.link{\r\n        color: #1a0dab !important;\r\n        cursor: pointer;\r\n}\r\n.color-white{\r\n    color: white;\r\n    font-size: .875rem;\r\n    line-height: 1.5;\r\n    font-weight: 600;\r\n\r\n}\r\n.color-pink{\r\n    color: #FF375D;\r\n    border: 1px solid #cad1d7;\r\n}\r\n::ng-deep .input-group-text-card{\r\n    display: flex;\r\n    align-items: center;\r\n    padding: 0.3rem 0.5rem;\r\n    margin-bottom: 0;\r\n    font-size: 1rem;\r\n    font-weight: 600;\r\n    line-height: 1.5;\r\n    color: #FF375D;\r\n    text-align: center;\r\n    white-space: nowrap;\r\n    border: 1px solid #ced4da;\r\n    border-radius: 0.25rem;\r\n}\r\n.display-line{\r\n    max-width: 90%;\r\n    padding-left: 9rem;\r\n    display: flex;\r\n}\r\n.font-home{\r\n    padding-top: 2rem; \r\n    padding-left: 8rem; \r\n    font-size: 1rem;\r\n    \r\n}\r\n.border-line{\r\n    color: white;\r\n    border-top: solid;\r\n}\r\n.mat-stepper-horizontal {\r\n    margin-top: 8px;\r\n  }\r\n.mat-form-field {\r\n    margin-top: 16px;\r\n  }\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0FBRUEsV0FBVyxDQUFDO0FBQ1o7O0FBRUEsaUJBQWlCO0FBQ2pCO0FBQ0E7O0lBRUksWUFBWTtBQUNoQjtBQUVBO1FBQ1EseUJBQXlCO1FBQ3pCLGVBQWU7QUFDdkI7QUFFQTtJQUNJLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLGdCQUFnQjs7QUFFcEI7QUFDQTtJQUNJLGNBQWM7SUFDZCx5QkFBeUI7QUFDN0I7QUFFQTtJQUNJLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsc0JBQXNCO0lBQ3RCLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIsc0JBQXNCO0FBQzFCO0FBRUE7SUFDSSxjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLGFBQWE7QUFDakI7QUFFQTtJQUNJLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsZUFBZTs7QUFFbkI7QUFFQTtJQUNJLFlBQVk7SUFDWixpQkFBaUI7QUFDckI7QUFDQTtJQUNJLGVBQWU7RUFDakI7QUFFQTtJQUNFLGdCQUFnQjtFQUNsQiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiW2Rpcj1ydGxdICBcclxuLmJ0bi1uZXV0cmFse1xyXG5mbG9hdDogbGVmdDt9XHJcbltkaXI9cnRsXSAgXHJcbi50ZXh0LW11dGVke1xyXG50ZXh0LWFsaWduOiByaWdodDtcclxufVxyXG5bZGlyPXJ0bF0gIFxyXG4uaDIsLm1yLTIsIC5teC0ye1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcblxyXG4ubGlua3tcclxuICAgICAgICBjb2xvcjogIzFhMGRhYiAhaW1wb3J0YW50O1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLmNvbG9yLXdoaXRle1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAuODc1cmVtO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcblxyXG59XHJcbi5jb2xvci1waW5re1xyXG4gICAgY29sb3I6ICNGRjM3NUQ7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2FkMWQ3O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLmlucHV0LWdyb3VwLXRleHQtY2FyZHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgcGFkZGluZzogMC4zcmVtIDAuNXJlbTtcclxuICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICBmb250LXNpemU6IDFyZW07XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgIGNvbG9yOiAjRkYzNzVEO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjZWQ0ZGE7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwLjI1cmVtO1xyXG59XHJcblxyXG4uZGlzcGxheS1saW5le1xyXG4gICAgbWF4LXdpZHRoOiA5MCU7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDlyZW07XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG4uZm9udC1ob21le1xyXG4gICAgcGFkZGluZy10b3A6IDJyZW07IFxyXG4gICAgcGFkZGluZy1sZWZ0OiA4cmVtOyBcclxuICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgIFxyXG59XHJcblxyXG4uYm9yZGVyLWxpbmV7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBib3JkZXItdG9wOiBzb2xpZDtcclxufVxyXG4ubWF0LXN0ZXBwZXItaG9yaXpvbnRhbCB7XHJcbiAgICBtYXJnaW4tdG9wOiA4cHg7XHJcbiAgfVxyXG4gIFxyXG4gIC5tYXQtZm9ybS1maWVsZCB7XHJcbiAgICBtYXJnaW4tdG9wOiAxNnB4O1xyXG4gIH1cclxuICAiXX0= */");

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent, CpChangeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CpChangeComponent", function() { return CpChangeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/fesm5/dialog.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _helpers_must_match_validator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./_helpers/must-match.validator */ "./src/app/home/_helpers/must-match.validator.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var HomeComponent = /** @class */ (function () {
    function HomeComponent(dialog) {
        this.dialog = dialog;
        this.options = [];
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.dialog.open(CpChangeComponent, {
            width: "700px",
            data: { options: this.options },
            disableClose: true
        });
    };
    HomeComponent.ctorParameters = function () { return [
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] }
    ]; };
    HomeComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]])
    ], HomeComponent);
    return HomeComponent;
}());

var CpChangeComponent = /** @class */ (function () {
    function CpChangeComponent(data, formBuilder, router) {
        this.data = data;
        this.formBuilder = formBuilder;
        this.router = router;
        this.submitted = false;
    }
    CpChangeComponent.prototype.ngOnInit = function () {
        this.registerForm = this.formBuilder.group({
            currpassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, this.password],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(8)]],
            confirmPassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        }, {
            validator: Object(_helpers_must_match_validator__WEBPACK_IMPORTED_MODULE_5__["MustMatch"])('password', 'confirmPassword')
        });
    };
    Object.defineProperty(CpChangeComponent.prototype, "f", {
        get: function () { return this.registerForm.controls; },
        enumerable: true,
        configurable: true
    });
    CpChangeComponent.prototype.onSubmit = function () {
        this.submitted = true;
        if (this.registerForm.valid) {
            if (this.current == "11") {
                var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: function (toast) {
                        toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.stopTimer);
                        toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.resumeTimer);
                    }
                });
                Toast.fire({
                    icon: 'success',
                    title: 'Password updated successfully'
                });
            }
            else {
                alert("Current password is wrong");
            }
        }
        else {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Password is wrong'
            });
        }
    };
    CpChangeComponent.prototype.trueChart = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 35 || charCode > 39) && (charCode < 42 || charCode > 43)
            && (charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57)
            && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
    };
    CpChangeComponent.prototype.onReset = function () {
        this.submitted = false;
        this.registerForm.reset();
    };
    CpChangeComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"],] }] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] }
    ]; };
    CpChangeComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cp-change',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./cp-change.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/cp-change.component.html")).default,
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__param"])(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [Object, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], CpChangeComponent);
    return CpChangeComponent;
}());



/***/ }),

/***/ "./src/app/language/language.constant.ts":
/*!***********************************************!*\
  !*** ./src/app/language/language.constant.ts ***!
  \***********************************************/
/*! exports provided: LANGUAGES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LANGUAGES", function() { return LANGUAGES; });
var LANGUAGES = [
    { name: 'en', title: 'English', isRTL: false, icon: 'assets/img/flags/US.png' },
    { name: 'ar', title: 'عربي', isRTL: true, icon: 'assets/img/flags/SA.png' }
];


/***/ }),

/***/ "./src/app/language/language.helper.ts":
/*!*********************************************!*\
  !*** ./src/app/language/language.helper.ts ***!
  \*********************************************/
/*! exports provided: LanguageHelper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LanguageHelper", function() { return LanguageHelper; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _language_constant__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./language.constant */ "./src/app/language/language.constant.ts");
/* harmony import */ var ngx_webstorage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-webstorage */ "./node_modules/ngx-webstorage/fesm5/ngx-webstorage.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");



// import { DOCUMENT } from '@angular/platform-browser';


var LanguageHelper = /** @class */ (function () {
    function LanguageHelper(
    // @Inject(DOCUMENT) private document,
    localStorageService, translateService) {
        this.localStorageService = localStorageService;
        this.translateService = translateService;
    }
    LanguageHelper.prototype.getAllLanguages = function () {
        return _language_constant__WEBPACK_IMPORTED_MODULE_2__["LANGUAGES"];
    };
    LanguageHelper.prototype.changeLang = function (lang) {
        this.localStorageService.store('lang', lang.name);
        this.translateService.use(lang.name);
    };
    LanguageHelper.prototype.currentLang = function () {
        var stored = this.localStorageService.retrieve('lang');
        var current = this.translateService.currentLang;
        var optLangs = [
            { name: 'en_US', title: 'English', isRTL: false },
            { name: 'ar_SA', title: 'عربي', isRTL: true }
        ];
        if (stored) {
            current = stored;
        }
        if (!current) {
            current = 'en';
        }
        var lang = _language_constant__WEBPACK_IMPORTED_MODULE_2__["LANGUAGES"].find(function (cur) { return cur.name === current; });
        if (!lang) {
            lang = optLangs.find(function (cur) { return cur.name === current; });
            if (lang.name == 'en_US')
                lang.name = 'en';
            if (lang.name == 'ar_SA')
                lang.name = 'ar';
        }
        return lang;
    };
    LanguageHelper.ctorParameters = function () { return [
        { type: ngx_webstorage__WEBPACK_IMPORTED_MODULE_3__["LocalStorageService"] },
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"] }
    ]; };
    LanguageHelper = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [ngx_webstorage__WEBPACK_IMPORTED_MODULE_3__["LocalStorageService"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"]])
    ], LanguageHelper);
    return LanguageHelper;
}());



/***/ }),

/***/ "./src/app/nav/nav.component.css":
/*!***************************************!*\
  !*** ./src/app/nav/nav.component.css ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".form-gap {\r\n    padding-top: 70px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmF2L25hdi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0FBQ3JCIiwiZmlsZSI6InNyYy9hcHAvbmF2L25hdi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcm0tZ2FwIHtcclxuICAgIHBhZGRpbmctdG9wOiA3MHB4O1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/nav/nav.component.ts":
/*!**************************************!*\
  !*** ./src/app/nav/nav.component.ts ***!
  \**************************************/
/*! exports provided: NavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavComponent", function() { return NavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NavComponent = /** @class */ (function () {
    function NavComponent() {
    }
    NavComponent.prototype.ngOnInit = function () {
    };
    NavComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-nav',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./nav.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/nav/nav.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./nav.component.css */ "./src/app/nav/nav.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], NavComponent);
    return NavComponent;
}());



/***/ }),

/***/ "./src/app/new-partner/new-partner.component.css":
/*!*******************************************************!*\
  !*** ./src/app/new-partner/new-partner.component.css ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".country-picker {\r\n    border: 1px solid #f0f3f5;\r\n    border-radius: 5px;\r\n    position: relative;\r\n    margin-bottom: 18px;\r\n}\r\n.picker-button {\r\n    background: #f0f3f5;\r\n    /* color: #737e88; */\r\n    width: 28px;\r\n    height: 31px;\r\n    text-align: center;\r\n    line-height: 30px;\r\n    cursor: pointer;\r\n    display: inline-block;\r\n}\r\n.current{\r\n    display: inline-block;\r\n}\r\n.country-picker ul {\r\n    list-style: none;\r\n    position: absolute;\r\n    background: #fff;\r\n    padding: 0;\r\n    width: 100%;\r\n    margin: 0;\r\n    height: calc(35px * 5);\r\n    overflow: hidden;\r\n    overflow-y: scroll;\r\n    z-index: 99;\r\n}\r\n.country-picker ul li {\r\n    padding: 7px;\r\n    cursor: pointer;\r\n    border-bottom: 1px solid #777;\r\n    transition: 0.3s;\r\n    margin-top: 20px;\r\n\r\n}\r\n.country-picker ul li:hover{\r\n    background: #4F008C;\r\n    color: #fff;\r\n}\r\n.country-picker img{\r\nwidth: 20px;\r\n\r\n}\r\n.region{\r\n    background: #f0f3f5;\r\n    color: #737e88;\r\n    width: 40px;\r\n    height: 30px;\r\n    text-align: center;\r\n    line-height: 30px;\r\n    cursor: pointer;\r\n    display: inline-block\r\n}\r\n.region li{\r\n\r\n    text-align: center;\r\n}\r\n.region ul li{\r\n    padding: 7px;\r\n    cursor: pointer;\r\n    border-bottom: 1px solid #777;\r\n    transition: 0.3s;\r\n    display: flex;\r\n}\r\n.regBu{\r\n    margin:auto;\r\n    display:block;\r\n   \r\n    display: flex;\r\n}\r\n.h2{\r\n    font-family: inherit;\r\n    font-weight: 600;\r\n    line-height: 1.5;\r\n    font-size: 1.25rem;\r\n}\r\n.background-pink{\r\n    background-color: #ff375d;\r\n}\r\n.color-white{\r\n    color: white;\r\n    background-color: #ff375d;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmV3LXBhcnRuZXIvbmV3LXBhcnRuZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLG1CQUFtQjtBQUN2QjtBQUNBO0lBQ0ksbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQixXQUFXO0lBQ1gsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixpQkFBaUI7SUFDakIsZUFBZTtJQUNmLHFCQUFxQjtBQUN6QjtBQUNBO0lBQ0kscUJBQXFCO0FBQ3pCO0FBQ0E7SUFDSSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixVQUFVO0lBQ1YsV0FBVztJQUNYLFNBQVM7SUFDVCxzQkFBc0I7SUFDdEIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixXQUFXO0FBQ2Y7QUFDQTtJQUNJLFlBQVk7SUFDWixlQUFlO0lBQ2YsNkJBQTZCO0lBQzdCLGdCQUFnQjtJQUNoQixnQkFBZ0I7O0FBRXBCO0FBQ0E7SUFDSSxtQkFBbUI7SUFDbkIsV0FBVztBQUNmO0FBQ0E7QUFDQSxXQUFXOztBQUVYO0FBQ0E7SUFDSSxtQkFBbUI7SUFDbkIsY0FBYztJQUNkLFdBQVc7SUFDWCxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2Y7QUFDSjtBQUNBOztJQUVJLGtCQUFrQjtBQUN0QjtBQUNBO0lBQ0ksWUFBWTtJQUNaLGVBQWU7SUFDZiw2QkFBNkI7SUFDN0IsZ0JBQWdCO0lBQ2hCLGFBQWE7QUFDakI7QUFFQTtJQUNJLFdBQVc7SUFDWCxhQUFhOztJQUViLGFBQWE7QUFDakI7QUFFQTtJQUNJLG9CQUFvQjtJQUNwQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtBQUN0QjtBQUVBO0lBQ0kseUJBQXlCO0FBQzdCO0FBRUE7SUFDSSxZQUFZO0lBQ1oseUJBQXlCO0FBQzdCIiwiZmlsZSI6InNyYy9hcHAvbmV3LXBhcnRuZXIvbmV3LXBhcnRuZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb3VudHJ5LXBpY2tlciB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZjBmM2Y1O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMThweDtcclxufVxyXG4ucGlja2VyLWJ1dHRvbiB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZjBmM2Y1O1xyXG4gICAgLyogY29sb3I6ICM3MzdlODg7ICovXHJcbiAgICB3aWR0aDogMjhweDtcclxuICAgIGhlaWdodDogMzFweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcbi5jdXJyZW50e1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcbi5jb3VudHJ5LXBpY2tlciB1bCB7XHJcbiAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIGhlaWdodDogY2FsYygzNXB4ICogNSk7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xyXG4gICAgei1pbmRleDogOTk7XHJcbn1cclxuLmNvdW50cnktcGlja2VyIHVsIGxpIHtcclxuICAgIHBhZGRpbmc6IDdweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjNzc3O1xyXG4gICAgdHJhbnNpdGlvbjogMC4zcztcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcblxyXG59XHJcbi5jb3VudHJ5LXBpY2tlciB1bCBsaTpob3ZlcntcclxuICAgIGJhY2tncm91bmQ6ICM0RjAwOEM7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxufVxyXG4uY291bnRyeS1waWNrZXIgaW1ne1xyXG53aWR0aDogMjBweDtcclxuXHJcbn1cclxuLnJlZ2lvbntcclxuICAgIGJhY2tncm91bmQ6ICNmMGYzZjU7XHJcbiAgICBjb2xvcjogIzczN2U4ODtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2tcclxufVxyXG4ucmVnaW9uIGxpe1xyXG5cclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4ucmVnaW9uIHVsIGxpe1xyXG4gICAgcGFkZGluZzogN3B4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICM3Nzc7XHJcbiAgICB0cmFuc2l0aW9uOiAwLjNzO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuLnJlZ0J1e1xyXG4gICAgbWFyZ2luOmF1dG87XHJcbiAgICBkaXNwbGF5OmJsb2NrO1xyXG4gICBcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbi5oMntcclxuICAgIGZvbnQtZmFtaWx5OiBpbmhlcml0O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgICBmb250LXNpemU6IDEuMjVyZW07XHJcbn1cclxuXHJcbi5iYWNrZ3JvdW5kLXBpbmt7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmYzNzVkO1xyXG59XHJcblxyXG4uY29sb3Itd2hpdGV7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmYzNzVkO1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/new-partner/new-partner.component.ts":
/*!******************************************************!*\
  !*** ./src/app/new-partner/new-partner.component.ts ***!
  \******************************************************/
/*! exports provided: NewPartnerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewPartnerComponent", function() { return NewPartnerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../shared/models/regionsEnum */ "./src/app/shared/models/regionsEnum.ts");
/* harmony import */ var _shared_services_shared_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../shared/services/shared.service */ "./src/app/shared/services/shared.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _language_language_helper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../language/language.helper */ "./src/app/language/language.helper.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");









var NewPartnerComponent = /** @class */ (function () {
    function NewPartnerComponent(shared, languageHelper, translateService, router) {
        this.shared = shared;
        this.languageHelper = languageHelper;
        this.translateService = translateService;
        this.router = router;
        this.isCountryOpen = false;
        this.isRegister = false;
        this.listCountries = [];
        this.selectedCountry = { name: "Select Country", flag: null };
        this.isRegionOpen = false;
        this.listRegion = [
            { display: "EU (European Union)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].EU },
            { display: "EFTA (European Free Trade Association)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].EFTA },
            { display: "CARICOM (Caribbean Community)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].CARICOM },
            { display: "USAN (Union of South American Nations)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].USAN },
            { display: "EEU (Eurasian Economic Union)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].EEU },
            { display: "AL (Arab League)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].AL },
            { display: "ASEAN (Association of Southeast Asian Nations)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].ASEAN },
            { display: "CAIS (Central American Integration System)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].CAIS },
            { display: "CEFTA (Central European Free Trade Agreement)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].CEFTA },
            { display: "NAFTA (North American Free Trade Agreement)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].NAFTA },
            { display: "SAARC (South Asian Association for Regional Cooperation)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].SAARC },
        ];
        this.getAllLangs();
        this.isRTL = this.languageHelper.currentLang().isRTL;
        this.partnerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('^[^\\s@]+@[^\\s@]+\\.[^\\s@]{2,}$'), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(25)])]),
            fName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(1), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(10)]),
            lName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(1), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(10)]),
            company: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(1), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(30)]),
            phone: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(1), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(15)]),
        });
    }
    NewPartnerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.shared.getAllCountries().subscribe(function (res) { return _this.listCountries = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(res); });
    };
    NewPartnerComponent.prototype.selectCountry = function (country) {
        console.log(country);
        this.isCountryOpen = false;
        this.selectedCountry = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"])({}, country);
        this.partnerForm.get("phone").setValue("+" + country.callingCodes[0]);
    };
    NewPartnerComponent.prototype.trueChart = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)
            && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
    };
    NewPartnerComponent.prototype.trueChartEmail = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57)
            && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
    };
    NewPartnerComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    NewPartnerComponent.prototype.selectRegion = function () {
        var _this = this;
        var region = this.partnerForm.get('region').value;
        console.log(_shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"][region]);
        this.shared.getCountriesWithRegion(_shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"][region]).subscribe(function (res) { return _this.listCountries = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(res); });
    };
    NewPartnerComponent.prototype.newPartner = function () {
        this.isRegister = true;
        if (this.partnerForm.valid) {
            this.router.navigate(['home']);
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'success',
                title: 'successfully registered'
            });
        }
        if (this.partnerForm.get('email').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Email Address is Worng'
            });
            // window.location.reload();
        }
        if (this.partnerForm.get('phone').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Phone entered is Worng'
            });
            // window.location.reload();
        }
        if (this.partnerForm.get('fName').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'First Name Entered is Wrong'
            });
        }
        if (this.partnerForm.get('lName').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Last Name Entered is Wrong'
            });
        }
        if (this.partnerForm.get('company').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Company Name Entered is Wrong'
            });
        }
    };
    NewPartnerComponent.prototype.changeLang = function (lang) {
        this.languageHelper.changeLang(lang);
        window.location.reload();
    };
    NewPartnerComponent.prototype.getAllLangs = function () {
        var arr = this.languageHelper.getAllLanguages();
        this.english = arr[0];
        this.arabic = arr[1];
        return this.languageHelper.getAllLanguages();
    };
    NewPartnerComponent.ctorParameters = function () { return [
        { type: _shared_services_shared_service__WEBPACK_IMPORTED_MODULE_2__["SharedService"] },
        { type: _language_language_helper__WEBPACK_IMPORTED_MODULE_6__["LanguageHelper"] },
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"] }
    ]; };
    NewPartnerComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
            selector: 'app-new-partner',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./new-partner.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/new-partner/new-partner.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./new-partner.component.css */ "./src/app/new-partner/new-partner.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_shared_services_shared_service__WEBPACK_IMPORTED_MODULE_2__["SharedService"], _language_language_helper__WEBPACK_IMPORTED_MODULE_6__["LanguageHelper"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"], _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"]])
    ], NewPartnerComponent);
    return NewPartnerComponent;
}());



/***/ }),

/***/ "./src/app/shared/models/regionsEnum.ts":
/*!**********************************************!*\
  !*** ./src/app/shared/models/regionsEnum.ts ***!
  \**********************************************/
/*! exports provided: RegionsEnum */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegionsEnum", function() { return RegionsEnum; });
var RegionsEnum;
(function (RegionsEnum) {
    RegionsEnum[RegionsEnum["EU"] = 0] = "EU";
    RegionsEnum[RegionsEnum["EFTA"] = 1] = "EFTA";
    RegionsEnum[RegionsEnum["CARICOM"] = 2] = "CARICOM";
    RegionsEnum[RegionsEnum["PA"] = 3] = "PA";
    RegionsEnum[RegionsEnum["AU"] = 4] = "AU";
    RegionsEnum[RegionsEnum["USAN"] = 5] = "USAN";
    RegionsEnum[RegionsEnum["EEU"] = 6] = "EEU";
    RegionsEnum[RegionsEnum["AL"] = 7] = "AL";
    RegionsEnum[RegionsEnum["ASEAN"] = 8] = "ASEAN";
    RegionsEnum[RegionsEnum["CAIS"] = 9] = "CAIS";
    RegionsEnum[RegionsEnum["CEFTA"] = 10] = "CEFTA";
    RegionsEnum[RegionsEnum["NAFTA"] = 11] = "NAFTA";
    RegionsEnum[RegionsEnum["SAARC"] = 12] = "SAARC";
})(RegionsEnum || (RegionsEnum = {}));


/***/ }),

/***/ "./src/app/shared/models/share.module.ts":
/*!***********************************************!*\
  !*** ./src/app/shared/models/share.module.ts ***!
  \***********************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _language_language_helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../language/language.helper */ "./src/app/language/language.helper.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var ngx_webstorage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-webstorage */ "./node_modules/ngx-webstorage/fesm5/ngx-webstorage.js");


// import { ServicesModule } from './services/services.module';





var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"], ngx_webstorage__WEBPACK_IMPORTED_MODULE_6__["NgxWebstorageModule"]],
            exports: [],
            declarations: [],
            providers: [_language_language_helper__WEBPACK_IMPORTED_MODULE_1__["LanguageHelper"]]
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./src/app/shared/services/shared.service.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/services/shared.service.ts ***!
  \***************************************************/
/*! exports provided: SharedService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedService", function() { return SharedService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var SharedService = /** @class */ (function () {
    function SharedService(http) {
        this.http = http;
    }
    SharedService.prototype.getAllCountries = function () {
        return this.http.get('https://restcountries.eu/rest/v2/all');
    };
    SharedService.prototype.getCountriesWithRegion = function (region) {
        return this.http.get('https://restcountries.eu/rest/v2/regionalbloc/' + region);
    };
    SharedService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    SharedService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], SharedService);
    return SharedService;
}());



/***/ }),

/***/ "./src/app/ttt/ttt.component.css":
/*!***************************************!*\
  !*** ./src/app/ttt/ttt.component.css ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3R0dC90dHQuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/ttt/ttt.component.ts":
/*!**************************************!*\
  !*** ./src/app/ttt/ttt.component.ts ***!
  \**************************************/
/*! exports provided: TttComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TttComponent", function() { return TttComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TttComponent = /** @class */ (function () {
    function TttComponent() {
    }
    TttComponent.prototype.ngOnInit = function () {
    };
    TttComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ttt',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./ttt.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/ttt/ttt.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./ttt.component.css */ "./src/app/ttt/ttt.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], TttComponent);
    return TttComponent;
}());



/***/ }),

/***/ "./src/app/upload/upload.component.css":
/*!*********************************************!*\
  !*** ./src/app/upload/upload.component.css ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("::ng-deep .nav-tabs .nav-link.active{\r\n    background :#ffff;\r\n}\r\n/* ::ng-deep .nav-tabs .nav-link.active{\r\nbackground: #4F008c !important;\r\n} */\r\n.form-group.hidden {\r\n  width: 0;\r\n  margin: 0;\r\n  border: none;\r\n  padding: 0;\r\n}\r\n.form-group.hidden .form-control{\r\n  display: none;\r\n}\r\n::ng-deep .custom-day {\r\n  text-align: center;\r\n  padding: 0.185rem 0.25rem;\r\n  display: inline-block;\r\n  height: 2rem;\r\n  width: 2rem;\r\n}\r\n.mb-0{\r\n  font-size: 1.0625rem;\r\n  font-weight: 600;\r\n}\r\n.header{\r\n  background: #f7fafc !important;\r\n}\r\n.form-control-label{\r\n  font-weight: 600;\r\n}\r\n.col-lg-4{\r\n  margin-left: 1rem;\r\n  margin-top: 1rem;\r\n}\r\n::ng-deep .custom-day.focused {\r\n  background-color: #e6e6e6;\r\n}\r\n::ng-deep .custom-day.range, ::ng-deep .custom-day:hover {\r\n  background-color: rgb(2, 117, 216);\r\n  color: white;\r\n}\r\n::ng-deep .custom-day.faded {\r\n  background-color: rgba(2, 117, 216, 0.5);\r\n}\r\n.btn-outline-secondary {\r\n  color: #23282c;\r\n  border: 1px solid #cad1d7;\r\n  background-color: #fff;\r\n}\r\n::ng-deep .tab-content .tab-pane {\r\n  padding: 0rem !important;\r\n}\r\n::ng-deep .justify-content-start {\r\npadding-left: 15px !important;\r\n}\r\n.submit{\r\n  color: #fff;\r\n  background-color: #FF375E;\r\n  border-color: #FF375E;\r\n  font-weight: 600;\r\n  text-align: center;\r\n  overflow: visible;\r\n    white-space: nowrap;\r\n    vertical-align: middle;\r\n    -webkit-user-select: none;\r\n    -moz-user-select: none;\r\n    -ms-user-select: none;\r\n    user-select: none;\r\n    border: 1px solid transparent;\r\n    padding: 0.625rem 1.25rem;\r\n    font-size: 1rem;\r\n    line-height: 1.5;\r\n    border-radius: 0.375rem;\r\n    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;\r\nbox-shadow: 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08);\r\n}\r\n::ng-deep .card-header {\r\n  padding: 1rem 1rem !important\r\n}\r\n.card{\r\n  background: #f7fafc;\r\n}\r\n.tag {\r\n  border-radius: 50%;\r\n  height: 15px;\r\n  width: 15px;\r\n  display: inline-block;\r\n}\r\n.tag-default {\r\n  background-color: gray;\r\n}\r\n.tag-success {\r\n  background-color: green;\r\n}\r\n.tag-warning {\r\n  background-color: orange;\r\n}\r\n.tag-danger {\r\n  background-color: red;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXBsb2FkL3VwbG9hZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0FBQ3JCO0FBQ0E7O0dBRUc7QUFDSDtFQUNFLFFBQVE7RUFDUixTQUFTO0VBQ1QsWUFBWTtFQUNaLFVBQVU7QUFDWjtBQUNBO0VBQ0UsYUFBYTtBQUNmO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIseUJBQXlCO0VBQ3pCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osV0FBVztBQUNiO0FBRUE7RUFDRSxvQkFBb0I7RUFDcEIsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSw4QkFBOEI7QUFDaEM7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UseUJBQXlCO0FBQzNCO0FBQ0E7RUFDRSxrQ0FBa0M7RUFDbEMsWUFBWTtBQUNkO0FBQ0E7RUFDRSx3Q0FBd0M7QUFDMUM7QUFDQTtFQUNFLGNBQWM7RUFDZCx5QkFBeUI7RUFDekIsc0JBQXNCO0FBQ3hCO0FBRUE7RUFDRSx3QkFBd0I7QUFDMUI7QUFDQTtBQUNBLDZCQUE2QjtBQUM3QjtBQUVBO0VBQ0UsV0FBVztFQUNYLHlCQUF5QjtFQUN6QixxQkFBcUI7RUFDckIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixpQkFBaUI7SUFDZixtQkFBbUI7SUFDbkIsc0JBQXNCO0lBQ3RCLHlCQUF5QjtJQUN6QixzQkFBc0I7SUFDdEIscUJBQXFCO0lBQ3JCLGlCQUFpQjtJQUNqQiw2QkFBNkI7SUFDN0IseUJBQXlCO0lBQ3pCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsdUJBQXVCO0lBQ3ZCLHFJQUFxSTtBQUN6SSwyRUFBMkU7QUFDM0U7QUFDQTtFQUNFO0FBQ0Y7QUFDQTtFQUNFLG1CQUFtQjtBQUNyQjtBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixXQUFXO0VBQ1gscUJBQXFCO0FBQ3ZCO0FBRUE7RUFDRSxzQkFBc0I7QUFDeEI7QUFFQTtFQUNFLHVCQUF1QjtBQUN6QjtBQUVBO0VBQ0Usd0JBQXdCO0FBQzFCO0FBRUE7RUFDRSxxQkFBcUI7QUFDdkIiLCJmaWxlIjoic3JjL2FwcC91cGxvYWQvdXBsb2FkLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6Om5nLWRlZXAgLm5hdi10YWJzIC5uYXYtbGluay5hY3RpdmV7XHJcbiAgICBiYWNrZ3JvdW5kIDojZmZmZjtcclxufVxyXG4vKiA6Om5nLWRlZXAgLm5hdi10YWJzIC5uYXYtbGluay5hY3RpdmV7XHJcbmJhY2tncm91bmQ6ICM0RjAwOGMgIWltcG9ydGFudDtcclxufSAqL1xyXG4uZm9ybS1ncm91cC5oaWRkZW4ge1xyXG4gIHdpZHRoOiAwO1xyXG4gIG1hcmdpbjogMDtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgcGFkZGluZzogMDtcclxufVxyXG4uZm9ybS1ncm91cC5oaWRkZW4gLmZvcm0tY29udHJvbHtcclxuICBkaXNwbGF5OiBub25lO1xyXG59XHJcbjo6bmctZGVlcCAuY3VzdG9tLWRheSB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHBhZGRpbmc6IDAuMTg1cmVtIDAuMjVyZW07XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIGhlaWdodDogMnJlbTtcclxuICB3aWR0aDogMnJlbTtcclxufVxyXG5cclxuLm1iLTB7XHJcbiAgZm9udC1zaXplOiAxLjA2MjVyZW07XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxufVxyXG4uaGVhZGVye1xyXG4gIGJhY2tncm91bmQ6ICNmN2ZhZmMgIWltcG9ydGFudDtcclxufVxyXG4uZm9ybS1jb250cm9sLWxhYmVse1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbn1cclxuXHJcbi5jb2wtbGctNHtcclxuICBtYXJnaW4tbGVmdDogMXJlbTtcclxuICBtYXJnaW4tdG9wOiAxcmVtO1xyXG59XHJcbjo6bmctZGVlcCAuY3VzdG9tLWRheS5mb2N1c2VkIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTZlNmU2O1xyXG59XHJcbjo6bmctZGVlcCAuY3VzdG9tLWRheS5yYW5nZSwgOjpuZy1kZWVwIC5jdXN0b20tZGF5OmhvdmVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMiwgMTE3LCAyMTYpO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG46Om5nLWRlZXAgLmN1c3RvbS1kYXkuZmFkZWQge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMiwgMTE3LCAyMTYsIDAuNSk7XHJcbn1cclxuLmJ0bi1vdXRsaW5lLXNlY29uZGFyeSB7XHJcbiAgY29sb3I6ICMyMzI4MmM7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2NhZDFkNztcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG59XHJcblxyXG46Om5nLWRlZXAgLnRhYi1jb250ZW50IC50YWItcGFuZSB7XHJcbiAgcGFkZGluZzogMHJlbSAhaW1wb3J0YW50O1xyXG59XHJcbjo6bmctZGVlcCAuanVzdGlmeS1jb250ZW50LXN0YXJ0IHtcclxucGFkZGluZy1sZWZ0OiAxNXB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5zdWJtaXR7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI0ZGMzc1RTtcclxuICBib3JkZXItY29sb3I6ICNGRjM3NUU7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgb3ZlcmZsb3c6IHZpc2libGU7XHJcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICAtbW96LXVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICAgIHBhZGRpbmc6IDAuNjI1cmVtIDEuMjVyZW07XHJcbiAgICBmb250LXNpemU6IDFyZW07XHJcbiAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMC4zNzVyZW07XHJcbiAgICB0cmFuc2l0aW9uOiBjb2xvciAwLjE1cyBlYXNlLWluLW91dCwgYmFja2dyb3VuZC1jb2xvciAwLjE1cyBlYXNlLWluLW91dCwgYm9yZGVyLWNvbG9yIDAuMTVzIGVhc2UtaW4tb3V0LCBib3gtc2hhZG93IDAuMTVzIGVhc2UtaW4tb3V0O1xyXG5ib3gtc2hhZG93OiAwIDRweCA2cHggcmdiYSg1MCwgNTAsIDkzLCAwLjExKSwgMCAxcHggM3B4IHJnYmEoMCwgMCwgMCwgMC4wOCk7XHJcbn1cclxuOjpuZy1kZWVwIC5jYXJkLWhlYWRlciB7XHJcbiAgcGFkZGluZzogMXJlbSAxcmVtICFpbXBvcnRhbnRcclxufVxyXG4uY2FyZHtcclxuICBiYWNrZ3JvdW5kOiAjZjdmYWZjO1xyXG59XHJcblxyXG4udGFnIHtcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgaGVpZ2h0OiAxNXB4O1xyXG4gIHdpZHRoOiAxNXB4O1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG5cclxuLnRhZy1kZWZhdWx0IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmF5O1xyXG59XHJcblxyXG4udGFnLXN1Y2Nlc3Mge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGdyZWVuO1xyXG59XHJcblxyXG4udGFnLXdhcm5pbmcge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IG9yYW5nZTtcclxufVxyXG5cclxuLnRhZy1kYW5nZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJlZDtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/upload/upload.component.ts":
/*!********************************************!*\
  !*** ./src/app/upload/upload.component.ts ***!
  \********************************************/
/*! exports provided: UploadComponent, TargerUserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadComponent", function() { return UploadComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargerUserComponent", function() { return TargerUserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/fesm5/ng2-file-upload.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/fesm5/dialog.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







// const URL = '/api/';
var URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
var UploadComponent = /** @class */ (function () {
    function UploadComponent(calendar, formatter, dialog, router) {
        var _this = this;
        this.calendar = calendar;
        this.formatter = formatter;
        this.dialog = dialog;
        this.router = router;
        this.options = [];
        this.size1 = "";
        this.size2 = "";
        this.size3 = "";
        this.hoveredDate = null;
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__["FileUploader"]({
            url: URL,
            disableMultipart: true,
            formatDataFunctionIsAsync: true,
            formatDataFunction: function (item) { return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, function () {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"])(this, function (_a) {
                    return [2 /*return*/, new Promise(function (resolve, reject) {
                            resolve({
                                name: item._file.name,
                                length: item._file.size,
                                contentType: item._file.type,
                                date: new Date()
                            });
                        })];
                });
            }); }
        });
        this.hasBaseDropZoneOver = false;
        this.hasAnotherDropZoneOver = false;
        this.response = '';
        this.uploader.response.subscribe(function (res) { return _this.response = res; });
        this.fromDate = calendar.getToday();
        this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
    }
    UploadComponent.prototype.openDialog = function () {
        this.dialog.open(TargerUserComponent, {
            width: "300px",
            data: { options: this.options }
        });
    };
    UploadComponent.prototype.onDateSelection = function (date) {
        if (!this.fromDate && !this.toDate) {
            this.fromDate = date;
        }
        else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
            this.toDate = date;
        }
        else {
            this.toDate = null;
            this.fromDate = date;
        }
    };
    UploadComponent.prototype.isHovered = function (date) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) &&
            date.before(this.hoveredDate);
    };
    UploadComponent.prototype.isInside = function (date) {
        return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
    };
    UploadComponent.prototype.isRange = function (date) {
        return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
    };
    UploadComponent.prototype.validateInput = function (currentValue, input) {
        var parsed = this.formatter.parse(input);
        return parsed && this.calendar.isValid(_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbDate"].from(parsed)) ? _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbDate"].from(parsed) : currentValue;
    };
    UploadComponent.prototype.getFileDetailF = function (event) {
        var files = event.target.files;
        if (this.validateFile(files[0].name)) {
            for (var i = 0; i < event.target.files.length; i++) {
                var size = event.target.files[i].size;
                var modifiedDate = event.target.files[i].lastModifiedDate;
                this.size1 = (Math.round(size / 1024) + " KB");
            }
            if (!this.validateFile(files[0].name)) {
                this.file1.nativeElement.value = null;
                var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.mixin({
                    toast: true,
                    position: 'bottom-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: function (toast) {
                        toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.stopTimer);
                        toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.resumeTimer);
                    }
                });
                Toast.fire({
                    icon: 'error',
                    title: 'Invalid File Format'
                });
                return false;
            }
        }
    };
    UploadComponent.prototype.validateFile = function (name) {
        var ext = name.substring(name.lastIndexOf('.') + 1);
        if (ext.toLowerCase() == 'zip' || (ext.toLowerCase() == 'xlsx') || (ext.toLowerCase() == 'txt')) {
            return true;
        }
        else {
            return false;
        }
    };
    UploadComponent.prototype.getFileDetailS = function (event) {
        var files = event.target.files;
        if (this.validateFile(files[0].name)) {
            for (var i = 0; i < event.target.files.length; i++) {
                var size = event.target.files[i].size;
                var modifiedDate = event.target.files[i].lastModifiedDate;
                this.size2 = (Math.round(size / 1024) + " KB");
                // this.modifiedDate1 = modifiedDate;
            }
            if (!this.validateFile(files[0].name)) {
                this.file2.nativeElement.value = null;
                var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.mixin({
                    toast: true,
                    position: 'bottom-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: function (toast) {
                        toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.stopTimer);
                        toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.resumeTimer);
                    }
                });
                Toast.fire({
                    icon: 'error',
                    title: 'Invalid File Format'
                });
                return false;
            }
        }
    };
    UploadComponent.prototype.getFileDetailTH = function (event) {
        var files = event.target.files;
        if (this.validateFile(files[0].name)) {
            for (var i = 0; i < event.target.files.length; i++) {
                var size = event.target.files[i].size;
                var modifiedDate = event.target.files[i].lastModifiedDate;
                this.size3 = (Math.round(size / 1024) + " KB");
            }
            if (!this.validateFile(files[0].name)) {
                this.file3.nativeElement.value = null;
                var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.mixin({
                    toast: true,
                    position: 'bottom-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: function (toast) {
                        toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.stopTimer);
                        toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.resumeTimer);
                    }
                });
                Toast.fire({
                    icon: 'error',
                    title: 'Invalid File Format'
                });
                return false;
            }
        }
    };
    UploadComponent.prototype.fileOverBase = function (e) {
        this.hasBaseDropZoneOver = e;
    };
    UploadComponent.prototype.fileOverAnother = function (e) {
        this.hasAnotherDropZoneOver = e;
    };
    UploadComponent.prototype.ngOnInit = function () {
    };
    UploadComponent.prototype.save = function () {
        var _this = this;
        var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            allowOutsideClick: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: function (toast) {
                toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.stopTimer);
                toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.resumeTimer);
            }
        });
        sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Save'
        }).then(function (result) {
            if (result.value) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('Saved!', 'Your Files has been uploaded.', 'success');
                _this.router.navigate(['home']);
            }
        });
    };
    UploadComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbCalendar"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbDateParserFormatter"] },
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('file1'),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], UploadComponent.prototype, "file1", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('file2'),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], UploadComponent.prototype, "file2", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('file3'),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], UploadComponent.prototype, "file3", void 0);
    UploadComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-upload',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./upload.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/upload/upload.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./upload.component.css */ "./src/app/upload/upload.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbCalendar"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbDateParserFormatter"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], UploadComponent);
    return UploadComponent;
}());

var TargerUserComponent = /** @class */ (function () {
    function TargerUserComponent(data) {
        this.data = data;
    }
    TargerUserComponent.prototype.ngOnInit = function () { };
    TargerUserComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MAT_DIALOG_DATA"],] }] }
    ]; };
    TargerUserComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-upload',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./targer-user.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/upload/targer-user.component.html")).default,
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__param"])(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MAT_DIALOG_DATA"])),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [Object])
    ], TargerUserComponent);
    return TargerUserComponent;
}());



/***/ }),

/***/ "./src/app/views/error/404.component.ts":
/*!**********************************************!*\
  !*** ./src/app/views/error/404.component.ts ***!
  \**********************************************/
/*! exports provided: P404Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "P404Component", function() { return P404Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var P404Component = /** @class */ (function () {
    function P404Component() {
    }
    P404Component = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./404.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/error/404.component.html")).default
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], P404Component);
    return P404Component;
}());



/***/ }),

/***/ "./src/app/views/error/500.component.ts":
/*!**********************************************!*\
  !*** ./src/app/views/error/500.component.ts ***!
  \**********************************************/
/*! exports provided: P500Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "P500Component", function() { return P500Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var P500Component = /** @class */ (function () {
    function P500Component() {
    }
    P500Component = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./500.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/error/500.component.html")).default
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], P500Component);
    return P500Component;
}());



/***/ }),

/***/ "./src/app/views/forgot-password/forgot-password.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/views/forgot-password/forgot-password.component.css ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".costom-btn {\r\n    margin: auto;\r\n    margin: auto;\r\n    line-height: initial;\r\n    background-color: blueviolet;\r\n    margin-left: 22vh;\r\n    margin-top: 22vh;\r\n}\r\n\r\n     li {\r\n      margin: 20 20px;\r\n    }\r\n\r\n     .btn-c\r\n{\r\n    margin-top: 10vh;\r\n}\r\n\r\n     .h2{\r\n    font-family: inherit;\r\n    font-weight: 600;\r\n    line-height: 1.5;\r\n    font-size: 1.25rem;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvZm9yZ290LXBhc3N3b3JkL2ZvcmdvdC1wYXNzd29yZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksWUFBWTtJQUNaLFlBQVk7SUFDWixvQkFBb0I7SUFDcEIsNEJBQTRCO0lBQzVCLGlCQUFpQjtJQUNqQixnQkFBZ0I7QUFDcEI7O0tBRUs7TUFDQyxlQUFlO0lBQ2pCOztLQUVKOztJQUVJLGdCQUFnQjtBQUNwQjs7S0FFQTtJQUNJLG9CQUFvQjtJQUNwQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtBQUN0QiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL2ZvcmdvdC1wYXNzd29yZC9mb3Jnb3QtcGFzc3dvcmQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb3N0b20tYnRuIHtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIGxpbmUtaGVpZ2h0OiBpbml0aWFsO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZXZpb2xldDtcclxuICAgIG1hcmdpbi1sZWZ0OiAyMnZoO1xyXG4gICAgbWFyZ2luLXRvcDogMjJ2aDtcclxufVxyXG5cclxuICAgICBsaSB7XHJcbiAgICAgIG1hcmdpbjogMjAgMjBweDtcclxuICAgIH1cclxuXHJcbi5idG4tY1xyXG57XHJcbiAgICBtYXJnaW4tdG9wOiAxMHZoO1xyXG59XHJcblxyXG4uaDJ7XHJcbiAgICBmb250LWZhbWlseTogaW5oZXJpdDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gICAgZm9udC1zaXplOiAxLjI1cmVtO1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/views/forgot-password/forgot-password.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/views/forgot-password/forgot-password.component.ts ***!
  \********************************************************************/
/*! exports provided: ForgotPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordComponent", function() { return ForgotPasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);






var ForgotPasswordComponent = /** @class */ (function () {
    function ForgotPasswordComponent(router, toast) {
        this.router = router;
        this.toast = toast;
        this.isRegister = false;
        this.forgetForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[^\\s@]+@[^\\s@]+\\.[^\\s@]{2,}$'), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(25)])]),
        });
    }
    ForgotPasswordComponent.prototype.ngOnInit = function () {
    };
    ForgotPasswordComponent.prototype.trueChartEmail = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57)
            && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
    };
    ForgotPasswordComponent.prototype.forgot = function () {
        this.isRegister = true;
        if (this.forgetForm.valid) {
            this.router.navigate(['home']);
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'success',
                title: 'Password has been Resetted Successfully'
            });
        }
        if (this.forgetForm.get('email').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Email Address Entered is Worng'
            });
        }
    };
    ForgotPasswordComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] }
    ]; };
    ForgotPasswordComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-forgot-password',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./forgot-password.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/forgot-password/forgot-password.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./forgot-password.component.css */ "./src/app/views/forgot-password/forgot-password.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]])
    ], ForgotPasswordComponent);
    return ForgotPasswordComponent;
}());



/***/ }),

/***/ "./src/app/views/login/login.component.ts":
/*!************************************************!*\
  !*** ./src/app/views/login/login.component.ts ***!
  \************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _language_language_helper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../language/language.helper */ "./src/app/language/language.helper.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_7__);








var LoginComponent = /** @class */ (function () {
    function LoginComponent(router, languageHelper, toast, translateService) {
        this.router = router;
        this.languageHelper = languageHelper;
        this.toast = toast;
        this.translateService = translateService;
        this.isSubmited = false;
        this.today = new Date();
        this.lastLogin = '';
        this.getAllLangs();
        this.isRTL = this.languageHelper.currentLang().isRTL;
        this.login = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(25)])
        });
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.translateService.setDefaultLang('en');
        this.translateService.use(this.languageHelper.currentLang().name);
        this.isRTL = this.languageHelper.currentLang().isRTL;
    };
    LoginComponent.prototype.changeLang = function (lang) {
        this.languageHelper.changeLang(lang);
        window.location.reload();
    };
    LoginComponent.prototype.trueChartEmail = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57)
            && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
    };
    LoginComponent.prototype.getAllLangs = function () {
        var arr = this.languageHelper.getAllLanguages();
        this.english = arr[0];
        this.arabic = arr[1];
        return this.languageHelper.getAllLanguages();
    };
    LoginComponent.prototype.onSubmit = function () {
        this.isSubmited = true;
        if (this.login.valid) {
            // this.lastLogin = formatDate(this.today, 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0300');
            this.router.navigate(['home']);
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'success',
                title: 'Signed in successfully'
            });
        }
        else {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Email Address/Password are Worng'
            });
            // window.location.reload();
        }
    };
    LoginComponent.prototype.register = function () {
        this.router.navigate(['register']);
    };
    LoginComponent.prototype.forget = function () {
        this.router.navigate(['forgot-password']);
    };
    LoginComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _language_language_helper__WEBPACK_IMPORTED_MODULE_4__["LanguageHelper"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"] },
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"] }
    ]; };
    LoginComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-login',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/login/login.component.html")).default
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _language_language_helper__WEBPACK_IMPORTED_MODULE_4__["LanguageHelper"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/views/register/register.component.ts":
/*!******************************************************!*\
  !*** ./src/app/views/register/register.component.ts ***!
  \******************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);






var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(router, toast) {
        this.router = router;
        this.toast = toast;
        this.isRegister = false;
        this.registerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            Email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[^\\s@]+@[^\\s@]+\\.[^\\s@]{2,}$'), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(25)])]),
            phone: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(7), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(15)]),
            code: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(3)]),
        });
    }
    RegisterComponent.prototype.ngOnInit = function () {
    };
    RegisterComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    RegisterComponent.prototype.trueChart = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)
            && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
    };
    RegisterComponent.prototype.trueChartEmail = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57)
            && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
    };
    RegisterComponent.prototype.onRegister = function () {
        this.isRegister = true;
        if (this.registerForm.valid) {
            this.router.navigate(['home']);
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'success',
                title: 'successfully registered'
            });
        }
        if (this.registerForm.get('Email').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Email Address/Password are Worng'
            });
            // window.location.reload();
        }
        if (this.registerForm.get('phone').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Phone entered is Worng'
            });
            // window.location.reload();
        }
        if (this.registerForm.get('code').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Code Entered is Wrong'
            });
            // window.location.reload();
        }
    };
    RegisterComponent.prototype.registerNav = function () {
        // this.route.navigate([])
    };
    RegisterComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] }
    ]; };
    RegisterComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./register.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/register/register.component.html")).default
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"], {
    useJit: true,
    preserveWhitespaces: true
})
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Thinkpad\Documents\wbu-portal\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map