(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tk": "./node_modules/moment/locale/tk.js",
	"./tk.js": "./node_modules/moment/locale/tk.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/approve-registration/approve-registration.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/Admin-Portal/approve-registration/approve-registration.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-8 pt-5 \">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"header-body\">\r\n            <!-- Table -->\r\n            <div class=\"card shadow\">\r\n                <div class=\"card-header border-0\">\r\n                    <div class=\"row align-items-center\">\r\n                        <div class=\"col\">\r\n                            <h3 class=\"mb-0\">Approve Registration</h3>\r\n                        </div>\r\n                      \r\n                    </div>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <!-- Projects table -->\r\n                    <table class=\"table align-items-center table-flush\">\r\n                        <thead class=\"thead-light\">\r\n                            <tr>\r\n                                <th scope=\"col\">Email</th>\r\n                                <th scope=\"col\">Company</th>\r\n                                <th scope=\"col\">Date </th>\r\n                                <th scope=\"col\"> </th>\r\n\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody style=\"font-weight: 600; color: #525f7f;\" *ngFor=\"let item of rows\" >\r\n                            <td >{{item.email}}</td>\r\n                            <td>{{item.company}}</td>\r\n                            <td>{{item.date | date:'dd/MM/yyyy'}}</td>\r\n\r\n                            \r\n                            <td> <button type=\"button\"  class=\"btn btn-danger btn-danger\"\r\n                                (click)=\"detail(item.id)\">Details</button> </td>\r\n                        </tbody>\r\n                       \r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/assets/assets.component.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/Admin-Portal/assets/assets.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-8 pt-5\">\r\n  <div class=\"container-fluid\">\r\n    <div class=\"header-body\">\r\n      <!-- Table -->\r\n      <div class=\"card shadow\">\r\n        <div class=\"border-0\">\r\n          <div class=\"row align-items-center\">\r\n            <div class=\"col\">\r\n              <h2 class=\"mb-0 mb-01\">Active Installed Services</h2>\r\n            </div>\r\n            <div>\r\n              <div class=\"search-hero\">\r\n                <input class=\"form-control\" type=\"text\" name=\"search\" [(ngModel)]=\"searchText\" autocomplete=\"off\"\r\n                  placeholder=\"&#61442;  Search...\">\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"table-responsive table-res\">\r\n            <!-- Projects table -->\r\n            <table class=\"table align-items-center table-flush\">\r\n              <thead class=\"thead-light\">\r\n                <tr>\r\n                  <th scope=\"col\">Service</th>\r\n                  <th scope=\"col\">Order Number</th>\r\n                  <th scope=\"col\">Service ID</th>\r\n                  <th scope=\"col\">Status</th>\r\n                  <th scope=\"col\">Parameter</th>\r\n                  <th scope=\"col\">Value</th>\r\n                  <th scope=\"col\">System Designation</th>\r\n                  <th scope=\"col\">NI Number</th>\r\n                  <th scope=\"col\">MRC</th>\r\n                  <th scope=\"col\">NRC</th>\r\n                  <th scope=\"col\">Added Value Charge</th>\r\n                  <th scope=\"col\">CIVIL WORK CHARGE</th>\r\n                  <th scope=\"col\">PORT PROVIDER CHARGE </th>\r\n                  <th scope=\"col\">OPERATION MAINTAINENCE CHARGE</th>\r\n                  <th scope=\"col\">RESTORATION CHARGE</th>\r\n                  <th scope=\"col\">INTERCONNECT CHARGE</th>\r\n                  <th scope=\"col\">Operator Code </th>\r\n                  <th scope=\"col\">GOVT FEE</th>\r\n                  <th scope=\"col\">SEGMENT S CHARGE</th>\r\n                  <th scope=\"col\">SEGMENT T CHARGE</th>\r\n                  <th scope=\"col\">SEGMENT ST CHARGE</th>\r\n                  <th scope=\"col\">FLAT CHARGE</th>\r\n                  <th scope=\"col\">TV CHARGE</th>\r\n                </tr>\r\n              </thead>\r\n              <tbody style=\"font-weight: 600; color: #525f7f;\">\r\n                <tr *ngFor=\"let item of asset | filter:searchText\">\r\n                  <td>{{item.service}}</td>\r\n                  <td>{{item.orderNum}}</td>\r\n                  <td>{{item.service_id}}</td>\r\n                  <td>{{item.status}}</td>\r\n                  <td>{{item.parameter}}</td>\r\n                  <td>{{item.value}}</td>\r\n                  <td>{{item.system_designation}}</td>\r\n                  <td>{{item.ni_number}}</td>\r\n                  <td>{{item.mrc}}</td>\r\n                  <td>{{item.nrc}}</td>\r\n                  <td>{{item.added_value_charge}}</td>\r\n                  <td>{{item.civil_work_charge}}</td>\r\n                  <td>{{item.port_provider_charge}}</td>\r\n                  <td>{{item.operation_maintainence_charge}}</td>\r\n                  <td>{{item.restoration_charge}}</td>\r\n                  <td>{{item.interconnect_charge}}</td>\r\n                  <td>{{item.operatorCode}}</td>\r\n                  <td>{{item.govt_fee}}</td>\r\n                  <td>{{item.segmentSCharge}}</td>\r\n                  <td>{{item.segmentTCcharge}}</td>\r\n                  <td>{{item.segmentStCharge}}</td>\r\n                  <td>{{item.flat_charge}}</td>\r\n                  <td>{{item.tv_charge}}</td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n          <mat-paginator [pageSizeOptions]=\"[5, 10, 25, 100]\"></mat-paginator>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/contact-admin/contact-admin.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/Admin-Portal/contact-admin/contact-admin.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div class=\"header bg-gradient-danger pb-8 pt-5 \">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"header-body\">\r\n            <div class=\"card shadow\">\r\n                <div class=\"card-header border-0\">\r\n                    <div class=\"row \">\r\n                        <div class=\"col\">\r\n                            <h2 class=\"mb-0\">Contact Admin </h2>\r\n                            <div id=\"outer\">\r\n                                <div class=\"inner\">\r\n                                    <div class=\"input-group mb-4\">\r\n                                        <div class=\"input-group-prepend\">\r\n                                            <span class=\"input-group-text\"><i class=\"fa fa-search\"\r\n                                                    aria-hidden=\"true\"></i></span>\r\n                                        </div>\r\n                                        <input type=\"text\" maxlength=\"50\" style=\"width: 19rem;\" autocomplete=\"off\"\r\n                                            class=\"form-control\" placeholder=\"Search by Email\" required>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"inner\">\r\n                                    <div class=\"input-group mb-4\">\r\n                                        <div class=\"input-group-prepend\">\r\n                                            <span class=\"input-group-text\"><i class=\"fa fa-search\"\r\n                                                    aria-hidden=\"true\"></i></span>\r\n                                        </div>\r\n                                        <input type=\"text\" style=\"width: 9rem;\" maxlength=\"50\" autocomplete=\"off\"\r\n                                            class=\"form-control\" placeholder=\"Search by Company\" required>\r\n                                    </div>\r\n                                </div>\r\n                                <h4 class=\"mb-0 label-text-center\">Area To Reflect Contact Attributes From NAWAQL</h4>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"table-responsive\">\r\n                        <table class=\"table align-items-center table-flush\">\r\n                            <thead class=\"thead-light\">\r\n                                <tr>\r\n                                    <th scope=\"col\">Email</th>\r\n                                    <th scope=\"col\">Company</th>\r\n                                    <th scope=\"col\">Portal Flag </th>\r\n                                    <th scope=\"col\"> Portal Role</th>\r\n                                </tr>\r\n                            </thead>\r\n                            <tbody style=\"font-weight: 600; color: #525f7f;\">\r\n                                <td>ABD@company.com</td>\r\n                                <td>Company A</td>\r\n                                <td>Active</td>\r\n                                <td>\r\n                                    <select class=\"form-control\">\r\n                                        <option disabled selected value></option>\r\n                                        <option>General Billing</option>\r\n                                        <option>General Commercial</option>\r\n                                        <option>Technical</option>\r\n                                        <option>Other</option>\r\n                                    </select></td>\r\n                            </tbody>\r\n                            <tbody style=\"font-weight: 600; color: #525f7f;\">\r\n                                <td>ABD@company.com</td>\r\n                                <td>Company A</td>\r\n                                <td>Inactive</td>\r\n                                <td>\r\n                                    <select class=\"form-control\">\r\n                                        <option disabled selected value></option>\r\n                                        <option>General Billing</option>\r\n                                        <option>General Commercial</option>\r\n                                        <option>Technical</option>\r\n                                        <option>Other</option>\r\n                                    </select></td>\r\n                            </tbody>\r\n                        </table>\r\n                        <div class=\"row\">\r\n                            <div class=\"col\">\r\n                                <div class=\"text-center\">\r\n                                    <button class=\"btn submit my-4\" (click)=\"disply()\" type=\"button\">Update Portal\r\n                                        Role</button>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div> -->\r\n\r\n    <img class=\"center\" src=\"assets/Under-development.png\" width=\"700\" height=\"500\">"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/history/history.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/Admin-Portal/history/history.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pt-5 \">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"header-body\">\r\n            <!-- Table -->\r\n            <div class=\"card shadow\">\r\n                <div class=\"card-header border-0\">\r\n                    <div class=\"row align-items-center\">\r\n                        <div class=\"col\">\r\n                            <h3 class=\"mb-0\">Archive </h3>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"table-responsive table-res\">\r\n                    <!-- Projects table -->\r\n                    <table class=\"table align-items-center table-flush\">\r\n                        <tr>\r\n                            <th scope=\"col\">User Email</th>\r\n                            <th scope=\"col\">Action</th>\r\n                            <th scope=\"col\">Date </th>\r\n                        </tr>\r\n                        <tbody>\r\n                            <tr *ngFor=\"let item of archive | paginate: { itemsPerPage: 7, currentPage: p }\">\r\n                                <td>{{item.userEmail}}</td>\r\n                                <td>{{item.action}}</td>\r\n                                <td>{{item.date | date:'dd/MM/yyyy'}}</td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n                <pagination-controls (pageChange)=\"p = $event\"></pagination-controls>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/new-role/new-role.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/Admin-Portal/new-role/new-role.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"bg-gradient-danger header pb-5 \">\r\n    <div class=\"container-fluid\">\r\n    </div>\r\n</div>\r\n<div class=\"bg-gradient-danger header pt-5 pt-md-8\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\">\r\n    <div class=\"container mt--8 pb-5\">\r\n        <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-5 col-md-7 thumbnail\">\r\n                <div class=\"card bg-secondary shadow border-0\">\r\n                    <img src=\"assets/stcs.png\" height=\"300\">\r\n                    <div class=\"caption\">\r\n                    </div>\r\n                    <div class=\"card-body px-lg-5\">\r\n                        <form [formGroup]=\"newRole\" (ngSubmit)=\"save()\" autocomplete=\"off\">\r\n                            <div class=\"form-group mb-3\">\r\n                                <div class=\"input-group input-group-alternative\">\r\n                                    <mat-form-field appearance=\"legacy\">\r\n                                        <input matInput placeholder=\"New Role Title\" formControlName=\"title\" (keypress)=\"trueChart($event)\">\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"form-group mb-3\">\r\n                                <div class=\"input-group input-group-alternative\">\r\n                                    <mat-form-field class=\"example-chip-list\">\r\n                                        <mat-chip-list #chipList aria-label=\"Role selection\">\r\n                                            <mat-chip *ngFor=\"let role of roles\" [selectable]=\"selectable\"\r\n                                                [removable]=\"removable\" (removed)=\"remove(role)\">\r\n                                                {{role}}\r\n                                                <mat-icon matChipRemove *ngIf=\"removable\">cancel</mat-icon>\r\n                                            </mat-chip>\r\n                                            <input placeholder=\"New Privilege\" #roleInput \r\n                                                [matAutocomplete]=\"auto\" [formControl]=\"roleForm\" [matChipInputFor]=\"chipList\"\r\n                                                [matChipInputSeparatorKeyCodes]=\"separatorKeysCodes\"\r\n                                                (matChipInputTokenEnd)=\"add($event)\" >\r\n                                        </mat-chip-list>\r\n                                        <mat-autocomplete #auto=\"matAutocomplete\"\r\n                                            (optionSelected)=\"selected($event)\">\r\n                                            <mat-option *ngFor=\"let role of filteredRoles | async\"\r\n                                                [value]=\"role\">\r\n                                                {{role}}\r\n                                            </mat-option>\r\n                                        </mat-autocomplete>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"text-center\">\r\n                                <button type=\"submit\" id=\"myBtn\" class=\"btn btn-primary\"\r\n                                    style=\"background-color:#ff375d;\">Save</button>\r\n\r\n                                <button type=\"button\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"><a\r\n                                        style=\"color: white;\" [routerLink]=\"['/role-access']\">Cancel</a></button>\r\n                            </div>\r\n                        </form>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/orders/orders.component.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/Admin-Portal/orders/orders.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-8 pt-5\">\r\n  <div class=\"container-fluid\">\r\n    <div class=\"header-body\">\r\n      <!-- Table -->\r\n      <div class=\"card shadow\">\r\n        <div class=\"border-0\">\r\n          <div class=\"row align-items-center\">\r\n            <div class=\"col\">\r\n              <h2 class=\"mb-0 mb-01\">In Progress Orders</h2>\r\n            </div>\r\n            <div>\r\n              <div class=\"search-hero\">\r\n                <input class=\"form-control\" type=\"text\" name=\"search\" [(ngModel)]=\"searchText\" autocomplete=\"off\"\r\n                  placeholder=\"&#61442;  Search...\">\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"table-responsive table-res\">\r\n            <!-- Projects table -->\r\n            <table class=\"table align-items-center table-flush\">\r\n              <thead class=\"thead-light\">\r\n                <tr>\r\n                  <th scope=\"col\">Service</th>\r\n                  <th scope=\"col\">Order Number</th>\r\n                  <th scope=\"col\">Service ID</th>\r\n                  <th scope=\"col\">Status</th>\r\n                  <th scope=\"col\">Parameter</th>\r\n                  <th scope=\"col\">Value</th>\r\n                  <th scope=\"col\">System Designation</th>\r\n                  <th scope=\"col\">NI Number</th>\r\n                  <th scope=\"col\">Billing Date</th>\r\n                  <th scope=\"col\">MRC</th>\r\n                  <th scope=\"col\">NRC</th>\r\n                  <th scope=\"col\">Added Value Charge</th>\r\n                  <th scope=\"col\">CIVIL WORK CHARGE</th>\r\n                  <th scope=\"col\">OPERATION MAINTAINENCE CHARGE</th>\r\n                  <th scope=\"col\">RESTORATION CHARGE</th>\r\n                  <th scope=\"col\">INTERCONNECT CHARGE</th>\r\n                  <th scope=\"col\">PORT PROVIDER CHARGE </th>\r\n                  <th scope=\"col\">GOVT FEE</th>\r\n                  <th scope=\"col\">SEGMENT S CHARGE</th>\r\n                  <th scope=\"col\">SEGMENT T CHARGE</th>\r\n                  <th scope=\"col\">SEGMENT ST CHARGE</th>\r\n                  <th scope=\"col\">FLAT CHARGE</th>\r\n                  <th scope=\"col\">TV CHARGE</th>\r\n                </tr>\r\n              </thead>\r\n              <tbody style=\"font-weight: 600; color: #525f7f;\">\r\n                <tr *ngFor=\"let item of ord | filter:searchText  | paginate: { itemsPerPage: 10, currentPage: p }\">\r\n                  <td>{{item.service}}</td>\r\n                  <td>{{item.orderNum}}</td>\r\n                  <td>{{item.serviceId}}</td>\r\n                  <td>{{item.status}}</td>\r\n                  <td>{{item.parameter}}</td>\r\n                  <td>{{item.value}}</td>\r\n                  <td>{{item.systemDesignation}}</td>\r\n                  <td>{{item.niNumber}}</td>\r\n                  <td>{{item.billingDate}}</td>\r\n                  <td>{{item.mrc}}</td>\r\n                  <td>{{item.nrc}}</td>\r\n                  <td>{{item.addedValueCharge}}</td>\r\n                  <td>{{item.civilWorkCharge}}</td>\r\n                  <td>{{item.operationMaintainenceCharge}}</td>\r\n                  <td>{{item.restorationCharge}}</td>\r\n                  <td>{{item.interconnectCharge}}</td>\r\n                  <td>{{item.portProviderCharge}}</td>\r\n                  <td>{{item.govtFee}}</td>\r\n                  <td>{{item.segmentStCharge}}</td>\r\n                  <td>{{item.segmentTCcharge}}</td>\r\n                  <td>{{item.segmentSCharge}}</td>\r\n                  <td>{{item.flatCharge}}</td>\r\n                  <td>{{item.tvCharge}}</td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n          <pagination-controls (pageChange)=\"p = $event\"></pagination-controls>\r\n          <!-- <mat-paginator [pageSizeOptions]=\"[2, 10, 25, 100]\"></mat-paginator> -->\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/portal-admin/portal-admin.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/Admin-Portal/portal-admin/portal-admin.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-8\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"header-body\">\r\n            <!-- Card stats -->\r\n\r\n            <div class=\"row\">\r\n                <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\r\n                    <div class=\"card card-stats mb-4 mb-xl-0\"> <a class=\"color-p\"\r\n                            [routerLink]=\"['/approve-registration']\">\r\n                            <div class=\"card-body\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"col\">\r\n                                        <span class=\"h2 font-weight-bold mb-0\">Approve\r\n                                            Registration</span>\r\n\r\n                                    </div>\r\n                                    <div class=\"col-auto\">\r\n                                        <img src=\"./assets/user.png\" height=\"50px\" width=\"50px\">\r\n                                    </div>\r\n                                </div>\r\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n                                </p>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\r\n                    <div class=\"card card-stats mb-4 mb-xl-0\"> <a class=\"color-p\" [routerLink]=\"['/registration-details']\">\r\n                            <div class=\"card-body\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"col\">\r\n                                        <span class=\"h2 font-weight-bold mb-0\">Registration\r\n                                            Details</span>\r\n\r\n                                    </div>\r\n                                    <div class=\"col-auto\">\r\n                                        <img src=\"./assets/detail.png\" height=\"50px\" width=\"50px\">\r\n                                    </div>\r\n                                </div>\r\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n                                </p>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\r\n                    <div class=\"card card-stats mb-4 mb-xl-0\">\r\n                        <a class=\"color-p\" [routerLink]=\"['/contact-admin']\">\r\n                            <div class=\"card-body\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"col\">\r\n                                        <span class=\"h2 font-weight-bold mb-0\">Contact Admin</span>\r\n                                    </div>\r\n                                    <div class=\"col-auto\">\r\n                                        <img src=\"./assets/contact.png\" height=\"50px\" width=\"50px\">\r\n                                    </div>\r\n                                </div>\r\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n                                </p>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\r\n                    <div class=\"card card-stats mb-4 mb-xl-0\">\r\n                        <a class=\"color-p\" [routerLink]=\"['/virtual-account']\">\r\n                            <div class=\"card-body\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"col\">\r\n                                        <span class=\"h2 font-weight-bold mb-0\">Virtual Account</span>\r\n                                    </div>\r\n                                    <div class=\"col-auto\">\r\n                                        <img src=\"./assets/virtual.png\" height=\"50px\" width=\"50px\">\r\n                                    </div>\r\n                                </div>\r\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n                                </p>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\r\n                    <div class=\"card card-stats mb-4 mb-xl-0\">\r\n                        <a class=\"color-p\" [routerLink]=\"['/role-access']\">\r\n                            <div class=\"card-body\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"col\">\r\n                                        <span class=\"h2 font-weight-bold mb-0\">Role & Access</span>\r\n                                    </div>\r\n                                    <div class=\"col-auto\">\r\n                                        <img src=\"./assets/access.png\" height=\"50px\" width=\"50px\">\r\n                                    </div>\r\n                                </div>\r\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n                                </p>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\r\n                    <div class=\"card card-stats mb-4 mb-xl-0\">\r\n                        <a class=\"color-p\" [routerLink]=\"['/history']\">\r\n                            <div class=\"card-body\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"col\">\r\n                                        <span class=\"h2 font-weight-bold mb-0\"\r\n                                            >Archive</span>\r\n                                    </div>\r\n                                    <div class=\"col-auto\">\r\n                                        <img src=\"./assets/archive.png\" height=\"50px\" width=\"50px\">\r\n                                    </div>\r\n                                </div>\r\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n                                </p>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\r\n                    <div class=\"card card-stats mb-4 mb-xl-0\">\r\n                        <a class=\"color-p\" [routerLink]=\"['/update-receiver']\">\r\n                            <div class=\"card-body\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"col\">\r\n                                        <span class=\"h2 font-weight-bold mb-0\">Update\r\n                                            Recevier</span>\r\n                                    </div>\r\n                                    <div class=\"col-auto\">\r\n                                        <img src=\"./assets/emails.png\" height=\"50px\" width=\"50px\">\r\n                                    </div>\r\n                                </div>\r\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n                                </p>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n                <!-- <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\r\n                    <div class=\"card card-stats mb-4 mb-xl-0\">\r\n                        <a class=\"color-p\" [routerLink]=\"['/orders']\">\r\n                            <div class=\"card-body\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"col\">\r\n                                        <span class=\"h2 font-weight-bold mb-0\">Orders</span>\r\n                                    </div>\r\n                                    <div class=\"col-auto\">\r\n                                        <img src=\"./assets/order.png\" height=\"50px\" width=\"50px\">\r\n                                    </div>\r\n                                </div>\r\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n                                </p>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div> -->\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class=\"container-fluid mt--7\">\r\n    <div class=\"row mt-5\">\r\n        <div class=\"col-xl-8 mb-5 mb-xl-0\">\r\n        </div>\r\n        <div class=\"col-xl-4\">\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/registration-action/registration-action.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/Admin-Portal/registration-action/registration-action.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pt-5 \">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"header-body\">\r\n            <!-- Table -->\r\n            <div class=\"card shadow\">\r\n                <div class=\"card-header border-0\">\r\n                    <div class=\"row align-items-center\">\r\n                        <div class=\"col\">\r\n                            <h2 class=\"mb-0\">Registration Details </h2>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"table-responsive table-res\">\r\n                    <table class=\"table align-items-center table-flush\">\r\n                        <div class=\"col-lg-9\">\r\n                            <p>\r\n                                <mat-form-field appearance=\"outline\">\r\n                                    <mat-label>First Name</mat-label>\r\n                                    <input matInput [(ngModel)]=\"regDetails.firstName\">\r\n                                </mat-form-field>\r\n \r\n                                <mat-form-field appearance=\"outline\">\r\n                                    <mat-label>Last Name</mat-label>\r\n                                    <input matInput [(ngModel)]=\"regDetails.lastName\">\r\n                                </mat-form-field>\r\n \r\n                                <mat-form-field appearance=\"outline\">\r\n                                    <mat-label>Email</mat-label>\r\n                                    <input matInput [(ngModel)]=\"regDetails.email\">\r\n                                </mat-form-field>\r\n \r\n                                <mat-form-field appearance=\"outline\">\r\n                                    <mat-label>Phone</mat-label>\r\n                                    <input matInput [(ngModel)]=\"regDetails.phone\">\r\n                                </mat-form-field>\r\n \r\n                                <mat-form-field appearance=\"outline\">\r\n                                    <mat-label>Company</mat-label>\r\n                                    <input matInput [(ngModel)]=\"regDetails.company\">\r\n                                </mat-form-field>\r\n \r\n                                <mat-form-field appearance=\"outline\">\r\n                                    <mat-label>Country</mat-label>\r\n                                    <input matInput [(ngModel)]=\"regDetails.country\">\r\n                                </mat-form-field>\r\n                            </p>\r\n                            <h4 class=\"card-title font-weight-bold\">NAWAQL Roles</h4>\r\n                            <mat-chip-list class=\"chip\" cdkDropList cdkDropListOrientation=\"horizontal\"\r\n                                >\r\n                                <mat-chip class=\"chip pb-3\" cdkDrag *ngFor=\"let capacity of capacities\">\r\n                                    {{capacity.name}}\r\n                                </mat-chip>\r\n                            </mat-chip-list>\r\n                            <br>\r\n                            <h4 class=\"card-title font-weight-bold\">Portal Roles</h4>\r\n                            <div class=\"form-group mb-3\">\r\n                                <div class=\"input-group input-group-alternative\">\r\n                                    <mat-form-field class=\"example-chip-list1\">\r\n                                        <mat-chip-list #chipList aria-label=\"Role selection\">\r\n                                            <mat-chip *ngFor=\"let role of roles\" [selectable]=\"selectable\"\r\n                                                [removable]=\"removable\" (removed)=\"remove(role)\">\r\n                                                {{role}}\r\n                                                <mat-icon matChipRemove *ngIf=\"removable\">cancel</mat-icon>\r\n                                            </mat-chip>\r\n                                            <input placeholder=\"New Roles\" #roleInput [matAutocomplete]=\"auto\"\r\n                                                [matChipInputFor]=\"chipList\" [formControl]=\"roleForm\"\r\n                                                [matChipInputSeparatorKeyCodes]=\"separatorKeysCodes\"\r\n                                                (matChipInputTokenEnd)=\"add($event)\" (keypress)=\"noText($event)\">\r\n                                        </mat-chip-list>\r\n                                        <mat-autocomplete #auto=\"matAutocomplete\" (optionSelected)=\"selected($event)\">\r\n                                            <mat-option *ngFor=\"let role of filteredRoles | async\" [value]=\"role\">\r\n                                                {{role}}\r\n                                            </mat-option>\r\n                                        </mat-autocomplete>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n                            <br>\r\n                            <div class=\"col-lg-000\">\r\n                                <div class=\"form-group\">\r\n                                    <label class=\"form-control-label\">Description</label>\r\n                                    <textarea class=\"form-control\" [formControl]=\"approve_text\" maxlength=\"500\" rows=\"3\"\r\n                                        style=\"resize: none;\"></textarea>\r\n                                    <span class=\"pull-right char\"><em>{{ descriptionLength | async }} / 500\r\n                                            characters</em></span>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n \r\n                    </table>\r\n                </div>\r\n                <div class=\"text-center\">\r\n                    <a [routerLink]=\"['/approve-registration']\" class=\"next round\">&#8249;</a>\r\n                    <button type=\"button\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"\r\n                        (click)=\"approve()\">Approve</button>\r\n                    <button type=\"button\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"\r\n                        (click)=\"reject()\">Reject</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/registration-details/registration-details.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/Admin-Portal/registration-details/registration-details.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-8 pt-5\">\r\n  <div class=\"container-fluid\">\r\n    <div class=\"header-body\">\r\n      <!-- Table -->\r\n      <div class=\"card shadow\">\r\n        <div class=\"border-0\">\r\n          <div class=\"row align-items-center\">\r\n            <div class=\"col\">\r\n              <h2 class=\"mb-0 mb-01\">Registration Details</h2>\r\n            </div>\r\n            <div>\r\n              <mat-form-field *ngFor=\"let filter of filterSelectObj\">\r\n                <mat-label>Filter {{filter.name}}</mat-label>\r\n                <select matNativeControl name=\"{{filter.columnProp}}\" [(ngModel)]=\"filter.modelValue\"\r\n                  (change)=\"filterChange(filter,$event)\">\r\n                  <option [value]=\"item\" *ngFor=\"let item of filter.options\">{{item}}</option>\r\n                </select>\r\n              </mat-form-field>\r\n            </div>\r\n            <div class=\"row\">\r\n              <div class=\"col\">\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"table-responsive\">\r\n          <!-- Projects table -->\r\n          <table class=\"table align-items-center table-flush\">\r\n            <thead class=\"thead-light\">\r\n              <tr>\r\n                <th scope=\"col\">Email</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody style=\"font-weight: 600; color: #525f7f;\">\r\n              <mat-accordion class=\"example-headers-align\">\r\n                <mat-expansion-panel [expanded]=\"step === 0\" (opened)=\"panelOpenState = true\"\r\n                  (closed)=\"panelOpenState = false\" *ngFor=\"let value of avengers\" hideToggle>\r\n                  <mat-expansion-panel-header>\r\n                      <mat-panel-title class=\"mb-0\"> {{value.email}} </mat-panel-title>\r\n                 <mat-panel-title>   <mat-chip-list aria-label=\"Fish selection\"></mat-chip-list>\r\n\r\n                      <mat-chip\r\n                            [ngStyle]=\"{ 'color' : 'white', 'background-color': (value.status === 'Approved') ? '#2ECC71' : (value.status === 'Rejeted') ? 'red' : (value.status === 'Submmited') ? 'gray':''}\">\r\n                            {{value.status}}</mat-chip> </mat-panel-title>  \r\n                    <mat-panel-description>{{panelOpenState ? 'Less' : 'More'}}\r\n                    </mat-panel-description>\r\n                  </mat-expansion-panel-header>\r\n                  <table class=\"table align-items-center table-flush\">\r\n                    <thead class=\"thead-light\">\r\n                      <tr>\r\n                        <th scope=\"col\">First Name</th>\r\n                        <th scope=\"col\">Last Name</th>\r\n                        <th scope=\"col\">Email</th>\r\n                        <th scope=\"col\">Phone</th>\r\n                        <th scope=\"col\">Company</th>\r\n                        <th scope=\"col\">country</th>\r\n                        <th scope=\"col\">Request Date</th>\r\n                        <th scope=\"col\"> Status</th>\r\n                      </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                      <tr>\r\n                        <td>{{value.fname}}</td>\r\n                        <td>{{value.lname}}</td>\r\n                        <td>{{value.email}}</td>\r\n                        <td>{{value.phone}}</td>\r\n                        <td>{{value.company}}</td>\r\n                        <td>{{value.country}}</td>\r\n                        <td>{{value.date | date }}</td>\r\n                        <td>   <mat-chip-list aria-label=\"Fish selection\"></mat-chip-list>\r\n                          <mat-chip \r\n                          [ngStyle]=\"{ 'color' : 'white', 'background-color': (value.status === 'Approved') ? 'green' : (value.status === 'Pending') ? 'coral' : ''}\">\r\n                          {{value.status}}</mat-chip></td>\r\n                      </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </mat-expansion-panel>\r\n              </mat-accordion>\r\n            </tbody>\r\n          </table>\r\n          \r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/role-access/role-access.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/Admin-Portal/role-access/role-access.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-8 pt-5 \">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"header-body\">\r\n            <!-- Table -->\r\n            <div class=\"card shadow\">\r\n                <div class=\"border-0\">\r\n                    <div class=\"row align-items-center\">\r\n                        <div class=\"col\">\r\n                            <h3 class=\"mb-0\">Role & Access Control </h3>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col\">\r\n                                <div class=\"text-center\">\r\n                                    <button class=\"btn submit my-4\" (click)=\"create()\" type=\"button\">New Role</button>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <!-- Projects table -->\r\n                    <table class=\"table align-items-center table-flush\">\r\n                        <thead class=\"thead-light\">\r\n                            <tr>\r\n                                <th scope=\"col\">Permission Title</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\r\n                            <mat-accordion class=\"example-headers-align\">\r\n                                <mat-expansion-panel [expanded]=\"step === 0\" (opened)=\"panelOpenState = true\"\r\n                                    (closed)=\"panelOpenState = false\" hideToggle>\r\n                                    <mat-expansion-panel-header>\r\n                                        <mat-panel-title class=\"mb-0\">Admin</mat-panel-title>\r\n                                        <mat-panel-description>{{panelOpenState ? 'Less' : 'More'}}\r\n                                            <mat-icon>account_circle</mat-icon>\r\n                                        </mat-panel-description>\r\n                                    </mat-expansion-panel-header>\r\n\r\n                                    <div class=\"table-responsive\">\r\n                                        <!-- Projects table -->\r\n                                        <table class=\"table align-items-center table-flush\">\r\n                                            <thead class=\"thead-light\">\r\n                                                <tr>\r\n                                                    <th scope=\"col\">Role Title</th>\r\n                                                    <th scope=\"col\">Creator Email</th>\r\n                                                    <th scope=\"col\">Created Date </th>\r\n                                                    <th scope=\"col\">Last Modification Date </th>\r\n                                                    <th scope=\"col\">Roles</th>\r\n                                                </tr>\r\n                                            </thead>\r\n                                            <tbody style=\"font-weight: 600; color: #525f7f;\">\r\n                                                <td>admin</td>\r\n                                                <td>a@stc.com.sa</td>\r\n                                                <td>2020-01-02</td>\r\n                                                <td>2020-01-05</td>\r\n                                                <td>\r\n                                                    <mat-chip-list class=\"example-chip\" cdkDropList\r\n                                                        cdkDropListOrientation=\"horizontal\"\r\n                                                        (cdkDropListDropped)=\"drop($event)\">\r\n                                                        <mat-chip class=\"example-box\" cdkDrag\r\n                                                            *ngFor=\"let role of roles\">\r\n                                                            {{role.name}}\r\n                                                        </mat-chip>\r\n                                                    </mat-chip-list>\r\n                                                </td>\r\n                                            </tbody>\r\n                                        </table>\r\n                                    </div>\r\n                                </mat-expansion-panel>\r\n                            </mat-accordion>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/role-view/role-view.component.front.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/Admin-Portal/role-view/role-view.component.front.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n    <div [ngClass]=\"toggleProperty ? 'flipped' : ''\" class=\"card\">\r\n      <div class=\"front\">\r\n        <ng-content select=\"flip-card-front\"></ng-content>\r\n        <div class=\"arrow-icon\" (click)=\"toggle()\">\r\n          <i class=\"ion-ios-arrow-back\"></i>\r\n        </div>\r\n      </div>\r\n      <div class=\"back\">\r\n        <ng-content select=\"flip-card-back\"></ng-content>\r\n        <div class=\"arrow-icon\" (click)=\"toggle()\">\r\n          <i class=\"ion-ios-arrow-back\"></i>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/update-receiver/update-receiver.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/Admin-Portal/update-receiver/update-receiver.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"header-body div1 card-body-img\">\r\n    <section>\r\n      <carousel [interval]=\"false\">\r\n        <div class=\"row div2\">\r\n          <slide>\r\n            <div class=\"col-xl-3 col-lg-6 display-line\" data-aos=\"fade-up\">\r\n              <div class=\"container\">\r\n                <div [ngClass]=\"toggleProperty ? 'flipped' : ''\" class=\"card\">\r\n                  <div class=\"front\">\r\n                    <ng-content select=\"flip-card-front\"></ng-content>\r\n                    <div class=\"card-body px-lg-5 py-lg-5\">\r\n                      <h2 class=\"text-center h2 font-weight-bold mb-0\">Contact Us Receiver</h2>\r\n                      <br>\r\n                      <br>\r\n                      <div class=\"form-group mb-3\">\r\n                        <div class=\"input-group input-group-alternative\">\r\n                          <div class=\"input-group-prepend\">\r\n                            <span class=\"input-group-text black\"><i class=\"ni ni-email-83\"></i> To Receiver </span>\r\n                          </div>\r\n                          <textarea class=\"form-control non-resize\" readonly>s@stc.com.sa, b@stc.com.sa</textarea>\r\n                        </div>\r\n                      </div>\r\n                      <br>\r\n                      <div class=\"form-group mb-3\">\r\n                        <div class=\"input-group input-group-alternative\">\r\n                          <div class=\"input-group-prepend\">\r\n                            <span class=\"input-group-text black\"> CC Receiver</span>\r\n                          </div>\r\n                          <textarea class=\"form-control non-resize\"\r\n                            readonly>s@stc.com.sa, b@stc.com.sa, r@stc.com.sa </textarea>\r\n                        </div>\r\n                      </div>\r\n\r\n                      <div class=\"arrow-icon\" (click)=\"toggle()\">\r\n                        <i class=\"ion-ios-arrow-back\">Edit</i>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"back\">\r\n                    <ng-content select=\"flip-card-back\"></ng-content>\r\n                    <div class=\"card-body px-lg-5 py-lg-5\">\r\n                      <div class=\"text-center text-muted mb-4\">\r\n                      </div>\r\n                      <h2 class=\"text-center h2 font-weight-bold mb-0\">Update Receiver</h2>\r\n                      <div class=\"form-group mb-3\">\r\n                        <div class=\"input-group input-group-alternative\">\r\n                          <div class=\"input-group-prepend\">\r\n                            <span class=\"input-group-text black\"><i class=\"ni ni-email-83\"></i> To Receiver </span>\r\n                          </div>\r\n                          <textarea class=\"form-control non-resize\">s@stc.com.sa, b@stc.com.sa</textarea>\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"form-group mb-3\">\r\n                        <div class=\"input-group input-group-alternative\">\r\n                          <div class=\"input-group-prepend\">\r\n                            <span class=\"input-group-text black\"> CC Receiver</span>\r\n                          </div>\r\n                          <textarea class=\"form-control non-resize\">s@stc.com.sa, b@stc.com.sa, r@stc.com.sa </textarea>\r\n                        </div>\r\n                      </div>\r\n                      <br>\r\n                      <br>\r\n\r\n                      <div class=\"text-center\">\r\n                        <button type=\"button\" id=\"myBtn\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"\r\n                          (click)=\"save()\">Save</button>\r\n\r\n                        <button type=\"button\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"><a\r\n                            style=\"color: white;\" (click)=\"toggle()\">Cancel</a></button>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </slide>\r\n          <slide>\r\n            <div class=\"col-xl-3 col-lg-6 display-line\" data-aos=\"fade-up\">\r\n              <div class=\"container \">\r\n                <div [ngClass]=\"toggleProperty ? 'flipped' : ''\" class=\"card\">\r\n                  <div class=\"front\">\r\n                    <ng-content select=\"flip-card-front\"></ng-content>\r\n                    <div class=\"card-body px-lg-5 py-lg-5\">\r\n                      <h2 class=\"text-center h2 font-weight-bold mb-0\">Operation</h2>\r\n                      <br>\r\n                      <br>\r\n                      <div class=\"form-group mb-3\">\r\n                        <div class=\"input-group input-group-alternative\">\r\n                          <div class=\"input-group-prepend\">\r\n                            <span class=\"input-group-text black\"><i class=\"ni ni-email-83\"></i> To Receiver </span>\r\n                          </div>\r\n                          <textarea class=\"form-control non-resize\" readonly>s@stc.com.sa, b@stc.com.sa</textarea>\r\n                        </div>\r\n                      </div>\r\n                      <br>\r\n                      <div class=\"arrow-icon\" (click)=\"toggle()\">\r\n                        <i class=\"ion-ios-arrow-back\">Edit</i>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"back\">\r\n                    <ng-content select=\"flip-card-back\"></ng-content>\r\n                    <div class=\"card-body px-lg-5 py-lg-5\">\r\n                      <div class=\"text-center text-muted mb-4\">\r\n                      </div>\r\n                      <h2 class=\"text-center h2 font-weight-bold mb-0\">Update Receiver</h2>\r\n                      <br>\r\n                      <br>\r\n                      <div class=\"form-group mb-3\">\r\n                        <div class=\"input-group input-group-alternative\">\r\n                          <div class=\"input-group-prepend\">\r\n                            <span class=\"input-group-text black\"><i class=\"ni ni-email-83\"></i> To Receiver </span>\r\n                          </div>\r\n                          <textarea class=\"form-control non-resize\">s@stc.com.sa, b@stc.com.sa</textarea>\r\n                        </div>\r\n                      </div>\r\n                      <br>\r\n                      <br>\r\n                      <br>\r\n                      <div class=\"text-center\">\r\n                        <button type=\"button\" id=\"myBtn\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"\r\n                          (click)=\"save()\">Save</button>\r\n                        <button type=\"button\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"><a\r\n                            style=\"color: white;\" (click)=\"toggle()\">Cancel</a></button>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </slide>\r\n        </div>\r\n      </carousel>\r\n    </section>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/virtual-account/virtual-account.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/Admin-Portal/virtual-account/virtual-account.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-8 pt-5 \">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"header-body\">\r\n            <!-- Table -->\r\n            <div class=\"card shadow\">\r\n                <div class=\"border-0\">\r\n                    <div class=\"row align-items-center\">\r\n                        <div class=\"col\">\r\n                            <h2 class=\"mb-0 mb-01\">Virtual Account</h2>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col\">\r\n                                <div class=\"text-center\">\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <!-- Projects table -->\r\n                    <table class=\"table align-items-center table-flush\">\r\n                        <thead class=\"thead-light\">\r\n                            <tr>\r\n                                <th scope=\"col\">Email</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\r\n                            <mat-accordion class=\"example-headers-align\">\r\n                                <mat-expansion-panel [expanded]=\"step === 0\" (opened)=\"panelOpenState = true\"\r\n                                    (closed)=\"panelOpenState = false\" hideToggle *ngFor=\"let item of virtual\"> \r\n                                    <mat-expansion-panel-header>\r\n                                        \r\n                                        <mat-panel-title class=\"mb-0\">{{item.email}}</mat-panel-title>\r\n                                        <mat-panel-description>{{panelOpenState ? 'Less' : 'More'}}\r\n                                        </mat-panel-description>\r\n                                    </mat-expansion-panel-header>\r\n\r\n                                    <div class=\"table-responsive\">\r\n                                        <!-- Projects table -->\r\n                                        <table class=\"table align-items-center table-flush\">\r\n                                            <thead class=\"thead-light\">\r\n\r\n                                                <tr>\r\n                                                    <th scope=\"col\">First Name</th>\r\n                                                    <th scope=\"col\">Last Name</th>\r\n                                                    <th scope=\"col\">Email</th>\r\n                                                    <th scope=\"col\">Phone</th>\r\n                                                    <th scope=\"col\">Company</th>\r\n                                                    <th scope=\"col\">country</th>\r\n                                                    <th scope=\"col\">Request Date</th>\r\n                                                    <th scope=\"col\"> Remark</th>\r\n                                                </tr>\r\n                                                <tr >\r\n                                                   \r\n                                                   <td>{{item.email}}</td>\r\n                                                   <td>{{item.company}}</td>\r\n                                                   <td>{{item.date | date:'dd/MM/yyyy'}}</td>\r\n                                                   <td>{{item.firstName}}</td>\r\n                                                   <td>{{item.id}}</td>\r\n                                                   <td>{{item.lastName}}</td>\r\n                                                   <td>{{item.phone}}</td>\r\n                                                   <td>{{item.status}}</td>\r\n                                                   <td>{{item.country}}</td>\r\n                                                   <tbody style=\"font-weight: 600; color: #525f7f;\">\r\n                                                \r\n                                                    <td> <button type=\"button\" class=\"btn btn-danger btn-danger\"\r\n                                                        (click)=\"onDelete(item)\">Delete</button>\r\n                                                </td>\r\n                                                </tbody>\r\n                                           \r\n                                                </tr>\r\n                                            </thead>\r\n                                            <!-- <tbody style=\"font-weight: 600; color: #525f7f;\">\r\n                                                \r\n                                                <td> <button type=\"button\" class=\"btn btn-danger btn-danger\"\r\n                                                    (click)=\"onDelete(item)\">Delete</button>\r\n                                            </td>\r\n                                            </tbody>\r\n                                        -->\r\n                                        </table>\r\n                                    </div>\r\n                                </mat-expansion-panel>\r\n                            </mat-accordion>\r\n                        </tbody>\r\n                    </table>\r\n                   \r\n                                          \r\n                                    </div>\r\n                               \r\n                    <mat-paginator [length]=\"100\" [pageSize]=\"10\" [pageSizeOptions]=\"[5, 10, 25, 100]\">\r\n                    </mat-paginator>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/admin-routing/admin-routing.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/admin-routing/admin-routing.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>admin-routing works!</p>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/complaint/complaint.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/complaint/complaint.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n    <div class=\"row justify-content-center\">\r\n        <div class=\"col-lg-8 col-md-4\">\r\n            <div class=\"card bg-secondary shadow border-0\">\r\n                <div class=\"card-body px-lg-5 py-lg-5\">\r\n                    <form [formGroup]=\"complaintForm\" (ngSubmit)=\"onSubmit()\" autocomplete=\"off\">\r\n                        <div class=\"resp_padding\">\r\n                            <div class=\"col\">\r\n                                <h3 class=\"mb-0\">Submit Complaint </h3>\r\n                            </div>\r\n                            <br>\r\n                            <div class=\"col-lg-4\">\r\n                                <div class=\"text-muted mb-4\">\r\n                                    <label class=\"form-control-label\">Complaint Type</label>\r\n                                    <span class=\"required_lable\">*</span>\r\n                                    <select class=\"form-control\" (change)=\"complainType($event)\">\r\n                                        <option value=\"billing\" selected>Billing</option>\r\n                                        <option value=\"general\">General</option>\r\n                                        <option value=\"technical\">Technical </option>\r\n                                    </select>\r\n                                </div>\r\n                                <div class=\"text-muted mb-4\" [hidden]=technical>\r\n                                    <label for=\"input-city\" class=\"form-control-label\">Invoice Ref#</label>\r\n                                    <span class=\"required_lable\">*</span>\r\n                                    <select class=\"form-control\">\r\n                                        <option>100</option>\r\n                                        <option>110</option>\r\n                                        <option>112</option>\r\n                                        <option>113</option>\r\n                                    </select>\r\n                                </div>\r\n                                <div class=\"text-muted mb-4\" [hidden]=general>\r\n                                    <label for=\"input-city\" class=\"form-control-label\">Service ID</label>\r\n                                    <span class=\"required_lable\">*</span>\r\n                                    <select class=\"form-control\">\r\n                                        <option>332</option>\r\n                                        <option>211</option>\r\n                                        <option>778</option>\r\n                                        <option>445</option>\r\n                                    </select>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-lg-000\">\r\n                                <div class=\"form-group\">\r\n                                    <label class=\"form-control-label\">Description Details</label>\r\n                                    <span class=\"required_lable\">*</span>\r\n                                    <textarea formControlName=\"desc\" #description class=\"form-control mb-1\"\r\n                                        maxlength=\"500\"></textarea>\r\n                                    <span class=\"pull-right char\"><em>{{ description.value.length }} / 500\r\n                                            characters</em></span>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"text-center pt-5\">\r\n                                <button type=\"submit\" class=\"btn btn-send\" [disabled]=\"!complaintForm.valid\">\r\n                                    Send</button>\r\n                                <button class=\"btn btn-cancel ml-1\" type=\"button\" [routerLink]=\"['/home']\">\r\n                                    Cancel</button>\r\n                            </div>\r\n                        </div>\r\n                    </form>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/contact-us/contact-us.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/contact-us/contact-us.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Page content -->\r\n<section class=\"contactUs\">\r\n  <div class=\"container\">\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"col-lg-8 col-md-4\">\r\n        <div class=\"card bg-secondary shadow border-0\">\r\n          <div class=\"card-body px-lg-5 py-lg-5\">\r\n            <div class=\"btn-group\" role=\"group\" aria-label=\"Button group with nested dropdown\">\r\n            </div>\r\n            <form [formGroup]=\"contactForm\" (ngSubmit)=\"onSubmit()\" autocomplete=\"off\">\r\n              <div class=\"resp_padding\">\r\n                <div class=\"contactus_question\">\r\n                  <img src=\"./assets/ask-question-large-icon.png\" width=\"87\" height=\"87\" alt=\"Ask a Question\">\r\n                </div>\r\n\r\n                <div class=\"floated-spans help-livechat-go contactus_images\">\r\n                  <div class=\"images-containers bottom-margin\"><img src=\"./assets/ask-question-title1.png\" width=\"169\"\r\n                      height=\"32\" alt=\"Ask a question\"></div>\r\n                  <div class=\"bottom_margin purple_label\" style=\"width:325px;\">We care about every single message we\r\n                    receive.</div>\r\n                </div>\r\n\r\n                <br class=\"clr\">\r\n                <br class=\"clr\">\r\n                <div id=\"validationErrorMessageForContactus\" class=\"bottom_margin error_msg\"\r\n                  style=\"display:none; background-color:#fff; border:solid 1px #e4188b; border-radius:6px; -webkit-border-radius:6px; -moz-border-radius:6px; padding-bottom:5px\">\r\n                </div>\r\n                <div id=\"dataSection\">\r\n                  <div class=\"bottom_margin purple_label floated_divs\" style=\"width:190px;\"><label\r\n                      for=\"request_type\">its\r\n                      about</label><span class=\"required_lable\"> *</span></div>\r\n\r\n                  <div class=\"floated_divs mobile_input_width right_margin_20\" style=\"width: 300px;\">\r\n                    <div class=\"bottom_margin\">\r\n                      <div class=\"_select medium_dropdown\">\r\n                        <span class=\"right_margin\">\r\n                          <select name=\"request_type\" id=\"request_type\">\r\n                            <option value=\"-1\">What?</option>\r\n                            <option value=\"H\" selected>Help and Support</option>\r\n                            <option value=\"S\">Suggestion</option>\r\n                            <option value=\"I\">Inquiry</option>\r\n                          </select>\r\n                        </span>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"text-muted mb-4\">\r\n                  <div class=\"form-group mb-3\">\r\n                    <label> Customer Company Name </label>\r\n                    <div class=\"input-group input-group-alternative\">\r\n                      <input type=\"text\" class=\"form-control\" maxlength=\"30\" placeholder=\"Customer Company Name\"\r\n                        (focus)=\"focus=true\" (blur)=\"focus=false\" formControlName=\"companyName\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group mb-3\">\r\n                    <label> Contact Name </label>\r\n                    <div class=\"input-group input-group-alternative\">\r\n                      <input type=\"text\" class=\"form-control\" maxlength=\"30\" placeholder=\"Contact Name\"\r\n                        (focus)=\"focus=true\" (blur)=\"focus=false\" formControlName=\"contactName\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group mb-3\">\r\n                    <label> Contact Mobile Number </label>\r\n                    <div class=\"input-group input-group-alternative\">\r\n                      <input type=\"text\" class=\"form-control\" maxlength=\"15\" placeholder=\"Contact Mobile Number\"\r\n                        (focus)=\"focus=true\" (blur)=\"focus=false\" formControlName=\"contactNumber\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group mb-3\">\r\n                    <label> Email </label>\r\n                    <div class=\"input-group input-group-alternative\">\r\n                      <input type=\"text\" class=\"form-control\" maxlength=\"30\" placeholder=\"Email\" (focus)=\"focus=true\"\r\n                        (blur)=\"focus=false\" formControlName=\"email\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group mb-3\">\r\n                    <span> Message Subject </span>\r\n                    <div class=\"input-group input-group-alternative\">\r\n                      <input type=\"text\" class=\"form-control\" maxlength=\"45\" [formControl]=\"textontrol\"\r\n                        (focus)=\"focus=true\" (blur)=\"focus=false\">\r\n                    </div>\r\n                    <span class=\"pull-right char\"><em>{{ subjectLength | async }} / 45 characters</em></span>\r\n                  </div>\r\n                  <label> Message Details </label>\r\n                  <textarea type=\"textarea\" [formControl]=\"textControl\" class=\"form-control mb-1\"\r\n                    maxlength=\"1000\"></textarea>\r\n                  <span class=\"pull-right char\"><em>{{ descriptionLength | async }} / 225 characters</em></span>\r\n                  <div class=\"clearfix\"></div>\r\n                  <div class=\"text-center pt-5\">\r\n                    <button type=\"submit\" class=\"btn btn-send\"> Send</button>\r\n                    <button class=\"btn btn-cancel ml-1\" type=\"button\" [routerLink]=\"['/home']\"> Cancel</button>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </form>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/containers/main-layout/main-layout.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/containers/main-layout/main-layout.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn custom-nav \">\r\n  <nav class=\"navbar navbar-expand-lg  custm-n\" style=\"background-color: #4F008C; padding: 3vh; height: 10vh;\">\r\n    <img src=\"./assets/FA_STC_RGB_NEG.ico\" height=\"60px\" width=\"80px\" [routerLink]=\"['/home']\">\r\n    <button aria-controls=\"navbarSupportedContent\" (click)=\"isCollapsed = !isCollapsed\"\r\n      [attr.aria-expanded]=\"!isCollapsed\" class=\"navbar-toggler\" type=\"button\" aria-label=\"Toggle navigation\">\r\n      <span class=\"navbar-toggler-icon\"></span>\r\n    </button>\r\n\r\n    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\" [collapse]=\"isCollapsed\" [isAnimated]=\"true\">\r\n      <ul class=\"navbar-nav mr-auto custom-nav-items color-white\">\r\n\r\n        <li class=\"nav-item active dropdown\" dropdown>\r\n          <a id=\"link-dropdown\" class=\"nav-link dropdown-toggle color-white\" href dropdownToggle (click)=\"false\"\r\n            aria-controls=\"your-dropdown\" style=\"color: white;\">\r\n            {{'HEADER.MENU.SERVICE'| translate}}\r\n          </a>\r\n          <div id=\"your-dropdown\" class=\"dropdown-menu\" aria-labelledby=\"link-dropdown\" *dropdownMenu>\r\n            <a class=\"dropdown-item\" [routerLink]=\"['/display-invoice']\">\r\n              {{'HEADER.SUB_MENU.DISPLAY_INVOICE'| translate}}</a>\r\n            <a class=\"dropdown-item\" (mouseenter)=\"openMyMenu()\" [matMenuTriggerFor]=\"service\">\r\n              {{'HEADER.SUB_MENU.DISPLAY_SERVICE'| translate}}</a>\r\n            <mat-menu #service=\"matMenu\">\r\n              <a class=\"dropdown-item\" [routerLink]=\"['/orders']\">\r\n                In Progress Orders\r\n              </a>\r\n              <a class=\"dropdown-item\" [routerLink]=\"['/assets']\">\r\n                Active Installed Services\r\n              </a>\r\n            </mat-menu>\r\n          </div>\r\n        </li>\r\n        <li class=\"nav-item dropdown\" dropdown>\r\n          <a id=\"link-dropdown\" class=\"nav-link dropdown-toggle color-white\" href dropdownToggle (click)=\"false\"\r\n            aria-controls=\"your-dropdown\" style=\"color: white;\">\r\n            {{'HEADER.MENU.REPORT'| translate}}\r\n          </a>\r\n          <div id=\"your-dropdown\" class=\"dropdown-menu\" aria-labelledby=\"link-dropdown\" *dropdownMenu>\r\n            <a class=\"dropdown-item\" href=\"#\"\r\n              [routerLink]=\"['/dashboard']\">{{'HEADER.SUB_MENU.DASHBOARD'| translate}}</a>\r\n            <a class=\"dropdown-item\" href=\"#\" [routerLink]=\"['/details']\"> {{'HEADER.SUB_MENU.DETAILS'| translate}}</a>\r\n          </div>\r\n        </li>\r\n\r\n\r\n        <li class=\"nav-item dropdown\" dropdown>\r\n          <a id=\"link-dropdown\" class=\"nav-link dropdown-toggle color-white\" href dropdownToggle (click)=\"false\"\r\n            aria-controls=\"your-dropdown\" style=\"color: white;\">\r\n            {{'HEADER.MENU.CDR'| translate}}\r\n          </a>\r\n          <div id=\"your-dropdown\" class=\"dropdown-menu\" aria-labelledby=\"link-dropdown\" *dropdownMenu>\r\n            <a class=\"dropdown-item\" href=\"#\" [routerLink]=\"['/upload']\">{{'HEADER.SUB_MENU.UPLOAD'| translate}}</a>\r\n            <a class=\"dropdown-item\" href=\"#\" [routerLink]=\"['/download']\">\r\n              {{'HEADER.SUB_MENU.DOWNLOAD'| translate}}</a>\r\n          </div>\r\n        </li>\r\n\r\n\r\n        <li class=\"nav-item dropdown\" dropdown>\r\n          <a id=\"link-dropdown\" class=\"nav-link dropdown-toggle color-white\" href dropdownToggle (click)=\"false\"\r\n            aria-controls=\"your-dropdown \" style=\"color: white;\">\r\n            {{'HEADER.MENU.CONTACT_US'| translate}}\r\n          </a>\r\n          <div id=\"your-dropdown\" class=\"dropdown-menu\" aria-labelledby=\"link-dropdown\" *dropdownMenu>\r\n            <a class=\"dropdown-item\" href=\"#\" [routerLink]=\"['/help']\"> {{'HEADER.SUB_MENU.HELP'| translate}} </a>\r\n            <a class=\"dropdown-item\" href=\"#\" [routerLink]=\"['/contact']\">{{'HEADER.MENU.CONTACT_US'| translate}} </a>\r\n\r\n          </div>\r\n        </li>\r\n        <li class=\"nav-item \">\r\n          <a class=\"nav-link color-white\" href=\"#\" [routerLink]=\"['/complaint']\" style=\"color: white;\">\r\n            {{'HEADER.MENU.COMPLAIN'| translate}}\r\n          </a><span class=\"sr-only\">(current)</span>\r\n        </li>\r\n        <li class=\"nav-item \">\r\n          <a class=\"nav-link color-white\" href=\"#\" [routerLink]=\"['/product-catalog']\" style=\"color: white;\">\r\n            Product Catalog\r\n          </a><span class=\"sr-only\">(current)</span>\r\n        </li>\r\n      </ul>\r\n      <ul class=\"navbar-nav align-items-center d-none d-md-flex color-white\">\r\n        <!-- <a *ngIf=\"!isRTL\" (click)=\"changeLang(arabic)\">\r\n          <i class=\"ni ni-support-16\"></i>\r\n          <span>ع</span>\r\n        </a>\r\n        <a *ngIf=\"isRTL\" (click)=\"changeLang(english)\">\r\n          <i class=\"ni ni-support-16\"></i>\r\n          <span>En</span>\r\n        </a> -->\r\n        <li class=\"nav-item color-white\" ngbDropdown placement=\"bottom-right\">\r\n          <a class=\"nav-link pr-0\" role=\"button\" ngbDropdownToggle>\r\n            <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>\r\n          </a>\r\n          <div class=\"dropdown-menu-arrow dropdown-menu-right\" ngbDropdownMenu>\r\n            <div class=\" dropdown-header noti-title\">\r\n              <h3 class=\"text-overflow m-0 h3\"> {{'HEADER.WELCOME'| translate}}  {{username }}</h3>\r\n              <p class=\"card-text\"><small class=\"text-muted\">Last Login:  {{lastLogin | date:'dd/MM/yyyy'}}</small></p>\r\n            </div>\r\n            <!-- <a routerLinkActive=\"active\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-single-02\"></i>\r\n              <span>{{'HEADER.MYPROFILE'| translate}}</span>\r\n            </a> -->\r\n            <a (click)=\"portal()\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-single-16\"></i>\r\n              <span>Portal Admin</span> </a>\r\n            <!-- </a>\r\n            <a class=\"dropdown-item\" *ngIf=\"!isRTL\" (click)=\"changeLang(arabic)\">\r\n              <i class=\"ni ni-support-16\"></i>\r\n              <span>العربيه</span>\r\n            </a>\r\n            <a class=\"dropdown-item\" *ngIf=\"isRTL\" (click)=\"changeLang(english)\">\r\n              <i class=\"ni ni-support-16\"></i>\r\n              <span>English</span>\r\n            </a> -->\r\n            <div class=\"dropdown-divider\"></div>\r\n            <a routerLink=\"/login\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-user-run\"></i>\r\n              <span>{{'HEADER.LOGOUT'| translate}}</span>\r\n            </a>\r\n          </div>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n    \r\n  </nav>\r\n</div>\r\n<router-outlet></router-outlet>\r\n\r\n<!-- Footer -->\r\n<footer>\r\n    <img src=\"./assets/FA_STC_RGB_NEG.ico\" height=\"40px\" width=\"60px\">\r\n    Copyright&copy; {{fullyear}} stc. All rights reserved.\r\n</footer>\r\n\r\n<script>\r\n  // Add active class to the current button (highlight it)\r\n  var header = document.getElementById(\"collapse\");\r\n  var btns = header.getElementsByClassName(\"nav-item\");\r\n  for (var i = 0; i < btns.length; i++) {\r\n    btns[i].addEventListener(\"click\", function() {\r\n    var current = document.getElementsByClassName(\"active\");\r\n    current[0].className = current[0].className.replace(\" active\", \"\");\r\n    this.className += \" active\";\r\n    });\r\n  }\r\n  </script>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/dashboard-wbu/dashboard-wbu.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/dashboard-wbu/dashboard-wbu.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-8\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"header-body\">\r\n            <div class=\"card shadow\">\r\n                <div class=\"card-header border-0\">\r\n                    <div class=\"row align-items-center\">\r\n                        <div class=\"col\">\r\n                            <h3 class=\"h2\">Dashboard</h3>\r\n                        </div>\r\n                        <div class=\"col text-right\">\r\n                            <!-- <a href=\"#!\" class=\"btn btn-sm btn-primary\">See all</a> -->\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <div class=\"container\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-sm-2 cards\">\r\n                                <label>OPEN</label>\r\n                                <i class=\"fa fa-key\" aria-hidden=\"true\"></i>\r\n                                <div>\r\n                                    <label class=\"card-txt\">00</label>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-sm-2 cards\">\r\n                                <label>DELAY</label>\r\n                                <i class=\"fa fa-clock-o\" aria-hidden=\"true\"></i>\r\n                                <div>\r\n                                    <label class=\"card-txt\">00</label>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-sm-2 cards\">\r\n                                <label>CLOSED</label>\r\n                                <i class=\"fa fa-cog\" aria-hidden=\"true\"></i>\r\n                                <div>\r\n                                    <label class=\"card-txt\">00</label>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-sm-2 cards\">\r\n                                <label>PENDING </label> <i class=\"fa fa-hourglass-end\" aria-hidden=\"true\"></i>\r\n                                <div>\r\n                                    <label class=\"card-txt\">00</label>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"chart-wrapper graph-align-right\" >\r\n                        <canvas baseChart [datasets]=\"barChartData\" [labels]=\"barChartLabels\"\r\n                            [options]=\"barChartOptions\" [plugins]=\"barChartPlugins\" [legend]=\"barChartLegend\"\r\n                            [chartType]=\"barChartType\">\r\n                        </canvas>\r\n                    </div>\r\n                    <div class=\"chart-wrapper graph-align-left\">\r\n                        <canvas baseChart [data]=\"pieChartData\" [labels]=\"pieChartLabels\" [chartType]=\"pieChartType\"\r\n                            [options]=\"pieChartOptions\" [plugins]=\"pieChartPlugins\" [legend]=\"pieChartLegend\">\r\n                        </canvas>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/details/details.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/details/details.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pt-5 pt-md-8\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"header-body\">\r\n            <!-- Table -->\r\n            <div class=\"card shadow\">\r\n                <div class=\"card-header border-0\">\r\n                    <div class=\"row align-items-center\">\r\n                        <div class=\"col\">\r\n                            <div class=\"h3\">Report Details </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <div>\r\n                        <mat-form-field *ngFor=\"let filter of filterSelectObj\" style=\"width: 12%;\">\r\n                            <mat-label>Filter {{filter.type}}</mat-label>\r\n                            <select matNativeControl name=\"{{filter.columnProp}}\" [(ngModel)]=\"filter.modelValue\"\r\n                                (change)=\"filterChange(filter,$event)\">\r\n                                <option [value]=\"item\" *ngFor=\"let item of filter.options\">{{item}}</option>\r\n                            </select>\r\n                        </mat-form-field>\r\n                    </div>\r\n\r\n                    <table mat-table class=\"table align-items-center table-flush\" [dataSource]=\"dataSource\">\r\n                        <thead class=\"thead-light\">\r\n                            <ng-container matColumnDef=\"complaint\">\r\n                                <th mat-header-cell *matHeaderCellDef> Complaint </th>\r\n                                <td mat-cell *matCellDef=\"let element\"> {{element.complaint}} </td>\r\n                            </ng-container>\r\n\r\n                            <ng-container matColumnDef=\"type\">\r\n                                <th mat-header-cell *matHeaderCellDef> Type </th>\r\n                                <td mat-cell *matCellDef=\"let element\"> {{element.type}} </td>\r\n                            </ng-container>\r\n\r\n                            <ng-container matColumnDef=\"assigned\">\r\n                                <th mat-header-cell *matHeaderCellDef> Assigned To </th>\r\n                                <td mat-cell *matCellDef=\"let element\"> {{element.assigned}} </td>\r\n                            </ng-container>\r\n\r\n                            <ng-container matColumnDef=\"customer\">\r\n                                <th mat-header-cell *matHeaderCellDef> customer </th>\r\n                                <td mat-cell *matCellDef=\"let element\"> {{element.customer}} </td>\r\n                            </ng-container>\r\n\r\n                            <ng-container matColumnDef=\"status\">\r\n                                <th mat-header-cell *matHeaderCellDef> status </th>\r\n                                <td mat-cell *matCellDef=\"let element\"> {{element.status}} </td>\r\n                            </ng-container>\r\n\r\n                            <ng-container matColumnDef=\"creation\">\r\n                                <th mat-header-cell *matHeaderCellDef> creation </th>\r\n                                <td mat-cell *matCellDef=\"let element\"> {{element.creation}} </td>\r\n                            </ng-container>\r\n\r\n                            <ng-container matColumnDef=\"close\">\r\n                                <th mat-header-cell *matHeaderCellDef> close </th>\r\n                                <td mat-cell *matCellDef=\"let element\"> {{element.close}} </td>\r\n                            </ng-container>\r\n                            <ng-container matColumnDef=\"mttr\">\r\n                                <th mat-header-cell *matHeaderCellDef> MTTR (H) </th>\r\n                                <td mat-cell *matCellDef=\"let element\"> {{element.mttr}} </td>\r\n                            </ng-container>\r\n                            <tr mat-header-row *matHeaderRowDef=\"displayedColumns;\"></tr>\r\n                            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n                        </thead>\r\n                    </table>\r\n                </div>\r\n                <mat-paginator [pageSizeOptions]=\"[5, 10, 25, 100]\"></mat-paginator>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/display-invoice/display-invoice.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/display-invoice/display-invoice.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-9\" [hidden]=invoiceSearch>\r\n    <div class=\"container mt--8 pb-5\">\r\n        <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-8 col-md-7\">\r\n                <div class=\"card bg-secondary shadow border-0\">\r\n                    <div class=\"header-body\">\r\n                        <!-- Table -->\r\n                        <div class=\"card shadow\">\r\n                            <div class=\"card-header border-0\" style=\"background-color: #F7FAFC;\">\r\n                                <div class=\"row align-items-center\">\r\n                                    <div class=\"col-auto\" style=\"padding-left: 40%;\">\r\n                                        <div class=\"icon icon-shape text-white rounded-circle shadow\">\r\n                                            <div class=\"col-auto\">\r\n                                                <img src=\"./assets/invoice.png\" height=\"70px\" width=\"70px\"\r\n                                                    style=\"margin-top: -5.5rem; margin-left: 5.5rem;\">\r\n                                            </div>\r\n                                            <i class=\"fas fa-chart-pie\"></i>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                          \r\n                            <div class=\"col\">\r\n                                <h3 class=\"mb-0 mb-1\"> Display Invoice </h3>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-lg-4\">\r\n                                    <label for=\"input-city\" class=\"form-control-label\">Invoice Number</label>\r\n                                    <input class=\"form-control\" type=\"text\" maxlength=\"10\" />\r\n                                </div>\r\n                                <div class=\"col-lg-4\">\r\n                                    <label for=\"input-city\" class=\"form-control-label\">Billing Period</label>\r\n                                    <select class=\"form-control\">\r\n                                        <option disabled selected value></option>\r\n                                        <option>20141201</option>\r\n                                        <option>20141202</option>\r\n                                        <option>20141203</option>\r\n                                        <option>20141204</option>\r\n                                    </select>\r\n                                </div>\r\n                                <div class=\"form-group hidden\">\r\n                                    <div class=\"input-group\">\r\n                                        <input name=\"datepicker\" class=\"form-control\" ngbDatepicker\r\n                                            #datepicker=\"ngbDatepicker\" maxlength=\"10\" (dateSelect)=\"onDateSelection($event)\"\r\n                                            [dayTemplate]=\"t\">\r\n                                        <ng-template #t let-date let-focused=\"focused\">\r\n                                            <span class=\"custom-day\" [class.focused]=\"focused\"\r\n                                                (mouseenter)=\"hoveredDate = date\" (mouseleave)=\"hoveredDate = null\">\r\n                                                {{ date.day }}\r\n                                            </span>\r\n                                        </ng-template>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"form-group col-lg-4\">\r\n                                    <label class=\"form-control-label\"\r\n                                        for=\"input-city\">{{\"Invoice Date\" | titlecase}}</label>\r\n                                    <div class=\"input-group\">\r\n                                        <input #dpFromDate class=\"form-control\" placeholder=\"yyyy-mm-dd\"\r\n                                            name=\"dpFromDate\" maxlength=\"10\" [value]=\"formatter.format(fromDate)\"\r\n                                            (input)=\"fromDate = validateInput(fromDate, dpFromDate.value)\">\r\n                                        <div class=\"input-group-append\">\r\n                                            <button class=\"btn btn-outline-secondary calendar\"\r\n                                                (click)=\"datepicker.toggle()\" type=\"button\">\r\n                                                <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                                            </button>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div class=\"col-lg-4\">\r\n                                    <label for=\"ccnumber\" class=\"form-control-label\">Service Type</label>\r\n\r\n                                    <select class=\"form-control\" id=\"yyy\">\r\n                                        <option disabled selected value></option>\r\n                                        <option>Voice</option>\r\n                                        <option>Non-Voice</option>\r\n                                        <option>Voice & Non-Voice</option>\r\n                                    </select>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"row\">\r\n                                <div class=\"col\">\r\n                                    <div class=\"text-center\">\r\n                                        <button class=\"btn submit my-4 \" (click)=\"disply()\"\r\n                                            type=\"button\">Search</button>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-8\" [hidden]=invoice>\r\n    <div class=\"container mt--8 pb-5\">\r\n        <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-8 col-md-7\">\r\n                <div class=\"card bg-secondary shadow border-0\">\r\n                    <div class=\"header-body\">\r\n                        <!-- Table -->\r\n                        <div class=\"card shadow\">\r\n                            <div class=\"card-header border-0\">\r\n                                <div class=\"row align-items-center\">\r\n                                    <div class=\"col\">\r\n                                        <h3 class=\"mb-0\">Invoice Details</h3>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"table-responsive\">\r\n                                <table class=\"table align-items-center table-flush\">\r\n                                    <thead class=\"thead-light\">\r\n                                        <tr>\r\n                                            <th scope=\"col\">Invoice Number</th>\r\n                                            <th scope=\"col\">Billing Period</th>\r\n                                            <th scope=\"col\">Billing Date</th>\r\n                                            <th scope=\"col\">Invoice Link</th>\r\n                                        </tr>\r\n                                    </thead>\r\n                                    <tbody>\r\n                                        <tr>\r\n                                            <td>\r\n                                                0000 0000 0000 0000\r\n                                            </td>\r\n                                            <td>\r\n                                                20141202\r\n                                            </td>\r\n                                            <td>\r\n                                                01-02-2020\r\n                                            </td>\r\n                                            <td>\r\n                                                <a href=\"/images/myw3schoolsimage.jpg\" download>Download </a>\r\n                                            </td>\r\n                                        </tr>\r\n                                    </tbody>\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/downloads-operations/downloads-operations.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/downloads-operations/downloads-operations.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pt-5 pt-md-8\" [hidden]=CDRsearch>\r\n  <div class=\"container mt--8 pb-5\">\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"col-lg-8 col-md-7\">\r\n        <div class=\"header-body\">\r\n          <ngb-tabset [destroyOnHide]=\"false\">\r\n            <ngb-tab>\r\n              <ng-template ngbTabTitle><b>Billing</b></ng-template>\r\n              <ng-template ngbTabContent>\r\n                <!-- Table -->\r\n                <div class=\"col-lg-4 custom-day\">\r\n                  <label for=\"input-city\" class=\"form-control-label\">Invoice Number</label>\r\n                  <select class=\"form-control\">\r\n                    <option>0000 0000 1233 2212</option>\r\n                    <option>0000 4452 1233 2212</option>\r\n                    <option>0000 0000 7512 2212</option>\r\n                    <option>0000 0000 1233 5823</option>\r\n                  </select>\r\n                </div>\r\n                <div class=\"col-lg-4 custom-day\">\r\n                  <label for=\"ccproduct\" class=\"form-control-label\">Product</label>\r\n                  <select class=\"form-control\">\r\n                    <option>Voice</option>\r\n                    <option>SMS</option>\r\n                    <option>MMS</option>\r\n                    <option>Other</option>\r\n                  </select>\r\n                </div>\r\n                <div class=\"col-lg-4\">\r\n                  <label for=\"input-city\" class=\"form-control-label\">Decument ID</label>\r\n                  <select class=\"form-control\">\r\n                    <option> </option>\r\n                    <option> </option>\r\n                    <option> </option>\r\n                    <option> </option>\r\n                  </select>\r\n                </div>\r\n                <div class=\"form-group hidden\">\r\n                  <div class=\"input-group\">\r\n                    <input name=\"datepicker\" maxlength=\"10\" class=\"form-control\" ngbDatepicker\r\n                      #datepicker=\"ngbDatepicker\" [autoClose]=\"'outside'\" (dateSelect)=\"onDateSelection($event)\"\r\n                      [displayMonths]=\"2\" [dayTemplate]=\"t\" outsideDays=\"hidden\" [startDate]=\"fromDate!\">\r\n                    <ng-template #t let-date let-focused=\"focused\">\r\n                      <span class=\"custom-day\" [class.focused]=\"focused\" [class.range]=\"isRange(date)\"\r\n                        [class.faded]=\"isHovered(date) || isInside(date)\" (mouseenter)=\"hoveredDate = date\"\r\n                        (mouseleave)=\"hoveredDate = null\">\r\n                        {{ date.day }}\r\n                      </span>\r\n                    </ng-template>\r\n                  </div>\r\n                </div>\r\n                <div class=\"form-group col-lg-4 custom-day\">\r\n                  <label class=\"form-control-label\" for=\"input-city\">{{\"date from\" | titlecase}}</label>\r\n                  <div class=\"input-group\">\r\n                    <input #dpFromDate class=\"form-control\" maxlength=\"10\" placeholder=\"yyyy-mm-dd\" name=\"dpFromDate\"\r\n                      [value]=\"formatter.format(fromDate)\"\r\n                      (input)=\"fromDate = validateInput(fromDate, dpFromDate.value)\">\r\n                    <div class=\"input-group-append\">\r\n                      <button class=\"btn btn-outline-secondary calendar\" (click)=\"datepicker.toggle()\" type=\"button\">\r\n                        <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                      </button>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"form-group col-lg-4 custom-day\">\r\n                  <label class=\"form-control-label\" for=\"input-city\">{{\"date to\" | titlecase}}\r\n                  </label>\r\n                  <div class=\"input-group\">\r\n                    <input #dpToDate class=\"form-control\" maxlength=\"10\" placeholder=\"yyyy-mm-dd\" name=\"dpToDate\"\r\n                      [value]=\"formatter.format(toDate)\" (input)=\"toDate = validateInput(toDate, dpToDate.value)\" readonly>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"row\">\r\n                  <div class=\"col\">\r\n                    <div class=\"text-center\">\r\n                      <mat-spinner *ngIf=\"showSpinner\"></mat-spinner>\r\n                      <button class=\"btn submit my-4\" (click)=\"displybilling()\">Search</button>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab>\r\n              <ng-template ngbTabTitle><b>Technical </b></ng-template>\r\n              <ng-template ngbTabContent>\r\n                <div [hidden]=CDRsearch>\r\n                  <div class=\"col-lg-4 custom-day\">\r\n                    <label for=\"input-city\" class=\"form-control-label\">Invoice Number</label>\r\n                    <select class=\"form-control\">\r\n                      <option>0000 0000 1233 2212</option>\r\n                      <option>0000 4452 1233 2212</option>\r\n                      <option>0000 0000 7512 2212</option>\r\n                      <option>0000 0000 1233 5823</option>\r\n                    </select>\r\n                  </div>\r\n                  <div class=\"col-lg-4 custom-day\">\r\n                    <label for=\"ccproduct\" class=\"form-control-label\">Product</label>\r\n                    <select class=\"form-control\">\r\n                      <option>Voice</option>\r\n                      <option>SMS</option>\r\n                      <option>MMS</option>\r\n                      <option>Other</option>\r\n                    </select>\r\n                  </div>\r\n                  <div class=\"col-lg-4 custom-day\">\r\n                    <label for=\"input-city\" class=\"form-control-label\">Account Name</label>\r\n                    <select class=\"form-control\">\r\n                      <option> </option>\r\n                      <option> </option>\r\n                      <option> </option>\r\n                      <option> </option>\r\n                    </select>\r\n                  </div>\r\n                  <div class=\"col-lg-4 custom-day\">\r\n                    <label for=\"input-city\" class=\"form-control-label\">Account Code</label>\r\n                    <select class=\"form-control\">\r\n                      <option> </option>\r\n                      <option> </option>\r\n                      <option> </option>\r\n                      <option> </option>\r\n                    </select>\r\n                  </div>\r\n                  <div class=\"col-lg-4 custom-day\">\r\n                    <label for=\"input-city\" class=\"form-control-label\">Decument ID</label>\r\n                    <select class=\"form-control\">\r\n                      <option> </option>\r\n                      <option> </option>\r\n                      <option> </option>\r\n                      <option> </option>\r\n                    </select>\r\n                  </div>\r\n                  <div class=\"form-group hidden\">\r\n                    <div class=\"input-group\">\r\n                      <input name=\"datepicker\" maxlength=\"10\" class=\"form-control\" ngbDatepicker\r\n                        #datepicker=\"ngbDatepicker\" [autoClose]=\"'outside'\" (dateSelect)=\"onDateSelection($event)\"\r\n                        [displayMonths]=\"2\" [dayTemplate]=\"t\" outsideDays=\"hidden\" [startDate]=\"fromDate!\">\r\n                      <ng-template #t let-date let-focused=\"focused\">\r\n                        <span class=\"custom-day\" [class.focused]=\"focused\" [class.range]=\"isRange(date)\"\r\n                          [class.faded]=\"isHovered(date) || isInside(date)\" (mouseenter)=\"hoveredDate = date\"\r\n                          (mouseleave)=\"hoveredDate = null\">\r\n                          {{ date.day }}\r\n                        </span>\r\n                      </ng-template>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group col-lg-4 custom-day\">\r\n                    <label class=\"form-control-label\" for=\"input-city\">{{\"date from\" | titlecase}}</label>\r\n                    <div class=\"input-group\">\r\n                      <input #dpFromDate class=\"form-control\" maxlength=\"10\" placeholder=\"yyyy-mm-dd\" name=\"dpFromDate\"\r\n                        [value]=\"formatter.format(fromDate)\"\r\n                        (input)=\"fromDate = validateInput(fromDate, dpFromDate.value)\">\r\n                      <div class=\"input-group-append\">\r\n                        <button class=\"btn btn-outline-secondary calendar\" (click)=\"datepicker.toggle()\" type=\"button\">\r\n                          <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                        </button>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group col-lg-4 custom-day\">\r\n                    <label class=\"form-control-label\" for=\"input-city\">{{\"date to\" | titlecase}}\r\n                    </label>\r\n                    <div class=\"input-group\">\r\n                      <input #dpToDate class=\"form-control\" maxlength=\"10\" placeholder=\"yyyy-mm-dd\" name=\"dpToDate\"\r\n                        [value]=\"formatter.format(toDate)\" (input)=\"toDate = validateInput(toDate, dpToDate.value)\" readonly>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"row\">\r\n                    <div class=\"col\">\r\n                      <div class=\"text-center\">\r\n                        <mat-spinner *ngIf=\"showSpinner\"></mat-spinner>\r\n                        <button class=\"btn submit my-4\" (click)=\"displyTech()\">Search</button>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </ng-template>\r\n            </ngb-tab>\r\n          </ngb-tabset>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"bg-gradient-danger pt-5\" [hidden]=CDR>\r\n  <div class=\"container-fluid \">\r\n    <div class=\"header-body\">\r\n      <!-- Table -->\r\n      <div class=\"card shadow\">\r\n        <div class=\"card-header border-0\">\r\n          <div class=\"row align-items-center\">\r\n            <div class=\"col\">\r\n              <h3 class=\"mb-0\">CDR Billing Details </h3>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"table-responsive table-res\">\r\n          <!-- Projects table -->\r\n          <table class=\"table align-items-center table-flush\">\r\n            <thead class=\"thead-light\">\r\n              <tr>\r\n                <th scope=\"col\">Invoice Number</th>\r\n                <th scope=\"col\">Decument ID</th>\r\n                <th scope=\"col\">Product</th>\r\n                <th scope=\"col\">Billing Period</th>\r\n                <th scope=\"col\">Billing From</th>\r\n                <th scope=\"col\">Billing To</th>\r\n                <th scope=\"col\">Description</th>\r\n                <th scope=\"col\">CDR Link</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody style=\"font-weight: 600; color: #525f7f;\">\r\n              <td>0000 0000 0000 0000</td>\r\n              <td>45678998765</td>\r\n              <td>Voice</td>\r\n              <td>2020-08-16</td>\r\n              <td>2020-08-16</td>\r\n              <td>01-02-2020</td>\r\n              <td>Voice data</td>\r\n              <td><a href=\"/images/myw3schoolsimage.jpg\" download>Download </a>\r\n              </td>\r\n            </tbody>\r\n          </table>\r\n          <div class=\"row\">\r\n            <div class=\"col\">\r\n              <div class=\"text-center\">\r\n                <mat-spinner *ngIf=\"showSpinner\"></mat-spinner>\r\n                <button class=\"btn submit my-4\" (click)=\"cancel()\">Cancel</button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <mat-paginator [length]=\"100\" [pageSize]=\"10\" [pageSizeOptions]=\"[5, 10, 25, 100]\">\r\n        </mat-paginator>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"bg-gradient-danger pt-5\" [hidden]=CDRTech>\r\n  <div class=\"container-fluid \">\r\n    <div class=\"header-body\">\r\n      <!-- Table -->\r\n      <div class=\"card shadow\">\r\n        <div class=\"card-header border-0\">\r\n          <div class=\"row align-items-center\">\r\n            <div class=\"col\">\r\n              <h3 class=\"mb-0\">CDR Technical Details </h3>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"table-responsive table-res\">\r\n          <!-- Projects table -->\r\n          <table class=\"table align-items-center table-flush\">\r\n            <thead class=\"thead-light\">\r\n              <tr>\r\n                <th scope=\"col\">Invoice Number</th>\r\n                <th scope=\"col\">Decument ID</th>\r\n                <th scope=\"col\">Account Name</th>\r\n                <th scope=\"col\">Account Code</th>\r\n                <th scope=\"col\">Product</th>\r\n                <th scope=\"col\">Uploaded By</th>\r\n                <th scope=\"col\">Billing Period</th>\r\n                <th scope=\"col\">Billing From</th>\r\n                <th scope=\"col\">Billing To</th>\r\n                <th scope=\"col\">Description</th>\r\n                <th scope=\"col\">CDR Link</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody style=\"font-weight: 600; color: #525f7f;\">\r\n              <td>0000 0000 0000 0000</td>\r\n              <td>345678987</td>\r\n              <td>VODAFONE</td>\r\n              <td>ALBVF</td>\r\n              <td>Voice</td>\r\n              <td>SAAD</td>\r\n              <td>2020-08-16</td>\r\n              <td>2020-08-16</td>\r\n              <td>01-02-2020</td>\r\n              <td>Voice data</td>\r\n              <td><a href=\"/images/myw3schoolsimage.jpg\" download>Download </a>\r\n              </td>\r\n            </tbody>\r\n          </table>\r\n          <div class=\"row\">\r\n            <div class=\"col\">\r\n              <div class=\"text-center\">\r\n                <mat-spinner *ngIf=\"showSpinner\"></mat-spinner>\r\n                <button class=\"btn submit my-4\" (click)=\"cancel()\">Cancel</button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <mat-paginator [length]=\"100\" [pageSize]=\"10\" [pageSizeOptions]=\"[5, 10, 25, 100]\">\r\n        </mat-paginator>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/email-confirmation/email-confirmation.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/email-confirmation/email-confirmation.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-5 pt-5 \">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"header-body\">\r\n            <img src=\"./assets/FA_STC_RGB_NEG.ico\" style=\"width: 114px;\">\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-8\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\">\r\n    <div class=\"container mt--8 pb-5\">\r\n        <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-5 col-md-7\">\r\n                <div class=\"card bg-secondary shadow border-0\">\r\n                    <div class=\"card-body px-lg-5 py-lg-5\">\r\n                        <div class=\"text-center text-muted mb-4\">\r\n                            <img src=\"./assets/successful.png\">\r\n                        </div>\r\n                        <h2 class=\"text-center h2 font-weight-bold mb-0\">Registration Successful</h2>\r\n                        <h3 class=\"text-center h2 font-weight-bold mb-0\"> <small>Thank you for registering.\r\n                                Please check your email for a confirmation.\r\n                                Click <u><a href=\"#/login\">here</a> </u> or you will be redirected in <countdown\r\n                                    [config]=\"{ leftTime: 10, format: 's' }\"></countdown> seconds\r\n                            </small></h3>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/email-reset/email-reset.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/email-reset/email-reset.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-5 pt-5 \">\r\n    <div class=\"container-fluid\">\r\n    </div>\r\n</div>\r\n<div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-8\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\">\r\n    <div class=\"container mt--8 pb-5\">\r\n        <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-5 col-md-7\">\r\n                <div class=\"card bg-secondary shadow border-0\">\r\n                    <div class=\"card-body px-lg-5 py-lg-5\">\r\n                        <div class=\"text-center text-muted mb-4\">\r\n                            <img src=\"./assets/successful.png\">\r\n                        </div>\r\n                        <h2 class=\"text-center h2 font-weight-bold mb-0\">Password Reset</h2>\r\n                        <h3 class=\"text-center h2 font-weight-bold mb-0\"> <small>\r\n                                An Email including a temporary password has been sent to ****.\r\n\r\n                                Please check you junk folder in the event you do not receive it\r\n                                Back to <u><a href=\"#/login\">login</a> </u> or you will be redirected in <countdown\r\n                                    [config]=\"{ leftTime: 10, format: 's' }\"></countdown> seconds\r\n                            </small></h3>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/help/help.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/help/help.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<img class=\"center\" src=\"assets/Under-development.png\" width=\"700\" height=\"500\">\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/home/cp-change.component.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/cp-change.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"registerForm\" (ngSubmit)=\"onSubmit()\" (keydown.enter)=\"onSubmit()\" (keypress)=\"trueChart($event)\" autocomplete=\"off\">\r\n  <h2 class=\"text-center h2 font-weight-bold mb-0\" style=\"padding-bottom: 2rem;\"> Change Password</h2>\r\n  <div class=\"form-row\">\r\n    <div class=\"form-group col\">\r\n      <label>Current Password</label>\r\n      <input type=\"password\" formControlName=\"currpassword\" [(ngModel)]=\"current\" onpaste=\"return false;\" class=\"form-control\"\r\n        [ngClass]=\"{'is-invalid': submitted && f.currpassword.errors }\" />\r\n      <div *ngIf=\"submitted && f.currpassword.errors\" class=\"invalid-feedback\">\r\n        <div *ngIf=\"f.currpassword.errors.required\">Current Password is required</div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"form-group col\">\r\n      <label>New Password</label>\r\n      <input type=\"password\" formControlName=\"password\" [(ngModel)]=\"password\" onpaste=\"return false;\" class=\"form-control\"\r\n        [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\" />\r\n      <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\r\n        <div *ngIf=\"f.password.errors.required\">Password is required</div>\r\n        <div *ngIf=\"f.password.errors.minlength\">Password must be at least 8 characters</div>\r\n      </div>\r\n    </div>\r\n    <div class=\"form-group col\">\r\n      <label>Confirm Password</label>\r\n      <input type=\"password\" formControlName=\"confirmPassword\" [(ngModel)]=\"confirm\" onpaste=\"return false;\" class=\"form-control\"\r\n        [ngClass]=\"{ 'is-invalid': submitted && f.confirmPassword.errors }\" />\r\n      <div *ngIf=\"submitted && f.confirmPassword.errors\" class=\"invalid-feedback\">\r\n        <div *ngIf=\"f.confirmPassword.errors.required\">Confirm Password is required</div>\r\n        <div *ngIf=\"f.confirmPassword.errors.mustMatch\">Passwords must match</div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"text-center\">\r\n    <button class=\"btn btn-secondary\">Confirm</button>\r\n    <button class=\"btn btn-secondary\" type=\"reset\" (click)=\"onReset()\">Reset</button>\r\n  </div>\r\n</form>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\" style=\"background-color: #4F008C;\">\r\n  <div class=\"header-body border-line\">\r\n    <div class=\"col \">\r\n      <label class=\"color-white font-home\">Special Offers</label>\r\n    </div>\r\n    <section class=\"main-slider\">\r\n      <carousel [interval]=\"false\" style=\"bottom: -20px;\">\r\n        <div class=\"row\">\r\n\r\n          <slide>\r\n            <div class=\"col-xl-3 col-lg-6 display-line\" data-aos=\"fade-up\">\r\n              <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                <div class=\"input-group mb-3\">\r\n                  <label class=\"form-control color-white\" style=\"background-color: #E6315B;\">Number of in Assets\r\n                  </label>\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text-card\">100</span>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                <div class=\"input-group mb-3\">\r\n                  <label class=\"form-control color-white\" style=\"background-color: #E96734;\">\r\n                    Number of in Progress Order </label>\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text-card \">20</span>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                <div class=\"input-group mb-3\">\r\n                  <label class=\"form-control color-white\" style=\"background-color: #5FC58D;\">\r\n                    Number of Ticket</label>\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text-card \">20</span>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </slide>\r\n          <slide>\r\n            <div class=\"col-xl-3 col-lg-6 display-line\" data-aos=\"fade-up\">\r\n              <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                <div class=\"input-group mb-3\">\r\n                  <label class=\"form-control color-white\" style=\"background-color: #E6315B;\">\r\n                    Number of Ticket</label>\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text-card\">20</span>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                <div class=\"input-group mb-3\">\r\n                  <label class=\"form-control color-white\" style=\"background-color: #E96734;\">\r\n                    Number of Ticket</label>\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text-card\"> 20 </span>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                <div class=\"input-group mb-3\">\r\n                  <label class=\"form-control color-white\" style=\"background-color: #66CED9;\">Number of Ticket</label>\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text-card\"> 20 </span>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </slide>\r\n        </div>\r\n      </carousel>\r\n    </section>\r\n  </div>\r\n</div>\r\n\r\n<section class=\"main-slider\">\r\n  <carousel [interval]=\"false\">\r\n    <slide>\r\n      <img src=\"./assets/img/BC3-P.png\" alt=\"First slide\" class=\"center-fit\" style=\"width: auto;\" height=\"425\">\r\n    </slide>\r\n    <slide >\r\n      <img src=\"https://picsum.photos/id/1026/900/500\" alt=\"Second slide\" class=\"center-fit\" height=\"425\">\r\n    </slide>\r\n    <slide>\r\n      <img src=\"https://picsum.photos/id/1031/900/500\" alt=\"Third slide\" class=\"center-fit\" height=\"425\">\r\n    </slide>\r\n  </carousel>\r\n</section>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/nav/nav.component.html":
/*!******************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/nav/nav.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<style>\r\n\r\n    * {\r\n  \r\n      box-sizing: border-box\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    body {\r\n  \r\n      font-family: Verdana, sans-serif;\r\n  \r\n      margin: 0\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    .mySlides {\r\n  \r\n      display: none\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    img {\r\n  \r\n      width: 500px;\r\n  \r\n      display: block;\r\n  \r\n      margin-left: auto;\r\n  \r\n      margin-right: auto;\r\n  \r\n      background-color: var(--air) !important;\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    /* Position the \"next button\" to the right */\r\n  \r\n    .next {\r\n  \r\n      right: 0;\r\n  \r\n      border-radius: 3px 0 0 3px;\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    /* On hover, add a black background color with a little bit see-through */\r\n  \r\n    .prev:hover,\r\n  \r\n    .next:hover {\r\n  \r\n      background-color: rgba(0, 0, 0, 0.8);\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    /* Caption text */\r\n  \r\n    .text {\r\n  \r\n      color: #f2f2f2;\r\n  \r\n      font-size: 15px;\r\n  \r\n      padding: 8px 12px;\r\n  \r\n      position: absolute;\r\n  \r\n      bottom: 8px;\r\n  \r\n      width: 10%;\r\n  \r\n      text-align: center;\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    /* On smaller screens, decrease text size */\r\n  \r\n    @media only screen and (max-width: 300px) {\r\n  \r\n   \r\n  \r\n      .prev,\r\n  \r\n      .next,\r\n  \r\n      .text {\r\n  \r\n        font-size: 11px\r\n  \r\n      }\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    .dropbtn {\r\n  \r\n      background-color:#FF375D;\r\n  \r\n      color: white;\r\n  \r\n      padding: 16px;\r\n  \r\n      font-size: 16px;\r\n  \r\n      border-radius: 30px;\r\n  \r\n      cursor: pointer;\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    .dropdown {\r\n  \r\n      position: relative;\r\n  \r\n      display: inline-block;\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    .dropdown-content {\r\n  \r\n      display: none;\r\n  \r\n      position: absolute;\r\n  \r\n      background-color: #f9f9f9;\r\n  \r\n      min-width: 160px;\r\n  \r\n      box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\r\n  \r\n      z-index: 1;\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    .dropdown-content a:hover {\r\n  \r\n      background-color: #f1f1f1\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    .dropdown:hover .dropdown-content {\r\n  \r\n      display: block;\r\n  \r\n    }\r\n  \r\n   \r\n  \r\n    .dropdown:hover .dropbtn {\r\n  \r\n      background-color: #FF375D;\r\n  \r\n    }\r\n  \r\n  </style>\r\n  \r\n  \r\n  \r\n  \r\n  <div class=\"navbar\" style=\"font-weight: 600; background-color: #4F008C;\">\r\n  \r\n    <div class=\"text-center text-muted mb-4\">\r\n  \r\n      <img src=\"./assets/FA_STC_RGB_NEG.ico\" style=\"width: 114px;\">\r\n      \r\n  \r\n    </div>\r\n\r\n\r\n\r\n    \r\n  \r\n    <!-- <a href=\"#home\">Home</a>\r\n  \r\n    <a href=\"#news\">News</a> -->\r\n  \r\n    <div class=\"dropdown\">\r\n  \r\n      <button class=\"dropbtn\">Report</button>\r\n  \r\n      <div class=\"dropdown-content\">\r\n  \r\n        <li>\r\n  \r\n          \r\n  \r\n        </li>\r\n  \r\n      </div>\r\n  \r\n    </div>\r\n  \r\n   \r\n\r\n\r\n\r\n    \r\n  \r\n    <div class=\"dropdown\">\r\n  \r\n      <button class=\"dropbtn\">CDR</button>\r\n  \r\n      <div class=\"dropdown-content\">\r\n  \r\n   \r\n  \r\n      </div>\r\n  \r\n    </div>\r\n  \r\n   \r\n  \r\n    <div class=\"dropdown\">\r\n  \r\n      <button class=\"dropbtn\">Log out</button>\r\n  \r\n      <div class=\"dropdown-content\">\r\n  \r\n      </div>\r\n  \r\n   \r\n  \r\n    </div>\r\n  \r\n  </div>\r\n  \r\n  <ngb-carousel>\r\n  \r\n    <ng-template ngbSlide>\r\n  \r\n      <div class=\"picsum-img-wrapper\">\r\n  \r\n        <img\r\n  \r\n          src=\"https://www.stc.com.sa/wps/wcm/connect/arabic/individual/resources/3/5/35d5b96b-99ac-4e1a-ba70-841acda4bed2/640x466_Ar.png\"\r\n  \r\n          alt=\"Angular Carousel 1\">\r\n  \r\n      </div>\r\n  \r\n  \r\n     \r\n  \r\n  \r\n        <h1 class=\"purpletext text  animate coverimage fadeInUp\" data-animation=\"fadeInUp\" data-delay=\"200\" style=\"background-color:#FF375D\">عروضنا تجيك وأنت في بيتك مع تطبيق mystc</h1>\r\n      \r\n     \r\n      <div class=\"carousel-caption\">\r\n  \r\n       \r\n      </div>\r\n  \r\n    </ng-template>\r\n  \r\n    <ng-template ngbSlide>\r\n  \r\n      <div class=\"picsum-img-wrapper\">\r\n  \r\n        <img\r\n  \r\n          src=\"https://www.stc.com.sa/wps/wcm/connect/arabic/individual/resources/4/5/455d19ba-5a8b-426c-b88f-a228239eafef/Corporate-Banners-640x466.png\"\r\n  \r\n          border=\"0\" alt=\"خصم %50 إنترنت لامحدود مع بيتي فايبر 100\" width=\"2000\" height=\"490\"\r\n  \r\n          title=\"خصم %50 إنترنت لامحدود مع بيتي فايبر 100\">\r\n  \r\n   \r\n  \r\n      </div>\r\n  \r\n      <div class=\"carousel-caption\">\r\n  \r\n      <h6 style=\"color: #FF375D;\">WBU PORTAL</h6>\r\n  \r\n     \r\n  \r\n      </div>\r\n  \r\n    </ng-template>\r\n  \r\n  </ngb-carousel>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/new-partner/new-partner.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/new-partner/new-partner.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"navbar-top\">\r\n  <img src=\"/assets/FA_STC_RGB_NEG.ico\" height=\"80px;\">\r\n\r\n  <main class=\"main d-flex align-items-center\" style=\"background-color: #4F008C;\">\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6 mx-auto\">\r\n          <div class=\"card mx-4\">\r\n            <div class=\"card-body p-4\">\r\n              <form [formGroup]=\"partnerForm\" (ngSubmit)=\"newPartner()\" (keydown.enter)=\"newPartner()\"\r\n                autocomplete=\"off\">\r\n                <div class=\"h2\"> {{'NEW_PARTNER.TITLE'| translate}} </div>\r\n                <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                  <span class=\"mb-0 text-sm  font-weight-bold\">{{'NEW_PARTNER.FIRST_NAME'| translate}}</span>\r\n                  <div class=\"input-group mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\">\r\n                        <i class=\"fa fa-user\" aria-hidden=\"true\"></i>\r\n                      </span>\r\n                    </div>\r\n                    <input type=\"text\" maxlength=\"10\" onpaste=\"return false;\" (keypress)=\"trueChart($event)\"\r\n                      class=\"form-control\" placeholder=\"{{'NEW_PARTNER.FIRST_NAME'| translate}}\" formControlName=\"fName\"\r\n                      autocomplete=\"off\" required>\r\n                    <div *ngIf=\"partnerForm.invalid  && isRegister\" style=\"width: 100%;\">\r\n                      <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('fName').errors.required\">First Name Is\r\n                        Required\r\n                      </div>\r\n                      <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('fName').errors.fName\">First Name Is Not Valid\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                  <span class=\"mb-0 text-sm  font-weight-bold\">{{'NEW_PARTNER.LAST_NAME'| translate}}</span>\r\n                  <div class=\"input-group mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\">\r\n                        <i class=\"fa fa-user\" aria-hidden=\"true\"></i>\r\n                      </span>\r\n                    </div>\r\n                    <input type=\"text\" maxlength=\"10\" onpaste=\"return false;\" (keypress)=\"trueChart($event)\"\r\n                      class=\"form-control\" placeholder=\"{{'NEW_PARTNER.LAST_NAME'| translate}}\" formControlName=\"lName\"\r\n                      autocomplete=\"off\" required>\r\n                    <div *ngIf=\"partnerForm.invalid  && isRegister\" style=\"width: 100%;\">\r\n                      <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('lName').errors.required\">Last Name Is Required\r\n                      </div>\r\n                      <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('lName').errors.lName\">Last Name Is Not Valid\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                  <span class=\"mb-0 text-sm  font-weight-bold\">{{'NEW_PARTNER.EMAIL'| translate}}</span>\r\n                  <div class=\"input-group mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\">\r\n                        <i class=\"fa fa-envelope\" aria-hidden=\"true\"></i>\r\n                      </span>\r\n                    </div>\r\n                    <input type=\"email\" maxlength=\"25\" onpaste=\"return false;\" class=\"form-control\"\r\n                      (keypress)=\"trueChartEmail($event)\" formControlName=\"email\"\r\n                      placeholder=\"{{'NEW_PARTNER.EMAIL'| translate}}\" autocomplete=\"off\" required>\r\n                    <div *ngIf=\"partnerForm.invalid  && isRegister\" style=\"width: 100%;\">\r\n                      <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('email').errors.required\">Email Is Required\r\n                      </div>\r\n                      <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('email').errors.email\">Email Is Not Valid</div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                  <span class=\"mb-0 text-sm  font-weight-bold\">{{'NEW_PARTNER.COMPANY'| translate}}</span>\r\n                  <div class=\"input-group mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\">\r\n                        <i class=\"fa fa-building\" aria-hidden=\"true\"></i>\r\n                      </span>\r\n                    </div>\r\n                    <input type=\"text\" maxlength=\"30\" onpaste=\"return false;\" class=\"form-control\"\r\n                      (keypress)=\"trueChart($event)\" formControlName=\"company\"\r\n                      placeholder=\"{{'NEW_PARTNER.COMPANY'| translate}}\" autocomplete=\"off\" autopostback=\"ture\"\r\n                      required>\r\n                    <div *ngIf=\"partnerForm.invalid  && isRegister\" style=\"width: 100%;\">\r\n                      <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('company').errors.required\">Company Name Is\r\n                        Required\r\n                      </div>\r\n                      <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('company').errors.company\">Company Name Is Not\r\n                        Valid</div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                  <span class=\"mb-0 text-sm  font-weight-bold\">{{'NEW_PARTNER.REGION'| translate}}</span>\r\n                  <div class=\"form-group\">\r\n                    <select class=\"form-control\" id=\"exampleFormControlSelect1\" (change)=\"selectRegion()\">\r\n                      <option *ngFor=\"let reg of listRegion \" [value]=\"reg.value\">{{reg.display}}</option>\r\n                    </select>\r\n                  </div>\r\n                </div>\r\n                <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                  <span class=\"mb-0 text-sm  font-weight-bold\">{{'NEW_PARTNER.COUNTRY'| translate}}</span>\r\n                  <div class=\" country-picker\">\r\n                    <div class=\"picker-button\" (click)=\"isCountryOpen = !isCountryOpen\">\r\n                      <i class=\"fa fa-globe\" aria-hidden=\"true\"></i>\r\n                    </div>\r\n                    <div class=\"current\">\r\n                      <img [src]=\"selectedCountry.flag\" alt=\"\">\r\n                      <span>{{selectedCountry.name}}</span>\r\n                    </div>\r\n                    <ul *ngIf=\"isCountryOpen\">\r\n                      <li *ngFor=\"let country of listCountries\" (click)=\"selectCountry(country)\">\r\n                        <img [src]=\"country.flag\" alt=\"\">\r\n                        <span>{{country.name}}</span>\r\n                      </li>\r\n                    </ul>\r\n                  </div>\r\n                </div>\r\n                <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                  <span class=\"mb-0 text-sm  font-weight-bold\">{{'NEW_PARTNER.PHONE'| translate}}</span>\r\n                  <div class=\"input-group mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\">\r\n                        <i class=\"fa fa-phone\" aria-hidden=\"true\"></i>\r\n                        <!-- <i class=\"icon-phone\"></i> -->\r\n                      </span>\r\n                    </div>\r\n                    <input type=\"text\" class=\"form-control\" (keypress)=\"numberOnly($event)\" maxlength=\"15\"\r\n                      placeholder=\"{{'NEW_PARTNER.PHONE'| translate}}\" formControlName=\"phone\" autocomplete=\"off\"\r\n                      onpaste=\"return false;\" required>\r\n                    <div *ngIf=\"partnerForm.invalid  && isRegister\" style=\"width: 100%;\">\r\n                      <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('phone').errors.required\">Phone Is Required\r\n                      </div>\r\n                      <div class=\"invalid-custom\" *ngIf=\"partnerForm.get('phone').errors.fName\">Phone Is Not Valid</div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\" text-center\">\r\n                  <button class=\"btn btn-primary\" type=\"submit\" style=\"background-color: #ff375d;\">Register</button>\r\n                  <button class=\"btn btn-primary\" type=\"button\" style=\"background-color:#ff375d;\"> <a\r\n                      style=\"color: white;\" [routerLink]=\"['/login']\"> Cancel</a></button>\r\n                </div>\r\n              </form>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/product-catalog/product-catalog.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/product-catalog/product-catalog.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pt-5 pt-md-7\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"header-body\">\r\n            <!-- Card stats -->\r\n            <div class=\"row\">\r\n                <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\r\n                    <div class=\"card card-stats mb-4 mb-xl-0\"> <a class=\"color-p\" [routerLink]=\"['/voice']\">\r\n                            <div class=\"card-body\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"col\">\r\n                                        <span class=\"h2 font-weight-bold mb-0\" style=\"font-size: 1.11rem;\">\r\n                                            Voice</span>\r\n                                    </div>\r\n                                    <div class=\"col-auto\">\r\n                                        <img src=\"./assets/voice.png\" height=\"50px\" width=\"50px\">\r\n                                    </div>\r\n                                </div>\r\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n                                </p>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\r\n                    <div class=\"card card-stats mb-4 mb-xl-0\">\r\n                        <a class=\"color-p\" [routerLink]=\"['/data']\">\r\n                            <div class=\"card-body\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"col\">\r\n                                        <span class=\"h2 font-weight-bold mb-0\">Data & IP</span>\r\n                                    </div>\r\n                                    <div class=\"col-auto\">\r\n                                        <img src=\"./assets/data.png\" height=\"50px\" width=\"50px\">\r\n                                    </div>\r\n                                </div>\r\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n                                </p>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\r\n                    <div class=\"card card-stats mb-4 mb-xl-0\">\r\n                        <a class=\"color-p\" [routerLink]=\"['/capacity']\">\r\n                            <div class=\"card-body\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"col\">\r\n                                        <span class=\"h2 font-weight-bold mb-0\">Capacity</span>\r\n                                    </div>\r\n                                    <div class=\"col-auto\">\r\n                                        <img src=\"./assets/capacity.png\" height=\"50px\" width=\"50px\">\r\n                                    </div>\r\n                                </div>\r\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n                                </p>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\r\n                    <div class=\"card card-stats mb-4 mb-xl-0\">\r\n                        <a class=\"color-p\" [routerLink]=\"['/colocation']\">\r\n                            <div class=\"card-body\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"col\">\r\n                                        <span class=\"h2 font-weight-bold mb-0\">Colocation</span>\r\n                                    </div>\r\n                                    <div class=\"col-auto\">\r\n                                        <img src=\"./assets/location.png\" height=\"50px\" width=\"50px\">\r\n                                    </div>\r\n                                </div>\r\n                                <p class=\"mt-3 mb-0 text-muted text-sm\">\r\n                                </p>\r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/product_catalog/capacity-catalog/capacity-catalog.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/product_catalog/capacity-catalog/capacity-catalog.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-5\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"header-body\">\r\n            <h4 class=\"m2 font-weight-bold\">{{regDetails.category}} Category</h4>\r\n            <div class=\"row\">\r\n                <div class=\"col-xl-2 col-lg-6\" data-aos=\"fade-up\">\r\n\r\n                </div>\r\n                <div class=\"col-xl-8 col-lg-6\" data-aos=\"fade-up\">\r\n                    <div class=\"card card-stats mb-4 mb-xl-0\">\r\n                        <img class=\"card-img-top img-fluid\" src=\"../wbuportal_old/WBU_product_img/{{regDetails.name}}.png\"> \r\n                        <div class=\"card-body\">\r\n                            <h4 class=\"card-title font-weight-bold\">{{regDetails.name}} </h4>\r\n                            <mat-chip-list class=\"chip\" cdkDropList cdkDropListOrientation=\"horizontal\"\r\n                                (cdkDropListDropped)=\"drop($event)\">\r\n                                <mat-chip class=\"chip pb-3\" cdkDrag *ngFor=\"let colocation of regDetails.child\">\r\n                                    {{colocation}}\r\n                                </mat-chip>\r\n                            </mat-chip-list>\r\n                            <p class=\"card-text\">\r\n                                {{regDetails.description}}\r\n                            </p>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/product_catalog/capacity/capacity.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/product_catalog/capacity/capacity.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-13 pt-5\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\">\r\n  <div class=\"container-fluid\">\r\n    <p class=\"h2 text-white mb-0\"> Capacity Catalog</p>\r\n    <div class=\"header-body\">\r\n      <!-- Card stats -->\r\n      <div class=\" row align-items-center py-4\">\r\n        <div class=\" col-lg-6 col-7\">\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div *ngFor='let item of voice' class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\r\n          <div class=\"card card-stats mb-4 mb-xl-0\">\r\n            <div class=\"card-body\">\r\n              <div class=\"row\">\r\n                <div class=\"col\">\r\n                  <a class=\"link\" (click)=\"product(item)\">\r\n                    <span class=\"h2 font-weight-bold mb-0\">{{item}}</span>\r\n                  </a>\r\n                </div>\r\n                <div class=\"col-auto\">\r\n                  <div class=\"icon icon-shape bg-info text-white rounded-circle shadow\">\r\n                    <i class=\"fas fa-chart-bar\"></i>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/product_catalog/colocation/colocation.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/product_catalog/colocation/colocation.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-13 pt-5\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\">\r\n  <div class=\"container-fluid\">\r\n    <p class=\"h2 text-white mb-0\"> Colocation Catalog</p>\r\n    <div class=\"header-body\">\r\n      <!-- Card stats -->\r\n      <div class=\" row align-items-center py-4\">\r\n        <div class=\" col-lg-6 col-7\">\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div *ngFor='let item of voice' class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\r\n          <div class=\"card card-stats mb-4 mb-xl-0\">\r\n            <div class=\"card-body\">\r\n              <div class=\"row\">\r\n                <div class=\"col\">\r\n                  <a class=\"link\" (click)=\"product(item)\">\r\n                    <span class=\"h2 font-weight-bold mb-0\">{{item}}</span>\r\n                  </a>\r\n                </div>\r\n                <div class=\"col-auto\">\r\n                  <div class=\"icon icon-shape bg-yellow text-white rounded-circle shadow\">\r\n                    <i class=\"fas fa-chart-bar\"></i>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/product_catalog/data/data.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/product_catalog/data/data.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-13 pt-5\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\">\r\n    <div class=\"container-fluid\">\r\n      <p class=\"h2 text-white mb-0\"> DATA & IP Catalog</p>\r\n      <div class=\"header-body\">\r\n        <!-- Card stats -->\r\n        <div class=\" row align-items-center py-4\">\r\n          <div class=\" col-lg-6 col-7\">\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div *ngFor='let item of voice' class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\r\n            <div class=\"card card-stats mb-4 mb-xl-0\">\r\n              <div class=\"card-body\">\r\n                <div class=\"row\">\r\n                  <div class=\"col\">\r\n                    <a class=\"link\" (click)=\"product(item)\">\r\n                      <span class=\"h2 font-weight-bold mb-0\">{{item}}</span>\r\n                    </a>\r\n                  </div>\r\n                  <div class=\"col-auto\">\r\n                    <div class=\"icon icon-shape bg-orange text-white rounded-circle shadow\">\r\n                      <i class=\"fas fa-chart-bar\"></i>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/product_catalog/voice/voice.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/product_catalog/voice/voice.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-13 pt-5\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\">\r\n  <div class=\"container-fluid\">\r\n    <p class=\"h2 text-white mb-0\"> Voice Catalog</p>\r\n    <div class=\"header-body\">\r\n      <!-- Card stats -->\r\n      <div class=\" row align-items-center py-4\">\r\n        <div class=\" col-lg-6 col-7\">\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div *ngFor='let item of voice' class=\"col-xl-3 col-lg-6\" data-aos=\"fade-up\">\r\n          <div class=\"card card-stats mb-4 mb-xl-0\">\r\n            <div class=\"card-body\">\r\n              <div class=\"row\">\r\n                <div class=\"col\">\r\n                  <a class=\"link\" (click)=\"product(item)\">\r\n                    <span class=\"h2 font-weight-bold mb-0\">{{item}}</span>\r\n                  </a>\r\n                </div>\r\n                <div class=\"col-auto\">\r\n                  <div class=\"icon icon-shape bg-danger text-white rounded-circle shadow\">\r\n                    <i class=\"fas fa-chart-bar\"></i>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/register-user/register-user.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/register-user/register-user.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"contactUs\">\r\n    <div class=\"container\">\r\n        <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-8 col-md-4\">\r\n                <div class=\"card bg-secondary shadow border-0\">\r\n                    <div class=\"card-body px-lg-5 py-lg-5\">\r\n                        <div class=\"btn-group\" role=\"group\" aria-label=\"Button group with nested dropdown\">\r\n                        </div>\r\n                        <form [formGroup]=\"registerForm\" (ngSubmit)=\"newPartner()\" (keydown.enter)=\"newPartner()\"\r\n                            autocomplete=\"off\">\r\n                            <div class=\"h2\"> Register New User </div>\r\n                            <table class=\"example-full-width\" cellspacing=\"0\">\r\n                                <tr>\r\n                                    <td>\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <mat-label>Email</mat-label>\r\n                                            <input matInput type=\"email\" maxlength=\"25\" onpaste=\"return false;\"\r\n                                                (keypress)=\"trueChartEmail($event)\" formControlName=\"email\"\r\n                                                placeholder=\"{{'NEW_PARTNER.EMAIL'| translate}}\" autocomplete=\"off\"\r\n                                                required>\r\n                                            <div *ngIf=\"registerForm.invalid  && isRegister\" style=\"width: 100%;\">\r\n                                                <div class=\"invalid-custom\"\r\n                                                    *ngIf=\"registerForm.get('email').errors.required\">\r\n                                                    Email Is Required\r\n                                                </div>\r\n                                                <div class=\"invalid-custom\"\r\n                                                    *ngIf=\"registerForm.get('email').errors.email\">Email\r\n                                                    Is Not Valid</div>\r\n                                            </div>\r\n                                        </mat-form-field>\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                            <table>\r\n                                <img src=\"assets/sign.png\" class=\"img\" height=\"300\">\r\n                                <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                                    <span\r\n                                        class=\"mb-0 text-sm  font-weight-bold\">{{'NEW_PARTNER.COUNTRY'| translate}}</span>\r\n                                    <div class=\" country-picker\">\r\n                                        <div class=\"picker-button\" (click)=\"isCountryOpen = !isCountryOpen\">\r\n                                            <i class=\"fa fa-globe\" aria-hidden=\"true\"></i>\r\n                                        </div>\r\n                                        <div class=\"current\">\r\n                                            <img [src]=\"selectedCountry.flag\" alt=\"\">\r\n                                            <span>{{selectedCountry.name}}</span>\r\n                                        </div>\r\n                                        <ul *ngIf=\"isCountryOpen\">\r\n                                            <li *ngFor=\"let country of listCountries\" (click)=\"selectCountry(country)\">\r\n                                                <img [src]=\"country.flag\" alt=\"\">\r\n                                                <span>{{country.name}}</span>\r\n                                            </li>\r\n                                        </ul>\r\n                                    </div>\r\n                                </div>\r\n                            </table>\r\n                            <table class=\"example-full-width\" cellspacing=\"0\">\r\n                                <tr>\r\n                                    <td>\r\n                                        <mat-form-field class=\"example-full-width\" [style.width.px]=87>\r\n                                            <mat-label>Country Code</mat-label>\r\n                                            <input matInput type=\"text\" (keypress)=\"numberOnly($event)\"\r\n                                                formControlName=\"code\" autocomplete=\"off\" onpaste=\"return false;\"\r\n                                                required readonly>\r\n                                        </mat-form-field>\r\n                                    </td>\r\n                                    <td>\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <mat-label>Phone Number</mat-label>\r\n                                            <input matInput type=\"text\" (keypress)=\"numberOnly($event)\" maxlength=\"15\"\r\n                                                placeholder=\"{{'NEW_PARTNER.PHONE'| translate}}\" formControlName=\"phone\"\r\n                                                autocomplete=\"off\" onpaste=\"return false;\" required>\r\n                                            <div *ngIf=\"registerForm.invalid  && isRegister\">\r\n                                                <div class=\"invalid-custom\"\r\n                                                    *ngIf=\"registerForm.get('phone').errors.required\">\r\n                                                    Phone Is Required\r\n                                                </div>\r\n                                                <div class=\"invalid-custom\"\r\n                                                    *ngIf=\"registerForm.get('phone').errors.fName\">\r\n                                                    Phone\r\n                                                    Is Not Valid</div>\r\n                                            </div>\r\n                                        </mat-form-field>\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                            <table class=\"example-full-width\" cellspacing=\"0\">\r\n                                <tr>\r\n                                    <td>\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <input matInput type=\"text\" maxlength=\"30\" onpaste=\"return false;\"\r\n                                                (keypress)=\"trueChart($event)\" formControlName=\"company\"\r\n                                                placeholder=\"{{'NEW_PARTNER.COMPANY'| translate}}\" autocomplete=\"off\"\r\n                                                required>\r\n                                            <div *ngIf=\"registerForm.invalid && isRegister\">\r\n                                                <div class=\"invalid-custom\"\r\n                                                    *ngIf=\"registerForm.get('company').errors.required\">\r\n                                                    Company Name is\r\n                                                    Required\r\n                                                </div>\r\n                                                <div class=\"invalid-custom\"\r\n                                                    *ngIf=\"registerForm.get('company').errors.company\">\r\n                                                    Company Name Is Not\r\n                                                    Valid</div>\r\n                                            </div>\r\n                                        </mat-form-field>\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n\r\n                            <table class=\"example-full-width\" cellspacing=\"0\">\r\n                                <tr>\r\n                                    <td>\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <mat-label>First Name</mat-label>\r\n                                            <input matInput type=\"text\" maxlength=\"10\" onpaste=\"return false;\"\r\n                                                (keypress)=\"trueChart($event)\" class=\"form-control\"\r\n                                                placeholder=\"{{'NEW_PARTNER.FIRST_NAME'| translate}}\"\r\n                                                formControlName=\"fName\" autocomplete=\"off\" class=\"oe_inline\" required>\r\n                                            <div *ngIf=\"registerForm.invalid  && isRegister\" style=\"width: 100%;\">\r\n                                                <div class=\"invalid-custom\"\r\n                                                    *ngIf=\"registerForm.get('fName').errors.required\">\r\n                                                    First Name Is Required\r\n                                                </div>\r\n                                                <div class=\"invalid-custom\"\r\n                                                    *ngIf=\"registerForm.get('fName').errors.fName\">First\r\n                                                    Name Is Not Valid\r\n                                                </div>\r\n                                            </div>\r\n                                        </mat-form-field>\r\n                                    </td>\r\n                                    <td>\r\n                                        <mat-form-field class=\"example-full-width\">\r\n                                            <mat-label>Last Name</mat-label>\r\n                                            <input matInput type=\"text\" maxlength=\"10\" onpaste=\"return false;\"\r\n                                                (keypress)=\"trueChart($event)\" class=\"form-control\"\r\n                                                placeholder=\"{{'NEW_PARTNER.LAST_NAME'| translate}}\"\r\n                                                formControlName=\"lName\" autocomplete=\"off\" class=\"oe_inline\" required>\r\n                                            <div *ngIf=\"registerForm.invalid  && isRegister\" style=\"width: 100%;\">\r\n                                                <div class=\"invalid-custom\"\r\n                                                    *ngIf=\"registerForm.get('lName').errors.required\">\r\n                                                    Last Name Is Required\r\n                                                </div>\r\n                                                <div class=\"invalid-custom\"\r\n                                                    *ngIf=\"registerForm.get('lName').errors.lName\">Last\r\n                                                    Name Is Not Valid\r\n                                                </div>\r\n                                            </div>\r\n                                        </mat-form-field>\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n\r\n                            <div class=\" text-center\">\r\n                                <button class=\"btn btn-primary\" type=\"submit\"\r\n                                    style=\"background-color: #ff375d;\">Register</button>\r\n\r\n                                <button class=\"btn btn-primary\" type=\"button\" style=\"background-color:#ff375d;\">\r\n                                    <a style=\"color: white;\" [routerLink]=\"['/login']\"> Cancel</a></button>\r\n                            </div>\r\n                        </form>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/sidebar/sidebar.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/sidebar/sidebar.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-vertical navbar-expand-md navbar-light bg-white\" id=\"sidenav-main\">\r\n    <div class=\"container-fluid\">\r\n      <!-- Toggler -->\r\n      <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed=!isCollapsed\"\r\n         aria-controls=\"sidenav-collapse-main\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <!-- Brand -->\r\n      <a class=\"navbar-brand pt-0\" routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">\r\n        <img src=\"assets/img/FA_STC_RGB_POS.ico\" class=\"navbar-brand-img\" alt=\"...\">\r\n      </a>\r\n      <!-- User -->\r\n      <ul class=\"nav align-items-center d-md-none\">\r\n        <li class=\"nav-item\" ngbDropdown placement=\"bottom-right\">\r\n          <a class=\"nav-link nav-link-icon\" role=\"button\" ngbDropdownToggle>\r\n            <i class=\"ni ni-bell-55\"></i>\r\n          </a>\r\n          <div class=\"dropdown-menu-arrow dropdown-menu-right\" ngbDropdownMenu>\r\n            <a class=\"dropdown-item\" href=\"javascript:void(0)\">Action</a>\r\n            <a class=\"dropdown-item\" href=\"javascript:void(0)\">Another action</a>\r\n            <div class=\"dropdown-divider\"></div>\r\n            <a class=\"dropdown-item\" href=\"javascript:void(0)\">Something else here</a>\r\n          </div>\r\n        </li>\r\n        <li class=\"nav-item\" ngbDropdown placement=\"bottom-right\">\r\n          <a class=\"nav-link\" role=\"button\" ngbDropdownToggle>\r\n            <div class=\"media align-items-center\">\r\n              <span class=\"avatar avatar-sm rounded-circle\">\r\n                <img alt=\"Image placeholder\"  src=\"assets/img/theme/angular.jpg\">\r\n              </span>\r\n            </div>\r\n          </a>\r\n          <div class=\"dropdown-menu-arrow dropdown-menu-right\" ngbDropdownMenu>\r\n            <div class=\" dropdown-header noti-title\">\r\n              <h6 class=\"text-overflow m-0\">Welcome!</h6>\r\n            </div>\r\n            <a routerLinkActive=\"active\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-single-02\"></i>\r\n              <span>My profile</span>\r\n            </a>\r\n            <a routerLinkActive=\"active\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-settings-gear-65\"></i>\r\n              <span>Settings</span>\r\n            </a>\r\n            <a routerLinkActive=\"active\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-calendar-grid-58\"></i>\r\n              <span>Home</span>\r\n            </a>\r\n            <a routerLinkActive=\"active\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-support-16\"></i>\r\n              <span>Time Sheet</span>\r\n            </a>\r\n            <a  class=\"dropdown-item\" *ngIf=\"!isRTL\"  (click)=\"changeLang(arabic)\" >\r\n              <i class=\"ni ni-support-16\"></i>\r\n              <span>العربيه</span>\r\n            </a>\r\n            <a  class=\"dropdown-item\" *ngIf=\"isRTL\"  (click)=\"changeLang(english)\" >\r\n              <i class=\"ni ni-support-16\"></i>\r\n              <span>English</span>\r\n            </a>\r\n            <div class=\"dropdown-divider\"></div>\r\n            <a href=\"#!\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-user-run\"></i>\r\n              <span>Logout</span>\r\n            </a>\r\n          </div>\r\n        </li>\r\n      </ul>\r\n      <!-- Collapse -->\r\n      <div class=\"collapse navbar-collapse\"  [ngbCollapse]=\"isCollapsed\" id=\"sidenav-collapse-main\">\r\n        <!-- Collapse header -->\r\n        <div class=\"navbar-collapse-header d-md-none\">\r\n          <div class=\"row\">\r\n            <div class=\"col-6 collapse-brand\">\r\n              <a  routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">\r\n                <img src=\"assets/img/FA_STC_RGB_POS.ico\" class=\"navbar-brand-img\" alt=\"...\">\r\n  \r\n              </a>\r\n            </div>\r\n            <div class=\"col-6 collapse-close\">\r\n              <button type=\"button\" class=\"navbar-toggler\" (click)=\"isCollapsed=!isCollapsed\">\r\n                <span></span>\r\n                <span></span>\r\n              </button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- Form -->\r\n        <form class=\"mt-4 mb-3 d-md-none\">\r\n          <div class=\"input-group input-group-rounded input-group-merge\">\r\n            <input type=\"search\" class=\"form-control form-control-rounded form-control-prepended\" placeholder=\"Search\" aria-label=\"Search\">\r\n            <div class=\"input-group-prepend\">\r\n              <div class=\"input-group-text\">\r\n                <span class=\"fa fa-search\"></span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </form>\r\n        <!-- Navigation -->\r\n        <ul class=\"navbar-nav\">\r\n            <li *ngFor=\"let menuItem of menuItems\" class=\"{{menuItem.class}} nav-item\">\r\n                <a routerLinkActive=\"active\" [routerLink]=\"[menuItem.path]\" class=\"nav-link\">\r\n                    <i class=\"ni {{menuItem.icon}}\"></i>\r\n                    {{menuItem.title}}\r\n                </a>\r\n            </li>\r\n        </ul>\r\n        <!-- Divider -->\r\n        <hr class=\"my-3\">\r\n        <!-- Heading -->\r\n        <!-- <h6 class=\"navbar-heading text-muted\">Documentation</h6> -->\r\n        <!-- Navigation -->\r\n        <!-- <ul class=\"navbar-nav mb-md-3\">\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link\" >\r\n              <i class=\"ni ni-spaceship\"></i> Getting started\r\n            </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link\" >\r\n              <i class=\"ni ni-palette\"></i> Foundation\r\n            </a>\r\n          </li>\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link\" >\r\n              <i class=\"ni ni-ui-04\"></i> Components\r\n            </a>\r\n          </li>\r\n        </ul> -->\r\n      </div>\r\n    </div>\r\n  </nav>\r\n  "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/upload/upload.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/upload/upload.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pt-5 pt-md-8\">\r\n  <div class=\"container mt--8 pb-5\">\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"col-lg-8 col-md-7\">\r\n        <div class=\"header-body\">\r\n          <ngb-tabset [destroyOnHide]=\"false\">\r\n            <ngb-tab>\r\n              <ng-template ngbTabTitle><b>Billing</b></ng-template>\r\n              <ng-template ngbTabContent>\r\n                <!-- Table -->\r\n                <form [formGroup]=\"billingForm\" (ngSubmit)=\"save()\" autocomplete=\"off\">\r\n                  <div class=\"col-lg-4 custom-day\">\r\n                    <label for=\"input-city\" class=\"form-control-label\">Invoice Number</label>\r\n                    <select class=\"form-control\" formControlName=\"invoice\">\r\n                      <option>0000 0000 1233 2212</option>\r\n                      <option>0000 4452 1233 2212</option>\r\n                      <option>0000 0000 7512 2212</option>\r\n                      <option>0000 0000 1233 5823</option>\r\n                    </select>\r\n                  </div>\r\n                  <div class=\"col-lg-4 custom-day\">\r\n                    <label for=\"ccuser\" class=\"form-control-label\">Billing Period</label>\r\n\r\n                    <select class=\"form-control\" formControlName=\"period\">\r\n                      <option>2019-01</option>\r\n                      <option>2019-02</option>\r\n                      <option>2019-03</option>\r\n                      <option>2019-04</option>\r\n                      <option>2019-05</option>\r\n                      <option>2019-06</option>\r\n                      <option>2019-07</option>\r\n                      <option>2019-08</option>\r\n                      <option>2019-09</option>\r\n                      <option>2019-10</option>\r\n                      <option>2019-11</option>\r\n                      <option>2019-12</option>\r\n                      <option>2020-01</option>\r\n                      <option>2020-02</option>\r\n                      <option>2020-03</option>\r\n                      <option>2020-04</option>\r\n                      <option>2020-05</option>\r\n                      <option>2020-06</option>\r\n                      <option>2020-07</option>\r\n                      <option>2020-08</option>\r\n                      <option>2020-09</option>\r\n                      <option>2020-10</option>\r\n                      <option>2020-11</option>\r\n                      <option>2020-12</option>\r\n                   \r\n                    </select>\r\n                  </div>\r\n                  <div class=\"form-group hidden custom-day\">\r\n                    <div class=\"input-group\">\r\n                      <input name=\"datepicker\"  class=\"form-control\" ngbDatepicker\r\n                        #datepicker=\"ngbDatepicker\" [autoClose]=\"'outside'\" (dateSelect)=\"onDateSelection($event)\"\r\n                        [displayMonths]=\"2\" [dayTemplate]=\"t\" outsideDays=\"hidden\" [startDate]=\"fromDate!\">\r\n                      <ng-template #t let-date let-focused=\"focused\">\r\n                        <span class=\"custom-day\" [class.focused]=\"focused\" [class.range]=\"isRange(date)\"\r\n                          [class.faded]=\"isHovered(date) || isInside(date)\" (mouseenter)=\"hoveredDate = date\"\r\n                          (mouseleave)=\"hoveredDate = null\">\r\n                          {{ date.day }}\r\n                        </span>\r\n                      </ng-template>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group col-lg-4 custom-day\">\r\n                    <label class=\"form-control-label\" for=\"input-city\">{{\"date from\" | titlecase}}</label>\r\n                    <div class=\"input-group\">\r\n                      <input #dpFromDate class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dpFromDate\"\r\n                        [value]=\"formatter.format(fromDate)\"\r\n                        (input)=\"fromDate = validateInput(fromDate, dpFromDate.value)\" >\r\n                      <div class=\"input-group-append\">\r\n                        <button class=\"btn btn-outline-secondary calendar\" (click)=\"datepicker.toggle()\" type=\"button\">\r\n                          <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                        </button>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group col-lg-4 custom-day\">\r\n                    <label class=\"form-control-label\" for=\"input-city\">{{\"date to\" | titlecase}}\r\n                    </label>\r\n                    <div class=\"input-group\">\r\n                      <input #dpToDate class=\"form-control\" placeholder=\"yyyy-mm-dd\" name=\"dpToDate\"\r\n                        [value]=\"formatter.format(toDate)\" (input)=\"toDate = validateInput(toDate, dpToDate.value)\"\r\n                        readonly>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-lg-4\">\r\n                    <label for=\"ccproduct\" class=\"form-control-label\">Product</label>\r\n\r\n                    <select class=\"form-control\" formControlName=\"product\">\r\n                      <option>Voice</option>\r\n                      <option>SMS</option>\r\n                      <option>MMS</option>\r\n                      <option>Other</option>\r\n\r\n                    </select>\r\n                  </div>\r\n\r\n                  <div class=\"col-lg-000\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\">Description</label>\r\n                      <textarea class=\"form-control\" [formControl]=\"textControl\" maxlength=\"500\" rows=\"3\"\r\n                       ></textarea>\r\n                      <span class=\"pull-right char\"><em>{{ descriptionLength | async }} / 500 characters</em></span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-lg-4\">\r\n                    <div class=\"form-group\">\r\n                      <span class=\"hidden-file\">\r\n                        <input type=\"file\" #fileInput ng2FileSelect [uploader]=\"uploader\"\r\n                          (change)=\"onFileSelected($event)\" accept=\".txt, .csv\"\r\n                          style=\"display:none\" />\r\n                      </span>\r\n                      <div class=\"btn-group\" (click)=\"fileInput.click()\">\r\n                        <button type=\"button\" class=\"btn btn-primary\">Add Documents</button>\r\n                      </div>\r\n                      <label>(*txt, *csv)</label>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-md-9 mt-5\">\r\n                    <table class=\"table\">\r\n                      <thead>\r\n                        <tr>\r\n                          <th>Name</th>\r\n                          <th>Size</th>\r\n                          <th>Actions</th>\r\n                        </tr>\r\n                      </thead>\r\n                      <tbody>\r\n                        <tr *ngFor=\"let item of uploader.queue\">\r\n                          <td>{{ item?.file?.name }}</td>\r\n                          <td>{{ item?.file?.size/1024/1024 | number:'.2' }} MB</td>\r\n                          <td>\r\n                            <mat-icon (click)=\"item.remove()\">delete</mat-icon>\r\n                          </td>\r\n                        </tr>\r\n                      </tbody>\r\n                    </table>\r\n                  </div>\r\n                  <div class=\"row\">\r\n                    <div class=\"col\">\r\n                      <div class=\"text-center\">\r\n                        <!-- <mat-spinner *ngIf=\"showSpinner\"></mat-spinner> -->\r\n                        <button class=\"btn submit my-4 \" >Submit</button>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </form>\r\n              </ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab>\r\n              <ng-template ngbTabTitle><b>Technical </b></ng-template>\r\n              <ng-template ngbTabContent>\r\n                <form [formGroup]=\"technicalForm\"  autocomplete=\"off\">\r\n                  <div class=\"col-lg-4 custom-day\">\r\n                    <label for=\"input-city\" class=\"form-control-label\">Invoice Number</label>\r\n                    <select class=\"form-control\" formControlName=\"invoiceTech\">\r\n                      <option>0000 0000 1233 2212</option>\r\n                      <option>0000 4452 1233 2212</option>\r\n                      <option>0000 0000 7512 2212</option>\r\n                      <option>0000 0000 1233 5823</option>\r\n                    </select>\r\n                  </div>\r\n                  <div class=\"col-lg-4 custom-day\">\r\n                    <label for=\"ccuser\" class=\"form-control-label\">Billing Period</label>\r\n\r\n                    <select class=\"form-control\" formControlName=\"periodTech\">\r\n                      <option>2020-01-02</option>\r\n                      <option>2020-02-09</option>\r\n                      <option>2020-03</option>\r\n                      <option>2020-04</option>\r\n                    </select>\r\n                  </div>\r\n                  <div class=\"form-group hidden custom-day\">\r\n                    <div class=\"input-group\">\r\n                      <input name=\"datepicker\"  class=\"form-control\" ngbDatepicker\r\n                        #datepicker=\"ngbDatepicker\" [autoClose]=\"'outside'\" (dateSelect)=\"onDateSelection($event)\"\r\n                        [displayMonths]=\"2\" [dayTemplate]=\"t\" outsideDays=\"hidden\" [startDate]=\"fromDate!\">\r\n                      <ng-template #t let-date let-focused=\"focused\">\r\n                        <span class=\"custom-day\" [class.focused]=\"focused\" [class.range]=\"isRange(date)\"\r\n                          [class.faded]=\"isHovered(date) || isInside(date)\" (mouseenter)=\"hoveredDate = date\"\r\n                          (mouseleave)=\"hoveredDate = null\">\r\n                          {{ date.day }}\r\n                        </span>\r\n                      </ng-template>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group col-lg-4 custom-day\">\r\n                    <label class=\"form-control-label\" for=\"input-city\">{{\"date from\" | titlecase}}</label>\r\n                    <div class=\"input-group\">\r\n                      <input #dpFromDate class=\"form-control\"  placeholder=\"yyyy-mm-dd\" name=\"dpFromDate\"\r\n                        [value]=\"formatter.format(fromDate)\"\r\n                        (input)=\"fromDate = validateInput(fromDate, dpFromDate.value)\" formControlName=\"fromDateTech\">\r\n                      <div class=\"input-group-append\">\r\n                        <button class=\"btn btn-outline-secondary calendar\" (click)=\"datepicker.toggle()\" type=\"button\">\r\n                          <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i>\r\n                        </button>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group col-lg-4 custom-day\">\r\n                    <label class=\"form-control-label\" for=\"input-city\">{{\"date to\" | titlecase}}\r\n                    </label>\r\n                    <div class=\"input-group\">\r\n                      <input #dpToDate class=\"form-control\"  placeholder=\"yyyy-mm-dd\" name=\"dpToDate\"\r\n                        [value]=\"formatter.format(toDate)\" (input)=\"toDate = validateInput(toDate, dpToDate.value)\"\r\n                        formControlName=\"toDateTech\" readonly>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-lg-4 custom-day\">\r\n                    <label for=\"ccproduct\" class=\"form-control-label\">Product</label>\r\n\r\n                    <select class=\"form-control\" formControlName=\"productTech\">\r\n                      <option>Voice</option>\r\n                      <option>SMS</option>\r\n                      <option>MMS</option>\r\n                      <option>Other</option>\r\n\r\n                    </select>\r\n                  </div>\r\n                  <div class=\"col-lg-4 custom-day\">\r\n                    <label for=\"ccuser\" class=\"form-control-label\">Target User</label>\r\n\r\n                    <select  class=\"form-control\"   >\r\n                      <option *ngFor=\"let item of targetU\"[value]=\"item.name\"\r\n                      >{{item.name}}</option>\r\n                   \r\n                    </select>\r\n                  </div>\r\n\r\n                  <div class=\"col-lg-000\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\">Description</label>\r\n                      <textarea class=\"form-control\" [formControl]=\"textTech\" maxlength=\"500\" rows=\"3\"\r\n                        formControlName=\"descTech\"></textarea>\r\n                      <span class=\"pull-right char\"><em>{{ descriptionTech | async }} / 500 characters</em></span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-lg-4\">\r\n                    <div class=\"form-group\">\r\n                      <span class=\"hidden-file\">\r\n                        <input type=\"file\" #fileInput ng2FileSelect [uploader]=\"uploader\"\r\n                          (change)=\"onFileSelected($event)\" formControlName=\"docTech\" accept=\".txt, .csv\"\r\n                          style=\"display:none\" />\r\n                      </span>\r\n                      <div class=\"btn-group\" (click)=\"fileInput.click()\">\r\n                        <button type=\"button\" class=\"btn btn-primary\">Add Documents</button>\r\n                      </div>\r\n                      <label>(*txt, *csv)</label>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-md-9 mt-5\">\r\n                    <table class=\"table\">\r\n                      <thead>\r\n                        <tr>\r\n                          <th>Name</th>\r\n                          <th>Size</th>\r\n                          <th>Actions</th>\r\n                        </tr>\r\n                      </thead>\r\n                      <tbody>\r\n                        <tr *ngFor=\"let item of uploader.queue\">\r\n                          <td>{{ item?.file?.name }}</td>\r\n                          <td>{{ item?.file?.size/1024/1024 | number:'.2' }} MB</td>\r\n                          <td>\r\n                            <mat-icon (click)=\"item.remove()\">delete</mat-icon>\r\n                          </td>\r\n                        </tr>\r\n                      </tbody>\r\n                    </table>\r\n                  </div>\r\n                  <div class=\"row\">\r\n                    <div class=\"col\">\r\n                      <div class=\"text-center\">\r\n                        <mat-spinner *ngIf=\"showSpinner\"></mat-spinner>\r\n                        <button class=\"btn submit my-4\" >Submit</button>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </form>\r\n              </ng-template>\r\n            </ngb-tab>\r\n          </ngb-tabset>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/user-setting/user-setting.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/user-setting/user-setting.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-8 pt-5\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"header-body\">\r\n            <!-- Table -->\r\n            <div class=\"card shadow\">\r\n                <div class=\"border-0\">\r\n                    <div class=\"row align-items-center\">\r\n                        <div class=\"col\">\r\n                            <h2 class=\"mb-0 mb-01\">User Setting</h2>\r\n                        </div>\r\n                        <div>\r\n                            <div class=\"search-hero\">\r\n                                <input class=\"form-control\" type=\"text\" name=\"search\" [(ngModel)]=\"searchText\"\r\n                                    autocomplete=\"off\" placeholder=\"&#61442;  Search...\">\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col\">\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"table-responsive\">\r\n                    <!-- Projects table -->\r\n                    <table class=\"table align-items-center table-flush\">\r\n                        <thead class=\"thead-light\">\r\n                            <tr>\r\n                                <th scope=\"col\">First Name</th>\r\n                                <th scope=\"col\">Last Name</th>\r\n                                <th scope=\"col\">Email</th>\r\n                                <th scope=\"col\">Phone</th>\r\n                                <th scope=\"col\">Company</th>\r\n                                <th scope=\"col\">NAWAQL Roles</th>\r\n                                <th scope=\"col\">Portal Roles</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody style=\"font-weight: 600; color: #525f7f;\">\r\n                            <tr *ngFor=\"let value of avengers | filter:searchText\">\r\n                                <td>{{value.fname}}</td>\r\n                                <td>{{value.lname}}</td>\r\n                                <td>{{value.email}}</td>\r\n                                <td>{{value.phone}}</td>\r\n                                <td>{{value.company}}</td>\r\n                                <td>\r\n                                    <mat-chip-list class=\"chip\" cdkDropList cdkDropListOrientation=\"horizontal\"\r\n                                        (cdkDropListDropped)=\"drop($event)\">\r\n                                        <mat-chip class=\"chip pb-3\" cdkDrag *ngFor=\"let capacity of capacities\">\r\n                                            {{capacity.name}}\r\n                                        </mat-chip>\r\n                                    </mat-chip-list>\r\n                                </td>\r\n                                <td>\r\n                                    <div class=\"form-group mb-3\">\r\n                                        <div class=\"input-group input-group-alternative\">\r\n                                            <mat-form-field class=\"example-chip-list\">\r\n                                                <mat-chip-list #chipList aria-label=\"Role selection\">\r\n                                                    <mat-chip *ngFor=\"let role of roles\" [selectable]=\"selectable\"\r\n                                                        [removable]=\"removable\" (removed)=\"remove(role)\">\r\n                                                        {{role.name}}\r\n                                                        <mat-icon matChipRemove *ngIf=\"removable\">cancel</mat-icon>\r\n                                                    </mat-chip>\r\n                                                    <input [matChipInputFor]=\"chipList\"\r\n                                                        [matChipInputSeparatorKeyCodes]=\"separatorKeysCodes\"\r\n                                                        [matChipInputAddOnBlur]=\"addOnBlur\"\r\n                                                        (matChipInputTokenEnd)=\"add($event)\">\r\n                                                </mat-chip-list>\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                    </div>\r\n                                </td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>\r\n                    <div class=\"text-center\">\r\n                        <button type=\"button\" id=\"myBtn\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"\r\n                          (click)=\"save()\">Save</button>\r\n\r\n                          <button type=\"button\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"><a\r\n                            style=\"color: white;\" [routerLink]=\"['/registration-details']\">Cancel</a></button>\r\n                      </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/email-approved/email-approved.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/email-approved/email-approved.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-5 pt-5 \">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"header-body\">\r\n            <img src=\"./assets/FA_STC_RGB_NEG.ico\" style=\"width: 114px;\">\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-8\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\">\r\n    <div class=\"container mt--8 pb-5\">\r\n        <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-5 col-md-7\">\r\n                <div class=\"card bg-secondary shadow border-0\">\r\n                    <div class=\"card-body px-lg-5 py-lg-5\">\r\n                        <div class=\"text-center text-muted mb-4\">\r\n                            <img src=\"./assets/successful.png\">\r\n                        </div>\r\n                        <h2 class=\"text-center h2 font-weight-bold mb-0\">Email verified</h2>\r\n                        <h3 class=\"text-center h2 font-weight-bold mb-0\"> <small>.\r\n                                Dear , <br>\r\n                               Congrats, Your email has been verified, <br>\r\n                                Now you are able to login to the WBU Portal by \r\n                                <u><a href=\"#/login\">click here</a> </u>\r\n                            </small></h3>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/error/403.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/error/403.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app flex-row align-items-center\">\r\n  <div class=\"container\">\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"col-md-6\">\r\n        <div class=\"clearfix\">\r\n          <h1 class=\"float-left display-3 mr-4 color-white\">403</h1>\r\n          <h1 class=\"pt-3 color-white\">Forbidden.</h1>\r\n          <p class=\"text-muted color-white\">you don't have a premission to access this page/ on this server.</p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/error/404.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/error/404.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app flex-row align-items-center\">\r\n  <div class=\"container\">\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"col-md-6\">\r\n        <div class=\"clearfix\">\r\n          <h1 class=\"float-left display-3 mr-4 color-white\">404</h1>\r\n          <h4 class=\"pt-3 color-white\">Oops! You're lost.</h4>\r\n          <p class=\"text-muted color-white\">The page you are looking for was not found.</p>\r\n        </div>\r\n        <!-- <div class=\"input-prepend input-group\">\r\n          <div class=\"input-group-prepend\">\r\n            <span class=\"input-group-text\"><i class=\"fa fa-search\"></i></span>\r\n          </div>\r\n          <input id=\"prependedInput\" class=\"form-control\" size=\"16\" type=\"text\" placeholder=\"What are you looking for?\">\r\n          <span class=\"input-group-append\">\r\n            <button class=\"btn btn-info\" type=\"button\">Search</button>\r\n          </span>\r\n        </div> -->\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/error/500.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/error/500.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app flex-row align-items-center\">\r\n  <div class=\"container\">\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"col-md-6\">\r\n        <div class=\"clearfix\">\r\n          <h1 class=\"float-left display-3 mr-4\">500</h1>\r\n          <h4 class=\"pt-3\">Houston, we have a problem!</h4>\r\n          <p class=\"text-muted\">The page you are looking for is temporarily unavailable.</p>\r\n        </div>\r\n        <div class=\"input-prepend input-group\">\r\n          <div class=\"input-group-prepend\">\r\n            <span class=\"input-group-text\"><i class=\"fa fa-search\"></i></span>\r\n          </div>\r\n          <input id=\"prependedInput\" class=\"form-control\" size=\"16\" type=\"text\" placeholder=\"What are you looking for?\">\r\n          <span class=\"input-group-append\">\r\n            <button class=\"btn btn-info\" type=\"button\">Search</button>\r\n          </span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/forgot-password/forgot-password.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/forgot-password/forgot-password.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-5 pt-5 \">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"header-body\" style=\"background-color: #4F008C;\">\r\n            <img src=\"./assets/FA_STC_RGB_NEG.ico\" style=\"width: 114px;\">\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-8\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\">\r\n    <div class=\"container mt--8 pb-5\">\r\n        <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-5 col-md-7\">\r\n                <div class=\"card bg-secondary shadow border-0\">\r\n                    <div class=\"card-body px-lg-5 py-lg-5\">\r\n                        <div class=\"text-center text-muted mb-4\">\r\n                            <h3 style=\"color: #4F008C;\"><i class=\"fa fa-lock fa-4x\"></i></h3>\r\n                        </div>\r\n                        <form [formGroup]=\"forgetForm\" (ngSubmit)=\"forgot()\" (keydown.enter)=\"forgot()\" autocomplete=\"off\">\r\n                            <h2 class=\"text-center h2 font-weight-bold mb-0\"> Forgot Password?</h2>\r\n                            <h3 class=\"text-center h2 font-weight-bold mb-0\"> <small>You can reset your password here.\r\n                                </small></h3>\r\n                            <div class=\"form-group mb-3\">\r\n                                <div class=\"input-group input-group-alternative\">\r\n                                    <div class=\"input-group-prepend\">\r\n                                        <span class=\"input-group-text\"><i class=\"ni ni-email-83\"></i></span>\r\n                                    </div>\r\n                                    <input type=\"email\" (keypress)=\"trueChartEmail($event)\" formControlName=\"email\"\r\n                                        maxlength=\"25\" class=\"form-control\" autocomplete=\"off\"\r\n                                        placeholder=\"Email Address\" (focus)=\"focus=true\" onpaste=\"return false;\"\r\n                                        (blur)=\"focus=false\">\r\n                                    <div *ngIf=\"forgetForm.invalid && isRegister\" style=\"width: 100%;\">\r\n                                        <div class=\"invalid-custom\" *ngIf=\"forgetForm.get('email').errors.required\">\r\n                                            Email Is Required\r\n                                        </div>\r\n                                        <div class=\"invalid-custom\" *ngIf=\"forgetForm.get('email').errors.email\">\r\n                                            Email Is Not Valid</div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"text-center\">\r\n                                <button type=\"submit\" id=\"myBtn\" class=\"btn btn-primary\"\r\n                                    style=\"background-color:#ff375d;\">Send</button>\r\n                                <button type=\"button\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"><a\r\n                                        style=\"color: white;\" [routerLink]=\"['/login']\">Cancel</a></button>\r\n                            </div>\r\n                        </form>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/login/login.component.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/login/login.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger py-7 py-lg-8\">\r\n</div>\r\n<!-- Page content -->\r\n<div class=\"container mt--8 pb-5 bg-gradient-danger\">\r\n  <div class=\"row justify-content-center\">\r\n    <div class=\"col-lg-5 col-md-7\">\r\n      <div class=\"card bg-secondary shadow border-0\">\r\n        <div class=\"card-body px-lg-5 py-lg-5\">\r\n          <div class=\"text-center text-muted mb-4\">\r\n            <img src=\"assets/FA_STC_RGB_POS.ico\" width=\" 147px\" />\r\n          </div>\r\n          <form [formGroup]=\"login\" (ngSubmit)=\"onSubmit()\" (keydown.enter)=\"onSubmit()\" autocomplete=\"off\">\r\n            <h3 class=\"h2\" style=\"text-align: center;\">\r\n              {{'LOGIN.TITLE'| translate}}</h3>\r\n            <!-- Sign In to your account</p> -->\r\n            <div class=\"input-group mb-3\">\r\n              <div class=\"input-group-prepend\">\r\n                <span class=\"input-group-text\"><i class=\"fa fa-envelope\"></i></span>\r\n              </div>\r\n              <input type=\"text\" id=\"username\" class=\"form-control\" (keypress)=\"trueChartEmail($event)\"\r\n                formControlName=\"email\" maxlength=\"30\" placeholder=\"Email \" required>\r\n              <div *ngIf=\"login.invalid  && isSubmited\" style=\"width: 100%;\">\r\n                <div class=\"invalid-custom\" *ngIf=\"login.get('email').errors.required\">Email Is Required</div>\r\n                <div class=\"invalid-custom\" *ngIf=\"login.get('email').errors.email\">Email Is Not Valid</div>\r\n              </div>\r\n            </div>\r\n            <div class=\"input-group mb-4\">\r\n              <div class=\"input-group-prepend\">\r\n                <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n              </div>\r\n              <!-- [(ngModel)]=\"password\" -->\r\n              <input type=\"password\" formControlName=\"password\"\r\n                [type]=\"!hide ? 'password' : 'text'\" class=\"form-control\" placeholder=\"Password\" maxlength=\"25\"\r\n                required>\r\n              <span class=\"input-group-text\"><i class=\"material-icons\"\r\n                  (click)=\"hide = !hide\">{{hide ? 'visibility_off' : 'visibility'}}</i></span>\r\n            </div>\r\n            <div class=\"text-center\">\r\n              <button type=\"button\" id=\"myBtn\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"\r\n                [disabled]=\"!login.valid\" (click)=\"onSubmit()\">{{'LOGIN.SIGN_IN'| translate}}</button>\r\n\r\n              <button type=\"button\" class=\"btn btn-primary\" style=\"background-color:#ff375d;\"><a style=\"color: white;\"\r\n                  [routerLink]=\"['/register']\">{{'LOGIN.NEW_USER'| translate}}</a></button>\r\n\r\n              <button type=\"button\" class=\"btn btn-link px-0\"><a style=\"color: #FF375D;\"\r\n                  [routerLink]=\"['/forgot-password']\">{{'LOGIN.RESET_PASSWORD'| translate}}\r\n                </a></button>\r\n            </div>\r\n          </form>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/password-expiry/password-expiry.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/password-expiry/password-expiry.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header bg-gradient-danger pb-5 pt-5 \">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"header-body\">\r\n            <img src=\"./assets/FA_STC_RGB_NEG.ico\" style=\"width: 114px;\">\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class=\"header bg-gradient-danger pb-8 pt-5 pt-md-8\" [ngClass]=\"{'dir':isRTL}\" [dir]=\"isRTL?'rtl':'ltr'\">\r\n    <div class=\"container mt--8 pb-5\">\r\n        <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-5 col-md-7\">\r\n                <div class=\"card bg-secondary shadow border-0\">\r\n                    <div class=\"card-body px-lg-5 py-lg-5\">\r\n                        <div class=\"text-center text-muted mb-4\">\r\n                            <img src=\"./assets/detail.png\">\r\n                        </div>\r\n                        <h2 class=\"text-center h2 font-weight-bold mb-0\">Warning</h2>\r\n                        <h3 class=\"text-center h2 font-weight-bold mb-0\"> <small>.\r\n                                Dear *****, <br>\r\n                                Password for your WBU Portal is expire.\r\n                                Please change your password to avoid any inconvenience\r\n                                <u><a href=\"#/login\">click here to cancel</a> </u>\r\n                            </small></h3>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/register/register.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/register/register.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"navbar-top\">\r\n  <img src=\"assets/FA_STC_RGB_NEG.ico\" height=\"80px;\">\r\n  <main class=\"main d-flex align-items-center\" style=\"background-color: #4F008C;\">\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6 mx-auto\">\r\n          <div class=\"card mx-4\">\r\n            <div class=\"card-body p-4\">\r\n              <form [formGroup]=\"registerForm\" (ngSubmit)=\"onRegister()\" (keydown.enter)=\"onRegister()\"\r\n                autocomplete=\"off\">\r\n                <h2 style=\"font-family: inherit; font-weight: 600; line-height: 1.5; font-size: 1.25rem;\">\r\n                  {{'NEW_PARTNER.TITLE'| translate}}</h2>\r\n                <div class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"> <i class=\"icon-phone\"></i></span>\r\n                  </div>\r\n                  <input type=\"text\" maxlength=\"3\" autocomplete=\"off\" class=\"form-control\" onpaste=\"return false;\"\r\n                    (keypress)=\"trueChart($event)\" placeholder=\"Customer Code\" formControlName=\"code\" required>\r\n                  <div *ngIf=\"registerForm.invalid  && isRegister\" style=\"width: 100%;\">\r\n                    <div class=\"invalid-custom\" *ngIf=\"registerForm.get('code').errors.required\">Code Is Required</div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"> <i class=\"icon-phone\"></i>\r\n                    </span>\r\n                  </div>\r\n                  <input type=\"text\" autocomplete=\"off\" maxlength=\"15\" class=\"form-control\" onpaste=\"return false;\"\r\n                    (keypress)=\"numberOnly($event)\" placeholder=\"Phone number\" formControlName=\"phone\" required>\r\n                  <div *ngIf=\"registerForm.invalid  && isRegister\" style=\"width: 100%;\">\r\n                    <div class=\"invalid-custom\" *ngIf=\"registerForm.get('phone').errors.required\">Phone Is Required\r\n                    </div>\r\n                    <div class=\"invalid-custom\" *ngIf=\"registerForm.get('phone').errors.phone\">Email Is Not Valid</div>\r\n\r\n                  </div>\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\">@</span>\r\n                  </div>\r\n                  <input type=\"email\" class=\"form-control\" autocomplete=\"off\" maxlength=\"25\" onpaste=\"return false;\"\r\n                    (keypress)=\"trueChartEmail($event)\" formControlName=\"Email\" placeholder=\"Email \" required>\r\n                  <div *ngIf=\"registerForm.invalid && isRegister\" style=\"width: 100%;\">\r\n                    <div class=\"invalid-custom\" *ngIf=\"registerForm.get('Email').errors.required\">Email Is Required\r\n                    </div>\r\n                    <div class=\"invalid-custom\" *ngIf=\"registerForm.get('Email').errors.email\">Email Is Not Valid</div>\r\n                  </div>\r\n                </div>\r\n                <div class=\" text-center\">\r\n                  <button class=\"btn btn-primary\" type=\"submit\" style=\"background-color:#ff375d;\" onclick=\" onRegister()\">Register</button>\r\n                  <button class=\"btn btn-primary\" type=\"button\" style=\"background-color:#ff375d;\"> <a\r\n                      style=\"color: white;\" [routerLink]=\"['/login']\"> Cancel</a></button>\r\n                  <button class=\"btn btn-link px-0\" type=\"button\"> <a style=\"color: #FF375D;\"\r\n                      [routerLink]=\"['/partner']\"> {{'LOGIN.NEW_PARTNER'| translate}}</a></button>\r\n                </div>\r\n              </form>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n</div>"

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/Admin-Portal/approve-registration/approve-registration.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/Admin-Portal/approve-registration/approve-registration.component.css ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("p {\r\n  font-family: Lato;\r\n}\r\n\r\ntable.sjs-table > tbody > tr:first-child > td {\r\n     background-color: lightgreen;\r\n}\r\n\r\n.mb-0{\r\n margin-bottom: 0.5rem;\r\n font-weight: 600;\r\n line-height: 1.5;\r\n color: #32325d;\r\n}\r\n\r\n.row{\r\n margin: 0;\r\n font-family: myFirstFont, sans-serif;\r\n font-size: 1rem;\r\n font-weight: 400;\r\n line-height: 1.5;\r\n color: #525f7f;\r\n text-align: left;\r\n}\r\n\r\n.custom-control-label::after {\r\n border-style: groove;\r\n\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL2FwcHJvdmUtcmVnaXN0cmF0aW9uL2FwcHJvdmUtcmVnaXN0cmF0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxpQkFBaUI7QUFDbkI7O0FBRUE7S0FDSyw0QkFBNEI7QUFDakM7O0FBRUE7Q0FDQyxxQkFBcUI7Q0FDckIsZ0JBQWdCO0NBQ2hCLGdCQUFnQjtDQUNoQixjQUFjO0FBQ2Y7O0FBQ0E7Q0FDQyxTQUFTO0NBQ1Qsb0NBQW9DO0NBQ3BDLGVBQWU7Q0FDZixnQkFBZ0I7Q0FDaEIsZ0JBQWdCO0NBQ2hCLGNBQWM7Q0FDZCxnQkFBZ0I7QUFDakI7O0FBRUE7Q0FDQyxvQkFBb0I7O0FBRXJCIiwiZmlsZSI6InNyYy9hcHAvQWRtaW4tUG9ydGFsL2FwcHJvdmUtcmVnaXN0cmF0aW9uL2FwcHJvdmUtcmVnaXN0cmF0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJwIHtcclxuICBmb250LWZhbWlseTogTGF0bztcclxufVxyXG5cclxudGFibGUuc2pzLXRhYmxlID4gdGJvZHkgPiB0cjpmaXJzdC1jaGlsZCA+IHRkIHtcclxuICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyZWVuO1xyXG59XHJcblxyXG4ubWItMHtcclxuIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcclxuIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiBsaW5lLWhlaWdodDogMS41O1xyXG4gY29sb3I6ICMzMjMyNWQ7XHJcbn1cclxuLnJvd3tcclxuIG1hcmdpbjogMDtcclxuIGZvbnQtZmFtaWx5OiBteUZpcnN0Rm9udCwgc2Fucy1zZXJpZjtcclxuIGZvbnQtc2l6ZTogMXJlbTtcclxuIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiBsaW5lLWhlaWdodDogMS41O1xyXG4gY29sb3I6ICM1MjVmN2Y7XHJcbiB0ZXh0LWFsaWduOiBsZWZ0O1xyXG59XHJcblxyXG4uY3VzdG9tLWNvbnRyb2wtbGFiZWw6OmFmdGVyIHtcclxuIGJvcmRlci1zdHlsZTogZ3Jvb3ZlO1xyXG5cclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/Admin-Portal/approve-registration/approve-registration.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/Admin-Portal/approve-registration/approve-registration.component.ts ***!
  \*************************************************************************************/
/*! exports provided: ApproveRegistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApproveRegistrationComponent", function() { return ApproveRegistrationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_approvereg_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/approvereg.service */ "./src/app/services/approvereg.service.ts");




var ApproveRegistrationComponent = /** @class */ (function () {
    function ApproveRegistrationComponent(router, ApproveregServices) {
        this.router = router;
        this.ApproveregServices = ApproveregServices;
        this.rows = new Array();
    }
    ApproveRegistrationComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.rows.push({id:55,firstName:"reema",lastName:"mohsen",company:"IST",country:"SA",email:"email@email.com",phone:"015645",status:"Active"});
        this.ApproveregServices.getReqList().subscribe(function (res) {
            _this.rows = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(res.registrationReq);
        });
    };
    ApproveRegistrationComponent.prototype.detail = function (id) {
        this.router.navigateByUrl("/registration-action/" + id);
    };
    ApproveRegistrationComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _services_approvereg_service__WEBPACK_IMPORTED_MODULE_3__["ApproveregServices"] }
    ]; };
    ApproveRegistrationComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-approve-registration',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./approve-registration.component.html */ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/approve-registration/approve-registration.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./approve-registration.component.css */ "./src/app/Admin-Portal/approve-registration/approve-registration.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _services_approvereg_service__WEBPACK_IMPORTED_MODULE_3__["ApproveregServices"]])
    ], ApproveRegistrationComponent);
    return ApproveRegistrationComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/assets/assets.component.css":
/*!**********************************************************!*\
  !*** ./src/app/Admin-Portal/assets/assets.component.css ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\r\n  padding-top: 50px;\r\n  padding-bottom: 10px;\r\n  color: #405065;\r\n}\r\n\r\n.search-hero {\r\n  max-width: 500px;\r\n  padding-bottom: 21px;\r\n  padding-top: 6%;\r\n  margin:auto;\r\n  padding-right: 11%;\r\n}\r\n\r\n.mb-01{\r\n padding-top: 17px;\r\n padding-bottom: 13px;\r\n}\r\n\r\n.mb-0{\r\n margin-bottom: 0.5rem;\r\n font-weight: 600;\r\n line-height: 1.5;\r\n color: #32325d;\r\n}\r\n\r\n.row{\r\n margin: 0;\r\n font-family: myFirstFont, sans-serif;\r\n font-size: 1rem;\r\n font-weight: 400;\r\n line-height: 1.5;\r\n color: #525f7f;\r\n text-align: left;\r\n}\r\n\r\n.form-control {\r\n  box-shadow: 0 10px 40px 0 #B0C1D9;\r\n}\r\n\r\n.form-control::-moz-placeholder  {\r\n  font-family: FontAwesome;\r\n}\r\n\r\n.form-control:-ms-input-placeholder  {\r\n  font-family: FontAwesome;\r\n}\r\n\r\n.form-control::placeholder  {\r\n  font-family: FontAwesome;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL2Fzc2V0cy9hc3NldHMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGlCQUFpQjtFQUNqQixvQkFBb0I7RUFDcEIsY0FBYztBQUNoQjs7QUFFQTtFQUNFLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLFdBQVc7RUFDWCxrQkFBa0I7QUFDcEI7O0FBR0E7Q0FDQyxpQkFBaUI7Q0FDakIsb0JBQW9CO0FBQ3JCOztBQUNBO0NBQ0MscUJBQXFCO0NBQ3JCLGdCQUFnQjtDQUNoQixnQkFBZ0I7Q0FDaEIsY0FBYztBQUNmOztBQUNBO0NBQ0MsU0FBUztDQUNULG9DQUFvQztDQUNwQyxlQUFlO0NBQ2YsZ0JBQWdCO0NBQ2hCLGdCQUFnQjtDQUNoQixjQUFjO0NBQ2QsZ0JBQWdCO0FBQ2pCOztBQUVBO0VBQ0UsaUNBQWlDO0FBQ25DOztBQUNBO0VBQ0Usd0JBQXdCO0FBQzFCOztBQUZBO0VBQ0Usd0JBQXdCO0FBQzFCOztBQUZBO0VBQ0Usd0JBQXdCO0FBQzFCIiwiZmlsZSI6InNyYy9hcHAvQWRtaW4tUG9ydGFsL2Fzc2V0cy9hc3NldHMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXIge1xyXG4gIHBhZGRpbmctdG9wOiA1MHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG4gIGNvbG9yOiAjNDA1MDY1O1xyXG59XHJcblxyXG4uc2VhcmNoLWhlcm8ge1xyXG4gIG1heC13aWR0aDogNTAwcHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDIxcHg7XHJcbiAgcGFkZGluZy10b3A6IDYlO1xyXG4gIG1hcmdpbjphdXRvO1xyXG4gIHBhZGRpbmctcmlnaHQ6IDExJTtcclxufVxyXG5cclxuXHJcbi5tYi0wMXtcclxuIHBhZGRpbmctdG9wOiAxN3B4O1xyXG4gcGFkZGluZy1ib3R0b206IDEzcHg7XHJcbn1cclxuLm1iLTB7XHJcbiBtYXJnaW4tYm90dG9tOiAwLjVyZW07XHJcbiBmb250LXdlaWdodDogNjAwO1xyXG4gbGluZS1oZWlnaHQ6IDEuNTtcclxuIGNvbG9yOiAjMzIzMjVkO1xyXG59XHJcbi5yb3d7XHJcbiBtYXJnaW46IDA7XHJcbiBmb250LWZhbWlseTogbXlGaXJzdEZvbnQsIHNhbnMtc2VyaWY7XHJcbiBmb250LXNpemU6IDFyZW07XHJcbiBmb250LXdlaWdodDogNDAwO1xyXG4gbGluZS1oZWlnaHQ6IDEuNTtcclxuIGNvbG9yOiAjNTI1ZjdmO1xyXG4gdGV4dC1hbGlnbjogbGVmdDtcclxufVxyXG5cclxuLmZvcm0tY29udHJvbCB7XHJcbiAgYm94LXNoYWRvdzogMCAxMHB4IDQwcHggMCAjQjBDMUQ5O1xyXG59XHJcbi5mb3JtLWNvbnRyb2w6OnBsYWNlaG9sZGVyICB7XHJcbiAgZm9udC1mYW1pbHk6IEZvbnRBd2Vzb21lO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/Admin-Portal/assets/assets.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/Admin-Portal/assets/assets.component.ts ***!
  \*********************************************************/
/*! exports provided: AssetsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssetsComponent", function() { return AssetsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_assetsServices__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../services/assetsServices */ "./src/app/services/assetsServices.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");



var AssetsComponent = /** @class */ (function () {
    function AssetsComponent(assetsServices) {
        this.assetsServices = assetsServices;
        this.asset = new Array();
    }
    AssetsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.assetsServices.getAssets().subscribe(function (res) {
            _this.asset = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(res.asstes);
        });
    };
    AssetsComponent.ctorParameters = function () { return [
        { type: _services_assetsServices__WEBPACK_IMPORTED_MODULE_1__["assetsServices"] }
    ]; };
    AssetsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-assets',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./assets.component.html */ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/assets/assets.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./assets.component.css */ "./src/app/Admin-Portal/assets/assets.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_services_assetsServices__WEBPACK_IMPORTED_MODULE_1__["assetsServices"]])
    ], AssetsComponent);
    return AssetsComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/contact-admin/contact-admin.component.css":
/*!************************************************************************!*\
  !*** ./src/app/Admin-Portal/contact-admin/contact-admin.component.css ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("p {\r\n    font-family: Lato;\r\n  }\r\n  \r\n  table.sjs-table > tbody > tr:first-child > td {\r\n       background-color: lightgreen;\r\n  }\r\n  \r\n  .mb-0{\r\n   margin-bottom: 0.5rem;\r\n   font-weight: 600;\r\n   line-height: 1.5;\r\n   color: #32325d;\r\n  }\r\n  \r\n  .center {\r\n    position: absolute;\r\n    top: 0;\r\n    bottom: 0;\r\n    left: 3;\r\n    right: 0;\r\n    margin: auto;\r\n  }\r\n  \r\n  .row{\r\n   margin: 0;\r\n   font-family: myFirstFont, sans-serif;\r\n   font-size: 1rem;\r\n   font-weight: 400;\r\n   line-height: 1.5;\r\n   color: #525f7f;\r\n   text-align: left;\r\n  }\r\n  \r\n  .custom-control-label::after {\r\n   border-style: groove;\r\n\r\n  }\r\n  \r\n  #outer\r\n{\r\n    width:100%;\r\n    text-align: center;\r\n}\r\n  \r\n  .inner\r\n{\r\n    display: inline-block;\r\n}\r\n  \r\n  .label-text-center{\r\n    display: block;\r\n    padding-top: 20px;\r\n}\r\n  \r\n  .btn{\r\n    background-color: #FF375E;\r\n    color: #fff;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL2NvbnRhY3QtYWRtaW4vY29udGFjdC1hZG1pbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0VBQ25COztFQUVBO09BQ0ssNEJBQTRCO0VBQ2pDOztFQUVBO0dBQ0MscUJBQXFCO0dBQ3JCLGdCQUFnQjtHQUNoQixnQkFBZ0I7R0FDaEIsY0FBYztFQUNmOztFQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLE1BQU07SUFDTixTQUFTO0lBQ1QsT0FBTztJQUNQLFFBQVE7SUFDUixZQUFZO0VBQ2Q7O0VBQ0E7R0FDQyxTQUFTO0dBQ1Qsb0NBQW9DO0dBQ3BDLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZ0JBQWdCO0dBQ2hCLGNBQWM7R0FDZCxnQkFBZ0I7RUFDakI7O0VBRUE7R0FDQyxvQkFBb0I7O0VBRXJCOztFQUVBOztJQUVFLFVBQVU7SUFDVixrQkFBa0I7QUFDdEI7O0VBQ0E7O0lBRUkscUJBQXFCO0FBQ3pCOztFQUVBO0lBQ0ksY0FBYztJQUNkLGlCQUFpQjtBQUNyQjs7RUFFQTtJQUNJLHlCQUF5QjtJQUN6QixXQUFXO0FBQ2YiLCJmaWxlIjoic3JjL2FwcC9BZG1pbi1Qb3J0YWwvY29udGFjdC1hZG1pbi9jb250YWN0LWFkbWluLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJwIHtcclxuICAgIGZvbnQtZmFtaWx5OiBMYXRvO1xyXG4gIH1cclxuICBcclxuICB0YWJsZS5zanMtdGFibGUgPiB0Ym9keSA+IHRyOmZpcnN0LWNoaWxkID4gdGQge1xyXG4gICAgICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmVlbjtcclxuICB9XHJcblxyXG4gIC5tYi0we1xyXG4gICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XHJcbiAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgIGNvbG9yOiAjMzIzMjVkO1xyXG4gIH1cclxuICAuY2VudGVyIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIGxlZnQ6IDM7XHJcbiAgICByaWdodDogMDtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICB9XHJcbiAgLnJvd3tcclxuICAgbWFyZ2luOiAwO1xyXG4gICBmb250LWZhbWlseTogbXlGaXJzdEZvbnQsIHNhbnMtc2VyaWY7XHJcbiAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgY29sb3I6ICM1MjVmN2Y7XHJcbiAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgfVxyXG5cclxuICAuY3VzdG9tLWNvbnRyb2wtbGFiZWw6OmFmdGVyIHtcclxuICAgYm9yZGVyLXN0eWxlOiBncm9vdmU7XHJcblxyXG4gIH1cclxuXHJcbiAgI291dGVyXHJcbntcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLmlubmVyXHJcbntcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG5cclxuLmxhYmVsLXRleHQtY2VudGVye1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBwYWRkaW5nLXRvcDogMjBweDtcclxufVxyXG5cclxuLmJ0bntcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNGRjM3NUU7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/Admin-Portal/contact-admin/contact-admin.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/Admin-Portal/contact-admin/contact-admin.component.ts ***!
  \***********************************************************************/
/*! exports provided: ContactAdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactAdminComponent", function() { return ContactAdminComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


var ContactAdminComponent = /** @class */ (function () {
    function ContactAdminComponent() {
    }
    ContactAdminComponent.prototype.ngOnInit = function () {
    };
    ContactAdminComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-contact-admin',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./contact-admin.component.html */ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/contact-admin/contact-admin.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./contact-admin.component.css */ "./src/app/Admin-Portal/contact-admin/contact-admin.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], ContactAdminComponent);
    return ContactAdminComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/history/history.component.css":
/*!************************************************************!*\
  !*** ./src/app/Admin-Portal/history/history.component.css ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("p {\r\n    font-family: Lato;\r\n  }\r\n  \r\n  table.sjs-table > tbody > tr:first-child > td {\r\n       background-color: lightgreen;\r\n  }\r\n  \r\n  .mb-0{\r\n   margin-bottom: 0.5rem;\r\n   font-weight: 600;\r\n   line-height: 1.5;\r\n   color: #32325d;\r\n  }\r\n  \r\n  .row{\r\n   margin: 0;\r\n   font-family: myFirstFont, sans-serif;\r\n   font-size: 1rem;\r\n   font-weight: 400;\r\n   line-height: 1.5;\r\n   color: #525f7f;\r\n   text-align: left;\r\n  }\r\n  \r\n  .custom-control-label::after {\r\n   border-style: groove;\r\n\r\n  }\r\n  \r\n  .mat-form-field {\r\n   margin-right: 12px;\r\n }\r\n  \r\n  .table-res{\r\n     height: 400px;\r\n     overflow-y: auto;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL2hpc3RvcnkvaGlzdG9yeS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0VBQ25COztFQUVBO09BQ0ssNEJBQTRCO0VBQ2pDOztFQUVBO0dBQ0MscUJBQXFCO0dBQ3JCLGdCQUFnQjtHQUNoQixnQkFBZ0I7R0FDaEIsY0FBYztFQUNmOztFQUNBO0dBQ0MsU0FBUztHQUNULG9DQUFvQztHQUNwQyxlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGdCQUFnQjtHQUNoQixjQUFjO0dBQ2QsZ0JBQWdCO0VBQ2pCOztFQUVBO0dBQ0Msb0JBQW9COztFQUVyQjs7RUFDQTtHQUNDLGtCQUFrQjtDQUNwQjs7RUFDQztLQUNHLGFBQWE7S0FDYixnQkFBZ0I7RUFDbkIiLCJmaWxlIjoic3JjL2FwcC9BZG1pbi1Qb3J0YWwvaGlzdG9yeS9oaXN0b3J5LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJwIHtcclxuICAgIGZvbnQtZmFtaWx5OiBMYXRvO1xyXG4gIH1cclxuICBcclxuICB0YWJsZS5zanMtdGFibGUgPiB0Ym9keSA+IHRyOmZpcnN0LWNoaWxkID4gdGQge1xyXG4gICAgICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmVlbjtcclxuICB9XHJcblxyXG4gIC5tYi0we1xyXG4gICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XHJcbiAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgIGNvbG9yOiAjMzIzMjVkO1xyXG4gIH1cclxuICAucm93e1xyXG4gICBtYXJnaW46IDA7XHJcbiAgIGZvbnQtZmFtaWx5OiBteUZpcnN0Rm9udCwgc2Fucy1zZXJpZjtcclxuICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICBmb250LXdlaWdodDogNDAwO1xyXG4gICBsaW5lLWhlaWdodDogMS41O1xyXG4gICBjb2xvcjogIzUyNWY3ZjtcclxuICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICB9XHJcblxyXG4gIC5jdXN0b20tY29udHJvbC1sYWJlbDo6YWZ0ZXIge1xyXG4gICBib3JkZXItc3R5bGU6IGdyb292ZTtcclxuXHJcbiAgfVxyXG4gIC5tYXQtZm9ybS1maWVsZCB7XHJcbiAgIG1hcmdpbi1yaWdodDogMTJweDtcclxuIH1cclxuICAudGFibGUtcmVze1xyXG4gICAgIGhlaWdodDogNDAwcHg7XHJcbiAgICAgb3ZlcmZsb3cteTogYXV0bztcclxuICB9Il19 */");

/***/ }),

/***/ "./src/app/Admin-Portal/history/history.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/Admin-Portal/history/history.component.ts ***!
  \***********************************************************/
/*! exports provided: HistoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryComponent", function() { return HistoryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_archive_services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/archive.services */ "./src/app/services/archive.services.ts");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/fesm5/table.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/fesm5/paginator.js");





var HistoryComponent = /** @class */ (function () {
    function HistoryComponent(archiveServices) {
        this.archiveServices = archiveServices;
        this.length = 100;
        this.pageSize = 10;
        this.pageSizeOptions = [5, 10, 25, 100];
        this.archive = new Array();
    }
    HistoryComponent.prototype.setPageSizeOptions = function (setPageSizeOptionsInput) {
        if (setPageSizeOptionsInput) {
            this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(function (str) { return +str; });
        }
    };
    HistoryComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.onTableUpdate();
        this.archiveServices.getArchive().subscribe(function (res) {
            _this.archive = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(res.log);
        });
    };
    HistoryComponent.prototype.onTableUpdate = function () {
        this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](this.archive);
        this.dataSource.paginator = this.paginator;
    };
    HistoryComponent.ctorParameters = function () { return [
        { type: _services_archive_services__WEBPACK_IMPORTED_MODULE_2__["archiveServices"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material_paginator__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"], { static: true }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_material_paginator__WEBPACK_IMPORTED_MODULE_4__["MatPaginator"])
    ], HistoryComponent.prototype, "paginator", void 0);
    HistoryComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-history',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./history.component.html */ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/history/history.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./history.component.css */ "./src/app/Admin-Portal/history/history.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_services_archive_services__WEBPACK_IMPORTED_MODULE_2__["archiveServices"]])
    ], HistoryComponent);
    return HistoryComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/new-role/new-role.component.css":
/*!**************************************************************!*\
  !*** ./src/app/Admin-Portal/new-role/new-role.component.css ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".costom-btn {\r\n    margin: auto;\r\n    margin: auto;\r\n    line-height: initial;\r\n    background-color: blueviolet;\r\n    margin-left: 22vh;\r\n    margin-top: 22vh;\r\n}\r\n\r\n     li {\r\n      margin: 20 20px;\r\n    }\r\n\r\n     .btn-c\r\n{\r\n    margin-top: 10vh;\r\n}\r\n\r\n     .h2{\r\n    font-family: inherit;\r\n    font-weight: 600;\r\n    line-height: 1.5;\r\n    font-size: 1.25rem;\r\n    margin-top: -15px;\r\n    padding-bottom: 15px;\r\n    color: white;\r\n}\r\n\r\n     img {\r\n    display: block;\r\n}\r\n\r\n     .thumbnail {\r\n    position: relative;\r\n    display: inline-block;\r\n}\r\n\r\n     .example-chip-list {\r\n    width: 100%;\r\n  }\r\n\r\n     .caption {\r\n    position: -webkit-sticky;\r\n    position: sticky;\r\n    top: 7%;\r\n    left: 67%;\r\n    transform: translate( -50%, -50% );\r\n    text-align: center;\r\n    color: white;\r\n    font-weight: bold;\r\n}\r\n\r\n     .card-body-img{\r\n   background-image: url('stcs.png'); \r\n   background-repeat: no-repeat;\r\n   background-size: 27rem;\r\n   background-position-y: -15pt;\r\n   background-position-x: -51pt;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL25ldy1yb2xlL25ldy1yb2xlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxZQUFZO0lBQ1osWUFBWTtJQUNaLG9CQUFvQjtJQUNwQiw0QkFBNEI7SUFDNUIsaUJBQWlCO0lBQ2pCLGdCQUFnQjtBQUNwQjs7S0FFSztNQUNDLGVBQWU7SUFDakI7O0tBRUo7O0lBRUksZ0JBQWdCO0FBQ3BCOztLQUVBO0lBQ0ksb0JBQW9CO0lBQ3BCLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixvQkFBb0I7SUFDcEIsWUFBWTtBQUNoQjs7S0FFQTtJQUNJLGNBQWM7QUFDbEI7O0tBRUE7SUFDSSxrQkFBa0I7SUFDbEIscUJBQXFCO0FBQ3pCOztLQUNBO0lBQ0ksV0FBVztFQUNiOztLQUNGO0lBQ0ksd0JBQWdCO0lBQWhCLGdCQUFnQjtJQUNoQixPQUFPO0lBQ1AsU0FBUztJQUNULGtDQUFrQztJQUNsQyxrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLGlCQUFpQjtBQUNyQjs7S0FFQTtHQUNHLGlDQUF3QztHQUN4Qyw0QkFBNEI7R0FDNUIsc0JBQXNCO0dBQ3RCLDRCQUE0QjtHQUM1Qiw0QkFBNEI7QUFDL0IiLCJmaWxlIjoic3JjL2FwcC9BZG1pbi1Qb3J0YWwvbmV3LXJvbGUvbmV3LXJvbGUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb3N0b20tYnRuIHtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIGxpbmUtaGVpZ2h0OiBpbml0aWFsO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZXZpb2xldDtcclxuICAgIG1hcmdpbi1sZWZ0OiAyMnZoO1xyXG4gICAgbWFyZ2luLXRvcDogMjJ2aDtcclxufVxyXG5cclxuICAgICBsaSB7XHJcbiAgICAgIG1hcmdpbjogMjAgMjBweDtcclxuICAgIH1cclxuXHJcbi5idG4tY1xyXG57XHJcbiAgICBtYXJnaW4tdG9wOiAxMHZoO1xyXG59XHJcblxyXG4uaDJ7XHJcbiAgICBmb250LWZhbWlseTogaW5oZXJpdDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gICAgZm9udC1zaXplOiAxLjI1cmVtO1xyXG4gICAgbWFyZ2luLXRvcDogLTE1cHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuaW1nIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4udGh1bWJuYWlsIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG4uZXhhbXBsZS1jaGlwLWxpc3Qge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4uY2FwdGlvbiB7XHJcbiAgICBwb3NpdGlvbjogc3RpY2t5O1xyXG4gICAgdG9wOiA3JTtcclxuICAgIGxlZnQ6IDY3JTtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKCAtNTAlLCAtNTAlICk7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG5cclxuLmNhcmQtYm9keS1pbWd7XHJcbiAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImFzc2V0cy9zdGNzLnBuZ1wiKTsgXHJcbiAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgIGJhY2tncm91bmQtc2l6ZTogMjdyZW07XHJcbiAgIGJhY2tncm91bmQtcG9zaXRpb24teTogLTE1cHQ7XHJcbiAgIGJhY2tncm91bmQtcG9zaXRpb24teDogLTUxcHQ7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/Admin-Portal/new-role/new-role.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/Admin-Portal/new-role/new-role.component.ts ***!
  \*************************************************************/
/*! exports provided: NewRoleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewRoleComponent", function() { return NewRoleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/keycodes */ "./node_modules/@angular/cdk/fesm5/keycodes.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/fesm5/autocomplete.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_8__);










var NewRoleComponent = /** @class */ (function () {
    function NewRoleComponent(toast, router) {
        var _this = this;
        this.toast = toast;
        this.router = router;
        this.visible = true;
        this.selectable = true;
        this.removable = true;
        this.separatorKeysCodes = [_angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_5__["ENTER"], _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_5__["COMMA"]];
        this.roleForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]();
        this.roles = [];
        this.allRoles = ['Complaints', 'Contact Us', 'PC', 'Services, Issue New RFP', 'Tracking Delivery Status', 'Active Circuits List'];
        this.newRole = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            title: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        });
        this.filteredRoles = this.roleForm.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (role) { return role ? _this._filter(role) : _this.allRoles.slice(); }));
    }
    NewRoleComponent.prototype.add = function (event) {
        var input = event.input;
        var value = event.value;
        // Add our role
        if ((value || '').trim()) {
            this.roles.push(value.trim());
        }
        // Reset the input value
        if (input) {
            input.value = '';
        }
        this.roleForm.setValue(null);
    };
    NewRoleComponent.prototype.remove = function (role) {
        var index = this.roles.indexOf(role);
        if (index >= 0) {
            this.roles.splice(index, 1);
        }
    };
    NewRoleComponent.prototype.selected = function (event) {
        this.roles.push(event.option.viewValue);
        this.roleInput.nativeElement.value = '';
        this.roleForm.setValue(null);
    };
    NewRoleComponent.prototype._filter = function (value) {
        var filterValue = value.toLowerCase();
        return this.allRoles.filter(function (role) { return role.toLowerCase().indexOf(filterValue) === 0; });
    };
    NewRoleComponent.prototype.ngOnInit = function () {
    };
    NewRoleComponent.prototype.trueChart = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)
            && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
    };
    NewRoleComponent.prototype.noText = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 1) {
            return false;
        }
        return true;
    };
    NewRoleComponent.prototype.save = function () {
        var _this = this;
        if (this.newRole.valid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                allowOutsideClick: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.resumeTimer);
                }
            });
            sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Save'
            }).then(function (result) {
                if (result.value) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Saved!', 'Your Changes has been made.', 'success');
                    _this.router.navigate(['role-access']);
                }
            });
        }
        if (this.newRole.invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Invalid Role Title'
            });
        }
    };
    NewRoleComponent.ctorParameters = function () { return [
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('roleInput'),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewRoleComponent.prototype, "roleInput", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('auto'),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_6__["MatAutocomplete"])
    ], NewRoleComponent.prototype, "matAutocomplete", void 0);
    NewRoleComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-new-role',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./new-role.component.html */ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/new-role/new-role.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./new-role.component.css */ "./src/app/Admin-Portal/new-role/new-role.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], NewRoleComponent);
    return NewRoleComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/orders/orders.component.css":
/*!**********************************************************!*\
  !*** ./src/app/Admin-Portal/orders/orders.component.css ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".search-hero {\r\n  max-width: 500px;\r\n  padding-bottom: 21px;\r\n  padding-top: 6%;\r\n  margin:auto;\r\n  padding-right: 11%;\r\n}\r\n\r\n.mb-01{\r\n padding-top: 17px;\r\n padding-bottom: 13px;\r\n}\r\n\r\n.mb-0{\r\n margin-bottom: 0.5rem;\r\n font-weight: 600;\r\n line-height: 1.5;\r\n color: #32325d;\r\n}\r\n\r\n.row{\r\n margin: 0;\r\n font-family: myFirstFont, sans-serif;\r\n font-size: 1rem;\r\n font-weight: 400;\r\n line-height: 1.5;\r\n color: #525f7f;\r\n text-align: left;\r\n}\r\n\r\n.form-control {\r\n  box-shadow: 0 10px 40px 0 #B0C1D9;\r\n}\r\n\r\n.form-control::-moz-placeholder  {\r\n  font-family: FontAwesome;\r\n}\r\n\r\n.form-control:-ms-input-placeholder  {\r\n  font-family: FontAwesome;\r\n}\r\n\r\n.form-control::placeholder  {\r\n  font-family: FontAwesome;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL29yZGVycy9vcmRlcnMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLFdBQVc7RUFDWCxrQkFBa0I7QUFDcEI7O0FBRUE7Q0FDQyxpQkFBaUI7Q0FDakIsb0JBQW9CO0FBQ3JCOztBQUNBO0NBQ0MscUJBQXFCO0NBQ3JCLGdCQUFnQjtDQUNoQixnQkFBZ0I7Q0FDaEIsY0FBYztBQUNmOztBQUNBO0NBQ0MsU0FBUztDQUNULG9DQUFvQztDQUNwQyxlQUFlO0NBQ2YsZ0JBQWdCO0NBQ2hCLGdCQUFnQjtDQUNoQixjQUFjO0NBQ2QsZ0JBQWdCO0FBQ2pCOztBQUVBO0VBQ0UsaUNBQWlDO0FBQ25DOztBQUNBO0VBQ0Usd0JBQXdCO0FBQzFCOztBQUZBO0VBQ0Usd0JBQXdCO0FBQzFCOztBQUZBO0VBQ0Usd0JBQXdCO0FBQzFCIiwiZmlsZSI6InNyYy9hcHAvQWRtaW4tUG9ydGFsL29yZGVycy9vcmRlcnMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zZWFyY2gtaGVybyB7XHJcbiAgbWF4LXdpZHRoOiA1MDBweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMjFweDtcclxuICBwYWRkaW5nLXRvcDogNiU7XHJcbiAgbWFyZ2luOmF1dG87XHJcbiAgcGFkZGluZy1yaWdodDogMTElO1xyXG59XHJcblxyXG4ubWItMDF7XHJcbiBwYWRkaW5nLXRvcDogMTdweDtcclxuIHBhZGRpbmctYm90dG9tOiAxM3B4O1xyXG59XHJcbi5tYi0we1xyXG4gbWFyZ2luLWJvdHRvbTogMC41cmVtO1xyXG4gZm9udC13ZWlnaHQ6IDYwMDtcclxuIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiBjb2xvcjogIzMyMzI1ZDtcclxufVxyXG4ucm93e1xyXG4gbWFyZ2luOiAwO1xyXG4gZm9udC1mYW1pbHk6IG15Rmlyc3RGb250LCBzYW5zLXNlcmlmO1xyXG4gZm9udC1zaXplOiAxcmVtO1xyXG4gZm9udC13ZWlnaHQ6IDQwMDtcclxuIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiBjb2xvcjogIzUyNWY3ZjtcclxuIHRleHQtYWxpZ246IGxlZnQ7XHJcbn1cclxuXHJcbi5mb3JtLWNvbnRyb2wge1xyXG4gIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IDAgI0IwQzFEOTtcclxufVxyXG4uZm9ybS1jb250cm9sOjpwbGFjZWhvbGRlciAge1xyXG4gIGZvbnQtZmFtaWx5OiBGb250QXdlc29tZTtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/Admin-Portal/orders/orders.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/Admin-Portal/orders/orders.component.ts ***!
  \*********************************************************/
/*! exports provided: OrdersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersComponent", function() { return OrdersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_orderServices__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/orderServices */ "./src/app/services/orderServices.ts");



var OrdersComponent = /** @class */ (function () {
    function OrdersComponent(orderServices) {
        this.orderServices = orderServices;
        this.ord = new Array();
    }
    OrdersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.orderServices.getOrders().subscribe(function (res) {
            _this.ord = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(res.orders);
        });
    };
    OrdersComponent.ctorParameters = function () { return [
        { type: _services_orderServices__WEBPACK_IMPORTED_MODULE_2__["orderServices"] }
    ]; };
    OrdersComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-orders',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./orders.component.html */ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/orders/orders.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./orders.component.css */ "./src/app/Admin-Portal/orders/orders.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_services_orderServices__WEBPACK_IMPORTED_MODULE_2__["orderServices"]])
    ], OrdersComponent);
    return OrdersComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/portal-admin/portal-admin.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/Admin-Portal/portal-admin/portal-admin.component.css ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".col {\r\n    max-width: 100%;\r\n    flex-basis: 0;\r\n    flex-grow: 1;\r\n    padding-top: 19px;\r\n}\r\n\r\n.color-p{\r\n    color: #525F7F\r\n}\r\n\r\n.col-xl-3 {\r\npadding-bottom: 17px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL3BvcnRhbC1hZG1pbi9wb3J0YWwtYWRtaW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGVBQWU7SUFDZixhQUFhO0lBQ2IsWUFBWTtJQUNaLGlCQUFpQjtBQUNyQjs7QUFFQTtJQUNJO0FBQ0o7O0FBQ0E7QUFDQSxvQkFBb0I7QUFDcEIiLCJmaWxlIjoic3JjL2FwcC9BZG1pbi1Qb3J0YWwvcG9ydGFsLWFkbWluL3BvcnRhbC1hZG1pbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbCB7XHJcbiAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICBmbGV4LWJhc2lzOiAwO1xyXG4gICAgZmxleC1ncm93OiAxO1xyXG4gICAgcGFkZGluZy10b3A6IDE5cHg7XHJcbn1cclxuXHJcbi5jb2xvci1we1xyXG4gICAgY29sb3I6ICM1MjVGN0ZcclxufVxyXG4uY29sLXhsLTMge1xyXG5wYWRkaW5nLWJvdHRvbTogMTdweDtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/Admin-Portal/portal-admin/portal-admin.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/Admin-Portal/portal-admin/portal-admin.component.ts ***!
  \*********************************************************************/
/*! exports provided: PortalAdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PortalAdminComponent", function() { return PortalAdminComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


var PortalAdminComponent = /** @class */ (function () {
    function PortalAdminComponent() {
    }
    PortalAdminComponent.prototype.ngOnInit = function () {
    };
    PortalAdminComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-portal-admin',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./portal-admin.component.html */ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/portal-admin/portal-admin.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./portal-admin.component.css */ "./src/app/Admin-Portal/portal-admin/portal-admin.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], PortalAdminComponent);
    return PortalAdminComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/registration-action/registration-action.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/Admin-Portal/registration-action/registration-action.component.css ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("p {\r\n    font-family: Lato;\r\n  }\r\n  \r\n  table.sjs-table > tbody > tr:first-child > td {\r\n       background-color: lightgreen;\r\n  }\r\n  \r\n  .mb-0{\r\n   margin-bottom: 0.5rem;\r\n   font-weight: 600;\r\n   line-height: 1.5;\r\n   color: #32325d;\r\n  }\r\n  \r\n  .row{\r\n   margin: 0;\r\n   font-family: myFirstFont, sans-serif;\r\n   font-size: 1rem;\r\n   font-weight: 400;\r\n   line-height: 1.5;\r\n   color: #525f7f;\r\n   text-align: left;\r\n  }\r\n  \r\n  .custom-control-label::after {\r\n   border-style: groove;\r\n\r\n  }\r\n  \r\n  .mat-form-field {\r\n   margin-right: 12px;\r\n }\r\n  \r\n  .table-res{\r\n     height: 400px;\r\n     overflow-y: auto;\r\n  }\r\n  \r\n  .example-box.cdk-drag-animating {\r\n    transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\r\n  }\r\n  \r\n  .example-chip .cdk-drop-list-dragging {\r\n    transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\r\n  }\r\n  \r\n  .chip{\r\n    padding: 16px 12px;\r\n    pointer-events: none;\r\n  }\r\n  \r\n  .non-clickable{\r\n    pointer-events: none;\r\n  }\r\n  \r\n  a {\r\n    text-decoration: none;\r\n    display: inline-block;\r\n    padding: 8px 16px;\r\n  }\r\n  \r\n  a:hover {\r\n    background-color: #ddd;\r\n    color: black;\r\n  }\r\n  \r\n  .next {\r\n    background-color: #4CAF50;\r\n    color: white;\r\n  }\r\n  \r\n  .round {\r\n  border-radius: 50%;\r\n}\r\n  \r\n  .custom-day {\r\n  display: inline-block;\r\n}\r\n  \r\n  .example-chip-list1 {\r\n  width: 100%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL3JlZ2lzdHJhdGlvbi1hY3Rpb24vcmVnaXN0cmF0aW9uLWFjdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0VBQ25COztFQUVBO09BQ0ssNEJBQTRCO0VBQ2pDOztFQUVBO0dBQ0MscUJBQXFCO0dBQ3JCLGdCQUFnQjtHQUNoQixnQkFBZ0I7R0FDaEIsY0FBYztFQUNmOztFQUNBO0dBQ0MsU0FBUztHQUNULG9DQUFvQztHQUNwQyxlQUFlO0dBQ2YsZ0JBQWdCO0dBQ2hCLGdCQUFnQjtHQUNoQixjQUFjO0dBQ2QsZ0JBQWdCO0VBQ2pCOztFQUVBO0dBQ0Msb0JBQW9COztFQUVyQjs7RUFDQTtHQUNDLGtCQUFrQjtDQUNwQjs7RUFDQztLQUNHLGFBQWE7S0FDYixnQkFBZ0I7RUFDbkI7O0VBQ0E7SUFDRSxzREFBc0Q7RUFDeEQ7O0VBRUE7SUFDRSxzREFBc0Q7RUFDeEQ7O0VBQ0E7SUFDRSxrQkFBa0I7SUFDbEIsb0JBQW9CO0VBQ3RCOztFQUVBO0lBQ0Usb0JBQW9CO0VBQ3RCOztFQUVBO0lBQ0UscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixpQkFBaUI7RUFDbkI7O0VBRUE7SUFDRSxzQkFBc0I7SUFDdEIsWUFBWTtFQUNkOztFQUVBO0lBQ0UseUJBQXlCO0lBQ3pCLFlBQVk7RUFDZDs7RUFFRjtFQUNFLGtCQUFrQjtBQUNwQjs7RUFDQTtFQUNFLHFCQUFxQjtBQUN2Qjs7RUFDQTtFQUNFLFdBQVc7QUFDYiIsImZpbGUiOiJzcmMvYXBwL0FkbWluLVBvcnRhbC9yZWdpc3RyYXRpb24tYWN0aW9uL3JlZ2lzdHJhdGlvbi1hY3Rpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInAge1xyXG4gICAgZm9udC1mYW1pbHk6IExhdG87XHJcbiAgfVxyXG4gIFxyXG4gIHRhYmxlLnNqcy10YWJsZSA+IHRib2R5ID4gdHI6Zmlyc3QtY2hpbGQgPiB0ZCB7XHJcbiAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyZWVuO1xyXG4gIH1cclxuXHJcbiAgLm1iLTB7XHJcbiAgIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcclxuICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgY29sb3I6ICMzMjMyNWQ7XHJcbiAgfVxyXG4gIC5yb3d7XHJcbiAgIG1hcmdpbjogMDtcclxuICAgZm9udC1mYW1pbHk6IG15Rmlyc3RGb250LCBzYW5zLXNlcmlmO1xyXG4gICBmb250LXNpemU6IDFyZW07XHJcbiAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgIGNvbG9yOiAjNTI1ZjdmO1xyXG4gICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIH1cclxuXHJcbiAgLmN1c3RvbS1jb250cm9sLWxhYmVsOjphZnRlciB7XHJcbiAgIGJvcmRlci1zdHlsZTogZ3Jvb3ZlO1xyXG5cclxuICB9XHJcbiAgLm1hdC1mb3JtLWZpZWxkIHtcclxuICAgbWFyZ2luLXJpZ2h0OiAxMnB4O1xyXG4gfVxyXG4gIC50YWJsZS1yZXN7XHJcbiAgICAgaGVpZ2h0OiA0MDBweDtcclxuICAgICBvdmVyZmxvdy15OiBhdXRvO1xyXG4gIH1cclxuICAuZXhhbXBsZS1ib3guY2RrLWRyYWctYW5pbWF0aW5nIHtcclxuICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSAyNTBtcyBjdWJpYy1iZXppZXIoMCwgMCwgMC4yLCAxKTtcclxuICB9XHJcbiAgXHJcbiAgLmV4YW1wbGUtY2hpcCAuY2RrLWRyb3AtbGlzdC1kcmFnZ2luZyB7XHJcbiAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMjUwbXMgY3ViaWMtYmV6aWVyKDAsIDAsIDAuMiwgMSk7XHJcbiAgfVxyXG4gIC5jaGlwe1xyXG4gICAgcGFkZGluZzogMTZweCAxMnB4O1xyXG4gICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XHJcbiAgfVxyXG5cclxuICAubm9uLWNsaWNrYWJsZXtcclxuICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xyXG4gIH1cclxuXHJcbiAgYSB7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBwYWRkaW5nOiA4cHggMTZweDtcclxuICB9XHJcbiAgXHJcbiAgYTpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGRkO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gIH1cclxuICBcclxuICAubmV4dCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNENBRjUwO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gIH1cclxuICBcclxuLnJvdW5kIHtcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbn1cclxuLmN1c3RvbS1kYXkge1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG4uZXhhbXBsZS1jaGlwLWxpc3QxIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/Admin-Portal/registration-action/registration-action.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/Admin-Portal/registration-action/registration-action.component.ts ***!
  \***********************************************************************************/
/*! exports provided: RegistrationActionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationActionComponent", function() { return RegistrationActionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/fesm5/autocomplete.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/cdk/keycodes */ "./node_modules/@angular/cdk/fesm5/keycodes.js");
/* harmony import */ var _dataModels_registeredMembersDto_model__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../dataModels/registeredMembersDto.model */ "./src/app/dataModels/registeredMembersDto.model.ts");
/* harmony import */ var _services_approvereg_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../services/approvereg.service */ "./src/app/services/approvereg.service.ts");
/* harmony import */ var _services_decision_services__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../services/decision.services */ "./src/app/services/decision.services.ts");
/* harmony import */ var _dataModels_decisionDto_mode__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../dataModels/decisionDto.mode */ "./src/app/dataModels/decisionDto.mode.ts");














var RegistrationActionComponent = /** @class */ (function () {
    function RegistrationActionComponent(router, _route, _approverReg, DecisionServices) {
        var _this = this;
        this.router = router;
        this._route = _route;
        this._approverReg = _approverReg;
        this.DecisionServices = DecisionServices;
        this.roleForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]();
        this.selectable = true;
        this.separatorKeysCodes = [_angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_8__["ENTER"], _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_8__["COMMA"]];
        this.removable = true;
        this.capacities = [
            { name: 'C042' },
            { name: 'MENA Gateway Service ' },
            { name: 'C046' },
            { name: 'C041' },
            { name: 'C043' },
        ];
        this.roles = [];
        this.allRoles = ['General billing ', 'Billing read only ', 'General commercial', 'Commercial read only', 'Technical read only', 'General technical', 'Guest', 'admin']; // decisionDto = new decisionDto();
        this.role = new Array();
        this.approve_text = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('');
        this.descriptionLength = new rxjs__WEBPACK_IMPORTED_MODULE_5__["BehaviorSubject"](0);
        this.regDetails = new _dataModels_registeredMembersDto_model__WEBPACK_IMPORTED_MODULE_9__["RegisteredMembersDto"]();
        this.regDetails.id = +this._route.snapshot.paramMap.get("id");
        this.approve_text.valueChanges.subscribe(function (v) { return _this.descriptionLength.next(v.length); });
        this.filteredRoles = this.roleForm.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (role) { return role ? _this._filter(role) : _this.allRoles.slice(); }));
    }
    RegistrationActionComponent.prototype.add = function (event) {
        var input = event.input;
        var value = event.value;
        // Add our role
        if ((value || '').trim()) {
            this.roles.push(value.trim());
        }
        // Reset the input value
        if (input) {
            input.value = '';
        }
        this.roleForm.setValue(null);
    };
    RegistrationActionComponent.prototype.remove = function (role) {
        var index = this.roles.indexOf(role);
        if (index >= 0) {
            this.roles.splice(index, 1);
        }
    };
    RegistrationActionComponent.prototype.selected = function (event) {
        this.roles.push(event.option.viewValue);
        this.roleInput.nativeElement.value = '';
        this.roleForm.setValue(null);
    };
    RegistrationActionComponent.prototype._filter = function (value) {
        var filterValue = value.toLowerCase();
        return this.allRoles.filter(function (role) { return role.toLowerCase().indexOf(filterValue) === 0; });
    };
    RegistrationActionComponent.prototype.noText = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 1) {
            return false;
        }
        return true;
    };
    RegistrationActionComponent.prototype.ngOnInit = function () {
        // this.regDetails={id:55,firstName:"reema",lastName:"Afnan",company:"IST",country:"SA",email:"email@email.com",phone:"015645",status:"Active"};
        var _this = this;
        this._approverReg.getReqDetails(this.regDetails.id).subscribe(function (res) {
            _this.regDetails.company = res.company;
            _this.regDetails.country = res.country;
            _this.regDetails.email = res.email;
            _this.regDetails.firstName = res.firstName;
            _this.regDetails.lastName = res.lastName;
            _this.regDetails.phone = res.phone;
            _this.regDetails.status = res.status;
        });
    };
    RegistrationActionComponent.prototype.reject = function () {
        var _this = this;
        var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: function (toast) {
                toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
            }
        });
        sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire({
            title: 'Reject',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.value) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire('Saved!', 'Your Changes has been made.', 'success');
                _this.router.navigateByUrl("/approve-registration");
            }
        });
    };
    RegistrationActionComponent.prototype.approve = function () {
        var _this = this;
        var decObj = new _dataModels_decisionDto_mode__WEBPACK_IMPORTED_MODULE_12__["decisionDto"]();
        decObj.decision = true;
        decObj.remark = this.regDetails.remark;
        decObj.reqId = this.regDetails.id;
        decObj.roles = this.roles;
        var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: function (toast) {
                toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
            }
        });
        sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Approve'
        }).then(function (result) {
            if (result.value) {
                _this.DecisionServices.getDecision(decObj).subscribe(function (res) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire('Saved!', 'Your Changes has been made.', 'success');
                });
            }
        });
    };
    RegistrationActionComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
        { type: _services_approvereg_service__WEBPACK_IMPORTED_MODULE_10__["ApproveregServices"] },
        { type: _services_decision_services__WEBPACK_IMPORTED_MODULE_11__["DecisionServices"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('roleInput'),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], RegistrationActionComponent.prototype, "roleInput", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('auto'),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_3__["MatAutocomplete"])
    ], RegistrationActionComponent.prototype, "matAutocomplete", void 0);
    RegistrationActionComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-registration-action',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./registration-action.component.html */ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/registration-action/registration-action.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./registration-action.component.css */ "./src/app/Admin-Portal/registration-action/registration-action.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"], _services_approvereg_service__WEBPACK_IMPORTED_MODULE_10__["ApproveregServices"], _services_decision_services__WEBPACK_IMPORTED_MODULE_11__["DecisionServices"]])
    ], RegistrationActionComponent);
    return RegistrationActionComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/registration-details/registration-details.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/Admin-Portal/registration-details/registration-details.component.css ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("p {\r\n  font-family: Lato;\r\n}\r\n\r\ntable.sjs-table > tbody > tr:first-child > td {\r\n     background-color: lightgreen;\r\n}\r\n\r\n.mb-01{\r\n padding-top: 17px;\r\n padding-bottom: 13px;\r\n}\r\n\r\n.mb-0{\r\n margin-bottom: 0.5rem;\r\n font-weight: 600;\r\n line-height: 1.5;\r\n color: #32325d;\r\n}\r\n\r\n.row{\r\n margin: 0;\r\n font-family: myFirstFont, sans-serif;\r\n font-size: 1rem;\r\n font-weight: 400;\r\n line-height: 1.5;\r\n color: #525f7f;\r\n text-align: left;\r\n}\r\n\r\n.custom-control-label::after {\r\n border-style: groove;\r\n\r\n}\r\n\r\n.btn{\r\n   background-color: #FF375E;\r\n   color: #fff;\r\n}\r\n\r\n.example-headers-align .mat-expansion-panel-header-title,\r\n.example-headers-align .mat-expansion-panel-header-description {\r\nflex-basis: 0;\r\n}\r\n\r\n.example-headers-align .mat-expansion-panel-header-description {\r\njustify-content: space-between;\r\nalign-items: center;\r\n}\r\n\r\n.example-headers-align .mat-form-field + .mat-form-field {\r\nmargin-left: 8px;\r\n}\r\n\r\n.example-button-row {\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: space-around;\r\n}\r\n\r\n.no-click {\r\n  pointer-events: none;\r\n}\r\n\r\ntable {\r\n  width: 100%;\r\n}\r\n\r\n.mat-form-field {\r\n  font-size: 14px;\r\n  width: 100%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL3JlZ2lzdHJhdGlvbi1kZXRhaWxzL3JlZ2lzdHJhdGlvbi1kZXRhaWxzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxpQkFBaUI7QUFDbkI7O0FBRUE7S0FDSyw0QkFBNEI7QUFDakM7O0FBQ0E7Q0FDQyxpQkFBaUI7Q0FDakIsb0JBQW9CO0FBQ3JCOztBQUNBO0NBQ0MscUJBQXFCO0NBQ3JCLGdCQUFnQjtDQUNoQixnQkFBZ0I7Q0FDaEIsY0FBYztBQUNmOztBQUNBO0NBQ0MsU0FBUztDQUNULG9DQUFvQztDQUNwQyxlQUFlO0NBQ2YsZ0JBQWdCO0NBQ2hCLGdCQUFnQjtDQUNoQixjQUFjO0NBQ2QsZ0JBQWdCO0FBQ2pCOztBQUVBO0NBQ0Msb0JBQW9COztBQUVyQjs7QUFFQTtHQUNHLHlCQUF5QjtHQUN6QixXQUFXO0FBQ2Q7O0FBRUE7O0FBRUEsYUFBYTtBQUNiOztBQUVBO0FBQ0EsOEJBQThCO0FBQzlCLG1CQUFtQjtBQUNuQjs7QUFFQTtBQUNBLGdCQUFnQjtBQUNoQjs7QUFDQTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsNkJBQTZCO0FBQy9COztBQUNBO0VBQ0Usb0JBQW9CO0FBQ3RCOztBQUVBO0VBQ0UsV0FBVztBQUNiOztBQUVBO0VBQ0UsZUFBZTtFQUNmLFdBQVc7QUFDYiIsImZpbGUiOiJzcmMvYXBwL0FkbWluLVBvcnRhbC9yZWdpc3RyYXRpb24tZGV0YWlscy9yZWdpc3RyYXRpb24tZGV0YWlscy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsicCB7XHJcbiAgZm9udC1mYW1pbHk6IExhdG87XHJcbn1cclxuXHJcbnRhYmxlLnNqcy10YWJsZSA+IHRib2R5ID4gdHI6Zmlyc3QtY2hpbGQgPiB0ZCB7XHJcbiAgICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmVlbjtcclxufVxyXG4ubWItMDF7XHJcbiBwYWRkaW5nLXRvcDogMTdweDtcclxuIHBhZGRpbmctYm90dG9tOiAxM3B4O1xyXG59XHJcbi5tYi0we1xyXG4gbWFyZ2luLWJvdHRvbTogMC41cmVtO1xyXG4gZm9udC13ZWlnaHQ6IDYwMDtcclxuIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiBjb2xvcjogIzMyMzI1ZDtcclxufVxyXG4ucm93e1xyXG4gbWFyZ2luOiAwO1xyXG4gZm9udC1mYW1pbHk6IG15Rmlyc3RGb250LCBzYW5zLXNlcmlmO1xyXG4gZm9udC1zaXplOiAxcmVtO1xyXG4gZm9udC13ZWlnaHQ6IDQwMDtcclxuIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiBjb2xvcjogIzUyNWY3ZjtcclxuIHRleHQtYWxpZ246IGxlZnQ7XHJcbn1cclxuXHJcbi5jdXN0b20tY29udHJvbC1sYWJlbDo6YWZ0ZXIge1xyXG4gYm9yZGVyLXN0eWxlOiBncm9vdmU7XHJcblxyXG59XHJcblxyXG4uYnRue1xyXG4gICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkYzNzVFO1xyXG4gICBjb2xvcjogI2ZmZjtcclxufVxyXG5cclxuLmV4YW1wbGUtaGVhZGVycy1hbGlnbiAubWF0LWV4cGFuc2lvbi1wYW5lbC1oZWFkZXItdGl0bGUsXHJcbi5leGFtcGxlLWhlYWRlcnMtYWxpZ24gLm1hdC1leHBhbnNpb24tcGFuZWwtaGVhZGVyLWRlc2NyaXB0aW9uIHtcclxuZmxleC1iYXNpczogMDtcclxufVxyXG5cclxuLmV4YW1wbGUtaGVhZGVycy1hbGlnbiAubWF0LWV4cGFuc2lvbi1wYW5lbC1oZWFkZXItZGVzY3JpcHRpb24ge1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5leGFtcGxlLWhlYWRlcnMtYWxpZ24gLm1hdC1mb3JtLWZpZWxkICsgLm1hdC1mb3JtLWZpZWxkIHtcclxubWFyZ2luLWxlZnQ6IDhweDtcclxufVxyXG4uZXhhbXBsZS1idXR0b24tcm93IHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbn1cclxuLm5vLWNsaWNrIHtcclxuICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxufVxyXG5cclxudGFibGUge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQge1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/Admin-Portal/registration-details/registration-details.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/Admin-Portal/registration-details/registration-details.component.ts ***!
  \*************************************************************************************/
/*! exports provided: RegistrationDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationDetailsComponent", function() { return RegistrationDetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/fesm5/table.js");



var RegistrationDetailsComponent = /** @class */ (function () {
    function RegistrationDetailsComponent() {
        this.filterValues = {};
        this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.displayedColumns = ['fname', 'lname', 'company', 'email', 'phone', 'country', 'date', 'status'];
        this.avengers = [];
        this.filterSelectObj = [];
        // Object to create Filter for
        this.filterSelectObj = [
            {
                name: 'STATUS',
                columnProp: 'status',
                options: []
            }
        ];
    }
    RegistrationDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.avengers =
            [{ fname: 'Sam', lname: 'Alanazi', company: 'Bret', email: 'Sam@april.biz', phone: '0555622277', country: 'Saudi Arabia', date: '2020-08-15', status: 'Rejeted' },
                { fname: 'Abdo', lname: 'alhosan', company: 'TATA', email: 'Abdo@melissa.tv', phone: '0556559277', country: 'UAE', date: '2020-08-17', status: 'Submmited' },
                { fname: 'Aziz', lname: 'almu', company: 'DATA', email: 'Aziz@yesenia.net', phone: '0556553223', country: 'kuwait', date: '2020-08-19', status: 'Approved' },
                { fname: 'Ree', lname: 'A', company: 'Mobily', email: 'Ree.OConner@kory.org', phone: '0559663224', country: 'Saudi Arabia', date: '2020-08-25', status: 'Rejeted' },
                { fname: 'Zack', lname: 'Bee', company: 'Zain', email: 'Zack@annie.ca', phone: '0556961145', country: 'USA', date: '2020-08-27', status: 'Submmited' }];
        this.dataSource.data = this.avengers;
        this.filterSelectObj.filter(function (o) {
            o.options = _this.getFilterObject(_this.avengers, o.columnProp);
        });
        this.dataSource.filterPredicate = this.createFilter();
    };
    RegistrationDetailsComponent.prototype.getFilterObject = function (fullObj, key) {
        var uniqChk = [];
        fullObj.filter(function (obj) {
            if (!uniqChk.includes(obj[key])) {
                uniqChk.push(obj[key]);
            }
            return obj;
        });
        return uniqChk;
    };
    // Called on Filter change
    RegistrationDetailsComponent.prototype.filterChange = function (filter, event) {
        //let filterValues = {}
        this.filterValues[filter.columnProp] = event.target.value.trim().toLowerCase();
        this.dataSource.filter = JSON.stringify(this.filterValues);
    };
    // Custom filter method fot Angular Material Datatable
    RegistrationDetailsComponent.prototype.createFilter = function () {
        var filterFunction = function (data, filter) {
            var searchTerms = JSON.parse(filter);
            var isFilterSet = false;
            for (var col in searchTerms) {
                if (searchTerms[col].toString() !== '') {
                    isFilterSet = true;
                }
                else {
                    delete searchTerms[col];
                }
            }
            var nameSearch = function () {
                var found = false;
                if (isFilterSet) {
                    var _loop_1 = function (col) {
                        searchTerms[col].trim().toLowerCase().split(' ').forEach(function (word) {
                            if (data[col].toString().toLowerCase().indexOf(word) != -1 && isFilterSet) {
                                found = true;
                            }
                        });
                    };
                    for (var col in searchTerms) {
                        _loop_1(col);
                    }
                    return found;
                }
                else {
                    return true;
                }
            };
            return nameSearch();
        };
        return filterFunction;
    };
    // Reset table filters
    RegistrationDetailsComponent.prototype.resetFilters = function () {
        this.filterValues = {};
        this.filterSelectObj.forEach(function (value, key) {
            value.modelValue = undefined;
        });
        this.dataSource.filter = "";
    };
    RegistrationDetailsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-registration-details',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./registration-details.component.html */ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/registration-details/registration-details.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./registration-details.component.css */ "./src/app/Admin-Portal/registration-details/registration-details.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], RegistrationDetailsComponent);
    return RegistrationDetailsComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/role-access/role-access.component.css":
/*!********************************************************************!*\
  !*** ./src/app/Admin-Portal/role-access/role-access.component.css ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("p {\r\n    font-family: Lato;\r\n  }\r\n  \r\n  table.sjs-table > tbody > tr:first-child > td {\r\n       background-color: lightgreen;\r\n  }\r\n  \r\n  .mb-0{\r\n   margin-bottom: 0.5rem;\r\n   font-weight: 600;\r\n   line-height: 1.5;\r\n   color: #32325d;\r\n  }\r\n  \r\n  .row{\r\n   margin: 0;\r\n   font-family: myFirstFont, sans-serif;\r\n   font-size: 1rem;\r\n   font-weight: 400;\r\n   line-height: 1.5;\r\n   color: #525f7f;\r\n   text-align: left;\r\n  }\r\n  \r\n  .custom-control-label::after {\r\n   border-style: groove;\r\n\r\n  }\r\n  \r\n  .btn{\r\n     background-color: #FF375E;\r\n     color: #fff;\r\n }\r\n  \r\n  .example-headers-align .mat-expansion-panel-header-title,\r\n.example-headers-align .mat-expansion-panel-header-description {\r\n  flex-basis: 0;\r\n}\r\n  \r\n  .example-headers-align .mat-expansion-panel-header-description {\r\n  justify-content: space-between;\r\n  align-items: center;\r\n}\r\n  \r\n  .example-headers-align .mat-form-field + .mat-form-field {\r\n  margin-left: 8px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL3JvbGUtYWNjZXNzL3JvbGUtYWNjZXNzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxpQkFBaUI7RUFDbkI7O0VBRUE7T0FDSyw0QkFBNEI7RUFDakM7O0VBRUE7R0FDQyxxQkFBcUI7R0FDckIsZ0JBQWdCO0dBQ2hCLGdCQUFnQjtHQUNoQixjQUFjO0VBQ2Y7O0VBQ0E7R0FDQyxTQUFTO0dBQ1Qsb0NBQW9DO0dBQ3BDLGVBQWU7R0FDZixnQkFBZ0I7R0FDaEIsZ0JBQWdCO0dBQ2hCLGNBQWM7R0FDZCxnQkFBZ0I7RUFDakI7O0VBRUE7R0FDQyxvQkFBb0I7O0VBRXJCOztFQUVBO0tBQ0cseUJBQXlCO0tBQ3pCLFdBQVc7Q0FDZjs7RUFFQTs7RUFFQyxhQUFhO0FBQ2Y7O0VBRUE7RUFDRSw4QkFBOEI7RUFDOUIsbUJBQW1CO0FBQ3JCOztFQUVBO0VBQ0UsZ0JBQWdCO0FBQ2xCIiwiZmlsZSI6InNyYy9hcHAvQWRtaW4tUG9ydGFsL3JvbGUtYWNjZXNzL3JvbGUtYWNjZXNzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJwIHtcclxuICAgIGZvbnQtZmFtaWx5OiBMYXRvO1xyXG4gIH1cclxuICBcclxuICB0YWJsZS5zanMtdGFibGUgPiB0Ym9keSA+IHRyOmZpcnN0LWNoaWxkID4gdGQge1xyXG4gICAgICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmVlbjtcclxuICB9XHJcblxyXG4gIC5tYi0we1xyXG4gICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XHJcbiAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgIGNvbG9yOiAjMzIzMjVkO1xyXG4gIH1cclxuICAucm93e1xyXG4gICBtYXJnaW46IDA7XHJcbiAgIGZvbnQtZmFtaWx5OiBteUZpcnN0Rm9udCwgc2Fucy1zZXJpZjtcclxuICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICBmb250LXdlaWdodDogNDAwO1xyXG4gICBsaW5lLWhlaWdodDogMS41O1xyXG4gICBjb2xvcjogIzUyNWY3ZjtcclxuICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICB9XHJcblxyXG4gIC5jdXN0b20tY29udHJvbC1sYWJlbDo6YWZ0ZXIge1xyXG4gICBib3JkZXItc3R5bGU6IGdyb292ZTtcclxuXHJcbiAgfVxyXG5cclxuICAuYnRue1xyXG4gICAgIGJhY2tncm91bmQtY29sb3I6ICNGRjM3NUU7XHJcbiAgICAgY29sb3I6ICNmZmY7XHJcbiB9XHJcblxyXG4gLmV4YW1wbGUtaGVhZGVycy1hbGlnbiAubWF0LWV4cGFuc2lvbi1wYW5lbC1oZWFkZXItdGl0bGUsXHJcbi5leGFtcGxlLWhlYWRlcnMtYWxpZ24gLm1hdC1leHBhbnNpb24tcGFuZWwtaGVhZGVyLWRlc2NyaXB0aW9uIHtcclxuICBmbGV4LWJhc2lzOiAwO1xyXG59XHJcblxyXG4uZXhhbXBsZS1oZWFkZXJzLWFsaWduIC5tYXQtZXhwYW5zaW9uLXBhbmVsLWhlYWRlci1kZXNjcmlwdGlvbiB7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5leGFtcGxlLWhlYWRlcnMtYWxpZ24gLm1hdC1mb3JtLWZpZWxkICsgLm1hdC1mb3JtLWZpZWxkIHtcclxuICBtYXJnaW4tbGVmdDogOHB4O1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/Admin-Portal/role-access/role-access.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/Admin-Portal/role-access/role-access.component.ts ***!
  \*******************************************************************/
/*! exports provided: RoleAccessComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleAccessComponent", function() { return RoleAccessComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



var RoleAccessComponent = /** @class */ (function () {
    function RoleAccessComponent(router) {
        this.router = router;
        this.roles = [
            { name: 'Portal Admin' },
            { name: 'Upload CDR' },
            { name: 'Dwonalod CDR' },
        ];
    }
    RoleAccessComponent.prototype.ngOnInit = function () {
    };
    RoleAccessComponent.prototype.create = function () {
        this.router.navigate(['new-role']);
    };
    RoleAccessComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    RoleAccessComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-role-access',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./role-access.component.html */ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/role-access/role-access.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./role-access.component.css */ "./src/app/Admin-Portal/role-access/role-access.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], RoleAccessComponent);
    return RoleAccessComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/role-view/role-view.component.css":
/*!****************************************************************!*\
  !*** ./src/app/Admin-Portal/role-view/role-view.component.css ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL0FkbWluLVBvcnRhbC9yb2xlLXZpZXcvcm9sZS12aWV3LmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/Admin-Portal/role-view/role-view.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/Admin-Portal/role-view/role-view.component.ts ***!
  \***************************************************************/
/*! exports provided: RoleViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleViewComponent", function() { return RoleViewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


var RoleViewComponent = /** @class */ (function () {
    function RoleViewComponent() {
        this.toggleProperty = false;
    }
    RoleViewComponent.prototype.ngOnInit = function () {
    };
    RoleViewComponent.prototype.toggle = function () {
        this.toggleProperty = !this.toggleProperty;
    };
    RoleViewComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-role-view',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./role-view.component.front.html */ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/role-view/role-view.component.front.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./role-view.component.css */ "./src/app/Admin-Portal/role-view/role-view.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], RoleViewComponent);
    return RoleViewComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/update-receiver/flip-card-back.ts":
/*!****************************************************************!*\
  !*** ./src/app/Admin-Portal/update-receiver/flip-card-back.ts ***!
  \****************************************************************/
/*! exports provided: FlipCardBackComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FlipCardBackComponent", function() { return FlipCardBackComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


var FlipCardBackComponent = /** @class */ (function () {
    function FlipCardBackComponent() {
    }
    FlipCardBackComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'flip-card-back',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./update-receiver.component.html */ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/update-receiver/update-receiver.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./update-receiver.component.css */ "./src/app/Admin-Portal/update-receiver/update-receiver.component.css")).default]
        })
    ], FlipCardBackComponent);
    return FlipCardBackComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/update-receiver/flip-card-front.ts":
/*!*****************************************************************!*\
  !*** ./src/app/Admin-Portal/update-receiver/flip-card-front.ts ***!
  \*****************************************************************/
/*! exports provided: FlipCardFrontComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FlipCardFrontComponent", function() { return FlipCardFrontComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


var FlipCardFrontComponent = /** @class */ (function () {
    function FlipCardFrontComponent() {
    }
    FlipCardFrontComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'flip-card-front',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./update-receiver.component.html */ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/update-receiver/update-receiver.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./update-receiver.component.css */ "./src/app/Admin-Portal/update-receiver/update-receiver.component.css")).default]
        })
    ], FlipCardFrontComponent);
    return FlipCardFrontComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/update-receiver/update-receiver.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/Admin-Portal/update-receiver/update-receiver.component.css ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".costom-btn {\r\n    margin: auto;\r\n    margin: auto;\r\n    line-height: initial;\r\n    background-color: blueviolet;\r\n    margin-left: 22vh;\r\n    margin-top: 22vh;\r\n}\r\n\r\n.h2{\r\n    font-family: inherit;\r\n    font-weight: 600;\r\n    line-height: 1.5;\r\n    font-size: 1.25rem;\r\n    margin-top: -32px;\r\n    padding-bottom: 15px\r\n}\r\n\r\n.card-body-img{\r\n   background-image: url('iphone.png'); \r\n   background-repeat: no-repeat;\r\n   background-position-y: -75pt;\r\n   background-position-x: -10pt;\r\n   width: auto;\r\n   height: auto;\r\n}\r\n\r\n:host {\r\n    display: grid;\r\n    place-items: center;\r\n    height: 70vh;\r\n    width: 100vw;\r\n  }\r\n\r\n.container {\r\n    width: 400px;\r\n    height: 380px;\r\n    display: block;\r\n    position: relative;\r\n    perspective: 800px;\r\n    border-radius: 4px;\r\n  }\r\n\r\n.card {\r\n    width: 100%;\r\n    height: 100%;\r\n    position: absolute;\r\n    transform-style: preserve-3d;\r\n    transition: transform 0.8s cubic-bezier(0.175, 0.885, 0.32, 1.275);\r\n    border-radius: 6px;\r\n    box-shadow: 0 6px 16px rgba(0, 0, 0, 0.15);\r\n    box-sizing: border-box;\r\n  }\r\n\r\n.card .front,\r\n  .card .back {\r\n    position: absolute;\r\n    width: 100%;\r\n    height: 100%;\r\n    -webkit-backface-visibility: hidden;\r\n            backface-visibility: hidden;\r\n    border-radius: 6px;\r\n    background-color: #f1f2f3;\r\n    display: grid;\r\n    place-items: center;\r\n  }\r\n\r\n.back {\r\n    transform: rotateY(180deg);\r\n  }\r\n\r\n.flipped {\r\n    transform: rotateY(180deg);\r\n  }\r\n\r\n.arrow-icon {\r\n    position: absolute;\r\n    right: 15px;\r\n    top: 5px;\r\n    opacity: 1;\r\n    font: 1.5em sans-serif;\r\n    width: 25px;\r\n    height: 25px;\r\n    cursor: pointer;\r\n    display: flex;\r\n    justify-content: center;\r\n    align-items: center;\r\n    border-radius: 50%;\r\n    /* border: 1px solid #000; */\r\n  \r\n  }\r\n\r\n.black{\r\n    color: #000;\r\n    font-weight: 600;\r\n    background-color: #e9ecef;\r\n  }\r\n\r\n.non-resize{\r\n    resize: none;\r\n  }\r\n\r\n.display-line{\r\n      max-width: 90%;\r\n      padding-left: 9rem;\r\n      display: flex;\r\n  }\r\n\r\n/* important stuff for this example */\r\n\r\n.row {\r\n  display:flex;\r\n}\r\n\r\n.div1 {\r\n  flex: 1 1 auto;\r\n}\r\n\r\n.div2 {\r\n  flex: 0 0 auto; \r\n}\r\n\r\n.row {\r\n  width:100%;\r\n  padding:1em;\r\n  overflow:hidden;\r\n}\r\n\r\n.example-container {\r\n  display: flex;\r\n  flex-direction: column;\r\n}\r\n\r\n.example-container > * {\r\n  width: 100%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL3VwZGF0ZS1yZWNlaXZlci91cGRhdGUtcmVjZWl2ZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFlBQVk7SUFDWixZQUFZO0lBQ1osb0JBQW9CO0lBQ3BCLDRCQUE0QjtJQUM1QixpQkFBaUI7SUFDakIsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksb0JBQW9CO0lBQ3BCLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQjtBQUNKOztBQUVBO0dBQ0csbUNBQTBDO0dBQzFDLDRCQUE0QjtHQUM1Qiw0QkFBNEI7R0FDNUIsNEJBQTRCO0dBQzVCLFdBQVc7R0FDWCxZQUFZO0FBQ2Y7O0FBRUE7SUFDSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixZQUFZO0VBQ2Q7O0FBQ0E7SUFDRSxZQUFZO0lBQ1osYUFBYTtJQUNiLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtFQUNwQjs7QUFFQTtJQUNFLFdBQVc7SUFDWCxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLDRCQUE0QjtJQUM1QixrRUFBa0U7SUFDbEUsa0JBQWtCO0lBQ2xCLDBDQUEwQztJQUMxQyxzQkFBc0I7RUFDeEI7O0FBRUE7O0lBRUUsa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxZQUFZO0lBQ1osbUNBQTJCO1lBQTNCLDJCQUEyQjtJQUMzQixrQkFBa0I7SUFDbEIseUJBQXlCO0lBQ3pCLGFBQWE7SUFDYixtQkFBbUI7RUFDckI7O0FBRUE7SUFDRSwwQkFBMEI7RUFDNUI7O0FBRUE7SUFDRSwwQkFBMEI7RUFDNUI7O0FBRUE7SUFDRSxrQkFBa0I7SUFDbEIsV0FBVztJQUNYLFFBQVE7SUFDUixVQUFVO0lBQ1Ysc0JBQXNCO0lBQ3RCLFdBQVc7SUFDWCxZQUFZO0lBQ1osZUFBZTtJQUNmLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQiw0QkFBNEI7O0VBRTlCOztBQUVBO0lBQ0UsV0FBVztJQUNYLGdCQUFnQjtJQUNoQix5QkFBeUI7RUFDM0I7O0FBRUE7SUFDRSxZQUFZO0VBQ2Q7O0FBRUE7TUFDSSxjQUFjO01BQ2Qsa0JBQWtCO01BQ2xCLGFBQWE7RUFDakI7O0FBQ0QscUNBQXFDOztBQUd0QztFQUNFLFlBQVk7QUFDZDs7QUFDQTtFQUNFLGNBQWM7QUFDaEI7O0FBRUE7RUFDRSxjQUFjO0FBQ2hCOztBQUNBO0VBQ0UsVUFBVTtFQUNWLFdBQVc7RUFDWCxlQUFlO0FBQ2pCOztBQUVBO0VBQ0UsYUFBYTtFQUNiLHNCQUFzQjtBQUN4Qjs7QUFFQTtFQUNFLFdBQVc7QUFDYiIsImZpbGUiOiJzcmMvYXBwL0FkbWluLVBvcnRhbC91cGRhdGUtcmVjZWl2ZXIvdXBkYXRlLXJlY2VpdmVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29zdG9tLWJ0biB7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICBsaW5lLWhlaWdodDogaW5pdGlhbDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsdWV2aW9sZXQ7XHJcbiAgICBtYXJnaW4tbGVmdDogMjJ2aDtcclxuICAgIG1hcmdpbi10b3A6IDIydmg7XHJcbn1cclxuXHJcbi5oMntcclxuICAgIGZvbnQtZmFtaWx5OiBpbmhlcml0O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgICBmb250LXNpemU6IDEuMjVyZW07XHJcbiAgICBtYXJnaW4tdG9wOiAtMzJweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiAxNXB4XHJcbn1cclxuXHJcbi5jYXJkLWJvZHktaW1ne1xyXG4gICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJhc3NldHMvaXBob25lLnBuZ1wiKTsgXHJcbiAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgIGJhY2tncm91bmQtcG9zaXRpb24teTogLTc1cHQ7XHJcbiAgIGJhY2tncm91bmQtcG9zaXRpb24teDogLTEwcHQ7XHJcbiAgIHdpZHRoOiBhdXRvO1xyXG4gICBoZWlnaHQ6IGF1dG87XHJcbn1cclxuXHJcbjpob3N0IHtcclxuICAgIGRpc3BsYXk6IGdyaWQ7XHJcbiAgICBwbGFjZS1pdGVtczogY2VudGVyO1xyXG4gICAgaGVpZ2h0OiA3MHZoO1xyXG4gICAgd2lkdGg6IDEwMHZ3O1xyXG4gIH1cclxuICAuY29udGFpbmVyIHtcclxuICAgIHdpZHRoOiA0MDBweDtcclxuICAgIGhlaWdodDogMzgwcHg7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHBlcnNwZWN0aXZlOiA4MDBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICB9XHJcbiAgXHJcbiAgLmNhcmQge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0cmFuc2Zvcm0tc3R5bGU6IHByZXNlcnZlLTNkO1xyXG4gICAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuOHMgY3ViaWMtYmV6aWVyKDAuMTc1LCAwLjg4NSwgMC4zMiwgMS4yNzUpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNnB4O1xyXG4gICAgYm94LXNoYWRvdzogMCA2cHggMTZweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICB9XHJcbiAgXHJcbiAgLmNhcmQgLmZyb250LFxyXG4gIC5jYXJkIC5iYWNrIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNnB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2YxZjJmMztcclxuICAgIGRpc3BsYXk6IGdyaWQ7XHJcbiAgICBwbGFjZS1pdGVtczogY2VudGVyO1xyXG4gIH1cclxuICBcclxuICAuYmFjayB7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZVkoMTgwZGVnKTtcclxuICB9XHJcbiAgXHJcbiAgLmZsaXBwZWQge1xyXG4gICAgdHJhbnNmb3JtOiByb3RhdGVZKDE4MGRlZyk7XHJcbiAgfVxyXG4gIFxyXG4gIC5hcnJvdy1pY29uIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHJpZ2h0OiAxNXB4O1xyXG4gICAgdG9wOiA1cHg7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gICAgZm9udDogMS41ZW0gc2Fucy1zZXJpZjtcclxuICAgIHdpZHRoOiAyNXB4O1xyXG4gICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIC8qIGJvcmRlcjogMXB4IHNvbGlkICMwMDA7ICovXHJcbiAgXHJcbiAgfVxyXG5cclxuICAuYmxhY2t7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTllY2VmO1xyXG4gIH1cclxuIFxyXG4gIC5ub24tcmVzaXple1xyXG4gICAgcmVzaXplOiBub25lO1xyXG4gIH1cclxuXHJcbiAgLmRpc3BsYXktbGluZXtcclxuICAgICAgbWF4LXdpZHRoOiA5MCU7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogOXJlbTtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICB9XHJcbiAvKiBpbXBvcnRhbnQgc3R1ZmYgZm9yIHRoaXMgZXhhbXBsZSAqL1xyXG5cclxuXHJcbi5yb3cge1xyXG4gIGRpc3BsYXk6ZmxleDtcclxufVxyXG4uZGl2MSB7XHJcbiAgZmxleDogMSAxIGF1dG87XHJcbn1cclxuXHJcbi5kaXYyIHtcclxuICBmbGV4OiAwIDAgYXV0bzsgXHJcbn1cclxuLnJvdyB7XHJcbiAgd2lkdGg6MTAwJTtcclxuICBwYWRkaW5nOjFlbTtcclxuICBvdmVyZmxvdzpoaWRkZW47XHJcbn1cclxuXHJcbi5leGFtcGxlLWNvbnRhaW5lciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcblxyXG4uZXhhbXBsZS1jb250YWluZXIgPiAqIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/Admin-Portal/update-receiver/update-receiver.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/Admin-Portal/update-receiver/update-receiver.component.ts ***!
  \***************************************************************************/
/*! exports provided: UpdateReceiverComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateReceiverComponent", function() { return UpdateReceiverComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





var UpdateReceiverComponent = /** @class */ (function () {
    function UpdateReceiverComponent(toast, router) {
        this.toast = toast;
        this.router = router;
        this.toggleProperty = false;
    }
    UpdateReceiverComponent.prototype.ngOnInit = function () {
    };
    UpdateReceiverComponent.prototype.toggle = function () {
        this.toggleProperty = !this.toggleProperty;
    };
    UpdateReceiverComponent.prototype.save = function () {
        var _this = this;
        var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: function (toast) {
                toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.stopTimer);
                toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.resumeTimer);
            }
        });
        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Save'
        }).then(function (result) {
            if (result.value) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire('Saved!', 'Your Changes has been made.', 'success');
                _this.router.navigate(['portal-admin']);
            }
        });
    };
    UpdateReceiverComponent.ctorParameters = function () { return [
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
    ]; };
    UpdateReceiverComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-update-receiver',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./update-receiver.component.html */ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/update-receiver/update-receiver.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./update-receiver.component.css */ "./src/app/Admin-Portal/update-receiver/update-receiver.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], UpdateReceiverComponent);
    return UpdateReceiverComponent;
}());



/***/ }),

/***/ "./src/app/Admin-Portal/virtual-account/virtual-account.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/Admin-Portal/virtual-account/virtual-account.component.css ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("p {\r\n     font-family: Lato;\r\n   }\r\n   \r\n   table.sjs-table > tbody > tr:first-child > td {\r\n        background-color: lightgreen;\r\n   }\r\n   \r\n   .mb-01{\r\n    padding-top: 17px;\r\n    padding-bottom: 13px;\r\n   }\r\n   \r\n   .mb-0{\r\n    margin-bottom: 0.5rem;\r\n    font-weight: 600;\r\n    line-height: 1.5;\r\n    color: #32325d;\r\n   }\r\n   \r\n   .row{\r\n    margin: 0;\r\n    font-family: myFirstFont, sans-serif;\r\n    font-size: 1rem;\r\n    font-weight: 400;\r\n    line-height: 1.5;\r\n    color: #525f7f;\r\n    text-align: left;\r\n   }\r\n   \r\n   .custom-control-label::after {\r\n    border-style: groove;\r\n \r\n   }\r\n   \r\n   .btn{\r\n      background-color: #FF375E;\r\n      color: #fff;\r\n  }\r\n   \r\n   .example-headers-align .mat-expansion-panel-header-title,\r\n .example-headers-align .mat-expansion-panel-header-description {\r\n   flex-basis: 0;\r\n }\r\n   \r\n   .example-headers-align .mat-expansion-panel-header-description {\r\n   justify-content: space-between;\r\n   align-items: center;\r\n }\r\n   \r\n   .example-headers-align .mat-form-field + .mat-form-field {\r\n   margin-left: 8px;\r\n }\r\n \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4tUG9ydGFsL3ZpcnR1YWwtYWNjb3VudC92aXJ0dWFsLWFjY291bnQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtLQUNLLGlCQUFpQjtHQUNuQjs7R0FFQTtRQUNLLDRCQUE0QjtHQUNqQzs7R0FDQTtJQUNDLGlCQUFpQjtJQUNqQixvQkFBb0I7R0FDckI7O0dBQ0E7SUFDQyxxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixjQUFjO0dBQ2Y7O0dBQ0E7SUFDQyxTQUFTO0lBQ1Qsb0NBQW9DO0lBQ3BDLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGNBQWM7SUFDZCxnQkFBZ0I7R0FDakI7O0dBRUE7SUFDQyxvQkFBb0I7O0dBRXJCOztHQUVBO01BQ0cseUJBQXlCO01BQ3pCLFdBQVc7RUFDZjs7R0FFQTs7R0FFQyxhQUFhO0NBQ2Y7O0dBRUE7R0FDRSw4QkFBOEI7R0FDOUIsbUJBQW1CO0NBQ3JCOztHQUVBO0dBQ0UsZ0JBQWdCO0NBQ2xCIiwiZmlsZSI6InNyYy9hcHAvQWRtaW4tUG9ydGFsL3ZpcnR1YWwtYWNjb3VudC92aXJ0dWFsLWFjY291bnQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInAge1xyXG4gICAgIGZvbnQtZmFtaWx5OiBMYXRvO1xyXG4gICB9XHJcbiAgIFxyXG4gICB0YWJsZS5zanMtdGFibGUgPiB0Ym9keSA+IHRyOmZpcnN0LWNoaWxkID4gdGQge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0Z3JlZW47XHJcbiAgIH1cclxuICAgLm1iLTAxe1xyXG4gICAgcGFkZGluZy10b3A6IDE3cHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTNweDtcclxuICAgfVxyXG4gICAubWItMHtcclxuICAgIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gICAgY29sb3I6ICMzMjMyNWQ7XHJcbiAgIH1cclxuICAgLnJvd3tcclxuICAgIG1hcmdpbjogMDtcclxuICAgIGZvbnQtZmFtaWx5OiBteUZpcnN0Rm9udCwgc2Fucy1zZXJpZjtcclxuICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gICAgY29sb3I6ICM1MjVmN2Y7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICB9XHJcbiBcclxuICAgLmN1c3RvbS1jb250cm9sLWxhYmVsOjphZnRlciB7XHJcbiAgICBib3JkZXItc3R5bGU6IGdyb292ZTtcclxuIFxyXG4gICB9XHJcbiBcclxuICAgLmJ0bntcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGMzc1RTtcclxuICAgICAgY29sb3I6ICNmZmY7XHJcbiAgfVxyXG4gXHJcbiAgLmV4YW1wbGUtaGVhZGVycy1hbGlnbiAubWF0LWV4cGFuc2lvbi1wYW5lbC1oZWFkZXItdGl0bGUsXHJcbiAuZXhhbXBsZS1oZWFkZXJzLWFsaWduIC5tYXQtZXhwYW5zaW9uLXBhbmVsLWhlYWRlci1kZXNjcmlwdGlvbiB7XHJcbiAgIGZsZXgtYmFzaXM6IDA7XHJcbiB9XHJcbiBcclxuIC5leGFtcGxlLWhlYWRlcnMtYWxpZ24gLm1hdC1leHBhbnNpb24tcGFuZWwtaGVhZGVyLWRlc2NyaXB0aW9uIHtcclxuICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gfVxyXG4gXHJcbiAuZXhhbXBsZS1oZWFkZXJzLWFsaWduIC5tYXQtZm9ybS1maWVsZCArIC5tYXQtZm9ybS1maWVsZCB7XHJcbiAgIG1hcmdpbi1sZWZ0OiA4cHg7XHJcbiB9XHJcbiAiXX0= */");

/***/ }),

/***/ "./src/app/Admin-Portal/virtual-account/virtual-account.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/Admin-Portal/virtual-account/virtual-account.component.ts ***!
  \***************************************************************************/
/*! exports provided: VirtualAccountComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VirtualAccountComponent", function() { return VirtualAccountComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_Virtual_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/Virtual.services */ "./src/app/services/Virtual.services.ts");





var VirtualAccountComponent = /** @class */ (function () {
    // virtualDto = new virtualDto();
    function VirtualAccountComponent(router, VirtualServices) {
        this.router = router;
        this.VirtualServices = VirtualServices;
        this.virtual = new Array();
    }
    VirtualAccountComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.VirtualServices.getVirtual().subscribe(function (res) {
            _this.virtual = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(res.registrationReq);
        });
    };
    VirtualAccountComponent.prototype.onDelete = function (obj) {
        this.VirtualServices.deleteVirtual().subscribe(function (res) {
            console.log(res);
        });
    };
    VirtualAccountComponent.prototype.save = function () {
        var _this = this;
        var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: function (toast) {
                toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.stopTimer);
                toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.resumeTimer);
            }
        });
        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
            title: 'Are you sure you want delet virtual account?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Save'
        }).then(function (result) {
            if (result.value) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire('Saved!', 'Your Changes has been made.', 'success');
                _this.router.navigate(['virtual-account']);
            }
        });
    };
    VirtualAccountComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _services_Virtual_services__WEBPACK_IMPORTED_MODULE_4__["VirtualServices"] }
    ]; };
    VirtualAccountComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-virtual-account',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./virtual-account.component.html */ "./node_modules/raw-loader/index.js!./src/app/Admin-Portal/virtual-account/virtual-account.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./virtual-account.component.css */ "./src/app/Admin-Portal/virtual-account/virtual-account.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _services_Virtual_services__WEBPACK_IMPORTED_MODULE_4__["VirtualServices"]])
    ], VirtualAccountComponent);
    return VirtualAccountComponent;
}());



/***/ }),

/***/ "./src/app/admin-routing/admin-routing.component.css":
/*!***********************************************************!*\
  !*** ./src/app/admin-routing/admin-routing.component.css ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluLXJvdXRpbmcvYWRtaW4tcm91dGluZy5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/admin-routing/admin-routing.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/admin-routing/admin-routing.component.ts ***!
  \**********************************************************/
/*! exports provided: AdminRoutingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminRoutingComponent", function() { return AdminRoutingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


var AdminRoutingComponent = /** @class */ (function () {
    function AdminRoutingComponent() {
    }
    AdminRoutingComponent.prototype.ngOnInit = function () {
    };
    AdminRoutingComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-admin-routing',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./admin-routing.component.html */ "./node_modules/raw-loader/index.js!./src/app/admin-routing/admin-routing.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./admin-routing.component.css */ "./src/app/admin-routing/admin-routing.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], AdminRoutingComponent);
    return AdminRoutingComponent;
}());



/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



var AppComponent = /** @class */ (function () {
    function AppComponent(router) {
        this.router = router;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.router.events.subscribe(function (evt) {
            if (!(evt instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"])) {
                return;
            }
            window.scrollTo(0, 0);
        });
    };
    AppComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            // tslint:disable-next-line
            selector: 'body',
            template: '<router-outlet></router-outlet>'
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: HttpLoaderFactory, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function() { return HttpLoaderFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/fesm5/paginator.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/fesm5/input.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var ngx_bootstrap_progressbar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/progressbar */ "./node_modules/ngx-bootstrap/progressbar/fesm5/ngx-bootstrap-progressbar.js");
/* harmony import */ var ngx_bootstrap_popover__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/popover */ "./node_modules/ngx-bootstrap/popover/fesm5/ngx-bootstrap-popover.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm5/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_collapse__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-bootstrap/collapse */ "./node_modules/ngx-bootstrap/collapse/fesm5/ngx-bootstrap-collapse.js");
/* harmony import */ var ngx_bootstrap_carousel__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-bootstrap/carousel */ "./node_modules/ngx-bootstrap/carousel/fesm5/ngx-bootstrap-carousel.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/fesm5/dialog.js");
/* harmony import */ var ngx_webstorage__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-webstorage */ "./node_modules/ngx-webstorage/fesm5/ngx-webstorage.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/fesm5/stepper.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/fesm5/expansion.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/fesm5/menu.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var angular_user_idle__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! angular-user-idle */ "./node_modules/angular-user-idle/fesm5/angular-user-idle.js");
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/fesm5/ngx-perfect-scrollbar.js");
/* harmony import */ var _angular_material_chips__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/material/chips */ "./node_modules/@angular/material/fesm5/chips.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _views_error_404_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./views/error/404.component */ "./src/app/views/error/404.component.ts");
/* harmony import */ var _views_error_500_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./views/error/500.component */ "./src/app/views/error/500.component.ts");
/* harmony import */ var _views_login_login_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./views/login/login.component */ "./src/app/views/login/login.component.ts");
/* harmony import */ var _views_register_register_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./views/register/register.component */ "./src/app/views/register/register.component.ts");
/* harmony import */ var _coreui_angular__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @coreui/angular */ "./node_modules/@coreui/angular/fesm5/coreui-angular.js");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ngx-bootstrap/tabs */ "./node_modules/ngx-bootstrap/tabs/fesm5/ngx-bootstrap-tabs.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/fesm2015/valor-software-ng2-charts.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/fesm5/ngx-translate-http-loader.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _nav_nav_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./nav/nav.component */ "./src/app/nav/nav.component.ts");
/* harmony import */ var _containers_main_layout_main_layout_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./containers/main-layout/main-layout.component */ "./src/app/containers/main-layout/main-layout.component.ts");
/* harmony import */ var _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./contact-us/contact-us.component */ "./src/app/contact-us/contact-us.component.ts");
/* harmony import */ var _new_partner_new_partner_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./new-partner/new-partner.component */ "./src/app/new-partner/new-partner.component.ts");
/* harmony import */ var _upload_upload_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./upload/upload.component */ "./src/app/upload/upload.component.ts");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/fesm5/ng2-file-upload.js");
/* harmony import */ var _complaint_complaint_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./complaint/complaint.component */ "./src/app/complaint/complaint.component.ts");
/* harmony import */ var _downloads_operations_downloads_operations_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./downloads-operations/downloads-operations.component */ "./src/app/downloads-operations/downloads-operations.component.ts");
/* harmony import */ var _help_help_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./help/help.component */ "./src/app/help/help.component.ts");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/fesm5/select.js");
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! @angular/material/progress-spinner */ "./node_modules/@angular/material/fesm5/progress-spinner.js");
/* harmony import */ var _dashboard_wbu_dashboard_wbu_component__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./dashboard-wbu/dashboard-wbu.component */ "./src/app/dashboard-wbu/dashboard-wbu.component.ts");
/* harmony import */ var _shared_models_share_module__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./shared/models/share.module */ "./src/app/shared/models/share.module.ts");
/* harmony import */ var _details_details_component__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./details/details.component */ "./src/app/details/details.component.ts");
/* harmony import */ var _display_invoice_display_invoice_component__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./display-invoice/display-invoice.component */ "./src/app/display-invoice/display-invoice.component.ts");
/* harmony import */ var _views_forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ./views/forgot-password/forgot-password.component */ "./src/app/views/forgot-password/forgot-password.component.ts");
/* harmony import */ var _Admin_Portal_portal_admin_portal_admin_component__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ./Admin-Portal/portal-admin/portal-admin.component */ "./src/app/Admin-Portal/portal-admin/portal-admin.component.ts");
/* harmony import */ var _Admin_Portal_approve_registration_approve_registration_component__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./Admin-Portal/approve-registration/approve-registration.component */ "./src/app/Admin-Portal/approve-registration/approve-registration.component.ts");
/* harmony import */ var _Admin_Portal_contact_admin_contact_admin_component__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! ./Admin-Portal/contact-admin/contact-admin.component */ "./src/app/Admin-Portal/contact-admin/contact-admin.component.ts");
/* harmony import */ var _Admin_Portal_virtual_account_virtual_account_component__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! ./Admin-Portal/virtual-account/virtual-account.component */ "./src/app/Admin-Portal/virtual-account/virtual-account.component.ts");
/* harmony import */ var _Admin_Portal_history_history_component__WEBPACK_IMPORTED_MODULE_59__ = __webpack_require__(/*! ./Admin-Portal/history/history.component */ "./src/app/Admin-Portal/history/history.component.ts");
/* harmony import */ var _Admin_Portal_update_receiver_update_receiver_component__WEBPACK_IMPORTED_MODULE_60__ = __webpack_require__(/*! ./Admin-Portal/update-receiver/update-receiver.component */ "./src/app/Admin-Portal/update-receiver/update-receiver.component.ts");
/* harmony import */ var _Admin_Portal_role_access_role_access_component__WEBPACK_IMPORTED_MODULE_61__ = __webpack_require__(/*! ./Admin-Portal/role-access/role-access.component */ "./src/app/Admin-Portal/role-access/role-access.component.ts");
/* harmony import */ var _Admin_Portal_role_view_role_view_component__WEBPACK_IMPORTED_MODULE_62__ = __webpack_require__(/*! ./Admin-Portal/role-view/role-view.component */ "./src/app/Admin-Portal/role-view/role-view.component.ts");
/* harmony import */ var _Admin_Portal_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_63__ = __webpack_require__(/*! ./Admin-Portal/new-role/new-role.component */ "./src/app/Admin-Portal/new-role/new-role.component.ts");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_64__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/fesm5/autocomplete.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_65__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/fesm5/icon.js");
/* harmony import */ var _register_user_register_user_component__WEBPACK_IMPORTED_MODULE_66__ = __webpack_require__(/*! ./register-user/register-user.component */ "./src/app/register-user/register-user.component.ts");
/* harmony import */ var _product_catalog_product_catalog_component__WEBPACK_IMPORTED_MODULE_67__ = __webpack_require__(/*! ./product-catalog/product-catalog.component */ "./src/app/product-catalog/product-catalog.component.ts");
/* harmony import */ var _product_catalog_voice_voice_component__WEBPACK_IMPORTED_MODULE_68__ = __webpack_require__(/*! ./product_catalog/voice/voice.component */ "./src/app/product_catalog/voice/voice.component.ts");
/* harmony import */ var _product_catalog_data_data_component__WEBPACK_IMPORTED_MODULE_69__ = __webpack_require__(/*! ./product_catalog/data/data.component */ "./src/app/product_catalog/data/data.component.ts");
/* harmony import */ var _product_catalog_capacity_capacity_component__WEBPACK_IMPORTED_MODULE_70__ = __webpack_require__(/*! ./product_catalog/capacity/capacity.component */ "./src/app/product_catalog/capacity/capacity.component.ts");
/* harmony import */ var _product_catalog_colocation_colocation_component__WEBPACK_IMPORTED_MODULE_71__ = __webpack_require__(/*! ./product_catalog/colocation/colocation.component */ "./src/app/product_catalog/colocation/colocation.component.ts");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_72__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/fesm5/card.js");
/* harmony import */ var _admin_routing_admin_routing_component__WEBPACK_IMPORTED_MODULE_73__ = __webpack_require__(/*! ./admin-routing/admin-routing.component */ "./src/app/admin-routing/admin-routing.component.ts");
/* harmony import */ var _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_74__ = __webpack_require__(/*! ./sidebar/sidebar.component */ "./src/app/sidebar/sidebar.component.ts");
/* harmony import */ var _Admin_Portal_registration_details_registration_details_component__WEBPACK_IMPORTED_MODULE_75__ = __webpack_require__(/*! ./Admin-Portal/registration-details/registration-details.component */ "./src/app/Admin-Portal/registration-details/registration-details.component.ts");
/* harmony import */ var _product_catalog_capacity_catalog_capacity_catalog_component__WEBPACK_IMPORTED_MODULE_76__ = __webpack_require__(/*! ./product_catalog/capacity-catalog/capacity-catalog.component */ "./src/app/product_catalog/capacity-catalog/capacity-catalog.component.ts");
/* harmony import */ var _Admin_Portal_registration_action_registration_action_component__WEBPACK_IMPORTED_MODULE_77__ = __webpack_require__(/*! ./Admin-Portal/registration-action/registration-action.component */ "./src/app/Admin-Portal/registration-action/registration-action.component.ts");
/* harmony import */ var _Admin_Portal_update_receiver_flip_card_front__WEBPACK_IMPORTED_MODULE_78__ = __webpack_require__(/*! ./Admin-Portal/update-receiver/flip-card-front */ "./src/app/Admin-Portal/update-receiver/flip-card-front.ts");
/* harmony import */ var _Admin_Portal_update_receiver_flip_card_back__WEBPACK_IMPORTED_MODULE_79__ = __webpack_require__(/*! ./Admin-Portal/update-receiver/flip-card-back */ "./src/app/Admin-Portal/update-receiver/flip-card-back.ts");
/* harmony import */ var _ngu_carousel__WEBPACK_IMPORTED_MODULE_80__ = __webpack_require__(/*! @ngu/carousel */ "./node_modules/@ngu/carousel/fesm2015/ngu-carousel.js");
/* harmony import */ var _Admin_Portal_orders_orders_component__WEBPACK_IMPORTED_MODULE_81__ = __webpack_require__(/*! ./Admin-Portal/orders/orders.component */ "./src/app/Admin-Portal/orders/orders.component.ts");
/* harmony import */ var ngx_countdown__WEBPACK_IMPORTED_MODULE_82__ = __webpack_require__(/*! ngx-countdown */ "./node_modules/ngx-countdown/fesm2015/ngx-countdown.js");
/* harmony import */ var _email_reset_email_reset_component__WEBPACK_IMPORTED_MODULE_83__ = __webpack_require__(/*! ./email-reset/email-reset.component */ "./src/app/email-reset/email-reset.component.ts");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_84__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/fesm5/table.js");
/* harmony import */ var ng2_search_filter__WEBPACK_IMPORTED_MODULE_85__ = __webpack_require__(/*! ng2-search-filter */ "./node_modules/ng2-search-filter/ng2-search-filter.es5.js");
/* harmony import */ var _views_password_expiry_password_expiry_component__WEBPACK_IMPORTED_MODULE_86__ = __webpack_require__(/*! ./views/password-expiry/password-expiry.component */ "./src/app/views/password-expiry/password-expiry.component.ts");
/* harmony import */ var _views_email_approved_email_approved_component__WEBPACK_IMPORTED_MODULE_87__ = __webpack_require__(/*! ./views/email-approved/email-approved.component */ "./src/app/views/email-approved/email-approved.component.ts");
/* harmony import */ var _email_confirmation_email_confirmation_component__WEBPACK_IMPORTED_MODULE_88__ = __webpack_require__(/*! ./email-confirmation/email-confirmation.component */ "./src/app/email-confirmation/email-confirmation.component.ts");
/* harmony import */ var _Admin_Portal_assets_assets_component__WEBPACK_IMPORTED_MODULE_89__ = __webpack_require__(/*! ./Admin-Portal/assets/assets.component */ "./src/app/Admin-Portal/assets/assets.component.ts");
/* harmony import */ var _views_error_403_component__WEBPACK_IMPORTED_MODULE_90__ = __webpack_require__(/*! ./views/error/403.component */ "./src/app/views/error/403.component.ts");
/* harmony import */ var _user_setting_user_setting_component__WEBPACK_IMPORTED_MODULE_91__ = __webpack_require__(/*! ./user-setting/user-setting.component */ "./src/app/user-setting/user-setting.component.ts");
























var DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
    suppressScrollX: true
};

// Import containers




var APP_CONTAINERS = [
    _containers_main_layout_main_layout_component__WEBPACK_IMPORTED_MODULE_40__["MainLayoutComponent"],
    _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_74__["SidebarComponent"],
];

// Import routing module

// Import 3rd party components






















































// import { AssetsComponent } from './Admin-Portal/assets/assets.component';







function HttpLoaderFactory(http) {
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_35__["TranslateHttpLoader"](http, './assets/i18n/', '.json');
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_16__["NgModule"])({
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_9__["BrowserModule"],
                angular_user_idle__WEBPACK_IMPORTED_MODULE_21__["UserIdleModule"].forRoot({ idle: 1800, timeout: 1, ping: 120 }),
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_38__["NgbModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__["MatDialogModule"],
                ng2_search_filter__WEBPACK_IMPORTED_MODULE_85__["Ng2SearchPipeModule"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_13__["NgxPaginationModule"],
                _angular_material_chips__WEBPACK_IMPORTED_MODULE_23__["MatChipsModule"],
                _angular_material_menu__WEBPACK_IMPORTED_MODULE_17__["MatMenuModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_84__["MatTableModule"],
                ngx_countdown__WEBPACK_IMPORTED_MODULE_82__["CountdownModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material_expansion__WEBPACK_IMPORTED_MODULE_15__["MatExpansionModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_65__["MatIconModule"],
                _angular_material_paginator__WEBPACK_IMPORTED_MODULE_1__["MatPaginatorModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_72__["MatCardModule"],
                _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_64__["MatAutocompleteModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpClientModule"],
                _ngu_carousel__WEBPACK_IMPORTED_MODULE_80__["NguCarouselModule"],
                _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_49__["MatProgressSpinnerModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_12__["ToastrModule"].forRoot(),
                _angular_material_select__WEBPACK_IMPORTED_MODULE_48__["MatSelectModule"],
                _shared_models_share_module__WEBPACK_IMPORTED_MODULE_51__["SharedModule"],
                _angular_material_stepper__WEBPACK_IMPORTED_MODULE_14__["MatStepperModule"],
                ngx_webstorage__WEBPACK_IMPORTED_MODULE_11__["NgxWebstorageModule"].forRoot({ prefix: 'app', separator: ':' }),
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_20__["BrowserAnimationsModule"],
                _app_routing__WEBPACK_IMPORTED_MODULE_30__["AppRoutingModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_34__["TranslateModule"].forRoot({
                    loader: {
                        provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_34__["TranslateLoader"],
                        useFactory: HttpLoaderFactory,
                        deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpClient"]]
                    }
                }),
                _angular_forms__WEBPACK_IMPORTED_MODULE_36__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_36__["FormsModule"],
                _coreui_angular__WEBPACK_IMPORTED_MODULE_29__["AppAsideModule"],
                _coreui_angular__WEBPACK_IMPORTED_MODULE_29__["AppBreadcrumbModule"].forRoot(),
                _coreui_angular__WEBPACK_IMPORTED_MODULE_29__["AppHeaderModule"],
                _coreui_angular__WEBPACK_IMPORTED_MODULE_29__["AppSidebarModule"],
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_22__["PerfectScrollbarModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_31__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_32__["TabsModule"].forRoot(),
                ng2_charts__WEBPACK_IMPORTED_MODULE_33__["ChartsModule"],
                ng2_file_upload__WEBPACK_IMPORTED_MODULE_44__["FileUploadModule"],
                ngx_bootstrap_carousel__WEBPACK_IMPORTED_MODULE_8__["CarouselModule"].forRoot(),
                ngx_bootstrap_collapse__WEBPACK_IMPORTED_MODULE_7__["CollapseModule"].forRoot(),
                ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_6__["PaginationModule"].forRoot(),
                ngx_bootstrap_popover__WEBPACK_IMPORTED_MODULE_5__["PopoverModule"].forRoot(),
                ngx_bootstrap_progressbar__WEBPACK_IMPORTED_MODULE_4__["ProgressbarModule"].forRoot(),
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_3__["TooltipModule"].forRoot()
            ],
            declarations: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])([
                _app_component__WEBPACK_IMPORTED_MODULE_24__["AppComponent"]
            ], APP_CONTAINERS, [
                _views_error_404_component__WEBPACK_IMPORTED_MODULE_25__["P404Component"],
                _views_error_403_component__WEBPACK_IMPORTED_MODULE_90__["P403Component"],
                _views_error_500_component__WEBPACK_IMPORTED_MODULE_26__["P500Component"],
                _views_login_login_component__WEBPACK_IMPORTED_MODULE_27__["LoginComponent"],
                _Admin_Portal_update_receiver_flip_card_front__WEBPACK_IMPORTED_MODULE_78__["FlipCardFrontComponent"],
                _Admin_Portal_update_receiver_flip_card_back__WEBPACK_IMPORTED_MODULE_79__["FlipCardBackComponent"],
                _views_register_register_component__WEBPACK_IMPORTED_MODULE_28__["RegisterComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_37__["HomeComponent"],
                _nav_nav_component__WEBPACK_IMPORTED_MODULE_39__["NavComponent"],
                _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_41__["ContactUsComponent"],
                _new_partner_new_partner_component__WEBPACK_IMPORTED_MODULE_42__["NewPartnerComponent"],
                _upload_upload_component__WEBPACK_IMPORTED_MODULE_43__["UploadComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_37__["CpChangeComponent"],
                _complaint_complaint_component__WEBPACK_IMPORTED_MODULE_45__["ComplaintComponent"],
                _downloads_operations_downloads_operations_component__WEBPACK_IMPORTED_MODULE_46__["DownloadsOperationsComponent"],
                _help_help_component__WEBPACK_IMPORTED_MODULE_47__["HelpComponent"],
                _dashboard_wbu_dashboard_wbu_component__WEBPACK_IMPORTED_MODULE_50__["DashboardWBUComponent"],
                _details_details_component__WEBPACK_IMPORTED_MODULE_52__["DetailsComponent"],
                _display_invoice_display_invoice_component__WEBPACK_IMPORTED_MODULE_53__["DisplayInvoiceComponent"],
                _Admin_Portal_portal_admin_portal_admin_component__WEBPACK_IMPORTED_MODULE_55__["PortalAdminComponent"],
                _Admin_Portal_approve_registration_approve_registration_component__WEBPACK_IMPORTED_MODULE_56__["ApproveRegistrationComponent"],
                _Admin_Portal_contact_admin_contact_admin_component__WEBPACK_IMPORTED_MODULE_57__["ContactAdminComponent"],
                _Admin_Portal_virtual_account_virtual_account_component__WEBPACK_IMPORTED_MODULE_58__["VirtualAccountComponent"],
                _Admin_Portal_role_access_role_access_component__WEBPACK_IMPORTED_MODULE_61__["RoleAccessComponent"],
                _views_forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_54__["ForgotPasswordComponent"],
                _Admin_Portal_history_history_component__WEBPACK_IMPORTED_MODULE_59__["HistoryComponent"],
                _Admin_Portal_update_receiver_update_receiver_component__WEBPACK_IMPORTED_MODULE_60__["UpdateReceiverComponent"],
                _Admin_Portal_role_view_role_view_component__WEBPACK_IMPORTED_MODULE_62__["RoleViewComponent"],
                _Admin_Portal_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_63__["NewRoleComponent"],
                _register_user_register_user_component__WEBPACK_IMPORTED_MODULE_66__["RegisterUserComponent"],
                _product_catalog_product_catalog_component__WEBPACK_IMPORTED_MODULE_67__["ProductCatalogComponent"],
                _product_catalog_voice_voice_component__WEBPACK_IMPORTED_MODULE_68__["VoiceComponent"],
                _product_catalog_data_data_component__WEBPACK_IMPORTED_MODULE_69__["DataComponent"],
                _product_catalog_capacity_capacity_component__WEBPACK_IMPORTED_MODULE_70__["CapacityComponent"],
                _product_catalog_colocation_colocation_component__WEBPACK_IMPORTED_MODULE_71__["ColocationComponent"],
                _admin_routing_admin_routing_component__WEBPACK_IMPORTED_MODULE_73__["AdminRoutingComponent"],
                _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_74__["SidebarComponent"],
                _Admin_Portal_registration_details_registration_details_component__WEBPACK_IMPORTED_MODULE_75__["RegistrationDetailsComponent"],
                _product_catalog_capacity_catalog_capacity_catalog_component__WEBPACK_IMPORTED_MODULE_76__["CapacityCatalogComponent"],
                _Admin_Portal_registration_action_registration_action_component__WEBPACK_IMPORTED_MODULE_77__["RegistrationActionComponent"],
                _Admin_Portal_orders_orders_component__WEBPACK_IMPORTED_MODULE_81__["OrdersComponent"],
                _email_confirmation_email_confirmation_component__WEBPACK_IMPORTED_MODULE_88__["EmailConfirmationComponent"],
                _email_reset_email_reset_component__WEBPACK_IMPORTED_MODULE_83__["EmailResetComponent"],
                _Admin_Portal_assets_assets_component__WEBPACK_IMPORTED_MODULE_89__["AssetsComponent"],
                _views_password_expiry_password_expiry_component__WEBPACK_IMPORTED_MODULE_86__["PasswordExpiryComponent"],
                _views_email_approved_email_approved_component__WEBPACK_IMPORTED_MODULE_87__["EmailApprovedComponent"],
                _user_setting_user_setting_component__WEBPACK_IMPORTED_MODULE_91__["UserSettingComponent"],
            ]),
            providers: [{
                    provide: _angular_common__WEBPACK_IMPORTED_MODULE_18__["LocationStrategy"],
                    useClass: _angular_common__WEBPACK_IMPORTED_MODULE_18__["HashLocationStrategy"]
                }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_24__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: routes, AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _details_details_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./details/details.component */ "./src/app/details/details.component.ts");
/* harmony import */ var _downloads_operations_downloads_operations_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./downloads-operations/downloads-operations.component */ "./src/app/downloads-operations/downloads-operations.component.ts");
/* harmony import */ var _complaint_complaint_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./complaint/complaint.component */ "./src/app/complaint/complaint.component.ts");
/* harmony import */ var _upload_upload_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./upload/upload.component */ "./src/app/upload/upload.component.ts");
/* harmony import */ var _new_partner_new_partner_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./new-partner/new-partner.component */ "./src/app/new-partner/new-partner.component.ts");
/* harmony import */ var _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contact-us/contact-us.component */ "./src/app/contact-us/contact-us.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _views_error_404_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./views/error/404.component */ "./src/app/views/error/404.component.ts");
/* harmony import */ var _views_error_500_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./views/error/500.component */ "./src/app/views/error/500.component.ts");
/* harmony import */ var _views_login_login_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./views/login/login.component */ "./src/app/views/login/login.component.ts");
/* harmony import */ var _containers_main_layout_main_layout_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./containers/main-layout/main-layout.component */ "./src/app/containers/main-layout/main-layout.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _help_help_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./help/help.component */ "./src/app/help/help.component.ts");
/* harmony import */ var _dashboard_wbu_dashboard_wbu_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./dashboard-wbu/dashboard-wbu.component */ "./src/app/dashboard-wbu/dashboard-wbu.component.ts");
/* harmony import */ var _display_invoice_display_invoice_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./display-invoice/display-invoice.component */ "./src/app/display-invoice/display-invoice.component.ts");
/* harmony import */ var _views_forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./views/forgot-password/forgot-password.component */ "./src/app/views/forgot-password/forgot-password.component.ts");
/* harmony import */ var _Admin_Portal_approve_registration_approve_registration_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./Admin-Portal/approve-registration/approve-registration.component */ "./src/app/Admin-Portal/approve-registration/approve-registration.component.ts");
/* harmony import */ var _Admin_Portal_contact_admin_contact_admin_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./Admin-Portal/contact-admin/contact-admin.component */ "./src/app/Admin-Portal/contact-admin/contact-admin.component.ts");
/* harmony import */ var _Admin_Portal_portal_admin_portal_admin_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./Admin-Portal/portal-admin/portal-admin.component */ "./src/app/Admin-Portal/portal-admin/portal-admin.component.ts");
/* harmony import */ var _Admin_Portal_virtual_account_virtual_account_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./Admin-Portal/virtual-account/virtual-account.component */ "./src/app/Admin-Portal/virtual-account/virtual-account.component.ts");
/* harmony import */ var _Admin_Portal_update_receiver_update_receiver_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./Admin-Portal/update-receiver/update-receiver.component */ "./src/app/Admin-Portal/update-receiver/update-receiver.component.ts");
/* harmony import */ var _Admin_Portal_history_history_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./Admin-Portal/history/history.component */ "./src/app/Admin-Portal/history/history.component.ts");
/* harmony import */ var _Admin_Portal_role_access_role_access_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./Admin-Portal/role-access/role-access.component */ "./src/app/Admin-Portal/role-access/role-access.component.ts");
/* harmony import */ var _Admin_Portal_role_view_role_view_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./Admin-Portal/role-view/role-view.component */ "./src/app/Admin-Portal/role-view/role-view.component.ts");
/* harmony import */ var _Admin_Portal_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./Admin-Portal/new-role/new-role.component */ "./src/app/Admin-Portal/new-role/new-role.component.ts");
/* harmony import */ var _register_user_register_user_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./register-user/register-user.component */ "./src/app/register-user/register-user.component.ts");
/* harmony import */ var _product_catalog_product_catalog_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./product-catalog/product-catalog.component */ "./src/app/product-catalog/product-catalog.component.ts");
/* harmony import */ var _product_catalog_voice_voice_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./product_catalog/voice/voice.component */ "./src/app/product_catalog/voice/voice.component.ts");
/* harmony import */ var _product_catalog_capacity_capacity_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./product_catalog/capacity/capacity.component */ "./src/app/product_catalog/capacity/capacity.component.ts");
/* harmony import */ var _Admin_Portal_registration_details_registration_details_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./Admin-Portal/registration-details/registration-details.component */ "./src/app/Admin-Portal/registration-details/registration-details.component.ts");
/* harmony import */ var _product_catalog_capacity_catalog_capacity_catalog_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./product_catalog/capacity-catalog/capacity-catalog.component */ "./src/app/product_catalog/capacity-catalog/capacity-catalog.component.ts");
/* harmony import */ var _Admin_Portal_registration_action_registration_action_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./Admin-Portal/registration-action/registration-action.component */ "./src/app/Admin-Portal/registration-action/registration-action.component.ts");
/* harmony import */ var _email_confirmation_email_confirmation_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./email-confirmation/email-confirmation.component */ "./src/app/email-confirmation/email-confirmation.component.ts");
/* harmony import */ var _email_reset_email_reset_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./email-reset/email-reset.component */ "./src/app/email-reset/email-reset.component.ts");
/* harmony import */ var _views_password_expiry_password_expiry_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./views/password-expiry/password-expiry.component */ "./src/app/views/password-expiry/password-expiry.component.ts");
/* harmony import */ var _views_email_approved_email_approved_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./views/email-approved/email-approved.component */ "./src/app/views/email-approved/email-approved.component.ts");
/* harmony import */ var _product_catalog_data_data_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./product_catalog/data/data.component */ "./src/app/product_catalog/data/data.component.ts");
/* harmony import */ var _product_catalog_colocation_colocation_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./product_catalog/colocation/colocation.component */ "./src/app/product_catalog/colocation/colocation.component.ts");
/* harmony import */ var _Admin_Portal_orders_orders_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./Admin-Portal/orders/orders.component */ "./src/app/Admin-Portal/orders/orders.component.ts");
/* harmony import */ var _Admin_Portal_assets_assets_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./Admin-Portal/assets/assets.component */ "./src/app/Admin-Portal/assets/assets.component.ts");
/* harmony import */ var _user_setting_user_setting_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./user-setting/user-setting.component */ "./src/app/user-setting/user-setting.component.ts");









// Import Containers


































var routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
    },
    {
        path: '404',
        component: _views_error_404_component__WEBPACK_IMPORTED_MODULE_9__["P404Component"],
        data: {
            title: 'Page 404'
        }
    },
    {
        path: '500',
        component: _views_error_500_component__WEBPACK_IMPORTED_MODULE_10__["P500Component"],
        data: {
            title: 'Page 500'
        }
    },
    {
        path: 'login',
        component: _views_login_login_component__WEBPACK_IMPORTED_MODULE_11__["LoginComponent"],
        data: {
            title: 'Login Page'
        }
    },
    {
        path: 'register',
        component: _register_user_register_user_component__WEBPACK_IMPORTED_MODULE_27__["RegisterUserComponent"],
        data: {
            title: 'Register Page'
        }
    },
    {
        path: 'forgot-password',
        component: _views_forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_17__["ForgotPasswordComponent"]
    },
    {
        path: 'partner',
        component: _new_partner_new_partner_component__WEBPACK_IMPORTED_MODULE_5__["NewPartnerComponent"]
    },
    {
        path: 'email-confirmation',
        component: _email_confirmation_email_confirmation_component__WEBPACK_IMPORTED_MODULE_34__["EmailConfirmationComponent"]
    },
    {
        path: 'email-reset',
        component: _email_reset_email_reset_component__WEBPACK_IMPORTED_MODULE_35__["EmailResetComponent"]
    },
    {
        path: 'password-expiry',
        component: _views_password_expiry_password_expiry_component__WEBPACK_IMPORTED_MODULE_36__["PasswordExpiryComponent"]
    },
    {
        path: 'email-approved',
        component: _views_email_approved_email_approved_component__WEBPACK_IMPORTED_MODULE_37__["EmailApprovedComponent"]
    },
    // {
    //   path:'nav',
    //   component:NavComponent,
    // },
    // {
    //   path:'contact',
    //   component:ContactUsComponent,
    // },
    // {
    //   path:'Dashboard',
    //   component:DashboardComponent,
    // },
    // {
    //   path: '',
    //   component: SidebarComponent,
    //   data: {
    //     title: 'Home'
    //   }
    //   children: [
    //     {
    //       path: 'base',
    //       loadChildren: () => import('./views/base/base.module').then(m => m.BaseModule)
    //     },
    //     {
    //       path: 'buttons',
    //       loadChildren: () => import('./views/buttons/buttons.module').then(m => m.ButtonsModule)
    //     },
    //     {
    //       path: 'charts',
    //       loadChildren: () => import('./views/chartjs/chartjs.module').then(m => m.ChartJSModule)
    //     },
    //     {
    //       path: 'dashboard',
    //       loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
    //     },
    //     {
    //       path: 'icons',
    //       loadChildren: () => import('./views/icons/icons.module').then(m => m.IconsModule)
    //     },
    //     {
    //       path: 'notifications',
    //       loadChildren: () => import('./views/notifications/notifications.module').then(m => m.NotificationsModule)
    //     },
    //     {
    //       path: 'theme',
    //       loadChildren: () => import('./views/theme/theme.module').then(m => m.ThemeModule)
    //     },
    //     {
    //       path: 'widgets',
    //       loadChildren: () => import('./views/widgets/widgets.module').then(m => m.WidgetsModule)
    //     }
    //   ]
    // },
    {
        path: '',
        component: _containers_main_layout_main_layout_component__WEBPACK_IMPORTED_MODULE_12__["MainLayoutComponent"],
        children: [
            {
                path: 'home',
                component: _home_home_component__WEBPACK_IMPORTED_MODULE_13__["HomeComponent"]
            },
            {
                path: 'contact',
                component: _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_6__["ContactUsComponent"],
            },
            {
                path: 'upload',
                component: _upload_upload_component__WEBPACK_IMPORTED_MODULE_4__["UploadComponent"],
            },
            {
                path: 'home',
                component: _home_home_component__WEBPACK_IMPORTED_MODULE_13__["CpChangeComponent"],
            },
            {
                path: 'complaint',
                component: _complaint_complaint_component__WEBPACK_IMPORTED_MODULE_3__["ComplaintComponent"],
            },
            {
                path: 'download',
                component: _downloads_operations_downloads_operations_component__WEBPACK_IMPORTED_MODULE_2__["DownloadsOperationsComponent"],
            },
            {
                path: 'display-invoice',
                component: _display_invoice_display_invoice_component__WEBPACK_IMPORTED_MODULE_16__["DisplayInvoiceComponent"],
            },
            {
                path: 'help',
                component: _help_help_component__WEBPACK_IMPORTED_MODULE_14__["HelpComponent"],
            },
            {
                path: 'dashboard',
                component: _dashboard_wbu_dashboard_wbu_component__WEBPACK_IMPORTED_MODULE_15__["DashboardWBUComponent"],
            },
            {
                path: 'details',
                component: _details_details_component__WEBPACK_IMPORTED_MODULE_1__["DetailsComponent"]
            },
            {
                path: 'portal-admin',
                component: _Admin_Portal_portal_admin_portal_admin_component__WEBPACK_IMPORTED_MODULE_20__["PortalAdminComponent"]
            },
            {
                path: 'approve-registration',
                component: _Admin_Portal_approve_registration_approve_registration_component__WEBPACK_IMPORTED_MODULE_18__["ApproveRegistrationComponent"]
            },
            {
                path: 'registration-action/:id',
                component: _Admin_Portal_registration_action_registration_action_component__WEBPACK_IMPORTED_MODULE_33__["RegistrationActionComponent"]
            },
            {
                path: 'registration-details',
                component: _Admin_Portal_registration_details_registration_details_component__WEBPACK_IMPORTED_MODULE_31__["RegistrationDetailsComponent"]
            },
            {
                path: 'contact-admin',
                component: _Admin_Portal_contact_admin_contact_admin_component__WEBPACK_IMPORTED_MODULE_19__["ContactAdminComponent"]
            },
            {
                path: 'virtual-account',
                component: _Admin_Portal_virtual_account_virtual_account_component__WEBPACK_IMPORTED_MODULE_21__["VirtualAccountComponent"]
            },
            {
                path: 'role-access',
                component: _Admin_Portal_role_access_role_access_component__WEBPACK_IMPORTED_MODULE_24__["RoleAccessComponent"]
            },
            {
                path: 'update-receiver',
                component: _Admin_Portal_update_receiver_update_receiver_component__WEBPACK_IMPORTED_MODULE_22__["UpdateReceiverComponent"]
            },
            {
                path: 'history',
                component: _Admin_Portal_history_history_component__WEBPACK_IMPORTED_MODULE_23__["HistoryComponent"]
            },
            {
                path: 'role-view',
                component: _Admin_Portal_role_view_role_view_component__WEBPACK_IMPORTED_MODULE_25__["RoleViewComponent"]
            },
            {
                path: 'new-role',
                component: _Admin_Portal_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_26__["NewRoleComponent"]
            },
            {
                path: 'product-catalog',
                component: _product_catalog_product_catalog_component__WEBPACK_IMPORTED_MODULE_28__["ProductCatalogComponent"]
            },
            {
                path: 'voice',
                component: _product_catalog_voice_voice_component__WEBPACK_IMPORTED_MODULE_29__["VoiceComponent"]
            },
            {
                path: 'capacity',
                component: _product_catalog_capacity_capacity_component__WEBPACK_IMPORTED_MODULE_30__["CapacityComponent"]
            },
            {
                path: 'data',
                component: _product_catalog_data_data_component__WEBPACK_IMPORTED_MODULE_38__["DataComponent"]
            },
            {
                path: 'colocation',
                component: _product_catalog_colocation_colocation_component__WEBPACK_IMPORTED_MODULE_39__["ColocationComponent"]
            },
            {
                path: 'product-catalog/:name',
                component: _product_catalog_capacity_catalog_capacity_catalog_component__WEBPACK_IMPORTED_MODULE_32__["CapacityCatalogComponent"]
            },
            {
                path: 'orders',
                component: _Admin_Portal_orders_orders_component__WEBPACK_IMPORTED_MODULE_40__["OrdersComponent"]
            },
            {
                path: 'assets',
                component: _Admin_Portal_assets_assets_component__WEBPACK_IMPORTED_MODULE_41__["AssetsComponent"]
            },
            {
                path: 'user-setting',
                component: _user_setting_user_setting_component__WEBPACK_IMPORTED_MODULE_42__["UserSettingComponent"]
            },
        ]
    },
    { path: '**', component: _views_error_404_component__WEBPACK_IMPORTED_MODULE_9__["P404Component"] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_7__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/complaint/complaint.component.css":
/*!***************************************************!*\
  !*** ./src/app/complaint/complaint.component.css ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header {\r\n  color:white;\r\n  font-size: 27px;\r\n  font: bold;\r\n  padding: 10px;\r\n  margin-top: 15vh;\r\n  max-width: 100%;\r\n}\r\n\r\n.bigicon {\r\n  font-size: 35px;\r\n  color:  #FF375D;}\r\n\r\n.costom-contact{\r\n      margin: auto;\r\n  }\r\n\r\n.btn-lg{\r\n      background-color:#FF375D ;\r\n    margin-right: 26vh;\r\n  }\r\n\r\n.bb{\r\n      background-color: #4F008C;\r\n\r\n  }\r\n\r\n/* .nav-item{\r\n      font: bold;\r\n      color: #FF375D;\r\n  } */\r\n\r\n.costom-con {\r\n  color: white;     \r\n  font-size: larger;\r\n  margin-left: 40vh;\r\n\r\n}\r\n\r\n.sub{\r\n\r\n  margin-right: 30vh;\r\n}\r\n\r\n.btn-cancel{\r\n  background-color: #FF375D;\r\n  color: aliceblue;\r\n}\r\n\r\n.btn-send{\r\n  background-color:#FF375D;\r\n  color: aliceblue;\r\n}\r\n\r\n.mb-0{\r\n  margin-left: -7%;\r\n  font-weight: 600;\r\n  line-height: 1.5;\r\n  color: #32325d;\r\n  margin-top: -4%;\r\n }\r\n\r\n.col-lg-000{\r\n  margin-top: 2rem;\r\n  max-width: 90.33333%;\r\n  flex: 0 0 33.33333%;\r\n  position: relative;\r\n  width: 100%;\r\n  min-height: 1px;\r\n  padding-right: 15px;\r\n  padding-left: 15px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcGxhaW50L2NvbXBsYWludC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBVztFQUNYLGVBQWU7RUFDZixVQUFVO0VBQ1YsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixlQUFlO0FBQ2pCOztBQUVBO0VBQ0UsZUFBZTtFQUNmLGVBQWUsQ0FBQzs7QUFDaEI7TUFDSSxZQUFZO0VBQ2hCOztBQUVBO01BQ0kseUJBQXlCO0lBQzNCLGtCQUFrQjtFQUNwQjs7QUFDQTtNQUNJLHlCQUF5Qjs7RUFFN0I7O0FBQ0E7OztLQUdHOztBQUVMO0VBQ0UsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixpQkFBaUI7O0FBRW5COztBQUNBOztFQUVFLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLHlCQUF5QjtFQUN6QixnQkFBZ0I7QUFDbEI7O0FBQ0E7RUFDRSx3QkFBd0I7RUFDeEIsZ0JBQWdCO0FBQ2xCOztBQUNBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGVBQWU7Q0FDaEI7O0FBQ0E7RUFDQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsV0FBVztFQUNYLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsa0JBQWtCO0FBQ3BCIiwiZmlsZSI6InNyYy9hcHAvY29tcGxhaW50L2NvbXBsYWludC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlciB7XHJcbiAgY29sb3I6d2hpdGU7XHJcbiAgZm9udC1zaXplOiAyN3B4O1xyXG4gIGZvbnQ6IGJvbGQ7XHJcbiAgcGFkZGluZzogMTBweDtcclxuICBtYXJnaW4tdG9wOiAxNXZoO1xyXG4gIG1heC13aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmJpZ2ljb24ge1xyXG4gIGZvbnQtc2l6ZTogMzVweDtcclxuICBjb2xvcjogICNGRjM3NUQ7fVxyXG4gIC5jb3N0b20tY29udGFjdHtcclxuICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gIH1cclxuXHJcbiAgLmJ0bi1sZ3tcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjojRkYzNzVEIDtcclxuICAgIG1hcmdpbi1yaWdodDogMjZ2aDtcclxuICB9XHJcbiAgLmJie1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNEYwMDhDO1xyXG5cclxuICB9XHJcbiAgLyogLm5hdi1pdGVte1xyXG4gICAgICBmb250OiBib2xkO1xyXG4gICAgICBjb2xvcjogI0ZGMzc1RDtcclxuICB9ICovXHJcblxyXG4uY29zdG9tLWNvbiB7XHJcbiAgY29sb3I6IHdoaXRlOyAgICAgXHJcbiAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDQwdmg7XHJcblxyXG59XHJcbi5zdWJ7XHJcblxyXG4gIG1hcmdpbi1yaWdodDogMzB2aDtcclxufVxyXG5cclxuLmJ0bi1jYW5jZWx7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI0ZGMzc1RDtcclxuICBjb2xvcjogYWxpY2VibHVlO1xyXG59XHJcbi5idG4tc2VuZHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiNGRjM3NUQ7XHJcbiAgY29sb3I6IGFsaWNlYmx1ZTtcclxufVxyXG4ubWItMHtcclxuICBtYXJnaW4tbGVmdDogLTclO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICBjb2xvcjogIzMyMzI1ZDtcclxuICBtYXJnaW4tdG9wOiAtNCU7XHJcbiB9XHJcbiAuY29sLWxnLTAwMHtcclxuICBtYXJnaW4tdG9wOiAycmVtO1xyXG4gIG1heC13aWR0aDogOTAuMzMzMzMlO1xyXG4gIGZsZXg6IDAgMCAzMy4zMzMzMyU7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG1pbi1oZWlnaHQ6IDFweDtcclxuICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xyXG4gIHBhZGRpbmctbGVmdDogMTVweDtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/complaint/complaint.component.ts":
/*!**************************************************!*\
  !*** ./src/app/complaint/complaint.component.ts ***!
  \**************************************************/
/*! exports provided: ComplaintComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComplaintComponent", function() { return ComplaintComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





var ComplaintComponent = /** @class */ (function () {
    function ComplaintComponent(router) {
        this.router = router;
        this.technical = false;
        this.general = true;
        this.complain = '';
        this.complaintForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            desc: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])
        });
    }
    ComplaintComponent.prototype.ngOnInit = function () {
    };
    ComplaintComponent.prototype.complainType = function (event) {
        this.complain = event.target.value;
        if (this.complain == "billing") {
            this.technical = false;
            this.general = true;
        }
        if (this.complain == "general") {
            this.technical = true;
            this.general = true;
        }
        if (this.complain == "technical") {
            this.technical = true;
            this.general = false;
        }
    };
    ComplaintComponent.prototype.onSubmit = function () {
        var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            allowOutsideClick: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: function (toast) {
                toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.stopTimer);
                toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.resumeTimer);
            }
        });
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire('Sent', 'Your Complaint has been sent and we will solve your complaint shortly.', 'success');
        this.router.navigate(['home']);
    };
    ComplaintComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
    ]; };
    ComplaintComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-complaint',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./complaint.component.html */ "./node_modules/raw-loader/index.js!./src/app/complaint/complaint.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./complaint.component.css */ "./src/app/complaint/complaint.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], ComplaintComponent);
    return ComplaintComponent;
}());



/***/ }),

/***/ "./src/app/contact-us/contact-us.component.css":
/*!*****************************************************!*\
  !*** ./src/app/contact-us/contact-us.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header {\r\n    color:white;\r\n    font-size: 27px;\r\n    font: bold;\r\n    padding: 10px;\r\n    margin-top: 15vh;\r\n    max-width: 100%;\r\n}\r\n\r\n.bigicon {\r\n    font-size: 35px;\r\n    color:  #FF375D;}\r\n\r\n.costom-contact{\r\n        margin: auto;\r\n    }\r\n\r\n.btn-lg{\r\n        background-color:#FF375D ;\r\n      margin-right: 26vh;\r\n    }\r\n\r\n.bb{\r\n        background-color: #4F008C;\r\n\r\n    }\r\n\r\n/* .nav-item{\r\n        font: bold;\r\n        color: #FF375D;\r\n    } */\r\n\r\n.costom-con {\r\n    color: white;     \r\n    font-size: larger;\r\n    margin-left: 40vh;\r\n\r\n}\r\n\r\n.sub{\r\n\r\n    margin-right: 30vh;\r\n}\r\n\r\n.btn-cancel{\r\n    background-color: #FF375D;\r\n    color: aliceblue;\r\n}\r\n\r\n.btn-send{\r\n    background-color:#4F008C;\r\n    color: aliceblue;\r\n\r\n}\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29udGFjdC11cy9jb250YWN0LXVzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxXQUFXO0lBQ1gsZUFBZTtJQUNmLFVBQVU7SUFDVixhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsZUFBZSxDQUFDOztBQUNoQjtRQUNJLFlBQVk7SUFDaEI7O0FBRUE7UUFDSSx5QkFBeUI7TUFDM0Isa0JBQWtCO0lBQ3BCOztBQUNBO1FBQ0kseUJBQXlCOztJQUU3Qjs7QUFDQTs7O09BR0c7O0FBRVA7SUFDSSxZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLGlCQUFpQjs7QUFFckI7O0FBQ0E7O0lBRUksa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0kseUJBQXlCO0lBQ3pCLGdCQUFnQjtBQUNwQjs7QUFDQTtJQUNJLHdCQUF3QjtJQUN4QixnQkFBZ0I7O0FBRXBCIiwiZmlsZSI6InNyYy9hcHAvY29udGFjdC11cy9jb250YWN0LXVzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyIHtcclxuICAgIGNvbG9yOndoaXRlO1xyXG4gICAgZm9udC1zaXplOiAyN3B4O1xyXG4gICAgZm9udDogYm9sZDtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxNXZoO1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uYmlnaWNvbiB7XHJcbiAgICBmb250LXNpemU6IDM1cHg7XHJcbiAgICBjb2xvcjogICNGRjM3NUQ7fVxyXG4gICAgLmNvc3RvbS1jb250YWN0e1xyXG4gICAgICAgIG1hcmdpbjogYXV0bztcclxuICAgIH1cclxuXHJcbiAgICAuYnRuLWxne1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6I0ZGMzc1RCA7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogMjZ2aDtcclxuICAgIH1cclxuICAgIC5iYntcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNEYwMDhDO1xyXG5cclxuICAgIH1cclxuICAgIC8qIC5uYXYtaXRlbXtcclxuICAgICAgICBmb250OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjRkYzNzVEO1xyXG4gICAgfSAqL1xyXG5cclxuLmNvc3RvbS1jb24ge1xyXG4gICAgY29sb3I6IHdoaXRlOyAgICAgXHJcbiAgICBmb250LXNpemU6IGxhcmdlcjtcclxuICAgIG1hcmdpbi1sZWZ0OiA0MHZoO1xyXG5cclxufVxyXG4uc3Vie1xyXG5cclxuICAgIG1hcmdpbi1yaWdodDogMzB2aDtcclxufVxyXG5cclxuLmJ0bi1jYW5jZWx7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkYzNzVEO1xyXG4gICAgY29sb3I6IGFsaWNlYmx1ZTtcclxufVxyXG4uYnRuLXNlbmR7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiM0RjAwOEM7XHJcbiAgICBjb2xvcjogYWxpY2VibHVlO1xyXG5cclxufVxyXG4gICJdfQ== */");

/***/ }),

/***/ "./src/app/contact-us/contact-us.component.ts":
/*!****************************************************!*\
  !*** ./src/app/contact-us/contact-us.component.ts ***!
  \****************************************************/
/*! exports provided: ContactUsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsComponent", function() { return ContactUsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");






var ContactUsComponent = /** @class */ (function () {
    function ContactUsComponent(router) {
        var _this = this;
        this.router = router;
        this.descriptionLength = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](0);
        this.subjectLength = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](0);
        this.textControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('');
        this.textontrol = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('');
        this.contactForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            companyName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            contactName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            contactNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
        });
        this.textControl.valueChanges.subscribe(function (v) { return _this.descriptionLength.next(v.length); });
        this.textontrol.valueChanges.subscribe(function (s) { return _this.subjectLength.next(s.length); });
    }
    ContactUsComponent.prototype.ngOnInit = function () {
        this.optionsSelect = [
            { value: 'Feedback', label: 'Feedback' },
            { value: 'Report a bug', label: 'Report a bug' },
            { value: 'Feature request', label: 'Feature request' },
            { value: 'Other stuff', label: 'Other stuff' },
        ];
    };
    ContactUsComponent.prototype.onSubmit = function () {
        if (this.contactForm.valid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                allowOutsideClick: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.resumeTimer);
                }
            });
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Sent', 'Your Inquiries has been sent and we will contact you shortly.', 'success');
            this.router.navigate(['home']);
        }
        if (this.contactForm.get('companyName').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Invalid Customer Company Name'
            });
        }
        if (this.contactForm.get('contactName').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Invalid Contact Name'
            });
        }
        if (this.contactForm.get('contactNumber').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Invalid Contact Mobile Number'
            });
        }
        if (this.contactForm.get('email').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Invalid Email Address'
            });
        }
    };
    ContactUsComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
    ]; };
    ContactUsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-contact-us',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./contact-us.component.html */ "./node_modules/raw-loader/index.js!./src/app/contact-us/contact-us.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./contact-us.component.css */ "./src/app/contact-us/contact-us.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], ContactUsComponent);
    return ContactUsComponent;
}());



/***/ }),

/***/ "./src/app/containers/main-layout/main-layout.component.css":
/*!******************************************************************!*\
  !*** ./src/app/containers/main-layout/main-layout.component.css ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".custm-n{\r\n  font-size: larger;\r\n  font-weight: bold;\r\n height: -webkit-max-content;\r\n height: -moz-max-content;\r\n height: max-content;\r\n}\r\n\r\n.color-white{\r\ncolor:white;\r\n}\r\n\r\na:hover {\r\n  border-radius: 25px;\r\n  background-color: #ff375d;\r\n  color:white;\r\n}\r\n\r\n.h3{\r\ncolor: #525f7f;\r\n}\r\n\r\n.h2{\r\ndisplay: inline-block;\r\nopacity: 0.6;\r\nfont-size: 11px;\r\nletter-spacing: 1px;\r\ncolor: #fff;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29udGFpbmVycy9tYWluLWxheW91dC9tYWluLWxheW91dC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsaUJBQWlCO0VBQ2pCLGlCQUFpQjtDQUNsQiwyQkFBbUI7Q0FBbkIsd0JBQW1CO0NBQW5CLG1CQUFtQjtBQUNwQjs7QUFFQTtBQUNBLFdBQVc7QUFDWDs7QUFFQTtFQUNFLG1CQUFtQjtFQUNuQix5QkFBeUI7RUFDekIsV0FBVztBQUNiOztBQUVBO0FBQ0EsY0FBYztBQUNkOztBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCLFlBQVk7QUFDWixlQUFlO0FBQ2YsbUJBQW1CO0FBQ25CLFdBQVc7QUFDWCIsImZpbGUiOiJzcmMvYXBwL2NvbnRhaW5lcnMvbWFpbi1sYXlvdXQvbWFpbi1sYXlvdXQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jdXN0bS1ue1xyXG4gIGZvbnQtc2l6ZTogbGFyZ2VyO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gaGVpZ2h0OiBtYXgtY29udGVudDtcclxufVxyXG5cclxuLmNvbG9yLXdoaXRle1xyXG5jb2xvcjp3aGl0ZTtcclxufVxyXG5cclxuYTpob3ZlciB7XHJcbiAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmYzNzVkO1xyXG4gIGNvbG9yOndoaXRlO1xyXG59XHJcblxyXG4uaDN7XHJcbmNvbG9yOiAjNTI1ZjdmO1xyXG59XHJcbi5oMntcclxuZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5vcGFjaXR5OiAwLjY7XHJcbmZvbnQtc2l6ZTogMTFweDtcclxubGV0dGVyLXNwYWNpbmc6IDFweDtcclxuY29sb3I6ICNmZmY7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/containers/main-layout/main-layout.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/containers/main-layout/main-layout.component.ts ***!
  \*****************************************************************/
/*! exports provided: MainLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainLayoutComponent", function() { return MainLayoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _language_language_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../language/language.helper */ "./src/app/language/language.helper.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var ngx_bootstrap_collapse__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/collapse */ "./node_modules/ngx-bootstrap/collapse/fesm5/ngx-bootstrap-collapse.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var angular_user_idle__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular-user-idle */ "./node_modules/angular-user-idle/fesm5/angular-user-idle.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/fesm5/menu.js");









var MainLayoutComponent = /** @class */ (function () {
    function MainLayoutComponent(renderer, languageHelper, translateService, router, userIdle) {
        this.renderer = renderer;
        this.languageHelper = languageHelper;
        this.translateService = translateService;
        this.router = router;
        this.userIdle = userIdle;
        this._isCollapsed = true;
        this.fullyear = new Date().getFullYear();
        this.getAllLangs();
        this.isRTL = this.languageHelper.currentLang().isRTL;
    }
    Object.defineProperty(MainLayoutComponent.prototype, "username", {
        get: function () {
            return this._username;
        },
        set: function (value) {
            this._username = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MainLayoutComponent.prototype, "lastLogin", {
        get: function () {
            return this._lastLogin;
        },
        set: function (value) {
            this._lastLogin = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MainLayoutComponent.prototype, "isCollapsed", {
        get: function () {
            if (this.collapseRef) {
                // temp fix for "overflow: hidden"
                if (getComputedStyle(this.collapseRef.nativeElement).getPropertyValue('display') === 'flex') {
                    this.renderer.removeStyle(this.collapseRef.nativeElement, 'overflow');
                }
            }
            return this._isCollapsed;
        },
        set: function (value) {
            this._isCollapsed = value;
        },
        enumerable: false,
        configurable: true
    });
    MainLayoutComponent.prototype.openMyMenu = function () {
        this.trigger.toggleMenu();
    };
    MainLayoutComponent.prototype.ngOnInit = function () {
        this.username = localStorage.getItem("username");
        this.lastLogin = localStorage.getItem("lastLogin");
        this.searchForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            searchValue: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.translateService.setDefaultLang('en');
        this.translateService.use(this.languageHelper.currentLang().name);
        this.isRTL = this.languageHelper.currentLang().isRTL;
        this.userIdle.startWatching();
        this.timeStart$ = this.userIdle.onTimerStart().subscribe();
        this.timeOut$ = this.userIdle.onTimeout().subscribe(function () {
            var timer = 10, isTimerStarted = false;
            (function customSwal() {
                // swal({
                //   title: "Time Out!",
                //   text: "Sesstion will expire in " + timer + " seconds",
                //   buttons: {
                //     cancel: true,
                //     confirm: true,
                //   },
                //   timer: !isTimerStarted ? timer * 1000 : 1100,
                // });]
                isTimerStarted = true;
                if (timer) {
                    if (timer == 8) {
                        window.location.reload();
                        this.router.navigateByUrl('\home');
                    }
                    timer--;
                    setTimeout(customSwal, 1000);
                }
            })();
        });
    };
    Object.defineProperty(MainLayoutComponent.prototype, "localStorage", {
        get: function () {
            return localStorage;
        },
        enumerable: false,
        configurable: true
    });
    MainLayoutComponent.prototype.changeLang = function (lang) {
        debugger;
        this.languageHelper.changeLang(lang);
        window.location.reload();
    };
    MainLayoutComponent.prototype.getAllLangs = function () {
        var arr = this.languageHelper.getAllLanguages();
        this.english = arr[0];
        this.arabic = arr[1];
        return this.languageHelper.getAllLanguages();
    };
    MainLayoutComponent.prototype.ngAfterViewChecked = function () {
        this.collapseRef = this.collapse;
    };
    MainLayoutComponent.prototype.portal = function () {
        this.router.navigate(['portal-admin']);
    };
    MainLayoutComponent.prototype.ngOnDestroy = function () {
        this.timeOut$.unsubscribe();
        this.timeStart$.unsubscribe();
    };
    MainLayoutComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Renderer2"] },
        { type: _language_language_helper__WEBPACK_IMPORTED_MODULE_2__["LanguageHelper"] },
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
        { type: angular_user_idle__WEBPACK_IMPORTED_MODULE_7__["UserIdleService"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ViewChild"])(_angular_material_menu__WEBPACK_IMPORTED_MODULE_8__["MatMenuTrigger"]),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_material_menu__WEBPACK_IMPORTED_MODULE_8__["MatMenuTrigger"])
    ], MainLayoutComponent.prototype, "trigger", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ViewChild"])(ngx_bootstrap_collapse__WEBPACK_IMPORTED_MODULE_5__["CollapseDirective"], { read: _angular_core__WEBPACK_IMPORTED_MODULE_4__["ElementRef"], static: false }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", ngx_bootstrap_collapse__WEBPACK_IMPORTED_MODULE_5__["CollapseDirective"])
    ], MainLayoutComponent.prototype, "collapse", void 0);
    MainLayoutComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
            selector: 'app-main-layout',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./main-layout.component.html */ "./node_modules/raw-loader/index.js!./src/app/containers/main-layout/main-layout.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./main-layout.component.css */ "./src/app/containers/main-layout/main-layout.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_4__["Renderer2"], _language_language_helper__WEBPACK_IMPORTED_MODULE_2__["LanguageHelper"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], angular_user_idle__WEBPACK_IMPORTED_MODULE_7__["UserIdleService"]])
    ], MainLayoutComponent);
    return MainLayoutComponent;
}());



/***/ }),

/***/ "./src/app/dashboard-wbu/dashboard-wbu.component.css":
/*!***********************************************************!*\
  !*** ./src/app/dashboard-wbu/dashboard-wbu.component.css ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".font-dashboard{\r\n    background-color:  #4F008C;\r\n    padding-top: 4px;\r\n    padding-bottom: 4px;\r\n\r\n}\r\n.cards{\r\n margin-top: 20px;\r\npadding-top: 10x;\r\npadding-bottom: 33px;\r\nbackground-color :#D5D8DC;\r\nwidth: 10px;\r\nmargin: 10px;\r\npadding-top: 15px;\r\ntext-align: center;\r\nfont-weight: 500;\r\nheight: 80px;\r\ncolor: #ff375d;\r\nborder-radius: 25px;\r\n\r\n}\r\n.chart-wrapper{\r\n    width: 30%;\r\n    height: 100vh;\r\n    margin-top: 50px;\r\n}\r\n.pt-5{\r\n    padding-top: 3rem !important;\r\n}\r\n.h2{\r\n    font-family: inherit;\r\n    font-weight: 600;\r\n    line-height: 1.5;\r\n    font-size: 1.25rem;\r\n}\r\n.graph-align-right{\r\n    margin-right: 300px;\r\n    float: right;\r\n}\r\n.graph-align-left{\r\n    /* float: center; */\r\n    margin-left: 100px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkLXdidS9kYXNoYm9hcmQtd2J1LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSwwQkFBMEI7SUFDMUIsZ0JBQWdCO0lBQ2hCLG1CQUFtQjs7QUFFdkI7QUFDQTtDQUNDLGdCQUFnQjtBQUNqQixnQkFBZ0I7QUFDaEIsb0JBQW9CO0FBQ3BCLHlCQUF5QjtBQUN6QixXQUFXO0FBQ1gsWUFBWTtBQUNaLGlCQUFpQjtBQUNqQixrQkFBa0I7QUFDbEIsZ0JBQWdCO0FBQ2hCLFlBQVk7QUFDWixjQUFjO0FBQ2QsbUJBQW1COztBQUVuQjtBQUNBO0lBQ0ksVUFBVTtJQUNWLGFBQWE7SUFDYixnQkFBZ0I7QUFDcEI7QUFFQTtJQUNJLDRCQUE0QjtBQUNoQztBQUVBO0lBQ0ksb0JBQW9CO0lBQ3BCLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0FBQ3RCO0FBQ0E7SUFDSSxtQkFBbUI7SUFDbkIsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksbUJBQW1CO0lBQ25CLGtCQUFrQjtBQUN0QiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC13YnUvZGFzaGJvYXJkLXdidS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvbnQtZGFzaGJvYXJke1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogICM0RjAwOEM7XHJcbiAgICBwYWRkaW5nLXRvcDogNHB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDRweDtcclxuXHJcbn1cclxuLmNhcmRze1xyXG4gbWFyZ2luLXRvcDogMjBweDtcclxucGFkZGluZy10b3A6IDEweDtcclxucGFkZGluZy1ib3R0b206IDMzcHg7XHJcbmJhY2tncm91bmQtY29sb3IgOiNENUQ4REM7XHJcbndpZHRoOiAxMHB4O1xyXG5tYXJnaW46IDEwcHg7XHJcbnBhZGRpbmctdG9wOiAxNXB4O1xyXG50ZXh0LWFsaWduOiBjZW50ZXI7XHJcbmZvbnQtd2VpZ2h0OiA1MDA7XHJcbmhlaWdodDogODBweDtcclxuY29sb3I6ICNmZjM3NWQ7XHJcbmJvcmRlci1yYWRpdXM6IDI1cHg7XHJcblxyXG59XHJcbi5jaGFydC13cmFwcGVye1xyXG4gICAgd2lkdGg6IDMwJTtcclxuICAgIGhlaWdodDogMTAwdmg7XHJcbiAgICBtYXJnaW4tdG9wOiA1MHB4O1xyXG59XHJcblxyXG4ucHQtNXtcclxuICAgIHBhZGRpbmctdG9wOiAzcmVtICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5oMntcclxuICAgIGZvbnQtZmFtaWx5OiBpbmhlcml0O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgICBmb250LXNpemU6IDEuMjVyZW07XHJcbn1cclxuLmdyYXBoLWFsaWduLXJpZ2h0e1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAzMDBweDtcclxuICAgIGZsb2F0OiByaWdodDtcclxufVxyXG4uZ3JhcGgtYWxpZ24tbGVmdHtcclxuICAgIC8qIGZsb2F0OiBjZW50ZXI7ICovXHJcbiAgICBtYXJnaW4tbGVmdDogMTAwcHg7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/dashboard-wbu/dashboard-wbu.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/dashboard-wbu/dashboard-wbu.component.ts ***!
  \**********************************************************/
/*! exports provided: DashboardWBUComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardWBUComponent", function() { return DashboardWBUComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/fesm2015/valor-software-ng2-charts.js");



var DashboardWBUComponent = /** @class */ (function () {
    function DashboardWBUComponent() {
        this.barChartOptions = {
            responsive: true,
        };
        this.barChartLabels = ['General', 'Technical', 'FD'];
        this.barChartType = 'bar';
        this.barChartLegend = true;
        this.barChartPlugins = [];
        this.barChartData = [
            { data: [45, 37, 66, 70, 46, 33], label: 'Total per Complain type' }
        ];
        this.pieChartOptions = {
            responsive: true,
        };
        this.pieChartLabels = [['FD'], ['Technical'], 'General'];
        this.pieChartData = [30, 50, 20];
        this.pieChartType = 'pie';
        this.pieChartLegend = true;
        this.pieChartPlugins = [];
        Object(ng2_charts__WEBPACK_IMPORTED_MODULE_2__["monkeyPatchChartJsTooltip"])();
        Object(ng2_charts__WEBPACK_IMPORTED_MODULE_2__["monkeyPatchChartJsLegend"])();
    }
    DashboardWBUComponent.prototype.ngOnInit = function () {
    };
    DashboardWBUComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard-wbu',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./dashboard-wbu.component.html */ "./node_modules/raw-loader/index.js!./src/app/dashboard-wbu/dashboard-wbu.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./dashboard-wbu.component.css */ "./src/app/dashboard-wbu/dashboard-wbu.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], DashboardWBUComponent);
    return DashboardWBUComponent;
}());



/***/ }),

/***/ "./src/app/dataModels/decisionDto.mode.ts":
/*!************************************************!*\
  !*** ./src/app/dataModels/decisionDto.mode.ts ***!
  \************************************************/
/*! exports provided: decisionDto */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "decisionDto", function() { return decisionDto; });
var decisionDto = /** @class */ (function () {
    function decisionDto() {
    }
    return decisionDto;
}());



/***/ }),

/***/ "./src/app/dataModels/login.model.ts":
/*!*******************************************!*\
  !*** ./src/app/dataModels/login.model.ts ***!
  \*******************************************/
/*! exports provided: loginDto */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loginDto", function() { return loginDto; });
var loginDto = /** @class */ (function () {
    function loginDto() {
    }
    return loginDto;
}());



/***/ }),

/***/ "./src/app/dataModels/newpartner.model.ts":
/*!************************************************!*\
  !*** ./src/app/dataModels/newpartner.model.ts ***!
  \************************************************/
/*! exports provided: partnerDto */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "partnerDto", function() { return partnerDto; });
var partnerDto = /** @class */ (function () {
    function partnerDto() {
    }
    return partnerDto;
}());



/***/ }),

/***/ "./src/app/dataModels/register.model.ts":
/*!**********************************************!*\
  !*** ./src/app/dataModels/register.model.ts ***!
  \**********************************************/
/*! exports provided: registerDto */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "registerDto", function() { return registerDto; });
var registerDto = /** @class */ (function () {
    function registerDto() {
    }
    return registerDto;
}());



/***/ }),

/***/ "./src/app/dataModels/registeredMembersDto.model.ts":
/*!**********************************************************!*\
  !*** ./src/app/dataModels/registeredMembersDto.model.ts ***!
  \**********************************************************/
/*! exports provided: RegisteredMembersDto */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisteredMembersDto", function() { return RegisteredMembersDto; });
var RegisteredMembersDto = /** @class */ (function () {
    function RegisteredMembersDto() {
    }
    return RegisteredMembersDto;
}());



/***/ }),

/***/ "./src/app/dataModels/uploads.model.ts":
/*!*********************************************!*\
  !*** ./src/app/dataModels/uploads.model.ts ***!
  \*********************************************/
/*! exports provided: uploadDto */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "uploadDto", function() { return uploadDto; });
var uploadDto = /** @class */ (function () {
    function uploadDto() {
    }
    return uploadDto;
}());



/***/ }),

/***/ "./src/app/dataModels/voiceDto.model.ts":
/*!**********************************************!*\
  !*** ./src/app/dataModels/voiceDto.model.ts ***!
  \**********************************************/
/*! exports provided: voiceDto */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "voiceDto", function() { return voiceDto; });
var voiceDto = /** @class */ (function () {
    function voiceDto() {
    }
    return voiceDto;
}());



/***/ }),

/***/ "./src/app/details/details.component.css":
/*!***********************************************!*\
  !*** ./src/app/details/details.component.css ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".bg-gradient-danger{\r\n    background: #4F008c !important;\r\n}\r\n.pt-md-8{\r\n    padding-top: 2rem !important;\r\n}\r\n.h3 .mat-typography{\r\n    font-family: inherit;\r\n    font-weight: 600 !important;\r\n    line-height: 1.5;\r\n    margin-bottom: 0 !important;\r\n    font-size: 1.0625rem;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGV0YWlscy9kZXRhaWxzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSw4QkFBOEI7QUFDbEM7QUFDQTtJQUNJLDRCQUE0QjtBQUNoQztBQUNBO0lBQ0ksb0JBQW9CO0lBQ3BCLDJCQUEyQjtJQUMzQixnQkFBZ0I7SUFDaEIsMkJBQTJCO0lBQzNCLG9CQUFvQjtBQUN4QiIsImZpbGUiOiJzcmMvYXBwL2RldGFpbHMvZGV0YWlscy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJnLWdyYWRpZW50LWRhbmdlcntcclxuICAgIGJhY2tncm91bmQ6ICM0RjAwOGMgIWltcG9ydGFudDtcclxufVxyXG4ucHQtbWQtOHtcclxuICAgIHBhZGRpbmctdG9wOiAycmVtICFpbXBvcnRhbnQ7XHJcbn1cclxuLmgzIC5tYXQtdHlwb2dyYXBoeXtcclxuICAgIGZvbnQtZmFtaWx5OiBpbmhlcml0O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMCAhaW1wb3J0YW50O1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgIG1hcmdpbi1ib3R0b206IDAgIWltcG9ydGFudDtcclxuICAgIGZvbnQtc2l6ZTogMS4wNjI1cmVtO1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/details/details.component.ts":
/*!**********************************************!*\
  !*** ./src/app/details/details.component.ts ***!
  \**********************************************/
/*! exports provided: DetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailsComponent", function() { return DetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/fesm5/table.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/fesm5/paginator.js");




var DetailsComponent = /** @class */ (function () {
    function DetailsComponent() {
        this.filterValues = {};
        this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.displayedColumns = ['complaint', 'type', 'assigned', 'customer', 'status', 'creation', 'close', 'mttr'];
        this.filterSelectObj = [];
        this.filterSelectObj = [
            {
                type: 'Complaint',
                columnProp: 'complaint',
                options: []
            }, {
                type: 'Type',
                columnProp: 'type',
                options: []
            }, {
                type: 'Assigned',
                columnProp: 'assigned',
                options: []
            }, {
                type: 'Customer',
                columnProp: 'customer',
                options: []
            }, {
                type: 'Status',
                columnProp: 'status',
                options: []
            }, {
                type: 'Creation',
                columnProp: 'creation',
                options: []
            }, {
                type: 'Close',
                columnProp: 'close',
                options: []
            }, {
                type: 'MTTR',
                columnProp: 'mttr',
                options: []
            }
        ];
    }
    DetailsComponent.prototype.ngOnInit = function () {
        this.getRemoteData();
        this.dataSource.filterPredicate = this.createFilter();
        // this.onTableUpdate()
    };
    DetailsComponent.prototype.getFilterObject = function (fullObj, key) {
        var uniqChk = [];
        fullObj.filter(function (obj) {
            if (!uniqChk.includes(obj[key])) {
                uniqChk.push(obj[key]);
            }
            return obj;
        });
        return uniqChk;
    };
    DetailsComponent.prototype.onTableUpdate = function () {
        this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](this.ELEMENT_DATA);
        this.dataSource.paginator = this.paginator;
    };
    DetailsComponent.prototype.getRemoteData = function () {
        var _this = this;
        var remoteDummyData = [
            {
                "complaint": "SR-124",
                "type": "Technical",
                "assigned": "WSMC",
                "customer": "Zain",
                "status": "In progress",
                "creation": "01-07-2020",
                "close": "11-07-2020",
                "mttr": "under proceing"
            },
            {
                "complaint": "SR-125",
                "type": "Commercial",
                "assigned": "WSMC",
                "customer": "Mobily",
                "status": "Closed",
                "creation": "02-07-2020",
                "close": "05-08-2020",
                "mttr": "Closed"
            }, {
                "complaint": "SR-127",
                "type": "Technical",
                "assigned": "WSMC",
                "customer": "Jawwy",
                "status": "In progress",
                "creation": "12-07-2020",
                "close": "02-08-2020",
                "mttr": "under proceing"
            }, {
                "complaint": "SR-129",
                "type": "Commercial",
                "assigned": "WSMC",
                "customer": "Zain",
                "status": "In progress",
                "creation": "22-07-2020",
                "close": "21-08-2020",
                "mttr": "Closed"
            }, {
                "complaint": "SR-130",
                "type": "Billing",
                "assigned": "WSMC",
                "customer": "Jawwy",
                "status": "Pending",
                "creation": "04-08-2020",
                "close": "28-08-2020",
                "mttr": "Pending"
            }, {
                "complaint": "SR-131",
                "type": "Technical",
                "assigned": "WSMC",
                "customer": "Zain",
                "status": "In progress",
                "creation": "25-07-2020",
                "close": "06-08-2020",
                "mttr": "Pending"
            }, {
                "complaint": "SR-132",
                "type": "Technical",
                "assigned": "WSMC",
                "customer": "Mobily",
                "status": "In progress",
                "creation": "07-08-2020",
                "close": "23-08-2020",
                "mttr": "under proceing"
            }, {
                "complaint": "SR-133",
                "type": "Billing",
                "assigned": "WSMC",
                "customer": "Zain",
                "status": "In progress",
                "creation": "22-07-2020",
                "close": "14-08-2020",
                "mttr": "under proceing"
            },
        ];
        this.ELEMENT_DATA = remoteDummyData;
        this.dataSource.data = remoteDummyData;
        this.filterSelectObj.filter(function (o) {
            o.options = _this.getFilterObject(remoteDummyData, o.columnProp);
        });
    };
    DetailsComponent.prototype.filterChange = function (filter, event) {
        this.filterValues[filter.columnProp] = event.target.value.trim().toLowerCase();
        this.dataSource.filter = JSON.stringify(this.filterValues);
    };
    DetailsComponent.prototype.createFilter = function () {
        var filterFunction = function (data, filter) {
            var searchTerms = JSON.parse(filter);
            var isFilterSet = false;
            for (var col in searchTerms) {
                if (searchTerms[col].toString() !== '') {
                    isFilterSet = true;
                }
                else {
                    delete searchTerms[col];
                }
            }
            var nameSearch = function () {
                var found = false;
                if (isFilterSet) {
                    var _loop_1 = function (col) {
                        searchTerms[col].trim().toLowerCase().split(' ').forEach(function (word) {
                            if (data[col].toString().toLowerCase().indexOf(word) != -1 && isFilterSet) {
                                found = true;
                            }
                        });
                    };
                    for (var col in searchTerms) {
                        _loop_1(col);
                    }
                    return found;
                }
                else {
                    return true;
                }
            };
            return nameSearch();
        };
        return filterFunction;
    };
    DetailsComponent.prototype.resetFilters = function () {
        this.filterValues = {};
        this.filterSelectObj.forEach(function (value, key) {
            value.modelValue = undefined;
        });
        this.dataSource.filter = "";
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material_paginator__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"], { static: true }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_material_paginator__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], DetailsComponent.prototype, "paginator", void 0);
    DetailsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-details',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./details.component.html */ "./node_modules/raw-loader/index.js!./src/app/details/details.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./details.component.css */ "./src/app/details/details.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], DetailsComponent);
    return DetailsComponent;
}());



/***/ }),

/***/ "./src/app/display-invoice/display-invoice.component.css":
/*!***************************************************************!*\
  !*** ./src/app/display-invoice/display-invoice.component.css ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("::ng-deep .nav-tabs .nav-link.active{\r\n    background :#ffff;\r\n  }\r\n  /* ::ng-deep .nav-tabs .nav-link.active{\r\n  background: #4F008c !important;\r\n  } */\r\n  .form-group.hidden {\r\n  width: 0;\r\n  margin: 0;\r\n  border: none;\r\n  padding: 0;\r\n  }\r\n  .form-group.hidden .form-control{\r\n  display: none;\r\n  }\r\n  ::ng-deep .custom-day {\r\n  text-align: center;\r\n  padding: 0.185rem 0.25rem;\r\n  display: inline-block;\r\n  height: 2rem;\r\n  width: 2rem;\r\n  }\r\n  .mb-0{\r\n  font-size: 1.0625rem;\r\n  font-weight: 600;\r\n  }\r\n  .mb-1{\r\n    margin-left: 4rem;\r\n    font-size: x-large;\r\n  }\r\n  .header{\r\n  background: #f7fafc !important;\r\n  }\r\n  .form-control-label{\r\n  font-weight: 600;\r\n  }\r\n  .col-lg-4{\r\n  margin-left: 5rem;\r\n  margin-top: 3rem;\r\n  }\r\n  ::ng-deep .custom-day.focused {\r\n  background-color: #e6e6e6;\r\n  }\r\n  ::ng-deep .custom-day.range, ::ng-deep .custom-day:hover {\r\n  background-color: rgb(2, 117, 216);\r\n  color: white;\r\n  }\r\n  ::ng-deep .custom-day.faded {\r\n  background-color: rgba(2, 117, 216, 0.5);\r\n  }\r\n  .btn-outline-secondary {\r\n    color: #23282c;\r\n    border: 1px solid #cad1d7;\r\n    background-color: #fff;\r\n  }\r\n  ::ng-deep .tab-content .tab-pane {\r\n  padding: 0rem !important;\r\n  }\r\n  ::ng-deep .justify-content-start {\r\n  padding-left: 15px !important;\r\n  }\r\n  .submit{\r\n  color: #fff;\r\n  background-color: #FF375E;\r\n  border-color: #FF375E;\r\n  font-weight: 600;\r\n  text-align: center;\r\n  overflow: visible;\r\n    white-space: nowrap;\r\n    vertical-align: middle;\r\n    -webkit-user-select: none;\r\n    -moz-user-select: none;\r\n    -ms-user-select: none;\r\n    user-select: none;\r\n    border: 1px solid transparent;\r\n    padding: 0.625rem 1.25rem;\r\n    font-size: 1rem;\r\n    line-height: 1.5;\r\n    border-radius: 0.375rem;\r\n    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;\r\n  box-shadow: 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08);\r\n  }\r\n  .card{\r\n    background: #f7fafc;\r\n  }\r\n  .col-lg-8 {\r\n    padding-top: 5rem;\r\n  }\r\n  .icon-shape{\r\n    padding-right: 100px;\r\n    padding-top: 100px;\r\n    margin-top: -5rem;\r\n    background-color: currentcolor;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGlzcGxheS1pbnZvaWNlL2Rpc3BsYXktaW52b2ljZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0VBQ25CO0VBQ0E7O0tBRUc7RUFDSDtFQUNBLFFBQVE7RUFDUixTQUFTO0VBQ1QsWUFBWTtFQUNaLFVBQVU7RUFDVjtFQUNBO0VBQ0EsYUFBYTtFQUNiO0VBQ0E7RUFDQSxrQkFBa0I7RUFDbEIseUJBQXlCO0VBQ3pCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osV0FBVztFQUNYO0VBRUE7RUFDQSxvQkFBb0I7RUFDcEIsZ0JBQWdCO0VBQ2hCO0VBRUE7SUFDRSxpQkFBaUI7SUFDakIsa0JBQWtCO0VBQ3BCO0VBQ0E7RUFDQSw4QkFBOEI7RUFDOUI7RUFDQTtFQUNBLGdCQUFnQjtFQUNoQjtFQUVBO0VBQ0EsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQjtFQUNBO0VBQ0EseUJBQXlCO0VBQ3pCO0VBQ0E7RUFDQSxrQ0FBa0M7RUFDbEMsWUFBWTtFQUNaO0VBQ0E7RUFDQSx3Q0FBd0M7RUFDeEM7RUFDQTtJQUNFLGNBQWM7SUFDZCx5QkFBeUI7SUFDekIsc0JBQXNCO0VBQ3hCO0VBRUE7RUFDQSx3QkFBd0I7RUFDeEI7RUFDQTtFQUNBLDZCQUE2QjtFQUM3QjtFQUVBO0VBQ0EsV0FBVztFQUNYLHlCQUF5QjtFQUN6QixxQkFBcUI7RUFDckIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixpQkFBaUI7SUFDZixtQkFBbUI7SUFDbkIsc0JBQXNCO0lBQ3RCLHlCQUF5QjtJQUN6QixzQkFBc0I7SUFDdEIscUJBQXFCO0lBQ3JCLGlCQUFpQjtJQUNqQiw2QkFBNkI7SUFDN0IseUJBQXlCO0lBQ3pCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsdUJBQXVCO0lBQ3ZCLHFJQUFxSTtFQUN2SSwyRUFBMkU7RUFDM0U7RUFFQTtJQUNFLG1CQUFtQjtFQUNyQjtFQUVBO0lBQ0UsaUJBQWlCO0VBQ25CO0VBRUE7SUFDRSxvQkFBb0I7SUFDcEIsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQiw4QkFBOEI7RUFDaEMiLCJmaWxlIjoic3JjL2FwcC9kaXNwbGF5LWludm9pY2UvZGlzcGxheS1pbnZvaWNlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6Om5nLWRlZXAgLm5hdi10YWJzIC5uYXYtbGluay5hY3RpdmV7XHJcbiAgICBiYWNrZ3JvdW5kIDojZmZmZjtcclxuICB9XHJcbiAgLyogOjpuZy1kZWVwIC5uYXYtdGFicyAubmF2LWxpbmsuYWN0aXZle1xyXG4gIGJhY2tncm91bmQ6ICM0RjAwOGMgIWltcG9ydGFudDtcclxuICB9ICovXHJcbiAgLmZvcm0tZ3JvdXAuaGlkZGVuIHtcclxuICB3aWR0aDogMDtcclxuICBtYXJnaW46IDA7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIHBhZGRpbmc6IDA7XHJcbiAgfVxyXG4gIC5mb3JtLWdyb3VwLmhpZGRlbiAuZm9ybS1jb250cm9se1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG4gIDo6bmctZGVlcCAuY3VzdG9tLWRheSB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHBhZGRpbmc6IDAuMTg1cmVtIDAuMjVyZW07XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIGhlaWdodDogMnJlbTtcclxuICB3aWR0aDogMnJlbTtcclxuICB9XHJcbiAgXHJcbiAgLm1iLTB7XHJcbiAgZm9udC1zaXplOiAxLjA2MjVyZW07XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB9XHJcblxyXG4gIC5tYi0xe1xyXG4gICAgbWFyZ2luLWxlZnQ6IDRyZW07XHJcbiAgICBmb250LXNpemU6IHgtbGFyZ2U7XHJcbiAgfVxyXG4gIC5oZWFkZXJ7XHJcbiAgYmFja2dyb3VuZDogI2Y3ZmFmYyAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAuZm9ybS1jb250cm9sLWxhYmVse1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5jb2wtbGctNHtcclxuICBtYXJnaW4tbGVmdDogNXJlbTtcclxuICBtYXJnaW4tdG9wOiAzcmVtO1xyXG4gIH1cclxuICA6Om5nLWRlZXAgLmN1c3RvbS1kYXkuZm9jdXNlZCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U2ZTZlNjtcclxuICB9XHJcbiAgOjpuZy1kZWVwIC5jdXN0b20tZGF5LnJhbmdlLCA6Om5nLWRlZXAgLmN1c3RvbS1kYXk6aG92ZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyLCAxMTcsIDIxNik7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIH1cclxuICA6Om5nLWRlZXAgLmN1c3RvbS1kYXkuZmFkZWQge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMiwgMTE3LCAyMTYsIDAuNSk7XHJcbiAgfVxyXG4gIC5idG4tb3V0bGluZS1zZWNvbmRhcnkge1xyXG4gICAgY29sb3I6ICMyMzI4MmM7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2FkMWQ3O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICB9XHJcbiAgXHJcbiAgOjpuZy1kZWVwIC50YWItY29udGVudCAudGFiLXBhbmUge1xyXG4gIHBhZGRpbmc6IDByZW0gIWltcG9ydGFudDtcclxuICB9XHJcbiAgOjpuZy1kZWVwIC5qdXN0aWZ5LWNvbnRlbnQtc3RhcnQge1xyXG4gIHBhZGRpbmctbGVmdDogMTVweCAhaW1wb3J0YW50O1xyXG4gIH1cclxuICBcclxuICAuc3VibWl0e1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNGRjM3NUU7XHJcbiAgYm9yZGVyLWNvbG9yOiAjRkYzNzVFO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG92ZXJmbG93OiB2aXNpYmxlO1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgICAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgLW1vei11c2VyLXNlbGVjdDogbm9uZTtcclxuICAgIC1tcy11c2VyLXNlbGVjdDogbm9uZTtcclxuICAgIHVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgdHJhbnNwYXJlbnQ7XHJcbiAgICBwYWRkaW5nOiAwLjYyNXJlbSAxLjI1cmVtO1xyXG4gICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xyXG4gICAgdHJhbnNpdGlvbjogY29sb3IgMC4xNXMgZWFzZS1pbi1vdXQsIGJhY2tncm91bmQtY29sb3IgMC4xNXMgZWFzZS1pbi1vdXQsIGJvcmRlci1jb2xvciAwLjE1cyBlYXNlLWluLW91dCwgYm94LXNoYWRvdyAwLjE1cyBlYXNlLWluLW91dDtcclxuICBib3gtc2hhZG93OiAwIDRweCA2cHggcmdiYSg1MCwgNTAsIDkzLCAwLjExKSwgMCAxcHggM3B4IHJnYmEoMCwgMCwgMCwgMC4wOCk7XHJcbiAgfVxyXG4gIFxyXG4gIC5jYXJke1xyXG4gICAgYmFja2dyb3VuZDogI2Y3ZmFmYztcclxuICB9XHJcblxyXG4gIC5jb2wtbGctOCB7XHJcbiAgICBwYWRkaW5nLXRvcDogNXJlbTtcclxuICB9XHJcblxyXG4gIC5pY29uLXNoYXBle1xyXG4gICAgcGFkZGluZy1yaWdodDogMTAwcHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTAwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAtNXJlbTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGN1cnJlbnRjb2xvcjtcclxuICB9Il19 */");

/***/ }),

/***/ "./src/app/display-invoice/display-invoice.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/display-invoice/display-invoice.component.ts ***!
  \**************************************************************/
/*! exports provided: DisplayInvoiceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DisplayInvoiceComponent", function() { return DisplayInvoiceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");



var DisplayInvoiceComponent = /** @class */ (function () {
    function DisplayInvoiceComponent(calendar, formatter) {
        this.calendar = calendar;
        this.formatter = formatter;
        this.hoveredDate = null;
        this.invoice = true;
        this.invoiceSearch = false;
        this.fromDate = calendar.getToday();
        this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
    }
    DisplayInvoiceComponent.prototype.onDateSelection = function (date) {
        if (!this.fromDate && !this.toDate) {
            this.fromDate = date;
        }
        else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
            this.toDate = date;
        }
        else {
            this.toDate = null;
            this.fromDate = date;
        }
    };
    DisplayInvoiceComponent.prototype.isHovered = function (date) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
    };
    DisplayInvoiceComponent.prototype.isInside = function (date) {
        return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
    };
    DisplayInvoiceComponent.prototype.isRange = function (date) {
        return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
    };
    DisplayInvoiceComponent.prototype.validateInput = function (currentValue, input) {
        var parsed = this.formatter.parse(input);
        return parsed && this.calendar.isValid(_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbDate"].from(parsed)) ? _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbDate"].from(parsed) : currentValue;
    };
    DisplayInvoiceComponent.prototype.ngOnInit = function () {
    };
    DisplayInvoiceComponent.prototype.disply = function () {
        this.invoice = false;
        this.invoiceSearch = true;
    };
    DisplayInvoiceComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbCalendar"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbDateParserFormatter"] }
    ]; };
    DisplayInvoiceComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-display-invoice',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./display-invoice.component.html */ "./node_modules/raw-loader/index.js!./src/app/display-invoice/display-invoice.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./display-invoice.component.css */ "./src/app/display-invoice/display-invoice.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbCalendar"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbDateParserFormatter"]])
    ], DisplayInvoiceComponent);
    return DisplayInvoiceComponent;
}());



/***/ }),

/***/ "./src/app/downloads-operations/downloads-operations.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/downloads-operations/downloads-operations.component.css ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("::ng-deep .nav-tabs .nav-link.active{\r\n  background :#ffff;\r\n}\r\n/* ::ng-deep .nav-tabs .nav-link.active{\r\nbackground: #4F008c !important;\r\n} */\r\n.form-group.hidden {\r\nwidth: 0;\r\nmargin: 0;\r\nborder: none;\r\npadding: 0;\r\n}\r\n.form-group.hidden .form-control{\r\ndisplay: none;\r\n}\r\n.custom-day {\r\ndisplay: inline-block;\r\n}\r\n.mb-0{\r\nfont-size: 1.0625rem;\r\nfont-weight: 600;\r\n}\r\n.header{\r\nbackground: #f7fafc !important;\r\n}\r\n.form-control-label{\r\nfont-weight: 600;\r\n}\r\n.col-lg-4{\r\nmargin-top: 2rem;\r\nmax-width: 49.33333%\r\n}\r\n.col-lg-000{\r\nmargin-top: 2rem;\r\nmax-width: 90.33333%;\r\nflex: 0 0 33.33333%;\r\nposition: relative;\r\nwidth: 100%;\r\nmin-height: 1px;\r\npadding-right: 15px;\r\npadding-left: 15px;\r\n}\r\n::ng-deep .custom-day.focused {\r\nbackground-color: #e6e6e6;\r\n}\r\n::ng-deep .custom-day.range, ::ng-deep .custom-day:hover {\r\ncolor: #FF375E;\r\n}\r\n.btn-outline-secondary {\r\ncolor: #23282c;\r\nborder: 1px solid #cad1d7;\r\nbackground-color: #fff;\r\n}\r\n::ng-deep .tab-content .tab-pane {\r\npadding: 0rem !important;\r\n}\r\n::ng-deep .justify-content-start {\r\npadding-left: 15px !important;\r\n}\r\n.submit{\r\ncolor: #fff;\r\nbackground-color: #FF375E;\r\nborder-color: #FF375E;\r\nfont-weight: 600;\r\ntext-align: center;\r\noverflow: visible;\r\n  white-space: nowrap;\r\n  vertical-align: middle;\r\n  -webkit-user-select: none;\r\n  -moz-user-select: none;\r\n  -ms-user-select: none;\r\n  user-select: none;\r\n  border: 1px solid transparent;\r\n  padding: 0.625rem 1.25rem;\r\n  font-size: 1rem;\r\n  line-height: 1.5;\r\n  border-radius: 0.375rem;\r\n  transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;\r\nbox-shadow: 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08);\r\n}\r\n::ng-deep .card-header {\r\npadding: 1rem 1rem !important\r\n}\r\n.card{\r\nbackground: #f7fafc;\r\n}\r\n.tag {\r\nborder-radius: 50%;\r\nheight: 15px;\r\nwidth: 15px;\r\ndisplay: inline-block;\r\n}\r\n.pp-8{\r\n  padding-top: -8rem !important\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZG93bmxvYWRzLW9wZXJhdGlvbnMvZG93bmxvYWRzLW9wZXJhdGlvbnMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGlCQUFpQjtBQUNuQjtBQUNBOztHQUVHO0FBQ0g7QUFDQSxRQUFRO0FBQ1IsU0FBUztBQUNULFlBQVk7QUFDWixVQUFVO0FBQ1Y7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBRUE7QUFDQSxvQkFBb0I7QUFDcEIsZ0JBQWdCO0FBQ2hCO0FBQ0E7QUFDQSw4QkFBOEI7QUFDOUI7QUFDQTtBQUNBLGdCQUFnQjtBQUNoQjtBQUVBO0FBQ0EsZ0JBQWdCO0FBQ2hCO0FBQ0E7QUFFQTtBQUNBLGdCQUFnQjtBQUNoQixvQkFBb0I7QUFDcEIsbUJBQW1CO0FBQ25CLGtCQUFrQjtBQUNsQixXQUFXO0FBQ1gsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixrQkFBa0I7QUFDbEI7QUFFQTtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBRUE7QUFDQSxjQUFjO0FBQ2QseUJBQXlCO0FBQ3pCLHNCQUFzQjtBQUN0QjtBQUVBO0FBQ0Esd0JBQXdCO0FBQ3hCO0FBQ0E7QUFDQSw2QkFBNkI7QUFDN0I7QUFFQTtBQUNBLFdBQVc7QUFDWCx5QkFBeUI7QUFDekIscUJBQXFCO0FBQ3JCLGdCQUFnQjtBQUNoQixrQkFBa0I7QUFDbEIsaUJBQWlCO0VBQ2YsbUJBQW1CO0VBQ25CLHNCQUFzQjtFQUN0Qix5QkFBeUI7RUFDekIsc0JBQXNCO0VBQ3RCLHFCQUFxQjtFQUNyQixpQkFBaUI7RUFDakIsNkJBQTZCO0VBQzdCLHlCQUF5QjtFQUN6QixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLHVCQUF1QjtFQUN2QixxSUFBcUk7QUFDdkksMkVBQTJFO0FBQzNFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQixZQUFZO0FBQ1osV0FBVztBQUNYLHFCQUFxQjtBQUNyQjtBQUVBO0VBQ0U7QUFDRiIsImZpbGUiOiJzcmMvYXBwL2Rvd25sb2Fkcy1vcGVyYXRpb25zL2Rvd25sb2Fkcy1vcGVyYXRpb25zLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6Om5nLWRlZXAgLm5hdi10YWJzIC5uYXYtbGluay5hY3RpdmV7XHJcbiAgYmFja2dyb3VuZCA6I2ZmZmY7XHJcbn1cclxuLyogOjpuZy1kZWVwIC5uYXYtdGFicyAubmF2LWxpbmsuYWN0aXZle1xyXG5iYWNrZ3JvdW5kOiAjNEYwMDhjICFpbXBvcnRhbnQ7XHJcbn0gKi9cclxuLmZvcm0tZ3JvdXAuaGlkZGVuIHtcclxud2lkdGg6IDA7XHJcbm1hcmdpbjogMDtcclxuYm9yZGVyOiBub25lO1xyXG5wYWRkaW5nOiAwO1xyXG59XHJcbi5mb3JtLWdyb3VwLmhpZGRlbiAuZm9ybS1jb250cm9se1xyXG5kaXNwbGF5OiBub25lO1xyXG59XHJcbi5jdXN0b20tZGF5IHtcclxuZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcblxyXG4ubWItMHtcclxuZm9udC1zaXplOiAxLjA2MjVyZW07XHJcbmZvbnQtd2VpZ2h0OiA2MDA7XHJcbn1cclxuLmhlYWRlcntcclxuYmFja2dyb3VuZDogI2Y3ZmFmYyAhaW1wb3J0YW50O1xyXG59XHJcbi5mb3JtLWNvbnRyb2wtbGFiZWx7XHJcbmZvbnQtd2VpZ2h0OiA2MDA7XHJcbn1cclxuXHJcbi5jb2wtbGctNHtcclxubWFyZ2luLXRvcDogMnJlbTtcclxubWF4LXdpZHRoOiA0OS4zMzMzMyVcclxufVxyXG5cclxuLmNvbC1sZy0wMDB7XHJcbm1hcmdpbi10b3A6IDJyZW07XHJcbm1heC13aWR0aDogOTAuMzMzMzMlO1xyXG5mbGV4OiAwIDAgMzMuMzMzMzMlO1xyXG5wb3NpdGlvbjogcmVsYXRpdmU7XHJcbndpZHRoOiAxMDAlO1xyXG5taW4taGVpZ2h0OiAxcHg7XHJcbnBhZGRpbmctcmlnaHQ6IDE1cHg7XHJcbnBhZGRpbmctbGVmdDogMTVweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5jdXN0b20tZGF5LmZvY3VzZWQge1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiAjZTZlNmU2O1xyXG59XHJcbjo6bmctZGVlcCAuY3VzdG9tLWRheS5yYW5nZSwgOjpuZy1kZWVwIC5jdXN0b20tZGF5OmhvdmVyIHtcclxuY29sb3I6ICNGRjM3NUU7XHJcbn1cclxuXHJcbi5idG4tb3V0bGluZS1zZWNvbmRhcnkge1xyXG5jb2xvcjogIzIzMjgyYztcclxuYm9yZGVyOiAxcHggc29saWQgI2NhZDFkNztcclxuYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxufVxyXG5cclxuOjpuZy1kZWVwIC50YWItY29udGVudCAudGFiLXBhbmUge1xyXG5wYWRkaW5nOiAwcmVtICFpbXBvcnRhbnQ7XHJcbn1cclxuOjpuZy1kZWVwIC5qdXN0aWZ5LWNvbnRlbnQtc3RhcnQge1xyXG5wYWRkaW5nLWxlZnQ6IDE1cHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLnN1Ym1pdHtcclxuY29sb3I6ICNmZmY7XHJcbmJhY2tncm91bmQtY29sb3I6ICNGRjM3NUU7XHJcbmJvcmRlci1jb2xvcjogI0ZGMzc1RTtcclxuZm9udC13ZWlnaHQ6IDYwMDtcclxudGV4dC1hbGlnbjogY2VudGVyO1xyXG5vdmVyZmxvdzogdmlzaWJsZTtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcclxuICAtbW96LXVzZXItc2VsZWN0OiBub25lO1xyXG4gIC1tcy11c2VyLXNlbGVjdDogbm9uZTtcclxuICB1c2VyLXNlbGVjdDogbm9uZTtcclxuICBib3JkZXI6IDFweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICBwYWRkaW5nOiAwLjYyNXJlbSAxLjI1cmVtO1xyXG4gIGZvbnQtc2l6ZTogMXJlbTtcclxuICBsaW5lLWhlaWdodDogMS41O1xyXG4gIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xyXG4gIHRyYW5zaXRpb246IGNvbG9yIDAuMTVzIGVhc2UtaW4tb3V0LCBiYWNrZ3JvdW5kLWNvbG9yIDAuMTVzIGVhc2UtaW4tb3V0LCBib3JkZXItY29sb3IgMC4xNXMgZWFzZS1pbi1vdXQsIGJveC1zaGFkb3cgMC4xNXMgZWFzZS1pbi1vdXQ7XHJcbmJveC1zaGFkb3c6IDAgNHB4IDZweCByZ2JhKDUwLCA1MCwgOTMsIDAuMTEpLCAwIDFweCAzcHggcmdiYSgwLCAwLCAwLCAwLjA4KTtcclxufVxyXG46Om5nLWRlZXAgLmNhcmQtaGVhZGVyIHtcclxucGFkZGluZzogMXJlbSAxcmVtICFpbXBvcnRhbnRcclxufVxyXG4uY2FyZHtcclxuYmFja2dyb3VuZDogI2Y3ZmFmYztcclxufVxyXG4udGFnIHtcclxuYm9yZGVyLXJhZGl1czogNTAlO1xyXG5oZWlnaHQ6IDE1cHg7XHJcbndpZHRoOiAxNXB4O1xyXG5kaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuXHJcbi5wcC04e1xyXG4gIHBhZGRpbmctdG9wOiAtOHJlbSAhaW1wb3J0YW50XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/downloads-operations/downloads-operations.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/downloads-operations/downloads-operations.component.ts ***!
  \************************************************************************/
/*! exports provided: DownloadsOperationsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DownloadsOperationsComponent", function() { return DownloadsOperationsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




var DownloadsOperationsComponent = /** @class */ (function () {
    function DownloadsOperationsComponent(calendar, formatter, router) {
        this.calendar = calendar;
        this.formatter = formatter;
        this.router = router;
        this.hoveredDate = null;
        this.CDR = true;
        this.CDRTech = true;
        this.CDRsearch = false;
        this.fromDate = calendar.getToday();
        this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
    }
    DownloadsOperationsComponent.prototype.onDateSelection = function (date) {
        if (!this.fromDate && !this.toDate) {
            this.fromDate = date;
        }
        else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
            this.toDate = date;
        }
        else {
            this.toDate = null;
            this.fromDate = date;
        }
    };
    DownloadsOperationsComponent.prototype.isHovered = function (date) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
    };
    DownloadsOperationsComponent.prototype.isInside = function (date) {
        return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
    };
    DownloadsOperationsComponent.prototype.isRange = function (date) {
        return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
    };
    DownloadsOperationsComponent.prototype.validateInput = function (currentValue, input) {
        var parsed = this.formatter.parse(input);
        return parsed && this.calendar.isValid(_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbDate"].from(parsed)) ? _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbDate"].from(parsed) : currentValue;
    };
    DownloadsOperationsComponent.prototype.ngOnInit = function () {
    };
    DownloadsOperationsComponent.prototype.displyTech = function () {
        this.CDRTech = false;
        this.CDRsearch = true;
    };
    DownloadsOperationsComponent.prototype.displybilling = function () {
        this.CDR = false;
        this.CDRsearch = true;
    };
    DownloadsOperationsComponent.prototype.cancel = function () {
        window.location.reload();
        this.router.navigateByUrl("/download");
    };
    DownloadsOperationsComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbCalendar"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbDateParserFormatter"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    DownloadsOperationsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-downloads-operations',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./downloads-operations.component.html */ "./node_modules/raw-loader/index.js!./src/app/downloads-operations/downloads-operations.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./downloads-operations.component.css */ "./src/app/downloads-operations/downloads-operations.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbCalendar"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbDateParserFormatter"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], DownloadsOperationsComponent);
    return DownloadsOperationsComponent;
}());



/***/ }),

/***/ "./src/app/email-confirmation/email-confirmation.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/email-confirmation/email-confirmation.component.css ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VtYWlsLWNvbmZpcm1hdGlvbi9lbWFpbC1jb25maXJtYXRpb24uY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/email-confirmation/email-confirmation.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/email-confirmation/email-confirmation.component.ts ***!
  \********************************************************************/
/*! exports provided: EmailConfirmationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailConfirmationComponent", function() { return EmailConfirmationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



var EmailConfirmationComponent = /** @class */ (function () {
    function EmailConfirmationComponent(router) {
        this.router = router;
    }
    EmailConfirmationComponent.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.router.navigate(['login']);
        }, 11000);
    };
    EmailConfirmationComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    EmailConfirmationComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-email-confirmation',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./email-confirmation.component.html */ "./node_modules/raw-loader/index.js!./src/app/email-confirmation/email-confirmation.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./email-confirmation.component.css */ "./src/app/email-confirmation/email-confirmation.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], EmailConfirmationComponent);
    return EmailConfirmationComponent;
}());



/***/ }),

/***/ "./src/app/email-reset/email-reset.component.css":
/*!*******************************************************!*\
  !*** ./src/app/email-reset/email-reset.component.css ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VtYWlsLXJlc2V0L2VtYWlsLXJlc2V0LmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/email-reset/email-reset.component.ts":
/*!******************************************************!*\
  !*** ./src/app/email-reset/email-reset.component.ts ***!
  \******************************************************/
/*! exports provided: EmailResetComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailResetComponent", function() { return EmailResetComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



var EmailResetComponent = /** @class */ (function () {
    function EmailResetComponent(router) {
        this.router = router;
    }
    EmailResetComponent.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.router.navigate(['login']);
        }, 11000);
    };
    EmailResetComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    EmailResetComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-email-reset',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./email-reset.component.html */ "./node_modules/raw-loader/index.js!./src/app/email-reset/email-reset.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./email-reset.component.css */ "./src/app/email-reset/email-reset.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], EmailResetComponent);
    return EmailResetComponent;
}());



/***/ }),

/***/ "./src/app/help/help.component.css":
/*!*****************************************!*\
  !*** ./src/app/help/help.component.css ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".center {\r\n  position: absolute;\r\n  top: 0;\r\n  bottom: 0;\r\n  left: 3;\r\n  right: 0;\r\n  margin: auto;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaGVscC9oZWxwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLFNBQVM7RUFDVCxPQUFPO0VBQ1AsUUFBUTtFQUNSLFlBQVk7QUFDZCIsImZpbGUiOiJzcmMvYXBwL2hlbHAvaGVscC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNlbnRlciB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMDtcclxuICBib3R0b206IDA7XHJcbiAgbGVmdDogMztcclxuICByaWdodDogMDtcclxuICBtYXJnaW46IGF1dG87XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/help/help.component.ts":
/*!****************************************!*\
  !*** ./src/app/help/help.component.ts ***!
  \****************************************/
/*! exports provided: HelpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelpComponent", function() { return HelpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


var HelpComponent = /** @class */ (function () {
    function HelpComponent() {
        this.name = 'Angular';
    }
    HelpComponent.prototype.ngAfterViewInit = function () {
        $("#myCarousel").on("slide.bs.carousel", function (e) {
            var $e = $(e.relatedTarget);
            var idx = $e.index();
            var itemsPerSlide = 3;
            var totalItems = $(".carousel-item").length;
            if (idx >= totalItems - (itemsPerSlide - 1)) {
                var it = (totalItems - idx * itemsPerSlide);
                for (var i = 0; i < it; i++) {
                    // append slides to end
                    if (e.direction == "left") {
                        $(".carousel-item")
                            .eq(i)
                            .appendTo(".carousel-inner");
                    }
                    else {
                        $(".carousel-item")
                            .eq(0)
                            .appendTo($(this).find(".carousel-inner"));
                    }
                }
            }
        });
    };
    HelpComponent.prototype.ngOnInit = function () {
    };
    HelpComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-help',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./help.component.html */ "./node_modules/raw-loader/index.js!./src/app/help/help.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./help.component.css */ "./src/app/help/help.component.css")).default]
        })
    ], HelpComponent);
    return HelpComponent;
}());



/***/ }),

/***/ "./src/app/home/_helpers/must-match.validator.ts":
/*!*******************************************************!*\
  !*** ./src/app/home/_helpers/must-match.validator.ts ***!
  \*******************************************************/
/*! exports provided: MustMatch */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MustMatch", function() { return MustMatch; });
// custom validator to check that two fields match
function MustMatch(controlName, matchingControlName) {
    return function (formGroup) {
        var control = formGroup.controls[controlName];
        var matchingControl = formGroup.controls[matchingControlName];
        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
        }
        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        }
        else {
            matchingControl.setErrors(null);
        }
    };
}


/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("[dir=rtl]  \r\n.btn-neutral{\r\nfloat: left;}\r\n[dir=rtl]  \r\n.text-muted{\r\ntext-align: right;\r\n}\r\n[dir=rtl]  \r\n.h2,.mr-2, .mx-2{\r\n    float: right;\r\n}\r\n.link{\r\n        color: #1a0dab !important;\r\n        cursor: pointer;\r\n}\r\n.color-white{\r\n    color: white;\r\n    font-size: .875rem;\r\n    line-height: 1.5;\r\n    font-weight: 600;\r\n\r\n}\r\n.color-pink{\r\n    color: #FF375D;\r\n    border: 1px solid #cad1d7;\r\n}\r\n::ng-deep .input-group-text-card{\r\n    display: flex;\r\n    align-items: center;\r\n    padding: 0.3rem 0.5rem;\r\n    margin-bottom: 0;\r\n    font-size: 1rem;\r\n    font-weight: 600;\r\n    line-height: 1.5;\r\n    color: #FF375D;\r\n    text-align: center;\r\n    white-space: nowrap;\r\n    border: 1px solid #ced4da;\r\n    border-radius: 0.25rem;\r\n}\r\n.display-line{\r\n    max-width: 90%;\r\n    padding-left: 9rem;\r\n    display: flex;\r\n}\r\n.font-home{\r\n    padding-top: 2rem; \r\n    padding-left: 8rem; \r\n    font-size: 1rem;\r\n    \r\n}\r\n.border-line{\r\n    color: white;\r\n    border-top: solid;\r\n}\r\n.mat-stepper-horizontal {\r\n    margin-top: 8px;\r\n  }\r\n.mat-form-field {\r\n    margin-top: 16px;\r\n  }\r\n* {\r\n    margin: 0;\r\n    padding: 0;\r\n}\r\n.center-fit {\r\n    padding: 0;\r\n    display: block;\r\n    margin: 0 auto;\r\n    width: -webkit-fit-content;\r\n    width: -moz-fit-content;\r\n    width: fit-content;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0FBRUEsV0FBVyxDQUFDO0FBQ1o7O0FBRUEsaUJBQWlCO0FBQ2pCO0FBQ0E7O0lBRUksWUFBWTtBQUNoQjtBQUVBO1FBQ1EseUJBQXlCO1FBQ3pCLGVBQWU7QUFDdkI7QUFFQTtJQUNJLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLGdCQUFnQjs7QUFFcEI7QUFDQTtJQUNJLGNBQWM7SUFDZCx5QkFBeUI7QUFDN0I7QUFFQTtJQUNJLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsc0JBQXNCO0lBQ3RCLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIsc0JBQXNCO0FBQzFCO0FBRUE7SUFDSSxjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLGFBQWE7QUFDakI7QUFFQTtJQUNJLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsZUFBZTs7QUFFbkI7QUFFQTtJQUNJLFlBQVk7SUFDWixpQkFBaUI7QUFDckI7QUFDQTtJQUNJLGVBQWU7RUFDakI7QUFFQTtJQUNFLGdCQUFnQjtFQUNsQjtBQUNBO0lBQ0UsU0FBUztJQUNULFVBQVU7QUFDZDtBQUVBO0lBQ0ksVUFBVTtJQUNWLGNBQWM7SUFDZCxjQUFjO0lBQ2QsMEJBQWtCO0lBQWxCLHVCQUFrQjtJQUFsQixrQkFBa0I7QUFDdEIiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIltkaXI9cnRsXSAgXHJcbi5idG4tbmV1dHJhbHtcclxuZmxvYXQ6IGxlZnQ7fVxyXG5bZGlyPXJ0bF0gIFxyXG4udGV4dC1tdXRlZHtcclxudGV4dC1hbGlnbjogcmlnaHQ7XHJcbn1cclxuW2Rpcj1ydGxdICBcclxuLmgyLC5tci0yLCAubXgtMntcclxuICAgIGZsb2F0OiByaWdodDtcclxufVxyXG5cclxuLmxpbmt7XHJcbiAgICAgICAgY29sb3I6ICMxYTBkYWIgIWltcG9ydGFudDtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi5jb2xvci13aGl0ZXtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogLjg3NXJlbTtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG5cclxufVxyXG4uY29sb3ItcGlua3tcclxuICAgIGNvbG9yOiAjRkYzNzVEO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2NhZDFkNztcclxufVxyXG5cclxuOjpuZy1kZWVwIC5pbnB1dC1ncm91cC10ZXh0LWNhcmR7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHBhZGRpbmc6IDAuM3JlbSAwLjVyZW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgICBjb2xvcjogI0ZGMzc1RDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2VkNGRhO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMC4yNXJlbTtcclxufVxyXG5cclxuLmRpc3BsYXktbGluZXtcclxuICAgIG1heC13aWR0aDogOTAlO1xyXG4gICAgcGFkZGluZy1sZWZ0OiA5cmVtO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuLmZvbnQtaG9tZXtcclxuICAgIHBhZGRpbmctdG9wOiAycmVtOyBcclxuICAgIHBhZGRpbmctbGVmdDogOHJlbTsgXHJcbiAgICBmb250LXNpemU6IDFyZW07XHJcbiAgICBcclxufVxyXG5cclxuLmJvcmRlci1saW5le1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXRvcDogc29saWQ7XHJcbn1cclxuLm1hdC1zdGVwcGVyLWhvcml6b250YWwge1xyXG4gICAgbWFyZ2luLXRvcDogOHB4O1xyXG4gIH1cclxuICBcclxuICAubWF0LWZvcm0tZmllbGQge1xyXG4gICAgbWFyZ2luLXRvcDogMTZweDtcclxuICB9XHJcbiAgKiB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG59XHJcblxyXG4uY2VudGVyLWZpdCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIHdpZHRoOiBmaXQtY29udGVudDtcclxufVxyXG5cclxuIl19 */");

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent, CpChangeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CpChangeComponent", function() { return CpChangeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/fesm5/dialog.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _helpers_must_match_validator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./_helpers/must-match.validator */ "./src/app/home/_helpers/must-match.validator.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");







var HomeComponent = /** @class */ (function () {
    function HomeComponent(dialog) {
        this.dialog = dialog;
        this.options = [];
    }
    HomeComponent.prototype.ngOnInit = function () {
        // this.dialog.open(CpChangeComponent, {
        //   width: "700px",
        //   data: { options: this.options },
        //   disableClose: true
        // });
    };
    HomeComponent.ctorParameters = function () { return [
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] }
    ]; };
    HomeComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]])
    ], HomeComponent);
    return HomeComponent;
}());

var CpChangeComponent = /** @class */ (function () {
    function CpChangeComponent(data, formBuilder, router) {
        this.data = data;
        this.formBuilder = formBuilder;
        this.router = router;
        this.submitted = false;
    }
    CpChangeComponent.prototype.ngOnInit = function () {
        this.registerForm = this.formBuilder.group({
            currpassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, this.password],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(8)]],
            confirmPassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        }, {
            validator: Object(_helpers_must_match_validator__WEBPACK_IMPORTED_MODULE_5__["MustMatch"])('password', 'confirmPassword')
        });
    };
    Object.defineProperty(CpChangeComponent.prototype, "f", {
        get: function () { return this.registerForm.controls; },
        enumerable: false,
        configurable: true
    });
    CpChangeComponent.prototype.onSubmit = function () {
        this.submitted = true;
        if (this.registerForm.valid) {
            if (this.current == "11") {
                var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: function (toast) {
                        toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.stopTimer);
                        toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.resumeTimer);
                    }
                });
                Toast.fire({
                    icon: 'success',
                    title: 'Password updated successfully'
                });
            }
            else {
                alert("Current password is wrong");
            }
        }
        else {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Password is wrong'
            });
        }
    };
    CpChangeComponent.prototype.trueChart = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 35 || charCode > 39) && (charCode < 42 || charCode > 43)
            && (charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57)
            && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
    };
    CpChangeComponent.prototype.onReset = function () {
        this.submitted = false;
        this.registerForm.reset();
    };
    CpChangeComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"],] }] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] }
    ]; };
    CpChangeComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cp-change',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./cp-change.component.html */ "./node_modules/raw-loader/index.js!./src/app/home/cp-change.component.html")).default,
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__param"])(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [Object, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], CpChangeComponent);
    return CpChangeComponent;
}());



/***/ }),

/***/ "./src/app/language/language.constant.ts":
/*!***********************************************!*\
  !*** ./src/app/language/language.constant.ts ***!
  \***********************************************/
/*! exports provided: LANGUAGES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LANGUAGES", function() { return LANGUAGES; });
var LANGUAGES = [
    { name: 'en', title: 'English', isRTL: false, icon: 'assets/img/flags/US.png' },
    { name: 'ar', title: 'عربي', isRTL: true, icon: 'assets/img/flags/SA.png' }
];


/***/ }),

/***/ "./src/app/language/language.helper.ts":
/*!*********************************************!*\
  !*** ./src/app/language/language.helper.ts ***!
  \*********************************************/
/*! exports provided: LanguageHelper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LanguageHelper", function() { return LanguageHelper; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _language_constant__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./language.constant */ "./src/app/language/language.constant.ts");
/* harmony import */ var ngx_webstorage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-webstorage */ "./node_modules/ngx-webstorage/fesm5/ngx-webstorage.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");



// import { DOCUMENT } from '@angular/platform-browser';


var LanguageHelper = /** @class */ (function () {
    function LanguageHelper(
    // @Inject(DOCUMENT) private document,
    localStorageService, translateService) {
        this.localStorageService = localStorageService;
        this.translateService = translateService;
    }
    LanguageHelper.prototype.getAllLanguages = function () {
        return _language_constant__WEBPACK_IMPORTED_MODULE_2__["LANGUAGES"];
    };
    LanguageHelper.prototype.changeLang = function (lang) {
        this.localStorageService.store('lang', lang.name);
        this.translateService.use(lang.name);
    };
    LanguageHelper.prototype.currentLang = function () {
        var stored = this.localStorageService.retrieve('lang');
        var current = this.translateService.currentLang;
        var optLangs = [
            { name: 'en_US', title: 'English', isRTL: false },
            { name: 'ar_SA', title: 'عربي', isRTL: true }
        ];
        if (stored) {
            current = stored;
        }
        if (!current) {
            current = 'en';
        }
        var lang = _language_constant__WEBPACK_IMPORTED_MODULE_2__["LANGUAGES"].find(function (cur) { return cur.name === current; });
        if (!lang) {
            lang = optLangs.find(function (cur) { return cur.name === current; });
            if (lang.name == 'en_US')
                lang.name = 'en';
            if (lang.name == 'ar_SA')
                lang.name = 'ar';
        }
        return lang;
    };
    LanguageHelper.ctorParameters = function () { return [
        { type: ngx_webstorage__WEBPACK_IMPORTED_MODULE_3__["LocalStorageService"] },
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"] }
    ]; };
    LanguageHelper = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [ngx_webstorage__WEBPACK_IMPORTED_MODULE_3__["LocalStorageService"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"]])
    ], LanguageHelper);
    return LanguageHelper;
}());



/***/ }),

/***/ "./src/app/nav/nav.component.css":
/*!***************************************!*\
  !*** ./src/app/nav/nav.component.css ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".form-gap {\r\n    padding-top: 70px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmF2L25hdi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0FBQ3JCIiwiZmlsZSI6InNyYy9hcHAvbmF2L25hdi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcm0tZ2FwIHtcclxuICAgIHBhZGRpbmctdG9wOiA3MHB4O1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/nav/nav.component.ts":
/*!**************************************!*\
  !*** ./src/app/nav/nav.component.ts ***!
  \**************************************/
/*! exports provided: NavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavComponent", function() { return NavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


var NavComponent = /** @class */ (function () {
    function NavComponent() {
    }
    NavComponent.prototype.ngOnInit = function () {
    };
    NavComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-nav',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./nav.component.html */ "./node_modules/raw-loader/index.js!./src/app/nav/nav.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./nav.component.css */ "./src/app/nav/nav.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], NavComponent);
    return NavComponent;
}());



/***/ }),

/***/ "./src/app/new-partner/new-partner.component.css":
/*!*******************************************************!*\
  !*** ./src/app/new-partner/new-partner.component.css ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".country-picker {\r\n    border: 1px solid #f0f3f5;\r\n    border-radius: 5px;\r\n    position: relative;\r\n    margin-bottom: 18px;\r\n}\r\n.picker-button {\r\n    background: #f0f3f5;\r\n    /* color: #737e88; */\r\n    width: 28px;\r\n    height: 31px;\r\n    text-align: center;\r\n    line-height: 30px;\r\n    cursor: pointer;\r\n    display: inline-block;\r\n}\r\n.current{\r\n    display: inline-block;\r\n}\r\n.country-picker ul {\r\n    list-style: none;\r\n    position: absolute;\r\n    background: #fff;\r\n    padding: 0;\r\n    width: 100%;\r\n    margin: 0;\r\n    height: calc(35px * 5);\r\n    overflow: hidden;\r\n    overflow-y: scroll;\r\n    z-index: 99;\r\n}\r\n.country-picker ul li {\r\n    padding: 7px;\r\n    cursor: pointer;\r\n    border-bottom: 1px solid #777;\r\n    transition: 0.3s;\r\n    margin-top: 20px;\r\n\r\n}\r\n.country-picker ul li:hover{\r\n    background: #4F008C;\r\n    color: #fff;\r\n}\r\n.country-picker img{\r\nwidth: 20px;\r\n\r\n}\r\n.region{\r\n    background: #f0f3f5;\r\n    color: #737e88;\r\n    width: 40px;\r\n    height: 30px;\r\n    text-align: center;\r\n    line-height: 30px;\r\n    cursor: pointer;\r\n    display: inline-block\r\n}\r\n.region li{\r\n\r\n    text-align: center;\r\n}\r\n.region ul li{\r\n    padding: 7px;\r\n    cursor: pointer;\r\n    border-bottom: 1px solid #777;\r\n    transition: 0.3s;\r\n    display: flex;\r\n}\r\n.regBu{\r\n    margin:auto;\r\n    display:block;\r\n   \r\n    display: flex;\r\n}\r\n.h2{\r\n    font-family: inherit;\r\n    font-weight: 600;\r\n    line-height: 1.5;\r\n    font-size: 1.25rem;\r\n}\r\n.background-pink{\r\n    background-color: #ff375d;\r\n}\r\n.color-white{\r\n    color: white;\r\n    background-color: #ff375d;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmV3LXBhcnRuZXIvbmV3LXBhcnRuZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLG1CQUFtQjtBQUN2QjtBQUNBO0lBQ0ksbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQixXQUFXO0lBQ1gsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixpQkFBaUI7SUFDakIsZUFBZTtJQUNmLHFCQUFxQjtBQUN6QjtBQUNBO0lBQ0kscUJBQXFCO0FBQ3pCO0FBQ0E7SUFDSSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixVQUFVO0lBQ1YsV0FBVztJQUNYLFNBQVM7SUFDVCxzQkFBc0I7SUFDdEIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixXQUFXO0FBQ2Y7QUFDQTtJQUNJLFlBQVk7SUFDWixlQUFlO0lBQ2YsNkJBQTZCO0lBQzdCLGdCQUFnQjtJQUNoQixnQkFBZ0I7O0FBRXBCO0FBQ0E7SUFDSSxtQkFBbUI7SUFDbkIsV0FBVztBQUNmO0FBQ0E7QUFDQSxXQUFXOztBQUVYO0FBQ0E7SUFDSSxtQkFBbUI7SUFDbkIsY0FBYztJQUNkLFdBQVc7SUFDWCxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2Y7QUFDSjtBQUNBOztJQUVJLGtCQUFrQjtBQUN0QjtBQUNBO0lBQ0ksWUFBWTtJQUNaLGVBQWU7SUFDZiw2QkFBNkI7SUFDN0IsZ0JBQWdCO0lBQ2hCLGFBQWE7QUFDakI7QUFFQTtJQUNJLFdBQVc7SUFDWCxhQUFhOztJQUViLGFBQWE7QUFDakI7QUFFQTtJQUNJLG9CQUFvQjtJQUNwQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtBQUN0QjtBQUVBO0lBQ0kseUJBQXlCO0FBQzdCO0FBRUE7SUFDSSxZQUFZO0lBQ1oseUJBQXlCO0FBQzdCIiwiZmlsZSI6InNyYy9hcHAvbmV3LXBhcnRuZXIvbmV3LXBhcnRuZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb3VudHJ5LXBpY2tlciB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZjBmM2Y1O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMThweDtcclxufVxyXG4ucGlja2VyLWJ1dHRvbiB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZjBmM2Y1O1xyXG4gICAgLyogY29sb3I6ICM3MzdlODg7ICovXHJcbiAgICB3aWR0aDogMjhweDtcclxuICAgIGhlaWdodDogMzFweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcbi5jdXJyZW50e1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcbi5jb3VudHJ5LXBpY2tlciB1bCB7XHJcbiAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIGhlaWdodDogY2FsYygzNXB4ICogNSk7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xyXG4gICAgei1pbmRleDogOTk7XHJcbn1cclxuLmNvdW50cnktcGlja2VyIHVsIGxpIHtcclxuICAgIHBhZGRpbmc6IDdweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjNzc3O1xyXG4gICAgdHJhbnNpdGlvbjogMC4zcztcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcblxyXG59XHJcbi5jb3VudHJ5LXBpY2tlciB1bCBsaTpob3ZlcntcclxuICAgIGJhY2tncm91bmQ6ICM0RjAwOEM7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxufVxyXG4uY291bnRyeS1waWNrZXIgaW1ne1xyXG53aWR0aDogMjBweDtcclxuXHJcbn1cclxuLnJlZ2lvbntcclxuICAgIGJhY2tncm91bmQ6ICNmMGYzZjU7XHJcbiAgICBjb2xvcjogIzczN2U4ODtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2tcclxufVxyXG4ucmVnaW9uIGxpe1xyXG5cclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4ucmVnaW9uIHVsIGxpe1xyXG4gICAgcGFkZGluZzogN3B4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICM3Nzc7XHJcbiAgICB0cmFuc2l0aW9uOiAwLjNzO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuLnJlZ0J1e1xyXG4gICAgbWFyZ2luOmF1dG87XHJcbiAgICBkaXNwbGF5OmJsb2NrO1xyXG4gICBcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbi5oMntcclxuICAgIGZvbnQtZmFtaWx5OiBpbmhlcml0O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgICBmb250LXNpemU6IDEuMjVyZW07XHJcbn1cclxuXHJcbi5iYWNrZ3JvdW5kLXBpbmt7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmYzNzVkO1xyXG59XHJcblxyXG4uY29sb3Itd2hpdGV7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmYzNzVkO1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/new-partner/new-partner.component.ts":
/*!******************************************************!*\
  !*** ./src/app/new-partner/new-partner.component.ts ***!
  \******************************************************/
/*! exports provided: NewPartnerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewPartnerComponent", function() { return NewPartnerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../shared/models/regionsEnum */ "./src/app/shared/models/regionsEnum.ts");
/* harmony import */ var _shared_services_shared_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../shared/services/shared.service */ "./src/app/shared/services/shared.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _language_language_helper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../language/language.helper */ "./src/app/language/language.helper.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _dataModels_newpartner_model__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../dataModels/newpartner.model */ "./src/app/dataModels/newpartner.model.ts");
/* harmony import */ var _services_newpartner_services__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../services/newpartner.services */ "./src/app/services/newpartner.services.ts");











var NewPartnerComponent = /** @class */ (function () {
    function NewPartnerComponent(shared, languageHelper, translateService, router, partnerServices) {
        this.shared = shared;
        this.languageHelper = languageHelper;
        this.translateService = translateService;
        this.router = router;
        this.partnerServices = partnerServices;
        this.isCountryOpen = false;
        this.isRegister = false;
        this.listCountries = [];
        this.selectedCountry = { name: "Select Country", flag: null };
        this.isRegionOpen = false;
        this.listRegion = [
            { display: "EU (European Union)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].EU },
            { display: "EFTA (European Free Trade Association)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].EFTA },
            { display: "CARICOM (Caribbean Community)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].CARICOM },
            { display: "USAN (Union of South American Nations)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].USAN },
            { display: "EEU (Eurasian Economic Union)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].EEU },
            { display: "AL (Arab League)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].AL },
            { display: "ASEAN (Association of Southeast Asian Nations)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].ASEAN },
            { display: "CAIS (Central American Integration System)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].CAIS },
            { display: "CEFTA (Central European Free Trade Agreement)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].CEFTA },
            { display: "NAFTA (North American Free Trade Agreement)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].NAFTA },
            { display: "SAARC (South Asian Association for Regional Cooperation)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"].SAARC },
        ];
        this.partnerDto = new _dataModels_newpartner_model__WEBPACK_IMPORTED_MODULE_9__["partnerDto"]();
        this.getAllLangs();
        this.isRTL = this.languageHelper.currentLang().isRTL;
        this.partnerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('^[^\\s@]+@[^\\s@]+\\.[^\\s@]{2,}$'), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(25)])]),
            fName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(1), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(10)]),
            lName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(1), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(10)]),
            company: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(1), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(30)]),
            phone: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(1), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(15)]),
        });
    }
    NewPartnerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.shared.getAllCountries().subscribe(function (res) { return _this.listCountries = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(res); });
    };
    NewPartnerComponent.prototype.selectCountry = function (country) {
        console.log(country);
        this.isCountryOpen = false;
        this.selectedCountry = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"])({}, country);
        this.partnerForm.get("phone").setValue("+" + country.callingCodes[0]);
    };
    NewPartnerComponent.prototype.trueChart = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)
            && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
    };
    NewPartnerComponent.prototype.trueChartEmail = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57)
            && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
    };
    NewPartnerComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    NewPartnerComponent.prototype.selectRegion = function () {
        var _this = this;
        var region = this.partnerForm.get('region').value;
        console.log(_shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"][region]);
        this.shared.getCountriesWithRegion(_shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_1__["RegionsEnum"][region]).subscribe(function (res) { return _this.listCountries = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(res); });
    };
    NewPartnerComponent.prototype.newPartner = function () {
        this.isRegister = true;
        if (this.partnerForm.valid) {
            this.router.navigate(['home']);
            this.partnerDto.company = this.partnerForm.value.company;
            this.partnerDto.phone = this.partnerForm.value.phone;
            this.partnerDto.email = this.partnerForm.value.email;
            this.partnerDto.name = this.partnerForm.value.fName;
            this.partnerDto.name = this.partnerForm.value.lName;
            this.partnerServices.postpartner(this.partnerDto).subscribe(function (res) {
                var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: function (toast) {
                        toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                        toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
                    }
                });
                Toast.fire({
                    icon: 'success',
                    title: 'successfully registered'
                });
                console.log(res);
            });
        }
        if (this.partnerForm.get('email').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Email Address is Worng'
            });
        }
        if (this.partnerForm.get('phone').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Phone entered is Worng'
            });
        }
        if (this.partnerForm.get('fName').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'First Name Entered is Wrong'
            });
        }
        if (this.partnerForm.get('lName').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Last Name Entered is Wrong'
            });
        }
        if (this.partnerForm.get('company').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Company Name Entered is Wrong'
            });
        }
    };
    NewPartnerComponent.prototype.changeLang = function (lang) {
        this.languageHelper.changeLang(lang);
        window.location.reload();
    };
    NewPartnerComponent.prototype.getAllLangs = function () {
        var arr = this.languageHelper.getAllLanguages();
        this.english = arr[0];
        this.arabic = arr[1];
        return this.languageHelper.getAllLanguages();
    };
    NewPartnerComponent.ctorParameters = function () { return [
        { type: _shared_services_shared_service__WEBPACK_IMPORTED_MODULE_2__["SharedService"] },
        { type: _language_language_helper__WEBPACK_IMPORTED_MODULE_6__["LanguageHelper"] },
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"] },
        { type: _services_newpartner_services__WEBPACK_IMPORTED_MODULE_10__["partnerServices"] }
    ]; };
    NewPartnerComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
            selector: 'app-new-partner',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./new-partner.component.html */ "./node_modules/raw-loader/index.js!./src/app/new-partner/new-partner.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./new-partner.component.css */ "./src/app/new-partner/new-partner.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_shared_services_shared_service__WEBPACK_IMPORTED_MODULE_2__["SharedService"], _language_language_helper__WEBPACK_IMPORTED_MODULE_6__["LanguageHelper"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"], _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"], _services_newpartner_services__WEBPACK_IMPORTED_MODULE_10__["partnerServices"]])
    ], NewPartnerComponent);
    return NewPartnerComponent;
}());



/***/ }),

/***/ "./src/app/product-catalog/product-catalog.component.css":
/*!***************************************************************!*\
  !*** ./src/app/product-catalog/product-catalog.component.css ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".col {\r\n    max-width: 100%;\r\n    flex-basis: 0;\r\n    flex-grow: 1;\r\n    padding-top: 19px;\r\n}\r\n\r\n.color-p{\r\n    color: #525F7F\r\n}\r\n\r\n.col-xl-3 {\r\npadding-bottom: 17px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdC1jYXRhbG9nL3Byb2R1Y3QtY2F0YWxvZy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksZUFBZTtJQUNmLGFBQWE7SUFDYixZQUFZO0lBQ1osaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0k7QUFDSjs7QUFDQTtBQUNBLG9CQUFvQjtBQUNwQiIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3QtY2F0YWxvZy9wcm9kdWN0LWNhdGFsb2cuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb2wge1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgZmxleC1iYXNpczogMDtcclxuICAgIGZsZXgtZ3JvdzogMTtcclxuICAgIHBhZGRpbmctdG9wOiAxOXB4O1xyXG59XHJcblxyXG4uY29sb3ItcHtcclxuICAgIGNvbG9yOiAjNTI1RjdGXHJcbn1cclxuLmNvbC14bC0zIHtcclxucGFkZGluZy1ib3R0b206IDE3cHg7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/product-catalog/product-catalog.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/product-catalog/product-catalog.component.ts ***!
  \**************************************************************/
/*! exports provided: ProductCatalogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductCatalogComponent", function() { return ProductCatalogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_product_services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/product.services */ "./src/app/services/product.services.ts");



var ProductCatalogComponent = /** @class */ (function () {
    function ProductCatalogComponent(productServices) {
        this.productServices = productServices;
        this.product = new Array();
    }
    ProductCatalogComponent.prototype.ngOnInit = function () {
        // this.productServices.getPro().subscribe(res=> {
        //   this.product= [...res.products]
        // });  
    };
    ProductCatalogComponent.ctorParameters = function () { return [
        { type: _services_product_services__WEBPACK_IMPORTED_MODULE_2__["productServices"] }
    ]; };
    ProductCatalogComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-product-catalog',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./product-catalog.component.html */ "./node_modules/raw-loader/index.js!./src/app/product-catalog/product-catalog.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./product-catalog.component.css */ "./src/app/product-catalog/product-catalog.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_services_product_services__WEBPACK_IMPORTED_MODULE_2__["productServices"]])
    ], ProductCatalogComponent);
    return ProductCatalogComponent;
}());



/***/ }),

/***/ "./src/app/product_catalog/capacity-catalog/capacity-catalog.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/product_catalog/capacity-catalog/capacity-catalog.component.css ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("[dir=rtl]  \r\n.btn-neutral{\r\nfloat: left;}\r\n[dir=rtl]  \r\n.text-muted{\r\ntext-align: right;\r\n}\r\n[dir=rtl]  \r\n.h2,.mr-2, .mx-2{\r\n    float: right;\r\n}\r\n.link{\r\n   \r\n    color: #1a0dab !important;\r\n    cursor: pointer;\r\n   \r\n}\r\n.m2{\r\n    text-align: center;\r\n    font-size: xx-large;\r\n    color: white;\r\n    padding-bottom: 3rem;\r\n}\r\n.example-box.cdk-drag-animating {\r\n    transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\r\n  }\r\n.example-chip .cdk-drop-list-dragging {\r\n    transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\r\n  }\r\n.chip{\r\n    padding: 16px 12px;\r\n        pointer-events: none;\r\n\r\n  }\r\n.non-clickable{\r\n    pointer-events: none;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdF9jYXRhbG9nL2NhcGFjaXR5LWNhdGFsb2cvY2FwYWNpdHktY2F0YWxvZy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztBQUVBLFdBQVcsQ0FBQztBQUNaOztBQUVBLGlCQUFpQjtBQUNqQjtBQUNBOztJQUVJLFlBQVk7QUFDaEI7QUFFQTs7SUFFSSx5QkFBeUI7SUFDekIsZUFBZTs7QUFFbkI7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLG9CQUFvQjtBQUN4QjtBQUNBO0lBQ0ksc0RBQXNEO0VBQ3hEO0FBRUE7SUFDRSxzREFBc0Q7RUFDeEQ7QUFDQTtJQUNFLGtCQUFrQjtRQUNkLG9CQUFvQjs7RUFFMUI7QUFFQTtJQUNFLG9CQUFvQjtFQUN0QiIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3RfY2F0YWxvZy9jYXBhY2l0eS1jYXRhbG9nL2NhcGFjaXR5LWNhdGFsb2cuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIltkaXI9cnRsXSAgXHJcbi5idG4tbmV1dHJhbHtcclxuZmxvYXQ6IGxlZnQ7fVxyXG5bZGlyPXJ0bF0gIFxyXG4udGV4dC1tdXRlZHtcclxudGV4dC1hbGlnbjogcmlnaHQ7XHJcbn1cclxuW2Rpcj1ydGxdICBcclxuLmgyLC5tci0yLCAubXgtMntcclxuICAgIGZsb2F0OiByaWdodDtcclxufVxyXG5cclxuLmxpbmt7XHJcbiAgIFxyXG4gICAgY29sb3I6ICMxYTBkYWIgIWltcG9ydGFudDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgXHJcbn1cclxuXHJcbi5tMntcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogeHgtbGFyZ2U7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogM3JlbTtcclxufVxyXG4uZXhhbXBsZS1ib3guY2RrLWRyYWctYW5pbWF0aW5nIHtcclxuICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSAyNTBtcyBjdWJpYy1iZXppZXIoMCwgMCwgMC4yLCAxKTtcclxuICB9XHJcbiAgXHJcbiAgLmV4YW1wbGUtY2hpcCAuY2RrLWRyb3AtbGlzdC1kcmFnZ2luZyB7XHJcbiAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMjUwbXMgY3ViaWMtYmV6aWVyKDAsIDAsIDAuMiwgMSk7XHJcbiAgfVxyXG4gIC5jaGlwe1xyXG4gICAgcGFkZGluZzogMTZweCAxMnB4O1xyXG4gICAgICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xyXG5cclxuICB9XHJcblxyXG4gIC5ub24tY2xpY2thYmxle1xyXG4gICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XHJcbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/product_catalog/capacity-catalog/capacity-catalog.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/product_catalog/capacity-catalog/capacity-catalog.component.ts ***!
  \********************************************************************************/
/*! exports provided: CapacityCatalogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CapacityCatalogComponent", function() { return CapacityCatalogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_productCata_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../services/productCata.service */ "./src/app/services/productCata.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _dataModels_voiceDto_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../dataModels/voiceDto.model */ "./src/app/dataModels/voiceDto.model.ts");





var CapacityCatalogComponent = /** @class */ (function () {
    function CapacityCatalogComponent(_route, productCataServices) {
        this._route = _route;
        this.productCataServices = productCataServices;
        this.regDetails = new _dataModels_voiceDto_model__WEBPACK_IMPORTED_MODULE_4__["voiceDto"]();
        this.regDetails.name = this._route.snapshot.paramMap.get("name");
    }
    CapacityCatalogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.productCataServices.getReqDetails(this.regDetails.name).subscribe(function (res) {
            _this.regDetails.category = res.category;
            _this.regDetails.child = res.child;
            _this.regDetails.description = res.description;
            _this.regDetails.name = res.name;
        });
        this.productCataServices.getImg(this.regDetails.name).subscribe(function (res) {
            _this.regDetails.name = res.name;
            // console.log("SAA", this.regDetails.name)
        });
    };
    CapacityCatalogComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _services_productCata_service__WEBPACK_IMPORTED_MODULE_1__["productCataServices"] }
    ]; };
    CapacityCatalogComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-capacity-catalog',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./capacity-catalog.component.html */ "./node_modules/raw-loader/index.js!./src/app/product_catalog/capacity-catalog/capacity-catalog.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./capacity-catalog.component.css */ "./src/app/product_catalog/capacity-catalog/capacity-catalog.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _services_productCata_service__WEBPACK_IMPORTED_MODULE_1__["productCataServices"]])
    ], CapacityCatalogComponent);
    return CapacityCatalogComponent;
}());



/***/ }),

/***/ "./src/app/product_catalog/capacity/capacity.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/product_catalog/capacity/capacity.component.css ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("[dir=rtl]  \r\n.btn-neutral{\r\nfloat: left;}\r\n[dir=rtl]  \r\n.text-muted{\r\ntext-align: right;\r\n}\r\n[dir=rtl]  \r\n.h2,.mr-2, .mx-2{\r\n    float: right;\r\n}\r\n.link{\r\n    color:#4F008c !important;\r\n    cursor: pointer;\r\n}\r\n.card{\r\n    height: 100px!important;\r\n    background-clip: padding-box;\r\n}\r\n.pb-13{\r\n        padding-bottom: 16rem !important\r\n}\r\n.py-4{\r\n    text-align: end;\r\n}\r\n.mb-0{\r\ntext-align: center;\r\npadding-bottom: 2rem;\r\n}\r\n.col-xl-3{\r\n    padding-bottom: 1rem;\r\n  }\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdF9jYXRhbG9nL2NhcGFjaXR5L2NhcGFjaXR5LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0FBRUEsV0FBVyxDQUFDO0FBQ1o7O0FBRUEsaUJBQWlCO0FBQ2pCO0FBQ0E7O0lBRUksWUFBWTtBQUNoQjtBQUVBO0lBQ0ksd0JBQXdCO0lBQ3hCLGVBQWU7QUFDbkI7QUFDQTtJQUNJLHVCQUF1QjtJQUN2Qiw0QkFBNEI7QUFDaEM7QUFDQTtRQUNRO0FBQ1I7QUFDQTtJQUNJLGVBQWU7QUFDbkI7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQixvQkFBb0I7QUFDcEI7QUFDQTtJQUNJLG9CQUFvQjtFQUN0QiIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3RfY2F0YWxvZy9jYXBhY2l0eS9jYXBhY2l0eS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiW2Rpcj1ydGxdICBcclxuLmJ0bi1uZXV0cmFse1xyXG5mbG9hdDogbGVmdDt9XHJcbltkaXI9cnRsXSAgXHJcbi50ZXh0LW11dGVke1xyXG50ZXh0LWFsaWduOiByaWdodDtcclxufVxyXG5bZGlyPXJ0bF0gIFxyXG4uaDIsLm1yLTIsIC5teC0ye1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcblxyXG4ubGlua3tcclxuICAgIGNvbG9yOiM0RjAwOGMgIWltcG9ydGFudDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG4uY2FyZHtcclxuICAgIGhlaWdodDogMTAwcHghaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZC1jbGlwOiBwYWRkaW5nLWJveDtcclxufVxyXG4ucGItMTN7XHJcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDE2cmVtICFpbXBvcnRhbnRcclxufVxyXG4ucHktNHtcclxuICAgIHRleHQtYWxpZ246IGVuZDtcclxufVxyXG4ubWItMHtcclxudGV4dC1hbGlnbjogY2VudGVyO1xyXG5wYWRkaW5nLWJvdHRvbTogMnJlbTtcclxufVxyXG4uY29sLXhsLTN7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMXJlbTtcclxuICB9XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/product_catalog/capacity/capacity.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/product_catalog/capacity/capacity.component.ts ***!
  \****************************************************************/
/*! exports provided: CapacityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CapacityComponent", function() { return CapacityComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_capacityServices__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../services/capacityServices */ "./src/app/services/capacityServices.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




var CapacityComponent = /** @class */ (function () {
    function CapacityComponent(router, capacityServices) {
        this.router = router;
        this.capacityServices = capacityServices;
        this.voice = new Array();
    }
    CapacityComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.capacityServices.getProducts().subscribe(function (res) {
            _this.voice = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(res.products);
        });
    };
    CapacityComponent.prototype.product = function (name) {
        this.router.navigate(['/product-catalog', name]);
    };
    CapacityComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _services_capacityServices__WEBPACK_IMPORTED_MODULE_1__["capacityServices"] }
    ]; };
    CapacityComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-capacity',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./capacity.component.html */ "./node_modules/raw-loader/index.js!./src/app/product_catalog/capacity/capacity.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./capacity.component.css */ "./src/app/product_catalog/capacity/capacity.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _services_capacityServices__WEBPACK_IMPORTED_MODULE_1__["capacityServices"]])
    ], CapacityComponent);
    return CapacityComponent;
}());



/***/ }),

/***/ "./src/app/product_catalog/colocation/colocation.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/product_catalog/colocation/colocation.component.css ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("[dir=rtl]  \r\n.btn-neutral{\r\nfloat: left;}\r\n[dir=rtl]  \r\n.text-muted{\r\ntext-align: right;\r\n}\r\n[dir=rtl]  \r\n.h2,.mr-2, .mx-2{\r\n    float: right;\r\n}\r\n.link{\r\n        color:#4F008c !important;\r\n        cursor: pointer;\r\n}\r\n.card{\r\n    height: 100px!important;\r\n    background-clip: padding-box;\r\n}\r\n.pb-13{\r\n        padding-bottom: 16rem !important\r\n}\r\n.py-4{\r\n    text-align: end;\r\n}\r\n.mb-0{\r\ntext-align: center;\r\npadding-bottom: 2rem;\r\n}\r\n.col-xl-3{\r\n    padding-bottom: 1rem;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdF9jYXRhbG9nL2NvbG9jYXRpb24vY29sb2NhdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztBQUVBLFdBQVcsQ0FBQztBQUNaOztBQUVBLGlCQUFpQjtBQUNqQjtBQUNBOztJQUVJLFlBQVk7QUFDaEI7QUFFQTtRQUNRLHdCQUF3QjtRQUN4QixlQUFlO0FBQ3ZCO0FBQ0E7SUFDSSx1QkFBdUI7SUFDdkIsNEJBQTRCO0FBQ2hDO0FBQ0E7UUFDUTtBQUNSO0FBQ0E7SUFDSSxlQUFlO0FBQ25CO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEIsb0JBQW9CO0FBQ3BCO0FBQ0E7SUFDSSxvQkFBb0I7RUFDdEIiLCJmaWxlIjoic3JjL2FwcC9wcm9kdWN0X2NhdGFsb2cvY29sb2NhdGlvbi9jb2xvY2F0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJbZGlyPXJ0bF0gIFxyXG4uYnRuLW5ldXRyYWx7XHJcbmZsb2F0OiBsZWZ0O31cclxuW2Rpcj1ydGxdICBcclxuLnRleHQtbXV0ZWR7XHJcbnRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcbltkaXI9cnRsXSAgXHJcbi5oMiwubXItMiwgLm14LTJ7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbn1cclxuXHJcbi5saW5re1xyXG4gICAgICAgIGNvbG9yOiM0RjAwOGMgIWltcG9ydGFudDtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuLmNhcmR7XHJcbiAgICBoZWlnaHQ6IDEwMHB4IWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQtY2xpcDogcGFkZGluZy1ib3g7XHJcbn1cclxuLnBiLTEze1xyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAxNnJlbSAhaW1wb3J0YW50XHJcbn1cclxuLnB5LTR7XHJcbiAgICB0ZXh0LWFsaWduOiBlbmQ7XHJcbn1cclxuLm1iLTB7XHJcbnRleHQtYWxpZ246IGNlbnRlcjtcclxucGFkZGluZy1ib3R0b206IDJyZW07XHJcbn1cclxuLmNvbC14bC0ze1xyXG4gICAgcGFkZGluZy1ib3R0b206IDFyZW07XHJcbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/product_catalog/colocation/colocation.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/product_catalog/colocation/colocation.component.ts ***!
  \********************************************************************/
/*! exports provided: ColocationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ColocationComponent", function() { return ColocationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_colocationServices__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../services/colocationServices */ "./src/app/services/colocationServices.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




var ColocationComponent = /** @class */ (function () {
    function ColocationComponent(router, colocationServices) {
        this.router = router;
        this.colocationServices = colocationServices;
        this.voice = new Array();
    }
    ColocationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.colocationServices.getProducts().subscribe(function (res) {
            _this.voice = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(res.products);
        });
    };
    ColocationComponent.prototype.product = function (name) {
        this.router.navigate(['/product-catalog', name]);
    };
    ColocationComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _services_colocationServices__WEBPACK_IMPORTED_MODULE_1__["colocationServices"] }
    ]; };
    ColocationComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-colocation',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./colocation.component.html */ "./node_modules/raw-loader/index.js!./src/app/product_catalog/colocation/colocation.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./colocation.component.css */ "./src/app/product_catalog/colocation/colocation.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _services_colocationServices__WEBPACK_IMPORTED_MODULE_1__["colocationServices"]])
    ], ColocationComponent);
    return ColocationComponent;
}());



/***/ }),

/***/ "./src/app/product_catalog/data/data.component.css":
/*!*********************************************************!*\
  !*** ./src/app/product_catalog/data/data.component.css ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("[dir=rtl]  \r\n.btn-neutral{\r\nfloat: left;}\r\n[dir=rtl]  \r\n.text-muted{\r\ntext-align: right;\r\n}\r\n[dir=rtl]  \r\n.h2,.mr-2, .mx-2{\r\n    float: right;\r\n}\r\n.link{\r\n        color:#4F008c !important;\r\n        cursor: pointer;\r\n}\r\n.card{\r\n    height: 100px!important;\r\n    background-clip: padding-box;\r\n}\r\n.pb-13{\r\n        padding-bottom: 16rem !important\r\n}\r\n.py-4{\r\n    text-align: end;\r\n}\r\n.mb-0{\r\ntext-align: center;\r\npadding-bottom: 2rem;\r\n}\r\n.col-xl-3{\r\n    padding-bottom: 1rem;\r\n  }\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdF9jYXRhbG9nL2RhdGEvZGF0YS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztBQUVBLFdBQVcsQ0FBQztBQUNaOztBQUVBLGlCQUFpQjtBQUNqQjtBQUNBOztJQUVJLFlBQVk7QUFDaEI7QUFFQTtRQUNRLHdCQUF3QjtRQUN4QixlQUFlO0FBQ3ZCO0FBQ0E7SUFDSSx1QkFBdUI7SUFDdkIsNEJBQTRCO0FBQ2hDO0FBQ0E7UUFDUTtBQUNSO0FBQ0E7SUFDSSxlQUFlO0FBQ25CO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEIsb0JBQW9CO0FBQ3BCO0FBQ0E7SUFDSSxvQkFBb0I7RUFDdEIiLCJmaWxlIjoic3JjL2FwcC9wcm9kdWN0X2NhdGFsb2cvZGF0YS9kYXRhLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJbZGlyPXJ0bF0gIFxyXG4uYnRuLW5ldXRyYWx7XHJcbmZsb2F0OiBsZWZ0O31cclxuW2Rpcj1ydGxdICBcclxuLnRleHQtbXV0ZWR7XHJcbnRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcbltkaXI9cnRsXSAgXHJcbi5oMiwubXItMiwgLm14LTJ7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbn1cclxuXHJcbi5saW5re1xyXG4gICAgICAgIGNvbG9yOiM0RjAwOGMgIWltcG9ydGFudDtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuLmNhcmR7XHJcbiAgICBoZWlnaHQ6IDEwMHB4IWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQtY2xpcDogcGFkZGluZy1ib3g7XHJcbn1cclxuLnBiLTEze1xyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAxNnJlbSAhaW1wb3J0YW50XHJcbn1cclxuLnB5LTR7XHJcbiAgICB0ZXh0LWFsaWduOiBlbmQ7XHJcbn1cclxuLm1iLTB7XHJcbnRleHQtYWxpZ246IGNlbnRlcjtcclxucGFkZGluZy1ib3R0b206IDJyZW07XHJcbn1cclxuLmNvbC14bC0ze1xyXG4gICAgcGFkZGluZy1ib3R0b206IDFyZW07XHJcbiAgfVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/product_catalog/data/data.component.ts":
/*!********************************************************!*\
  !*** ./src/app/product_catalog/data/data.component.ts ***!
  \********************************************************/
/*! exports provided: DataComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataComponent", function() { return DataComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_dataServices__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/dataServices */ "./src/app/services/dataServices.ts");




var DataComponent = /** @class */ (function () {
    function DataComponent(router, dataService) {
        this.router = router;
        this.dataService = dataService;
        this.voice = new Array();
    }
    DataComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataService.getProducts().subscribe(function (res) {
            _this.voice = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(res.products);
        });
    };
    DataComponent.prototype.product = function (name) {
        this.router.navigate(['/product-catalog', name]);
    };
    DataComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _services_dataServices__WEBPACK_IMPORTED_MODULE_3__["dataServices"] }
    ]; };
    DataComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-data',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./data.component.html */ "./node_modules/raw-loader/index.js!./src/app/product_catalog/data/data.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./data.component.css */ "./src/app/product_catalog/data/data.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _services_dataServices__WEBPACK_IMPORTED_MODULE_3__["dataServices"]])
    ], DataComponent);
    return DataComponent;
}());



/***/ }),

/***/ "./src/app/product_catalog/voice/voice.component.css":
/*!***********************************************************!*\
  !*** ./src/app/product_catalog/voice/voice.component.css ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".text-muted{\r\n    text-align: right;\r\n }\r\n    [dir=rtl]  \r\n.h2,.mr-2, .mx-2{\r\n    float: right;\r\n }\r\n    .link{\r\n    color:#4F008c !important;\r\n    cursor: pointer;\r\n }\r\n    .card{\r\n    height: 100px!important;\r\n    background-clip: padding-box;\r\n  }\r\n    .pb-13{\r\n    padding-bottom: 16rem !important\r\n  }\r\n    .py-4{\r\n    text-align: end;\r\n }\r\n    .mb-0{\r\n    text-align: center;\r\n    padding-bottom: 2rem;\r\n }\r\n    .col-xl-3{\r\n    padding-bottom: 1rem;\r\n  }\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdF9jYXRhbG9nL3ZvaWNlL3ZvaWNlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxpQkFBaUI7Q0FDcEI7SUFDRzs7SUFFQSxZQUFZO0NBQ2Y7SUFFRDtJQUNJLHdCQUF3QjtJQUN4QixlQUFlO0NBQ2xCO0lBQ0Q7SUFDSSx1QkFBdUI7SUFDdkIsNEJBQTRCO0VBQzlCO0lBQ0Y7SUFDSTtFQUNGO0lBQ0Y7SUFDSSxlQUFlO0NBQ2xCO0lBQ0Q7SUFDSSxrQkFBa0I7SUFDbEIsb0JBQW9CO0NBQ3ZCO0lBQ0Q7SUFDSSxvQkFBb0I7RUFDdEIiLCJmaWxlIjoic3JjL2FwcC9wcm9kdWN0X2NhdGFsb2cvdm9pY2Uvdm9pY2UuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi50ZXh0LW11dGVke1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiB9XHJcbiAgICBbZGlyPXJ0bF0gIFxyXG4uaDIsLm1yLTIsIC5teC0ye1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gfVxyXG5cclxuLmxpbmt7XHJcbiAgICBjb2xvcjojNEYwMDhjICFpbXBvcnRhbnQ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiB9XHJcbi5jYXJke1xyXG4gICAgaGVpZ2h0OiAxMDBweCFpbXBvcnRhbnQ7XHJcbiAgICBiYWNrZ3JvdW5kLWNsaXA6IHBhZGRpbmctYm94O1xyXG4gIH1cclxuLnBiLTEze1xyXG4gICAgcGFkZGluZy1ib3R0b206IDE2cmVtICFpbXBvcnRhbnRcclxuICB9XHJcbi5weS00e1xyXG4gICAgdGV4dC1hbGlnbjogZW5kO1xyXG4gfVxyXG4ubWItMHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHBhZGRpbmctYm90dG9tOiAycmVtO1xyXG4gfVxyXG4uY29sLXhsLTN7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMXJlbTtcclxuICB9XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/product_catalog/voice/voice.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/product_catalog/voice/voice.component.ts ***!
  \**********************************************************/
/*! exports provided: VoiceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VoiceComponent", function() { return VoiceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_voiceServices__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../services/voiceServices */ "./src/app/services/voiceServices.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




var VoiceComponent = /** @class */ (function () {
    function VoiceComponent(router, voiceServices) {
        this.router = router;
        this.voiceServices = voiceServices;
        this.voice = new Array();
    }
    VoiceComponent.prototype.ngAfterViewInit = function () {
    };
    VoiceComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.voiceServices.getProducts().subscribe(function (res) {
            _this.voice = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(res.products);
        });
    };
    VoiceComponent.prototype.product = function (name) {
        this.router.navigate(['/product-catalog', name]);
    };
    VoiceComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _services_voiceServices__WEBPACK_IMPORTED_MODULE_1__["voiceServices"] }
    ]; };
    VoiceComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-voice',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./voice.component.html */ "./node_modules/raw-loader/index.js!./src/app/product_catalog/voice/voice.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./voice.component.css */ "./src/app/product_catalog/voice/voice.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _services_voiceServices__WEBPACK_IMPORTED_MODULE_1__["voiceServices"]])
    ], VoiceComponent);
    return VoiceComponent;
}());



/***/ }),

/***/ "./src/app/register-user/register-user.component.css":
/*!***********************************************************!*\
  !*** ./src/app/register-user/register-user.component.css ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".bg-gradient-danger{\r\n    background: #4F008c !important;\r\n}\r\n.pt-md-8{\r\n    padding-top: 2rem !important;\r\n}\r\n.h3 .mat-typography{\r\n    font-family: inherit;\r\n    font-weight: 600 !important;\r\n    line-height: 1.5;\r\n    margin-bottom: 0 !important;\r\n    font-size: 1.0625rem;\r\n}\r\n.header {\r\n    color:white;\r\n    font-size: 27px;\r\n    font: bold;\r\n    padding: 10px;\r\n    margin-top: 15vh;\r\n    max-width: 100%;\r\n}\r\n.bigicon {\r\n    font-size: 35px;\r\n    color:  #FF375D;}\r\n.costom-contact{\r\n        margin: auto;\r\n    }\r\n.btn-lg{\r\n        background-color:#FF375D ;\r\n      margin-right: 26vh;\r\n    }\r\n.bb{\r\n        background-color: #4F008C;\r\n\r\n    }\r\n/* .nav-item{\r\n        font: bold;\r\n        color: #FF375D;\r\n    } */\r\n.costom-con {\r\n    color: white;     \r\n    font-size: larger;\r\n    margin-left: 40vh;\r\n\r\n}\r\n.sub{\r\n\r\n    margin-right: 30vh;\r\n}\r\n.btn-cancel{\r\n    background-color: #FF375D;\r\n    color: aliceblue;\r\n}\r\n.btn-send{\r\n    background-color:#4F008C;\r\n    color: aliceblue;\r\n\r\n}\r\n.country-picker {\r\n    border: 1px solid #f0f3f5;\r\n    border-radius: 5px;\r\n    position: relative;\r\n    margin-bottom: 18px;\r\n    width: 150%;\r\n}\r\n.picker-button {\r\n    background: #f0f3f5;\r\n    /* color: #737e88; */\r\n    width: 28px;\r\n    height: 31px;\r\n    text-align: center;\r\n    line-height: 30px;\r\n    cursor: pointer;\r\n    display: inline-block;\r\n}\r\n.current{\r\n    display: inline-block;\r\n}\r\n.country-picker ul {\r\n    list-style: none;\r\n    position: absolute;\r\n    background: #fff;\r\n    padding: 0;\r\n    width: 100%;\r\n    margin: 0;\r\n    height: calc(35px * 5);\r\n    overflow: hidden;\r\n    overflow-y: scroll;\r\n    z-index: 99;\r\n}\r\n.country-picker ul li {\r\n    padding: 7px;\r\n    cursor: pointer;\r\n    border-bottom: 1px solid #777;\r\n    transition: 0.3s;\r\n    margin-top: 20px;\r\n\r\n}\r\n.country-picker ul li:hover{\r\n    background: #4F008C;\r\n    color: #fff;\r\n}\r\n.country-picker img{\r\nwidth: 20px;\r\n\r\n}\r\n.region{\r\n    background: #f0f3f5;\r\n    color: #737e88;\r\n    width: 40px;\r\n    height: 30px;\r\n    text-align: center;\r\n    line-height: 30px;\r\n    cursor: pointer;\r\n    display: inline-block\r\n}\r\n.region li{\r\n\r\n    text-align: center;\r\n}\r\n.region ul li{\r\n    padding: 7px;\r\n    cursor: pointer;\r\n    border-bottom: 1px solid #777;\r\n    transition: 0.3s;\r\n    display: flex;\r\n}\r\n.regBu{\r\n    margin:auto;\r\n    display:block;\r\n    display: flex;\r\n}\r\n.img{\r\n    position: absolute;\r\n    left: 26rem;\r\n    top: 3rem;\r\n}\r\n.h2{\r\n    font-family: inherit;\r\n    font-weight: 600;\r\n    line-height: 1.5;\r\n    font-size: 1.25rem;\r\n}\r\n.background-pink{\r\n    background-color: #ff375d;\r\n}\r\n.color-white{\r\n    color: white;\r\n    background-color: #ff375d;\r\n}\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVnaXN0ZXItdXNlci9yZWdpc3Rlci11c2VyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSw4QkFBOEI7QUFDbEM7QUFDQTtJQUNJLDRCQUE0QjtBQUNoQztBQUNBO0lBQ0ksb0JBQW9CO0lBQ3BCLDJCQUEyQjtJQUMzQixnQkFBZ0I7SUFDaEIsMkJBQTJCO0lBQzNCLG9CQUFvQjtBQUN4QjtBQUNBO0lBQ0ksV0FBVztJQUNYLGVBQWU7SUFDZixVQUFVO0lBQ1YsYUFBYTtJQUNiLGdCQUFnQjtJQUNoQixlQUFlO0FBQ25CO0FBRUE7SUFDSSxlQUFlO0lBQ2YsZUFBZSxDQUFDO0FBQ2hCO1FBQ0ksWUFBWTtJQUNoQjtBQUVBO1FBQ0kseUJBQXlCO01BQzNCLGtCQUFrQjtJQUNwQjtBQUNBO1FBQ0kseUJBQXlCOztJQUU3QjtBQUNBOzs7T0FHRztBQUVQO0lBQ0ksWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixpQkFBaUI7O0FBRXJCO0FBQ0E7O0lBRUksa0JBQWtCO0FBQ3RCO0FBRUE7SUFDSSx5QkFBeUI7SUFDekIsZ0JBQWdCO0FBQ3BCO0FBQ0E7SUFDSSx3QkFBd0I7SUFDeEIsZ0JBQWdCOztBQUVwQjtBQUNBO0lBQ0kseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLFdBQVc7QUFDZjtBQUNBO0lBQ0ksbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQixXQUFXO0lBQ1gsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixpQkFBaUI7SUFDakIsZUFBZTtJQUNmLHFCQUFxQjtBQUN6QjtBQUNBO0lBQ0kscUJBQXFCO0FBQ3pCO0FBQ0E7SUFDSSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixVQUFVO0lBQ1YsV0FBVztJQUNYLFNBQVM7SUFDVCxzQkFBc0I7SUFDdEIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixXQUFXO0FBQ2Y7QUFDQTtJQUNJLFlBQVk7SUFDWixlQUFlO0lBQ2YsNkJBQTZCO0lBQzdCLGdCQUFnQjtJQUNoQixnQkFBZ0I7O0FBRXBCO0FBQ0E7SUFDSSxtQkFBbUI7SUFDbkIsV0FBVztBQUNmO0FBQ0E7QUFDQSxXQUFXOztBQUVYO0FBQ0E7SUFDSSxtQkFBbUI7SUFDbkIsY0FBYztJQUNkLFdBQVc7SUFDWCxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2Y7QUFDSjtBQUNBOztJQUVJLGtCQUFrQjtBQUN0QjtBQUNBO0lBQ0ksWUFBWTtJQUNaLGVBQWU7SUFDZiw2QkFBNkI7SUFDN0IsZ0JBQWdCO0lBQ2hCLGFBQWE7QUFDakI7QUFFQTtJQUNJLFdBQVc7SUFDWCxhQUFhO0lBQ2IsYUFBYTtBQUNqQjtBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxTQUFTO0FBQ2I7QUFFQTtJQUNJLG9CQUFvQjtJQUNwQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtBQUN0QjtBQUVBO0lBQ0kseUJBQXlCO0FBQzdCO0FBRUE7SUFDSSxZQUFZO0lBQ1oseUJBQXlCO0FBQzdCIiwiZmlsZSI6InNyYy9hcHAvcmVnaXN0ZXItdXNlci9yZWdpc3Rlci11c2VyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmctZ3JhZGllbnQtZGFuZ2Vye1xyXG4gICAgYmFja2dyb3VuZDogIzRGMDA4YyAhaW1wb3J0YW50O1xyXG59XHJcbi5wdC1tZC04e1xyXG4gICAgcGFkZGluZy10b3A6IDJyZW0gIWltcG9ydGFudDtcclxufVxyXG4uaDMgLm1hdC10eXBvZ3JhcGh5e1xyXG4gICAgZm9udC1mYW1pbHk6IGluaGVyaXQ7XHJcbiAgICBmb250LXdlaWdodDogNjAwICFpbXBvcnRhbnQ7XHJcbiAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xyXG4gICAgZm9udC1zaXplOiAxLjA2MjVyZW07XHJcbn1cclxuLmhlYWRlciB7XHJcbiAgICBjb2xvcjp3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMjdweDtcclxuICAgIGZvbnQ6IGJvbGQ7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMTV2aDtcclxuICAgIG1heC13aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmJpZ2ljb24ge1xyXG4gICAgZm9udC1zaXplOiAzNXB4O1xyXG4gICAgY29sb3I6ICAjRkYzNzVEO31cclxuICAgIC5jb3N0b20tY29udGFjdHtcclxuICAgICAgICBtYXJnaW46IGF1dG87XHJcbiAgICB9XHJcblxyXG4gICAgLmJ0bi1sZ3tcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiNGRjM3NUQgO1xyXG4gICAgICBtYXJnaW4tcmlnaHQ6IDI2dmg7XHJcbiAgICB9XHJcbiAgICAuYmJ7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzRGMDA4QztcclxuXHJcbiAgICB9XHJcbiAgICAvKiAubmF2LWl0ZW17XHJcbiAgICAgICAgZm9udDogYm9sZDtcclxuICAgICAgICBjb2xvcjogI0ZGMzc1RDtcclxuICAgIH0gKi9cclxuXHJcbi5jb3N0b20tY29uIHtcclxuICAgIGNvbG9yOiB3aGl0ZTsgICAgIFxyXG4gICAgZm9udC1zaXplOiBsYXJnZXI7XHJcbiAgICBtYXJnaW4tbGVmdDogNDB2aDtcclxuXHJcbn1cclxuLnN1YntcclxuXHJcbiAgICBtYXJnaW4tcmlnaHQ6IDMwdmg7XHJcbn1cclxuXHJcbi5idG4tY2FuY2Vse1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGMzc1RDtcclxuICAgIGNvbG9yOiBhbGljZWJsdWU7XHJcbn1cclxuLmJ0bi1zZW5ke1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjojNEYwMDhDO1xyXG4gICAgY29sb3I6IGFsaWNlYmx1ZTtcclxuXHJcbn1cclxuLmNvdW50cnktcGlja2VyIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNmMGYzZjU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxOHB4O1xyXG4gICAgd2lkdGg6IDE1MCU7XHJcbn1cclxuLnBpY2tlci1idXR0b24ge1xyXG4gICAgYmFja2dyb3VuZDogI2YwZjNmNTtcclxuICAgIC8qIGNvbG9yOiAjNzM3ZTg4OyAqL1xyXG4gICAgd2lkdGg6IDI4cHg7XHJcbiAgICBoZWlnaHQ6IDMxcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBsaW5lLWhlaWdodDogMzBweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG4uY3VycmVudHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG4uY291bnRyeS1waWNrZXIgdWwge1xyXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBoZWlnaHQ6IGNhbGMoMzVweCAqIDUpO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIG92ZXJmbG93LXk6IHNjcm9sbDtcclxuICAgIHotaW5kZXg6IDk5O1xyXG59XHJcbi5jb3VudHJ5LXBpY2tlciB1bCBsaSB7XHJcbiAgICBwYWRkaW5nOiA3cHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzc3NztcclxuICAgIHRyYW5zaXRpb246IDAuM3M7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG5cclxufVxyXG4uY291bnRyeS1waWNrZXIgdWwgbGk6aG92ZXJ7XHJcbiAgICBiYWNrZ3JvdW5kOiAjNEYwMDhDO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbn1cclxuLmNvdW50cnktcGlja2VyIGltZ3tcclxud2lkdGg6IDIwcHg7XHJcblxyXG59XHJcbi5yZWdpb257XHJcbiAgICBiYWNrZ3JvdW5kOiAjZjBmM2Y1O1xyXG4gICAgY29sb3I6ICM3MzdlODg7XHJcbiAgICB3aWR0aDogNDBweDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrXHJcbn1cclxuLnJlZ2lvbiBsaXtcclxuXHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLnJlZ2lvbiB1bCBsaXtcclxuICAgIHBhZGRpbmc6IDdweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjNzc3O1xyXG4gICAgdHJhbnNpdGlvbjogMC4zcztcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbi5yZWdCdXtcclxuICAgIG1hcmdpbjphdXRvO1xyXG4gICAgZGlzcGxheTpibG9jaztcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbi5pbWd7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiAyNnJlbTtcclxuICAgIHRvcDogM3JlbTtcclxufVxyXG5cclxuLmgye1xyXG4gICAgZm9udC1mYW1pbHk6IGluaGVyaXQ7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcclxufVxyXG5cclxuLmJhY2tncm91bmQtcGlua3tcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZjM3NWQ7XHJcbn1cclxuXHJcbi5jb2xvci13aGl0ZXtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZjM3NWQ7XHJcbn1cclxuICAiXX0= */");

/***/ }),

/***/ "./src/app/register-user/register-user.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/register-user/register-user.component.ts ***!
  \**********************************************************/
/*! exports provided: RegisterUserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterUserComponent", function() { return RegisterUserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/models/regionsEnum */ "./src/app/shared/models/regionsEnum.ts");
/* harmony import */ var _shared_services_shared_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/services/shared.service */ "./src/app/shared/services/shared.service.ts");
/* harmony import */ var _language_language_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../language/language.helper */ "./src/app/language/language.helper.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_regiser_services__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/regiser.services */ "./src/app/services/regiser.services.ts");
/* harmony import */ var _dataModels_register_model__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../dataModels/register.model */ "./src/app/dataModels/register.model.ts");










// import { registerDto } from './../../../dataModels/register.model';
var RegisterUserComponent = /** @class */ (function () {
    function RegisterUserComponent(shared, languageHelper, translateService, router, formBuilder, registerServices) {
        this.shared = shared;
        this.languageHelper = languageHelper;
        this.translateService = translateService;
        this.router = router;
        this.formBuilder = formBuilder;
        this.registerServices = registerServices;
        this.isCountryOpen = false;
        this.isRegister = false;
        this.registerDto = new _dataModels_register_model__WEBPACK_IMPORTED_MODULE_9__["registerDto"]();
        this.listCountries = [];
        this.selectedCountry = { name: "Saudi Arabia", flag: null };
        this.isRegionOpen = false;
        this.listRegion = [
            { display: "EU (European Union)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_3__["RegionsEnum"].EU },
            { display: "EFTA (European Free Trade Association)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_3__["RegionsEnum"].EFTA },
            { display: "CARICOM (Caribbean Community)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_3__["RegionsEnum"].CARICOM },
            { display: "USAN (Union of South American Nations)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_3__["RegionsEnum"].USAN },
            { display: "EEU (Eurasian Economic Union)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_3__["RegionsEnum"].EEU },
            { display: "AL (Arab League)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_3__["RegionsEnum"].AL },
            { display: "ASEAN (Association of Southeast Asian Nations)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_3__["RegionsEnum"].ASEAN },
            { display: "CAIS (Central American Integration System)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_3__["RegionsEnum"].CAIS },
            { display: "CEFTA (Central European Free Trade Agreement)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_3__["RegionsEnum"].CEFTA },
            { display: "NAFTA (North American Free Trade Agreement)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_3__["RegionsEnum"].NAFTA },
            { display: "SAARC (South Asian Association for Regional Cooperation)", value: _shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_3__["RegionsEnum"].SAARC },
        ];
        this.isRTL = this.languageHelper.currentLang().isRTL;
    }
    RegisterUserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.shared.getAllCountries().subscribe(function (res) { return _this.listCountries = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(res); });
        this.registerForm = this.formBuilder.group({
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[^\\s@]+@[^\\s@]+\\.[^\\s@]{2,}$'), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(25)])]],
            fName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(1), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(10)]],
            lName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(1), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(10)]],
            company: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(1), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(30)]],
            code: ['+966'],
            phone: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(1), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(15)]],
        });
    };
    RegisterUserComponent.prototype.selectCountry = function (country) {
        this.isCountryOpen = false;
        this.selectedCountry = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"])({}, country);
        this.registerForm.get("code").setValue("+" + country.callingCodes[0]);
    };
    RegisterUserComponent.prototype.trueChart = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)
            && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
    };
    RegisterUserComponent.prototype.trueChartEmail = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57)
            && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
    };
    RegisterUserComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    RegisterUserComponent.prototype.selectRegion = function () {
        var _this = this;
        var region = this.registerForm.get('region').value;
        console.log(_shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_3__["RegionsEnum"][region]);
        this.shared.getCountriesWithRegion(_shared_models_regionsEnum__WEBPACK_IMPORTED_MODULE_3__["RegionsEnum"][region]).subscribe(function (res) { return _this.listCountries = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(res); });
    };
    RegisterUserComponent.prototype.newPartner = function () {
        var _this = this;
        this.isRegister = true;
        if (this.registerForm.valid) {
            this.registerDto.company = this.registerForm.value.company;
            this.registerDto.email = this.registerForm.value.email;
            this.registerDto.country = this.registerForm.value.phone;
            this.registerDto.phone = this.registerForm.value.phone;
            this.registerDto.firstName = this.registerForm.value.fName;
            this.registerDto.lastName = this.registerForm.value.lName;
            // this.registerDto.country=this.registerForm.value.country
            this.registerServices.postRegister(this.registerDto).subscribe(function (res) {
                console.log(res.message);
                _this.router.navigate(['email-confirmation']);
            });
        }
        if (this.registerForm.get('email').invalid) {
        }
        if (this.registerForm.get('phone').invalid) {
        }
        if (this.registerForm.get('fName').invalid) {
        }
        if (this.registerForm.get('lName').invalid) {
        }
        if (this.registerForm.get('company').invalid) {
        }
    };
    RegisterUserComponent.ctorParameters = function () { return [
        { type: _shared_services_shared_service__WEBPACK_IMPORTED_MODULE_4__["SharedService"] },
        { type: _language_language_helper__WEBPACK_IMPORTED_MODULE_5__["LanguageHelper"] },
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _services_regiser_services__WEBPACK_IMPORTED_MODULE_8__["registerServices"] }
    ]; };
    RegisterUserComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-register-user',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./register-user.component.html */ "./node_modules/raw-loader/index.js!./src/app/register-user/register-user.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./register-user.component.css */ "./src/app/register-user/register-user.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_shared_services_shared_service__WEBPACK_IMPORTED_MODULE_4__["SharedService"], _language_language_helper__WEBPACK_IMPORTED_MODULE_5__["LanguageHelper"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateService"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _services_regiser_services__WEBPACK_IMPORTED_MODULE_8__["registerServices"]])
    ], RegisterUserComponent);
    return RegisterUserComponent;
}());



/***/ }),

/***/ "./src/app/services/Virtual.services.ts":
/*!**********************************************!*\
  !*** ./src/app/services/Virtual.services.ts ***!
  \**********************************************/
/*! exports provided: VirtualServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VirtualServices", function() { return VirtualServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



var VirtualServices = /** @class */ (function () {
    function VirtualServices(_http) {
        this._http = _http;
        this.mainURL = "https://172.21.51.217:19090";
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
        this.headers = this.headers.append('Access-Control-Allow-Origin', '*');
    }
    VirtualServices.prototype.getVirtual = function () {
        return this._http.get(this.mainURL + "/wbu/registration/virtual-account?size=10&page=1", { headers: this.headers });
    };
    VirtualServices.prototype.deleteVirtual = function () {
        return this._http.post(this.mainURL + "/wbu/registration/virtual-account/delete", { headers: this.headers });
    };
    VirtualServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    VirtualServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], VirtualServices);
    return VirtualServices;
}());



/***/ }),

/***/ "./src/app/services/approvereg.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/approvereg.service.ts ***!
  \************************************************/
/*! exports provided: ApproveregServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApproveregServices", function() { return ApproveregServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



var ApproveregServices = /** @class */ (function () {
    function ApproveregServices(_http) {
        this._http = _http;
        this.mainURL = "https://172.21.51.217:19090";
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
        this.headers = this.headers.append('Access-Control-Allow-Origin', '*');
    }
    ApproveregServices.prototype.getReqList = function () {
        return this._http.get(this.mainURL + "/wbu/registration/submitted?size=10&page=1", { headers: this.headers });
    };
    ApproveregServices.prototype.getReqDetails = function (id) {
        return this._http.post(this.mainURL + "/wbu/registration/submitted/details", { id: id }, { headers: this.headers });
    };
    ApproveregServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    ApproveregServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ApproveregServices);
    return ApproveregServices;
}());



/***/ }),

/***/ "./src/app/services/archive.services.ts":
/*!**********************************************!*\
  !*** ./src/app/services/archive.services.ts ***!
  \**********************************************/
/*! exports provided: archiveServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "archiveServices", function() { return archiveServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



// import {archiveDto}from '../../src/app/dataModels/archive.model'
// import { archiveListDto } from './dataModels/archiveListDto.model';
var archiveServices = /** @class */ (function () {
    function archiveServices(_http) {
        this._http = _http;
        this.mainURL = "https://172.21.51.217:19090";
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
    }
    archiveServices.prototype.getArchive = function () {
        return this._http.get(this.mainURL + '/wbu/admin/history?size=10&page=1', { headers: this.headers });
    };
    archiveServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    archiveServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], archiveServices);
    return archiveServices;
}());



/***/ }),

/***/ "./src/app/services/assetsServices.ts":
/*!********************************************!*\
  !*** ./src/app/services/assetsServices.ts ***!
  \********************************************/
/*! exports provided: assetsServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "assetsServices", function() { return assetsServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



var assetsServices = /** @class */ (function () {
    function assetsServices(_http) {
        this._http = _http;
        this.mainURL = "https://172.21.51.217:19090";
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
    }
    assetsServices.prototype.getAssets = function () {
        return this._http.get(this.mainURL + '/wbu/services/assets?size=4&page=1', { headers: this.headers });
    };
    assetsServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    assetsServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], assetsServices);
    return assetsServices;
}());



/***/ }),

/***/ "./src/app/services/capacityServices.ts":
/*!**********************************************!*\
  !*** ./src/app/services/capacityServices.ts ***!
  \**********************************************/
/*! exports provided: capacityServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "capacityServices", function() { return capacityServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



var capacityServices = /** @class */ (function () {
    function capacityServices(_http) {
        this._http = _http;
        this.mainURL = "https://172.21.51.217:19090";
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
    }
    capacityServices.prototype.getProducts = function () {
        return this._http.get(this.mainURL + '/wbu/products?category=Capacity', { headers: this.headers });
    };
    capacityServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    capacityServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], capacityServices);
    return capacityServices;
}());



/***/ }),

/***/ "./src/app/services/colocationServices.ts":
/*!************************************************!*\
  !*** ./src/app/services/colocationServices.ts ***!
  \************************************************/
/*! exports provided: colocationServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "colocationServices", function() { return colocationServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



var colocationServices = /** @class */ (function () {
    function colocationServices(_http) {
        this._http = _http;
        this.mainURL = "https://172.21.51.217:19090";
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
    }
    colocationServices.prototype.getProducts = function () {
        return this._http.get(this.mainURL + '/wbu/products?category=Co-Location', { headers: this.headers });
    };
    colocationServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    colocationServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], colocationServices);
    return colocationServices;
}());



/***/ }),

/***/ "./src/app/services/dataServices.ts":
/*!******************************************!*\
  !*** ./src/app/services/dataServices.ts ***!
  \******************************************/
/*! exports provided: dataServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dataServices", function() { return dataServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



var dataServices = /** @class */ (function () {
    function dataServices(_http) {
        this._http = _http;
        this.mainURL = "https://172.21.51.217:19090";
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
    }
    dataServices.prototype.getProducts = function () {
        return this._http.get(this.mainURL + '/wbu/products?category=Data Services', { headers: this.headers });
    };
    dataServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    dataServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], dataServices);
    return dataServices;
}());



/***/ }),

/***/ "./src/app/services/decision.services.ts":
/*!***********************************************!*\
  !*** ./src/app/services/decision.services.ts ***!
  \***********************************************/
/*! exports provided: DecisionServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DecisionServices", function() { return DecisionServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



var DecisionServices = /** @class */ (function () {
    function DecisionServices(_http) {
        this._http = _http;
        this.mainURL = "https://172.21.51.217:19090";
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
        this.headers = this.headers.append('Access-Control-Allow-Origin', '*');
    }
    DecisionServices.prototype.getDecision = function (Body) {
        return this._http.post(this.mainURL + "/wbu/registration/decision", Body, { headers: this.headers });
    };
    DecisionServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    DecisionServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], DecisionServices);
    return DecisionServices;
}());



/***/ }),

/***/ "./src/app/services/forgotpass.services.ts":
/*!*************************************************!*\
  !*** ./src/app/services/forgotpass.services.ts ***!
  \*************************************************/
/*! exports provided: forgotPassServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "forgotPassServices", function() { return forgotPassServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");



var forgotPassServices = /** @class */ (function () {
    function forgotPassServices(https) {
        this.https = https;
        this.mainURl = "https://172.21.51.217:19090/wbu/password/forgot";
    }
    forgotPassServices.prototype.postpass = function (email) {
        return this.https.post(this.mainURl, { email: email });
    };
    forgotPassServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
    ]; };
    forgotPassServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], forgotPassServices);
    return forgotPassServices;
}());



/***/ }),

/***/ "./src/app/services/login.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/login.service.ts ***!
  \*******************************************/
/*! exports provided: loginServices, logoutServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loginServices", function() { return loginServices; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "logoutServices", function() { return logoutServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");



var loginServices = /** @class */ (function () {
    function loginServices(https) {
        this.https = https;
        this.mainURl = "https://172.21.51.217:19090/wbu/login";
    }
    loginServices.prototype.postlogin = function (obj) {
        return this.https.post(this.mainURl, obj);
    };
    loginServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
    ]; };
    loginServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], loginServices);
    return loginServices;
}());

var logoutServices = /** @class */ (function () {
    function logoutServices(https) {
        this.https = https;
        this.mainURl = "https://172.21.51.217:19090/wbu/logout";
    }
    return logoutServices;
}());



/***/ }),

/***/ "./src/app/services/newpartner.services.ts":
/*!*************************************************!*\
  !*** ./src/app/services/newpartner.services.ts ***!
  \*************************************************/
/*! exports provided: partnerServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "partnerServices", function() { return partnerServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");



var partnerServices = /** @class */ (function () {
    function partnerServices(https) {
        this.https = https;
        this.mainURl = "https://172.21.51.217:19090/wbu/registration/new-partner";
    }
    partnerServices.prototype.postpartner = function (obj) {
        return this.https.post(this.mainURl, obj);
    };
    partnerServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
    ]; };
    partnerServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], partnerServices);
    return partnerServices;
}());



/***/ }),

/***/ "./src/app/services/orderServices.ts":
/*!*******************************************!*\
  !*** ./src/app/services/orderServices.ts ***!
  \*******************************************/
/*! exports provided: orderServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "orderServices", function() { return orderServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



// import {archiveDto}from '../../src/app/dataModels/archive.model'
// import { archiveListDto } from './dataModels/archiveListDto.model';
var orderServices = /** @class */ (function () {
    function orderServices(_http) {
        this._http = _http;
        this.mainURL = "https://172.21.51.217:19090";
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
    }
    orderServices.prototype.getOrders = function () {
        return this._http.get(this.mainURL + '/wbu/services/orders?size=10&page=1', { headers: this.headers });
    };
    orderServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    orderServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], orderServices);
    return orderServices;
}());



/***/ }),

/***/ "./src/app/services/product.services.ts":
/*!**********************************************!*\
  !*** ./src/app/services/product.services.ts ***!
  \**********************************************/
/*! exports provided: productServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productServices", function() { return productServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



var productServices = /** @class */ (function () {
    function productServices(_http) {
        this._http = _http;
        this.mainURL = "https://172.21.51.217:19090";
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
        // this.headers=this.headers.append('category','Capacity')
    }
    productServices.prototype.getPro = function () {
        return this._http.get(this.mainURL + '/wbu/products?category=Capacity', { headers: this.headers });
    };
    productServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    productServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], productServices);
    return productServices;
}());



/***/ }),

/***/ "./src/app/services/productCata.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/productCata.service.ts ***!
  \*************************************************/
/*! exports provided: productCataServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productCataServices", function() { return productCataServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



var productCataServices = /** @class */ (function () {
    function productCataServices(_http) {
        this._http = _http;
        this.mainURL = "https://172.21.51.217:19090";
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
        this.headers = this.headers.append('Access-Control-Allow-Origin', '*');
    }
    productCataServices.prototype.getReqDetails = function (name) {
        return this._http.post(this.mainURL + "/wbu/products/details", { name: name }, { headers: this.headers });
    };
    productCataServices.prototype.getImg = function (name) {
        return this._http.get(this.mainURL + "/wbu/products/img?imgName=" + name, { headers: this.headers });
    };
    productCataServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    productCataServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], productCataServices);
    return productCataServices;
}());



/***/ }),

/***/ "./src/app/services/regiser.services.ts":
/*!**********************************************!*\
  !*** ./src/app/services/regiser.services.ts ***!
  \**********************************************/
/*! exports provided: registerServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "registerServices", function() { return registerServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");



var registerServices = /** @class */ (function () {
    function registerServices(https) {
        this.https = https;
        this.mainURl = "https://172.21.51.217:19090/wbu/registration/partner";
    }
    registerServices.prototype.postRegister = function (obj) {
        return this.https.post(this.mainURl, obj);
    };
    registerServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
    ]; };
    registerServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], registerServices);
    return registerServices;
}());



/***/ }),

/***/ "./src/app/services/target.services.ts":
/*!*********************************************!*\
  !*** ./src/app/services/target.services.ts ***!
  \*********************************************/
/*! exports provided: TargetServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TargetServices", function() { return TargetServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



var TargetServices = /** @class */ (function () {
    // headers: HttpHeaders | { [header: string]: string | string[]; };
    function TargetServices(https) {
        this.https = https;
        this.mainURl = "https://172.21.51.217:19090";
    }
    TargetServices.prototype.getTarget = function () {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers = headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
        console.log(headers);
        return this.https.get(this.mainURl + "/wbu/cdrs/users/billing");
    };
    TargetServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    TargetServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], TargetServices);
    return TargetServices;
}());



/***/ }),

/***/ "./src/app/services/upload.services.ts":
/*!*********************************************!*\
  !*** ./src/app/services/upload.services.ts ***!
  \*********************************************/
/*! exports provided: uploadServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "uploadServices", function() { return uploadServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");



var uploadServices = /** @class */ (function () {
    function uploadServices(https) {
        this.https = https;
        this.mainURl = "https://172.21.51.217:19090/wbu/files/upload";
        this.URL = "https://172.21.51.217:19090/wbu/cdrs/billing";
    }
    uploadServices.prototype.postUpload = function (obj) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]();
        headers = headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
        console.log(headers);
        return this.https.post(this.mainURl, obj, { headers: headers });
    };
    uploadServices.prototype.postCdrs = function (obj) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]();
        headers = headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
        console.log(headers);
        return this.https.post(this.URL, obj, { headers: headers });
    };
    uploadServices.prototype.submitCDR = function () {
    };
    uploadServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
    ]; };
    uploadServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], uploadServices);
    return uploadServices;
}());



/***/ }),

/***/ "./src/app/services/voiceServices.ts":
/*!*******************************************!*\
  !*** ./src/app/services/voiceServices.ts ***!
  \*******************************************/
/*! exports provided: voiceServices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "voiceServices", function() { return voiceServices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



var voiceServices = /** @class */ (function () {
    function voiceServices(_http) {
        this._http = _http;
        this.mainURL = "https://172.21.51.217:19090";
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        this.headers = this.headers.append('Authorization', "Bearer " + localStorage.getItem("token"));
    }
    voiceServices.prototype.getProducts = function () {
        return this._http.get(this.mainURL + '/wbu/products?category=Voice', { headers: this.headers });
    };
    voiceServices.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    voiceServices = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], voiceServices);
    return voiceServices;
}());



/***/ }),

/***/ "./src/app/shared/models/regionsEnum.ts":
/*!**********************************************!*\
  !*** ./src/app/shared/models/regionsEnum.ts ***!
  \**********************************************/
/*! exports provided: RegionsEnum */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegionsEnum", function() { return RegionsEnum; });
var RegionsEnum;
(function (RegionsEnum) {
    RegionsEnum[RegionsEnum["EU"] = 0] = "EU";
    RegionsEnum[RegionsEnum["EFTA"] = 1] = "EFTA";
    RegionsEnum[RegionsEnum["CARICOM"] = 2] = "CARICOM";
    RegionsEnum[RegionsEnum["PA"] = 3] = "PA";
    RegionsEnum[RegionsEnum["AU"] = 4] = "AU";
    RegionsEnum[RegionsEnum["USAN"] = 5] = "USAN";
    RegionsEnum[RegionsEnum["EEU"] = 6] = "EEU";
    RegionsEnum[RegionsEnum["AL"] = 7] = "AL";
    RegionsEnum[RegionsEnum["ASEAN"] = 8] = "ASEAN";
    RegionsEnum[RegionsEnum["CAIS"] = 9] = "CAIS";
    RegionsEnum[RegionsEnum["CEFTA"] = 10] = "CEFTA";
    RegionsEnum[RegionsEnum["NAFTA"] = 11] = "NAFTA";
    RegionsEnum[RegionsEnum["SAARC"] = 12] = "SAARC";
})(RegionsEnum || (RegionsEnum = {}));


/***/ }),

/***/ "./src/app/shared/models/share.module.ts":
/*!***********************************************!*\
  !*** ./src/app/shared/models/share.module.ts ***!
  \***********************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _language_language_helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../language/language.helper */ "./src/app/language/language.helper.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var ngx_webstorage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-webstorage */ "./node_modules/ngx-webstorage/fesm5/ngx-webstorage.js");


// import { ServicesModule } from './services/services.module';





var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"], ngx_webstorage__WEBPACK_IMPORTED_MODULE_6__["NgxWebstorageModule"]],
            exports: [],
            declarations: [],
            providers: [_language_language_helper__WEBPACK_IMPORTED_MODULE_1__["LanguageHelper"]]
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./src/app/shared/services/shared.service.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/services/shared.service.ts ***!
  \***************************************************/
/*! exports provided: SharedService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedService", function() { return SharedService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



var SharedService = /** @class */ (function () {
    function SharedService(http) {
        this.http = http;
    }
    SharedService.prototype.getAllCountries = function () {
        return this.http.get('https://restcountries.eu/rest/v2/all');
    };
    SharedService.prototype.getCountriesWithRegion = function (region) {
        return this.http.get('https://restcountries.eu/rest/v2/regionalbloc/' + region);
    };
    SharedService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    SharedService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], SharedService);
    return SharedService;
}());



/***/ }),

/***/ "./src/app/sidebar/sidebar.component.css":
/*!***********************************************!*\
  !*** ./src/app/sidebar/sidebar.component.css ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NpZGViYXIvc2lkZWJhci5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/sidebar/sidebar.component.ts":
/*!**********************************************!*\
  !*** ./src/app/sidebar/sidebar.component.ts ***!
  \**********************************************/
/*! exports provided: ROUTES, SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




var ROUTES = [
    { path: '/dashboard', title: 'Dashboard', icon: 'ni-tv-2 text-primary', class: '' },
    { path: '/upload-file', title: 'Time Sheet', icon: 'ni-planet text-blue', class: '' },
    { path: '/my-task', title: 'My Task', icon: 'ni-planet text-blue', class: '' },
];
var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(location, element, router) {
        this.element = element;
        this.router = router;
        this.isCollapsed = true;
    }
    SidebarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        this.router.events.subscribe(function (event) {
            _this.isCollapsed = true;
        });
    };
    SidebarComponent.ctorParameters = function () { return [
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    SidebarComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sidebar',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./sidebar.component.html */ "./node_modules/raw-loader/index.js!./src/app/sidebar/sidebar.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./sidebar.component.css */ "./src/app/sidebar/sidebar.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/upload/upload.component.css":
/*!*********************************************!*\
  !*** ./src/app/upload/upload.component.css ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("::ng-deep .nav-tabs .nav-link.active{\r\n    background :#ffff;\r\n}\r\n/* ::ng-deep .nav-tabs .nav-link.active{\r\nbackground: #4F008c !important;\r\n} */\r\n.form-group.hidden {\r\n  width: 0;\r\n  margin: 0;\r\n  border: none;\r\n  padding: 0;\r\n}\r\n.form-group.hidden .form-control{\r\n  display: none;\r\n}\r\n.custom-day {\r\n  display: inline-block;\r\n}\r\n.mb-0{\r\n  font-size: 1.0625rem;\r\n  font-weight: 600;\r\n}\r\n.header{\r\n  background: #f7fafc !important;\r\n}\r\n.form-control-label{\r\n  font-weight: 600;\r\n}\r\n.col-lg-4{\r\n  margin-top: 2rem;\r\n  max-width: 49.33333%\r\n}\r\n.col-lg-000{\r\n  margin-top: 2rem;\r\n  max-width: 90.33333%;\r\n  flex: 0 0 33.33333%;\r\n  position: relative;\r\n  width: 100%;\r\n  min-height: 1px;\r\n  padding-right: 15px;\r\n  padding-left: 15px;\r\n}\r\n::ng-deep .custom-day.focused {\r\n  background-color: #e6e6e6;\r\n}\r\n::ng-deep .custom-day.range, ::ng-deep .custom-day:hover {\r\n  color: #FF375E;\r\n}\r\n.btn-outline-secondary {\r\n  color: #23282c;\r\n  border: 1px solid #cad1d7;\r\n  background-color: #fff;\r\n}\r\n::ng-deep .tab-content .tab-pane {\r\n  padding: 0rem !important;\r\n}\r\n::ng-deep .justify-content-start {\r\npadding-left: 15px !important;\r\n}\r\n.submit{\r\n  color: #fff;\r\n  background-color: #FF375E;\r\n  border-color: #FF375E;\r\n  font-weight: 600;\r\n  text-align: center;\r\n  overflow: visible;\r\n    white-space: nowrap;\r\n    vertical-align: middle;\r\n    -webkit-user-select: none;\r\n    -moz-user-select: none;\r\n    -ms-user-select: none;\r\n    user-select: none;\r\n    border: 1px solid transparent;\r\n    padding: 0.625rem 1.25rem;\r\n    font-size: 1rem;\r\n    line-height: 1.5;\r\n    border-radius: 0.375rem;\r\n    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;\r\nbox-shadow: 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08);\r\n}\r\n::ng-deep .card-header {\r\n  padding: 1rem 1rem !important\r\n}\r\n.card{\r\n  background: #f7fafc;\r\n}\r\n.tag {\r\n  border-radius: 50%;\r\n  height: 15px;\r\n  width: 15px;\r\n  display: inline-block;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXBsb2FkL3VwbG9hZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0FBQ3JCO0FBQ0E7O0dBRUc7QUFDSDtFQUNFLFFBQVE7RUFDUixTQUFTO0VBQ1QsWUFBWTtFQUNaLFVBQVU7QUFDWjtBQUNBO0VBQ0UsYUFBYTtBQUNmO0FBQ0E7RUFDRSxxQkFBcUI7QUFDdkI7QUFFQTtFQUNFLG9CQUFvQjtFQUNwQixnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLDhCQUE4QjtBQUNoQztBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBRUE7RUFDRSxnQkFBZ0I7RUFDaEI7QUFDRjtBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLGtCQUFrQjtBQUNwQjtBQUVBO0VBQ0UseUJBQXlCO0FBQzNCO0FBQ0E7RUFDRSxjQUFjO0FBQ2hCO0FBRUE7RUFDRSxjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLHNCQUFzQjtBQUN4QjtBQUVBO0VBQ0Usd0JBQXdCO0FBQzFCO0FBQ0E7QUFDQSw2QkFBNkI7QUFDN0I7QUFFQTtFQUNFLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIscUJBQXFCO0VBQ3JCLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsaUJBQWlCO0lBQ2YsbUJBQW1CO0lBQ25CLHNCQUFzQjtJQUN0Qix5QkFBeUI7SUFDekIsc0JBQXNCO0lBQ3RCLHFCQUFxQjtJQUNyQixpQkFBaUI7SUFDakIsNkJBQTZCO0lBQzdCLHlCQUF5QjtJQUN6QixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLHVCQUF1QjtJQUN2QixxSUFBcUk7QUFDekksMkVBQTJFO0FBQzNFO0FBQ0E7RUFDRTtBQUNGO0FBQ0E7RUFDRSxtQkFBbUI7QUFDckI7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osV0FBVztFQUNYLHFCQUFxQjtBQUN2QiIsImZpbGUiOiJzcmMvYXBwL3VwbG9hZC91cGxvYWQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIjo6bmctZGVlcCAubmF2LXRhYnMgLm5hdi1saW5rLmFjdGl2ZXtcclxuICAgIGJhY2tncm91bmQgOiNmZmZmO1xyXG59XHJcbi8qIDo6bmctZGVlcCAubmF2LXRhYnMgLm5hdi1saW5rLmFjdGl2ZXtcclxuYmFja2dyb3VuZDogIzRGMDA4YyAhaW1wb3J0YW50O1xyXG59ICovXHJcbi5mb3JtLWdyb3VwLmhpZGRlbiB7XHJcbiAgd2lkdGg6IDA7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIGJvcmRlcjogbm9uZTtcclxuICBwYWRkaW5nOiAwO1xyXG59XHJcbi5mb3JtLWdyb3VwLmhpZGRlbiAuZm9ybS1jb250cm9se1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuLmN1c3RvbS1kYXkge1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG5cclxuLm1iLTB7XHJcbiAgZm9udC1zaXplOiAxLjA2MjVyZW07XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxufVxyXG4uaGVhZGVye1xyXG4gIGJhY2tncm91bmQ6ICNmN2ZhZmMgIWltcG9ydGFudDtcclxufVxyXG4uZm9ybS1jb250cm9sLWxhYmVse1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbn1cclxuXHJcbi5jb2wtbGctNHtcclxuICBtYXJnaW4tdG9wOiAycmVtO1xyXG4gIG1heC13aWR0aDogNDkuMzMzMzMlXHJcbn1cclxuXHJcbi5jb2wtbGctMDAwe1xyXG4gIG1hcmdpbi10b3A6IDJyZW07XHJcbiAgbWF4LXdpZHRoOiA5MC4zMzMzMyU7XHJcbiAgZmxleDogMCAwIDMzLjMzMzMzJTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgbWluLWhlaWdodDogMXB4O1xyXG4gIHBhZGRpbmctcmlnaHQ6IDE1cHg7XHJcbiAgcGFkZGluZy1sZWZ0OiAxNXB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLmN1c3RvbS1kYXkuZm9jdXNlZCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U2ZTZlNjtcclxufVxyXG46Om5nLWRlZXAgLmN1c3RvbS1kYXkucmFuZ2UsIDo6bmctZGVlcCAuY3VzdG9tLWRheTpob3ZlciB7XHJcbiAgY29sb3I6ICNGRjM3NUU7XHJcbn1cclxuXHJcbi5idG4tb3V0bGluZS1zZWNvbmRhcnkge1xyXG4gIGNvbG9yOiAjMjMyODJjO1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNjYWQxZDc7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxufVxyXG5cclxuOjpuZy1kZWVwIC50YWItY29udGVudCAudGFiLXBhbmUge1xyXG4gIHBhZGRpbmc6IDByZW0gIWltcG9ydGFudDtcclxufVxyXG46Om5nLWRlZXAgLmp1c3RpZnktY29udGVudC1zdGFydCB7XHJcbnBhZGRpbmctbGVmdDogMTVweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uc3VibWl0e1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNGRjM3NUU7XHJcbiAgYm9yZGVyLWNvbG9yOiAjRkYzNzVFO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG92ZXJmbG93OiB2aXNpYmxlO1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgICAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgLW1vei11c2VyLXNlbGVjdDogbm9uZTtcclxuICAgIC1tcy11c2VyLXNlbGVjdDogbm9uZTtcclxuICAgIHVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgdHJhbnNwYXJlbnQ7XHJcbiAgICBwYWRkaW5nOiAwLjYyNXJlbSAxLjI1cmVtO1xyXG4gICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDAuMzc1cmVtO1xyXG4gICAgdHJhbnNpdGlvbjogY29sb3IgMC4xNXMgZWFzZS1pbi1vdXQsIGJhY2tncm91bmQtY29sb3IgMC4xNXMgZWFzZS1pbi1vdXQsIGJvcmRlci1jb2xvciAwLjE1cyBlYXNlLWluLW91dCwgYm94LXNoYWRvdyAwLjE1cyBlYXNlLWluLW91dDtcclxuYm94LXNoYWRvdzogMCA0cHggNnB4IHJnYmEoNTAsIDUwLCA5MywgMC4xMSksIDAgMXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMDgpO1xyXG59XHJcbjo6bmctZGVlcCAuY2FyZC1oZWFkZXIge1xyXG4gIHBhZGRpbmc6IDFyZW0gMXJlbSAhaW1wb3J0YW50XHJcbn1cclxuLmNhcmR7XHJcbiAgYmFja2dyb3VuZDogI2Y3ZmFmYztcclxufVxyXG4udGFnIHtcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgaGVpZ2h0OiAxNXB4O1xyXG4gIHdpZHRoOiAxNXB4O1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/upload/upload.component.ts":
/*!********************************************!*\
  !*** ./src/app/upload/upload.component.ts ***!
  \********************************************/
/*! exports provided: UploadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadComponent", function() { return UploadComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/fesm5/ng2-file-upload.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/fesm5/dialog.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_upload_services__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/upload.services */ "./src/app/services/upload.services.ts");
/* harmony import */ var _dataModels_uploads_model__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../dataModels/uploads.model */ "./src/app/dataModels/uploads.model.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _services_target_services__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../services/target.services */ "./src/app/services/target.services.ts");












// const URL = '/api/';
var URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
var UploadComponent = /** @class */ (function () {
    // targetU =new targetDtto();
    function UploadComponent(calendar, formatter, dialog, router, uploadServices, TargetServices) {
        var _this = this;
        this.calendar = calendar;
        this.formatter = formatter;
        this.dialog = dialog;
        this.router = router;
        this.uploadServices = uploadServices;
        this.TargetServices = TargetServices;
        this.mainURl = "https://172.21.51.217:19090/wbuTest3/files/upload";
        this.URL = "https://172.21.51.217:19090/wbu/cdrs/billing";
        this.options = [];
        this.size1 = "";
        this.textTech = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]('');
        this.textControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]('');
        this.descriptionTech = new rxjs__WEBPACK_IMPORTED_MODULE_10__["BehaviorSubject"](0);
        this.descriptionLength = new rxjs__WEBPACK_IMPORTED_MODULE_10__["BehaviorSubject"](0);
        this.showSpinner = false;
        this.targetU = new Array();
        this.isUpload = false;
        this.uploadDto = new _dataModels_uploads_model__WEBPACK_IMPORTED_MODULE_9__["uploadDto"]();
        this.billingForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormGroup"]({
            period: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required]),
            product: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required]),
            // fromDate: new FormControl("", [Validators.required]),
            // toDate: new FormControl("", [Validators.required]),
            // desc: new FormControl("", [Validators.required]),
            // doc: new FormControl(""),
            invoice: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required])
        });
        this.technicalForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormGroup"]({
            invoiceTech: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required]),
            periodTech: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required]),
            fromDateTech: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required]),
            toDateTech: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required]),
            productTech: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required]),
            targetTech: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required]),
            descTech: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required]),
            docTech: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]("", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required])
        });
        this.textControl.valueChanges.subscribe(function (v) { return _this.descriptionLength.next(v.length); });
        this.textTech.valueChanges.subscribe(function (v) { return _this.descriptionTech.next(v.length); });
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__["FileUploader"]({
            url: URL,
            disableMultipart: true,
            formatDataFunctionIsAsync: true,
            formatDataFunction: function (item) { return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, function () {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"])(this, function (_a) {
                    return [2 /*return*/, new Promise(function (resolve, reject) {
                            resolve({
                                name: item._file.name,
                                length: item._file.size,
                                contentType: item._file.type,
                                date: new Date()
                            });
                        })];
                });
            }); }
        });
        this.hasBaseDropZoneOver = false;
        this.hasAnotherDropZoneOver = false;
        this.response = '';
        this.uploader.response.subscribe(function (res) { return _this.response = res; });
        this.fromDate = calendar.getToday();
        this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
    }
    UploadComponent.prototype.openDialog = function () {
        // this.dialog.open(TargerUserComponent, {
        //   width: "300px",
        //   data: { options: this.options }
        // });
    };
    UploadComponent.prototype.getTarget = function () {
        var _this = this;
        this.TargetServices.getTarget().subscribe(function (res) {
            _this.targetU = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spreadArrays"])(res.targetUsers);
            // this.targetDto.targetUsers=[...res.targetUsers]
            console.log(res);
        });
    };
    UploadComponent.prototype.onDateSelection = function (date) {
        if (!this.fromDate && !this.toDate) {
            this.fromDate = date;
        }
        else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
            this.toDate = date;
        }
        else {
            this.toDate = null;
            this.fromDate = date;
        }
    };
    UploadComponent.prototype.isHovered = function (date) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) &&
            date.before(this.hoveredDate);
    };
    UploadComponent.prototype.isInside = function (date) {
        return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
    };
    UploadComponent.prototype.isRange = function (date) {
        return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
    };
    UploadComponent.prototype.validateInput = function (currentValue, input) {
        var parsed = this.formatter.parse(input);
        return parsed && this.calendar.isValid(_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbDate"].from(parsed)) ? _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbDate"].from(parsed) : currentValue;
    };
    UploadComponent.prototype.onFileSelected = function (event) {
        var files = event.target.files;
        if (this.validateFile(files[0].name)) {
            var file = event[0];
            this.uploadFile(files[0]);
        }
    };
    UploadComponent.prototype.validateFile = function (name) {
        var ext = name.substring(name.lastIndexOf('.') + 1);
        if ((ext.toLowerCase() == 'txt') || (ext.toLowerCase() == 'csv')) {
            return true;
        }
        else {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Invalid File Format'
            });
            return window.location.reload();
        }
    };
    UploadComponent.prototype.fileOverBase = function (e) {
        this.hasBaseDropZoneOver = e;
    };
    UploadComponent.prototype.fileOverAnother = function (e) {
        this.hasAnotherDropZoneOver = e;
    };
    UploadComponent.prototype.ngOnInit = function () {
        this.getTarget();
    };
    UploadComponent.prototype.uploadFile = function (file) {
        var fileData = new FormData();
        fileData.append("document", file);
        fileData.append("fileDetails", new Blob([JSON.stringify({ "fileType": file.type })], { type: "Application/json" }));
        console.log(fileData);
        console.log(file);
        this.uploadServices.postUpload(fileData).subscribe(function (res) {
            // console.log(fileData)
            // this.billingForm.get("doc").setValue(res)
        });
    };
    // "fileDetails", JSON.stringify({"fileType": file.type})
    UploadComponent.prototype.save = function () {
        // this.billingForm.get("fromDate").setValue(this._convertToIsoString(this.fromDate))
        // this.billingForm.get("toDate").setValue(this._convertToIsoString(this.toDate))
        // this.billingForm.get("period").setValue(this._convertToIsoString(this.period))
        var _this = this;
        // if(this.billingForm.get('doc').invalid){
        //   const Toast = Swal.mixin({
        //     toast: true,
        //     position: 'bottom-end',
        //     showConfirmButton: false,
        //     timer: 3000,
        //     timerProgressBar: true,
        //     onOpen: (toast) => {
        //       toast.addEventListener('mouseenter', Swal.stopTimer)
        //       toast.addEventListener('mouseleave', Swal.resumeTimer)
        //     }
        //   });
        //   Toast.fire({
        //     icon: 'error',
        //     title: 'Please Upload File'
        //   })
        // }
        if (this.billingForm.valid) {
            this.uploadDto.billingPeriod = this.billingForm.value.period;
            this.uploadDto.description = this.billingForm.value.desc;
            this.uploadDto.fileId = this.billingForm.value.doc;
            this.uploadDto.fromDate = this.billingForm.value.fromDate;
            this.uploadDto.toDate = this.billingForm.value.toDate;
            this.uploadDto.invoiceNumber = this.billingForm.value.invoice;
            // this.uploadDto.targetTeam=this.billingForm.value.
            this.uploadDto.product = this.billingForm.value.product;
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Save'
            }).then(function (result) {
                _this.showSpinner = true;
                setTimeout(function () {
                    _this.showSpinner = false;
                }, 5000);
                if (result.value) {
                    _this.uploadServices.postCdrs(_this.uploadDto).subscribe(function (res) {
                        console.log(res);
                        _this.router.navigate(['home']);
                    });
                }
            });
        }
        if (this.billingForm.invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Form Is Not Valid'
            });
        }
    };
    UploadComponent.prototype._convertToIsoString = function (date, beginTime) {
        if (beginTime === void 0) { beginTime = null; }
        if (!date) {
            return null;
        }
        if (!(date.year && date.month && date.day)) {
            return null;
        }
        if (beginTime == true) {
            var dateFormated = new Date(date.year, date.month - 1, date.day, 0, 0, 0).toISOString();
            return dateFormated;
        }
        else if (beginTime == false) {
            var dateFormated = new Date(date.year, date.month - 1, date.day, 23, 59, 59).toISOString();
            return dateFormated;
        }
        else {
            var Now = new Date();
            var dateFormated = new Date(date.year, date.month - 1, date.day, Now.getHours(), Now.getMinutes(), Now.getSeconds()).toISOString();
            return dateFormated;
        }
    };
    UploadComponent.ctorParameters = function () { return [
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbCalendar"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbDateParserFormatter"] },
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
        { type: _services_upload_services__WEBPACK_IMPORTED_MODULE_8__["uploadServices"] },
        { type: _services_target_services__WEBPACK_IMPORTED_MODULE_11__["TargetServices"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput'),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
    ], UploadComponent.prototype, "fileInput", void 0);
    UploadComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-upload',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./upload.component.html */ "./node_modules/raw-loader/index.js!./src/app/upload/upload.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./upload.component.css */ "./src/app/upload/upload.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbCalendar"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbDateParserFormatter"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"], _services_upload_services__WEBPACK_IMPORTED_MODULE_8__["uploadServices"], _services_target_services__WEBPACK_IMPORTED_MODULE_11__["TargetServices"]])
    ], UploadComponent);
    return UploadComponent;
}());



/***/ }),

/***/ "./src/app/user-setting/user-setting.component.css":
/*!*********************************************************!*\
  !*** ./src/app/user-setting/user-setting.component.css ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n  .mb-01{\r\n    padding-top: 17px;\r\n    padding-bottom: 13px;\r\n   }\r\n   .mb-0{\r\n    margin-bottom: 0.5rem;\r\n    font-weight: 600;\r\n    line-height: 1.5;\r\n    color: #32325d;\r\n   }\r\n   .row{\r\n    margin: 0;\r\n    font-family: myFirstFont, sans-serif;\r\n    font-size: 1rem;\r\n    font-weight: 400;\r\n    line-height: 1.5;\r\n    color: #525f7f;\r\n    text-align: left;\r\n   }\r\n   table {\r\n     width: 100%;\r\n   }\r\n   .search-hero {\r\n     max-width: 500px;\r\n       padding-bottom: 21px;\r\n       padding-top: 6%;\r\n       margin:auto;\r\n   }\r\n   .form-control {\r\n     box-shadow: 0 10px 40px 0 #B0C1D9;\r\n   }\r\n   .form-control::-moz-placeholder  {\r\n     font-family: FontAwesome;\r\n   }\r\n   .form-control:-ms-input-placeholder  {\r\n     font-family: FontAwesome;\r\n   }\r\n   .form-control::placeholder  {\r\n     font-family: FontAwesome;\r\n   }\r\n \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci1zZXR0aW5nL3VzZXItc2V0dGluZy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7RUFDRTtJQUNFLGlCQUFpQjtJQUNqQixvQkFBb0I7R0FDckI7R0FDQTtJQUNDLHFCQUFxQjtJQUNyQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGNBQWM7R0FDZjtHQUNBO0lBQ0MsU0FBUztJQUNULG9DQUFvQztJQUNwQyxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2QsZ0JBQWdCO0dBQ2pCO0dBRUE7S0FDRSxXQUFXO0dBQ2I7R0FFQTtLQUNFLGdCQUFnQjtPQUNkLG9CQUFvQjtPQUNwQixlQUFlO09BQ2YsV0FBVztHQUNmO0dBQ0E7S0FDRSxpQ0FBaUM7R0FDbkM7R0FDQTtLQUNFLHdCQUF3QjtHQUMxQjtHQUZBO0tBQ0Usd0JBQXdCO0dBQzFCO0dBRkE7S0FDRSx3QkFBd0I7R0FDMUIiLCJmaWxlIjoic3JjL2FwcC91c2VyLXNldHRpbmcvdXNlci1zZXR0aW5nLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuICAubWItMDF7XHJcbiAgICBwYWRkaW5nLXRvcDogMTdweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiAxM3B4O1xyXG4gICB9XHJcbiAgIC5tYi0we1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgICBjb2xvcjogIzMyMzI1ZDtcclxuICAgfVxyXG4gICAucm93e1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgZm9udC1mYW1pbHk6IG15Rmlyc3RGb250LCBzYW5zLXNlcmlmO1xyXG4gICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgICBjb2xvcjogIzUyNWY3ZjtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgIH1cclxuICAgXHJcbiAgIHRhYmxlIHtcclxuICAgICB3aWR0aDogMTAwJTtcclxuICAgfVxyXG4gICAgIFxyXG4gICAuc2VhcmNoLWhlcm8ge1xyXG4gICAgIG1heC13aWR0aDogNTAwcHg7XHJcbiAgICAgICBwYWRkaW5nLWJvdHRvbTogMjFweDtcclxuICAgICAgIHBhZGRpbmctdG9wOiA2JTtcclxuICAgICAgIG1hcmdpbjphdXRvO1xyXG4gICB9XHJcbiAgIC5mb3JtLWNvbnRyb2wge1xyXG4gICAgIGJveC1zaGFkb3c6IDAgMTBweCA0MHB4IDAgI0IwQzFEOTtcclxuICAgfVxyXG4gICAuZm9ybS1jb250cm9sOjpwbGFjZWhvbGRlciAge1xyXG4gICAgIGZvbnQtZmFtaWx5OiBGb250QXdlc29tZTtcclxuICAgfVxyXG4gIl19 */");

/***/ }),

/***/ "./src/app/user-setting/user-setting.component.ts":
/*!********************************************************!*\
  !*** ./src/app/user-setting/user-setting.component.ts ***!
  \********************************************************/
/*! exports provided: UserSettingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserSettingComponent", function() { return UserSettingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/fesm5/autocomplete.js");
/* harmony import */ var _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/keycodes */ "./node_modules/@angular/cdk/fesm5/keycodes.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");






var UserSettingComponent = /** @class */ (function () {
    function UserSettingComponent(router) {
        this.router = router;
        this.visible = true;
        this.selectable = true;
        this.removable = true;
        this.addOnBlur = true;
        this.separatorKeysCodes = [_angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_3__["ENTER"], _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_3__["COMMA"]];
        this.roles = [
            { name: 'Complaints' },
            { name: 'Contact Us' },
            { name: 'PC' },
            { name: 'Services, Issue New RFP' },
        ];
        this.capacities = [
            { name: 'C042' },
            { name: 'MENA Gateway Service ' },
            { name: 'C046' },
            { name: 'C041' },
            { name: 'C043' },
        ];
    }
    UserSettingComponent.prototype.ngOnInit = function () {
        this.avengers =
            [{ fname: 'Sam', lname: 'Alanazi', company: 'Bret', email: 'Sam@april.biz', phone: '0555622277', country: 'Saudi Arabia', date: '2020-08-15', status: 'Pending' },
                { fname: 'Abdo', lname: 'alhosan', company: 'TATA', email: 'Abdo@melissa.tv', phone: '0556559277', country: 'UAE', date: '2020-08-17', status: 'Closed' },
                { fname: 'Aziz', lname: 'almu', company: 'DATA', email: 'Aziz@yesenia.net', phone: '0556553223', country: 'kuwait', date: '2020-08-19', status: 'Approved' },
                { fname: 'Ree', lname: 'A', company: 'Mobily', email: 'Ree.OConner@kory.org', phone: '0559663224', country: 'Saudi Arabia', date: '2020-08-25', status: 'Pending' },
                { fname: 'Zack', lname: 'Bee', company: 'Zain', email: 'Zack@annie.ca', phone: '0556961145', country: 'USA', date: '2020-08-27', status: 'Closed' }];
    };
    UserSettingComponent.prototype.add = function (event) {
        var input = event.input;
        var value = event.value;
        // Add our role
        if ((value || '').trim()) {
            this.roles.push({ name: value.trim() });
        }
        // Reset the input value
        if (input) {
            input.value = '';
        }
    };
    UserSettingComponent.prototype.remove = function (role) {
        var index = this.roles.indexOf(role);
        if (index >= 0) {
            this.roles.splice(index, 1);
        }
    };
    UserSettingComponent.prototype.save = function () {
        var _this = this;
        var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: function (toast) {
                toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.stopTimer);
                toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.resumeTimer);
            }
        });
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Save'
        }).then(function (result) {
            if (result.value) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire('Saved!', 'Your Changes has been made.', 'success');
                _this.router.navigate(['registration-details']);
            }
        });
    };
    UserSettingComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('roleInput'),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], UserSettingComponent.prototype, "roleInput", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('auto'),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_2__["MatAutocomplete"])
    ], UserSettingComponent.prototype, "matAutocomplete", void 0);
    UserSettingComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-setting',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./user-setting.component.html */ "./node_modules/raw-loader/index.js!./src/app/user-setting/user-setting.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./user-setting.component.css */ "./src/app/user-setting/user-setting.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], UserSettingComponent);
    return UserSettingComponent;
}());



/***/ }),

/***/ "./src/app/views/email-approved/email-approved.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/views/email-approved/email-approved.component.css ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL2VtYWlsLWFwcHJvdmVkL2VtYWlsLWFwcHJvdmVkLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/views/email-approved/email-approved.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/views/email-approved/email-approved.component.ts ***!
  \******************************************************************/
/*! exports provided: EmailApprovedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailApprovedComponent", function() { return EmailApprovedComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


var EmailApprovedComponent = /** @class */ (function () {
    function EmailApprovedComponent() {
    }
    EmailApprovedComponent.prototype.ngOnInit = function () {
    };
    EmailApprovedComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-email-approved',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./email-approved.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/email-approved/email-approved.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./email-approved.component.css */ "./src/app/views/email-approved/email-approved.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], EmailApprovedComponent);
    return EmailApprovedComponent;
}());



/***/ }),

/***/ "./src/app/views/error/403.component.ts":
/*!**********************************************!*\
  !*** ./src/app/views/error/403.component.ts ***!
  \**********************************************/
/*! exports provided: P403Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "P403Component", function() { return P403Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


var P403Component = /** @class */ (function () {
    function P403Component() {
    }
    P403Component = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-403',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./403.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/error/403.component.html")).default
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], P403Component);
    return P403Component;
}());



/***/ }),

/***/ "./src/app/views/error/404.component.ts":
/*!**********************************************!*\
  !*** ./src/app/views/error/404.component.ts ***!
  \**********************************************/
/*! exports provided: P404Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "P404Component", function() { return P404Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


var P404Component = /** @class */ (function () {
    function P404Component() {
    }
    P404Component = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-404',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./404.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/error/404.component.html")).default
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], P404Component);
    return P404Component;
}());



/***/ }),

/***/ "./src/app/views/error/500.component.ts":
/*!**********************************************!*\
  !*** ./src/app/views/error/500.component.ts ***!
  \**********************************************/
/*! exports provided: P500Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "P500Component", function() { return P500Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


var P500Component = /** @class */ (function () {
    function P500Component() {
    }
    P500Component = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./500.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/error/500.component.html")).default
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], P500Component);
    return P500Component;
}());



/***/ }),

/***/ "./src/app/views/forgot-password/forgot-password.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/views/forgot-password/forgot-password.component.css ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".costom-btn {\r\n    margin: auto;\r\n    margin: auto;\r\n    line-height: initial;\r\n    background-color: blueviolet;\r\n    margin-left: 22vh;\r\n    margin-top: 22vh;\r\n}\r\n\r\n     li {\r\n      margin: 20 20px;\r\n    }\r\n\r\n     .btn-c\r\n{\r\n    margin-top: 10vh;\r\n}\r\n\r\n     .h2{\r\n    font-family: inherit;\r\n    font-weight: 600;\r\n    line-height: 1.5;\r\n    font-size: 1.25rem;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvZm9yZ290LXBhc3N3b3JkL2ZvcmdvdC1wYXNzd29yZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksWUFBWTtJQUNaLFlBQVk7SUFDWixvQkFBb0I7SUFDcEIsNEJBQTRCO0lBQzVCLGlCQUFpQjtJQUNqQixnQkFBZ0I7QUFDcEI7O0tBRUs7TUFDQyxlQUFlO0lBQ2pCOztLQUVKOztJQUVJLGdCQUFnQjtBQUNwQjs7S0FFQTtJQUNJLG9CQUFvQjtJQUNwQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtBQUN0QiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL2ZvcmdvdC1wYXNzd29yZC9mb3Jnb3QtcGFzc3dvcmQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb3N0b20tYnRuIHtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIGxpbmUtaGVpZ2h0OiBpbml0aWFsO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZXZpb2xldDtcclxuICAgIG1hcmdpbi1sZWZ0OiAyMnZoO1xyXG4gICAgbWFyZ2luLXRvcDogMjJ2aDtcclxufVxyXG5cclxuICAgICBsaSB7XHJcbiAgICAgIG1hcmdpbjogMjAgMjBweDtcclxuICAgIH1cclxuXHJcbi5idG4tY1xyXG57XHJcbiAgICBtYXJnaW4tdG9wOiAxMHZoO1xyXG59XHJcblxyXG4uaDJ7XHJcbiAgICBmb250LWZhbWlseTogaW5oZXJpdDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gICAgZm9udC1zaXplOiAxLjI1cmVtO1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/views/forgot-password/forgot-password.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/views/forgot-password/forgot-password.component.ts ***!
  \********************************************************************/
/*! exports provided: ForgotPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordComponent", function() { return ForgotPasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _services_forgotpass_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/forgotpass.services */ "./src/app/services/forgotpass.services.ts");







var ForgotPasswordComponent = /** @class */ (function () {
    function ForgotPasswordComponent(router, toast, forgotPassServices) {
        this.router = router;
        this.toast = toast;
        this.forgotPassServices = forgotPassServices;
        this.isRegister = false;
        this.forgetForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[^\\s@]+@[^\\s@]+\\.[^\\s@]{2,}$'), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(25)])]),
        });
    }
    ForgotPasswordComponent.prototype.ngOnInit = function () {
    };
    ForgotPasswordComponent.prototype.trueChartEmail = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57)
            && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
    };
    ForgotPasswordComponent.prototype.forgot = function () {
        var _this = this;
        this.isRegister = true;
        if (this.forgetForm.valid) {
            this.forgotPassServices.postpass(this.forgetForm.value.email).subscribe(function (res) {
                _this.router.navigate(['email-reset']);
                console.log(_this.forgetForm);
                console.log(res);
            });
        }
        if (this.forgetForm.get('email').invalid) {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Email Address Entered is wrong'
            });
        }
    };
    ForgotPasswordComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] },
        { type: _services_forgotpass_services__WEBPACK_IMPORTED_MODULE_6__["forgotPassServices"] }
    ]; };
    ForgotPasswordComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-forgot-password',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./forgot-password.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/forgot-password/forgot-password.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./forgot-password.component.css */ "./src/app/views/forgot-password/forgot-password.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"], _services_forgotpass_services__WEBPACK_IMPORTED_MODULE_6__["forgotPassServices"]])
    ], ForgotPasswordComponent);
    return ForgotPasswordComponent;
}());



/***/ }),

/***/ "./src/app/views/login/login.component.ts":
/*!************************************************!*\
  !*** ./src/app/views/login/login.component.ts ***!
  \************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _language_language_helper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../language/language.helper */ "./src/app/language/language.helper.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _dataModels_login_model__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../dataModels/login.model */ "./src/app/dataModels/login.model.ts");










var LoginComponent = /** @class */ (function () {
    function LoginComponent(router, languageHelper, toast, translateService, loginServices) {
        this.router = router;
        this.languageHelper = languageHelper;
        this.toast = toast;
        this.translateService = translateService;
        this.loginServices = loginServices;
        this.isSubmited = false;
        this.today = new Date();
        this.lastLogin = '';
        this.loginDto = new _dataModels_login_model__WEBPACK_IMPORTED_MODULE_9__["loginDto"]();
        this.getAllLangs();
        this.isRTL = this.languageHelper.currentLang().isRTL;
        this.login = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(25)]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required)
        });
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.translateService.setDefaultLang('en');
        this.translateService.use(this.languageHelper.currentLang().name);
        this.isRTL = this.languageHelper.currentLang().isRTL;
    };
    LoginComponent.prototype.changeLang = function (lang) {
        this.languageHelper.changeLang(lang);
        window.location.reload();
    };
    LoginComponent.prototype.trueChartEmail = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57)
            && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
    };
    LoginComponent.prototype.getAllLangs = function () {
        var arr = this.languageHelper.getAllLanguages();
        this.english = arr[0];
        this.arabic = arr[1];
        return this.languageHelper.getAllLanguages();
    };
    LoginComponent.prototype.onSubmit = function () {
        var _this = this;
        this.isSubmited = true;
        if (this.login.valid) {
            this.loginDto.email = this.login.value.email;
            this.loginDto.password = this.login.value.password;
            this.loginServices.postlogin(this.loginDto).subscribe(function (res) {
                localStorage.setItem("token", res.sessionId);
                localStorage.setItem("usename", res.username);
                localStorage.setItem("lastLogin", res.lastLogin);
                _this.router.navigate(['/home']);
                var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: function (toast) {
                        toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                        toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
                    }
                });
                Toast.fire({
                    icon: 'success',
                    title: 'Signed in successfully'
                });
                console.log(res);
            });
        }
        else {
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'error',
                title: 'Email Address/Password are wrong'
            });
        }
    };
    LoginComponent.prototype.register = function () {
        this.router.navigate(['register']);
    };
    LoginComponent.prototype.forget = function () {
        this.router.navigate(['forgot-password']);
    };
    LoginComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _language_language_helper__WEBPACK_IMPORTED_MODULE_4__["LanguageHelper"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"] },
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"] },
        { type: _services_login_service__WEBPACK_IMPORTED_MODULE_8__["loginServices"] }
    ]; };
    LoginComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-login',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/login/login.component.html")).default
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _language_language_helper__WEBPACK_IMPORTED_MODULE_4__["LanguageHelper"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"], _services_login_service__WEBPACK_IMPORTED_MODULE_8__["loginServices"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/views/password-expiry/password-expiry.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/views/password-expiry/password-expiry.component.css ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3Bhc3N3b3JkLWV4cGlyeS9wYXNzd29yZC1leHBpcnkuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/views/password-expiry/password-expiry.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/views/password-expiry/password-expiry.component.ts ***!
  \********************************************************************/
/*! exports provided: PasswordExpiryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PasswordExpiryComponent", function() { return PasswordExpiryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


var PasswordExpiryComponent = /** @class */ (function () {
    function PasswordExpiryComponent() {
    }
    PasswordExpiryComponent.prototype.ngOnInit = function () {
    };
    PasswordExpiryComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-password-expiry',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./password-expiry.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/password-expiry/password-expiry.component.html")).default,
            styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./password-expiry.component.css */ "./src/app/views/password-expiry/password-expiry.component.css")).default]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
    ], PasswordExpiryComponent);
    return PasswordExpiryComponent;
}());



/***/ }),

/***/ "./src/app/views/register/register.component.ts":
/*!******************************************************!*\
  !*** ./src/app/views/register/register.component.ts ***!
  \******************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);






var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(router, toast) {
        this.router = router;
        this.toast = toast;
        this.isRegister = false;
        this.registerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            Email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[^\\s@]+@[^\\s@]+\\.[^\\s@]{2,}$'), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(25)])]),
            phone: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(100), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(15)]),
            company: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(50), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(3)]),
            country: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(50), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(3)]),
            firstname: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(50), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(3)]),
            lastname: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(50), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(3)]),
        });
    }
    RegisterComponent.prototype.ngOnInit = function () {
    };
    RegisterComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    RegisterComponent.prototype.trueChart = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)
            && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
    };
    RegisterComponent.prototype.trueChartEmail = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57)
            && (charCode < 64 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
    };
    RegisterComponent.prototype.onRegister = function () {
        this.isRegister = true;
        if (this.registerForm.valid) {
            this.router.navigate(['home']);
            var Toast = sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: function (toast) {
                    toast.addEventListener('mouseenter', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.stopTimer);
                    toast.addEventListener('mouseleave', sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.resumeTimer);
                }
            });
            Toast.fire({
                icon: 'success',
                title: 'successfully registered'
            });
        }
    };
    RegisterComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] }
    ]; };
    RegisterComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard',
            template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./register.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/register/register.component.html")).default
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]])
    ], RegisterComponent);
    return RegisterComponent;
}());

//   } if (this.registerForm.get('Email').invalid) {
//     const Toast = Swal.mixin({
//       toast: true,
//       position: 'bottom-end',
//       showConfirmButton: false,
//       timer: 3000,
//       timerProgressBar: true,
//       onOpen: (toast) => {
//         toast.addEventListener('mouseenter', Swal.stopTimer)
//         toast.addEventListener('mouseleave', Swal.resumeTimer)
//       }
//     })
//     Toast.fire({
//       icon: 'error',
//       title: 'Email Address/Password are Worng'
//     })
//     // window.location.reload();
//   }
//   if (this.registerForm.get('phone').invalid) {
//     const Toast = Swal.mixin({
//       toast: true,
//       position: 'bottom-end',
//       showConfirmButton: false,
//       timer: 3000,
//       timerProgressBar: true,
//       onOpen: (toast) => {
//         toast.addEventListener('mouseenter', Swal.stopTimer)
//         toast.addEventListener('mouseleave', Swal.resumeTimer)
//       }
//     })
//     Toast.fire({
//       icon: 'error',
//       title: 'Phone entered is Worng'
//     })
//     // window.location.reload();
//   }
//   if (this.registerForm.get('code').invalid) {
//     const Toast = Swal.mixin({
//       toast: true,
//       position: 'bottom-end',
//       showConfirmButton: false,
//       timer: 3000,
//       timerProgressBar: true,
//       onOpen: (toast) => {
//         toast.addEventListener('mouseenter', Swal.stopTimer)
//         toast.addEventListener('mouseleave', Swal.resumeTimer)
//       }
//     })
//     Toast.fire({
//       icon: 'error',
//       title: 'Code Entered is Wrong'
//     })
//     // window.location.reload();
//   }
// }
//   registerNav() {
//   }
// }


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"], {
    useJit: true,
    preserveWhitespaces: true
})
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Thinkpad\Documents\wbu-portal\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map